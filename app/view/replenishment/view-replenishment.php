<?php 
include('config/config.php');

$role = $_SESSION['role'];
$placedby = $_SESSION['nama'];

    if (isset($_GET['status'])) {
        $status = $_GET['status'];

        if ($status == 1) {
            echo '<div class="alert alert-success" role="alert">
                Data updated
            </div>';
        }elseif ($status == 0) {
            echo '<div class="alert alert-danger" role="alert">
                Terjadi Kesalahan! silahkan coba kembali
            </div>';
        }else{
            echo '<div class="alert alert-warning" role="alert">
                Terjadi kesalahan koneksi internet, silkahkan ulangi kembali
            </div>';
        }
    }

    if (isset($_GET['editstatus'])) {
        $status = $_GET['editstatus'];

        if ($status == 1) {
            echo '<div class="alert alert-success" role="alert">
                Data user berhasil di perbaruhi
            </div>';
        }elseif ($status == 0) {
            echo '<div class="alert alert-danger" role="alert">
                Terjadi Kesalahan! tidak dapat mengubah data user
            </div>';
        }else{
            echo '<div class="alert alert-warning" role="alert">
                Terjadi kesalahan koneksi internet, silkahkan ulangi kembali
            </div>';
        }
    }
 ?>

     <div class="row">
<div class="col-md-12">
        
        
        

<br>
<!-- <h4>Placement</h4>
<hr> -->
<div class="row">
    <div class="col-md-12">
        <div class="table-responsive table-data" style="height: auto">
            <table id="tableok" class="mdl-data-table" style="width:100%">
                <thead>
                    <tr>
                        <td>Any Sales</td>
                        <td>No</td>
                        <td>Kode Outlet</td>
                        <td>Nama Destributor</td>
                        <td>Tanggal Survey</td>
                        <td>Frrezer Owner</td>
                        <td>Nama Vendor</td>
                        <td>Tipe Freezer</td>
                        <td>Email to Vendor Date</td>
                        <td>Placement Date</td>
                        <td>Lead Time</td>
                        <td>Status</td>
                        <td>Tanggal Validasi</td>
                        <td>Freezer Code</td>
                        <td>IDNumber</td>
                        <td>BatchPO</td>
                        <td>Placement Notes</td>
                        <td>Nama Supir</td>
                        <td>Tanggal Pengiriman</td>
                        <td>Realisasi Tipe Freezer</td>
                        <td>Nama PIC</td>
                        <td>No Telp PIC</td>
                        <td>Kondisi Fisik Outlet</td>
                        <td>Patokan</td>
                        <td>Longitude</td>
                        <td>Latitude</td>
                        <td>Alamat</td>
                        <td>Provinsi</td>
                        <td>Kabupaten / Kota</td>
                        <td>Kecamatan</td>
                        <td>Kelurahan</td>
                        <td>Kode Pos</td>
                        <td>Jam Buka</td>
                        <td>Jam Tutup</td>
                        <td>Updated By</td>
                        <!-- <td>Action</td> -->
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $no = 1;
                        $requester = $_SESSION['nama'];
                        $query = "SELECT * FROM distributor, focus_freezer_placement WHERE `focus_freezer_placement`.`approved_status` = 'APPROVED' AND `distributor`.`iddistributor`=`focus_freezer_placement`.`id_distributor` AND `focus_freezer_placement`.`placement_date` != '0000-00-00'";
                        $data = mysqli_query($koneksi,$query);

                        while ($result = mysqli_fetch_assoc($data)) {

                     ?>

                     <?php 
                        //find the sales date if persist than don't show the data
                            $id_distributor = $result['id_distributor'];

                            $q_sales = "SELECT * FROM focus_replenish WHERE id_distributor = $id_distributor";
                            $d_sales = mysqli_query($koneksi,$q_sales);
                            $r_sales = mysqli_fetch_assoc($d_sales);

                            if ($r_sales['sales_date'] == '') {
                     ?>
                    <tr>
                        <td>
                            <form action="model/replenishment/sales.php" method="post" class="form-tindaklanjut" id="form-approved-<?php echo $result['id_distributor']?>">
                                <input type="hidden" id="id" name="id_distributor" value=" <?php echo $result['id_distributor'] ?>">

                                <!-- <div class="row">
                                    <div class="col-md-12">
                                        <select name="sales" class="form-control" required>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div> -->

                                <div class="row">
                                    <div class="col-md-12">
                                        <label>First Sales Date</label>
                                        <input type="date" name="sales_date" class="form-control">
                                    </div>
                                </div>
                                
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-sm btn-primary">
                                            Update
                                        </button>
                                   
                                        <a href="#" class="btn btn-sm btn-dark batal-tindaklanjut">Batal</a>
                                    </div>
                                 </div>
                                    
                                </div>
                            </form>

                            <a href="#" id="approved_placement_<?php echo $result['id_distributor']?>" class="btn btn-sm btn-primary approved_placement" data-toggle="tooltip" data-placement="top" title="Any Sales" acuan="<?php echo $result['id_distributor']?>">
                                <i class="fas fa fa-calendar"></i>
                            </a>
                        </td>
                        <td><?php echo $no ?></td>
                        <td>
                            <?php echo $result['custid'] ?>
                        </td>
                        <td>
                            <?php echo $result['distname'] ?>
                        </td>
                        <td>
                            <?php echo $result['inputdatetime'] ?>
                        </td>
                        <td>
                            <?php echo $result['freezer_owner'] ?>
                        </td>
                        <td>
                            <?php echo $result['nama_vendor'] ?>
                        </td>
                        <td>
                            <?php echo $result['pilihanfreezeroutlet'] ?>
                        </td>
                        <td>
                            <?php echo $result['approved_date'] ?>
                        </td>
                        <td>
                            <?php 

                                if ($result['placement_date'] == '0000-00-00') {
                                    echo 'Tanggal belum tersedia';
                                }else{
                                    echo $result['placement_date'];
                                } 
                                ?>
                        </td>
                        <td>
                            <?php
                                echo $result['lead_time'].' Hari';
                            ?>
                        </td>
                        <td>
                            <?php 
                                $status = $result['approved_status'];
                                $tanggal_penempatan = $result['placement_date'];

                                if ($status == 'APPROVED' AND $tanggal_penempatan !== '0000-00-00') {
                                    echo '<span class="role marketing">SUCCESS</span>';
                                }elseif ($status == 'APPROVED' AND $tanggal_penempatan == '') {
                                    echo '<span class="role supervisor">PROCESSING BY VENDOR</span>';
                                }else{
                                    echo '<span class="role investors">PROCESSING BY VENDOR</span>';
                                }
                             ?>
                        </td>
                        <td>
                            <?php echo $result['approved_date'] ?>
                        </td>
                        <td>
                            <?php echo $result['freezercode'] ?>
                        </td>
                        <td>
                            <?php echo $result['IDNumber'] ?>
                        </td>
                        <td>
                            <?php echo $result['batch_po'] ?>
                        </td>
                        <td>
                            <?php echo $result['placement_notes'] ?>
                        </td>
                        <td>
                            <?php echo $result['nama_supir'] ?>
                        </td>
                        <td>
                            <?php 

                               if ($result['tanggal_pengiriman'] == '0000-00-00') {
                                    echo "Tanggal belum tersedia";
                               }else{
                                    echo $result['tanggal_pengiriman']; 
                               }
                            ?>
                        </td>
                        <td>
                            <?php echo $result['realisasi_tipe_freezer'] ?>
                        </td>
                        <td>
                            <?php echo $result['nameowner'] ?>
                        </td>
                        <td>
                            <?php echo $result['telephone'] ?>
                        </td>
                        
                        <td>
                            <?php echo $result['kondisifisik'] ?>
                        </td>
                        <td>
                            <?php echo $result['patokan'] ?>
                        </td>
                        <td>
                            <?php echo $result['longitude'] ?>
                        </td>
                        <td>
                            <?php echo $result['latitude'] ?>
                        </td>
                        
                        <td>
                            <?php echo $result['address'] ?>
                        </td>
                        <td>
                            <?php echo $result['province'] ?>
                        </td>
                        <td>
                            <?php echo $result['city'] ?>
                        </td>
                        <td>
                            <?php echo $result['kecamatan'] ?>
                        </td>
                        <td>
                            <?php echo $result['kelurahan'] ?>
                        </td>
                        <td>
                            <?php echo $result['kodepos'] ?>
                        </td>
                        <td>
                            <?php echo $result['timeopen'] ?>
                        </td>
                        <td>
                            <?php echo $result['timeclosed'] ?>
                        </td>
                        <td>
                            <?php echo $result['placed_by'] ?>
                        </td>
                    </tr>

                     <?php $no++;  
                        }else{//do nothing 
                            }
                        } 
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


</div>

    




</div>





