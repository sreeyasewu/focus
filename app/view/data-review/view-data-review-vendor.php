<?php 
    if (isset($_GET['status'])) {
        $status = $_GET['status'];

        if ($status == 1) {
            echo '<div class="alert alert-success" role="alert">
                Data updated!
            </div>';
        }elseif ($status == 0) {
            echo '<div class="alert alert-danger" role="alert">
                Terjadi Kesalahan! silahkan ulangi kembali
            </div>';
        }else{
            echo '<div class="alert alert-warning" role="alert">
                Terjadi kesalahan koneksi internet, silkahkan ulangi kembali
            </div>';
        }
    }

    if (isset($_GET['editstatus'])) {
        $status = $_GET['editstatus'];

        if ($status == 1) {
            echo '<div class="alert alert-success" role="alert">
                Data investor berhasil di perbaruhi
            </div>';
        }elseif ($status == 0) {
            echo '<div class="alert alert-danger" role="alert">
                Terjadi Kesalahan! tidak dapat mengubah data user
            </div>';
        }else{
            echo '<div class="alert alert-warning" role="alert">
                Terjadi kesalahan koneksi internet, silkahkan ulangi kembali
            </div>';
        }
    }
 ?>
<div class="row">
<div class="col-md-12">
        <!-- DATA TABLE -->
        <h3 class="title-5 m-b-35">
            <br>
            <?php 
                // if ($_GET['class'] == 'view') {
                //     echo '<a href="index.php?mod=datareview&class=view" class="btn btn-primary">NEW</a>
                //         <a href="index.php?mod=datareview&class=reject_freezer" class="btn btn-dark">REJECT (FREEZER PLACEMENT)</a>';
                //         // <a href="index.php?mod=datareview&class=reject_vendor" class="btn btn-dark">REJECT (VENDOR)</a>';

                // }elseif ($_GET['class'] == 'reject_freezer') {
                //     echo '<a href="index.php?mod=datareview&class=view" class="btn btn-dark">NEW</a>
                //         <a href="index.php?mod=datareview&class=reject_freezer" class="btn btn-primary">REJECT (FREEZER PLACEMENT)</a>';
                //         // <a href="index.php?mod=datareview&class=reject_vendor" class="btn btn-dark">REJECT (VENDOR)</a>';

                // }elseif ($_GET['class'] == 'reject_vendor') {
                //     echo ' <a href="index.php?mod=datareview&class=view" class="btn btn-dark">NEW</a>
                //         <a href="index.php?mod=datareview&class=reject_freezer" class="btn btn-dark">REJECT (FREEZER PLACEMENT)</a>';
                //         // <a href="index.php?mod=datareview&class=reject_vendor" class="btn btn-primary">REJECT (VENDOR)</a>';
                // }
             ?>
           

            <?php 
                
                    echo ' <a href="index.php?mod=freezerplacement&class=view" class="btn btn-dark">NEW PLACEMENT</a>
                        <a href="index.php?mod=datareview&class=reject_vendor" class="btn btn-primary">REJECT (VENDOR)</a>';
                
             ?>
        </h3>
        <div class="table-data__tool">
            <!-- <div class="table-data__tool-left">
                <div class="rs-select2--light rs-select2--md">
                    <select class="js-select2" name="property">
                        <option selected="selected">Roles</option>
                        <option value="">owner</option>
                        <option value="">stakeholders</option>
                        <option value="">investors</option>
                        <option value="">marketing</option>
                        <option value="">admin</option>
                        <option value="">approval</option>
                    </select>
                    <div class="dropDownSelect2"></div>
                </div>
                <div class="rs-select2--light rs-select2--sm">
                    <select class="js-select2" name="time">
                        <option selected="selected">Sort By</option>
                        <option value="">Name</option>
                        <option value="">Date created</option>
                    </select>
                    <div class="dropDownSelect2"></div>
                </div>
                <button class="au-btn-filter">
                    <i class="zmdi zmdi-filter-list"></i>filters</button>
            </div>
            <div class="table-data__tool-right">
                <button class="au-btn au-btn-icon au-btn--green au-btn--small">
                    <i class="fas fa-print"></i>print</button>
                <div class="rs-select2--dark rs-select2--sm rs-select2--dark2">
                    <select class="js-select2" name="type">
                        <option selected="selected">Export</option>
                        <option value="">PDF</option>
                        <option value="">CSV</option>
                    </select>
                    <div class="dropDownSelect2"></div>
                </div>
            </div> -->
        </div>

        <div class="table-responsive table-data" style="height: auto">
            <table id="tableok" class="mdl-data-table table table-borderless" style="width:100%">
                <thead>
                    <tr>
                        <td>
                            Action                        
                        <!-- <label class="au-checkbox">
                        <input type="checkbox" id="selectAll">
                        <span class="au-checkmark"></span>
                        </label> -->
                        </td>
                        <td>No</td>
                        <td>Status</td>
                        <td>Ket</td>
                        <td>Reject Date</td>
                        <td>Nama Outlet</td>
                        <td>BFI Code</td>
                        <td>Nama Distributor</td>
                        <td>Area Distributor</td>
                        <td>Tgl Survey</td>
                        <td>Nama Surveyor</td>
                        <td>Status Kunjungan</td>
                        <td>Channel</td>
                        <td>Sub-Channel</td>
                        <td>Segment</td>
                        <td>Nama PIC</td>
                        <td>No KTP</td>
                        <td>No Telp PIC</td>
                        <td>Longitude</td>
                        <td>Latitude</td>
                        <td>Alamat</td>
                        <td>Provinsi</td>
                        <td>Kabupaten/Kota</td>
                        <td>Kecamatan</td>
                        <td>Keluarahan</td>
                        <td>Kode Pos</td>
                        <td>Jam Buka (WIB)</td>
                        <td>Jam Tutup (WIB)</td>
                        <td>Luas Bangunan</td>
                        <td>Paket Awal</td>
                        <td>Foto - Outlet Tampak Depan</td>
                        <td>Foto - Daerah Depan Sekitar Toko</td>
                        <td>Foto - Deal Form + KTP</td>
                        <td>Lokasi Sekitar</td>
                        <td>Patokan</td>
                        <td>Lokasi (1)</td>
                        <td>Lokasi (2)</td>
                        <td>Freezer Es Krim</td>
                        <td>Merk Freezer Es Krim</td>
                        <td>Freezer Makanan Beku</td>
                        <td>Merk Freezer Makanan Beku</td>
                        <td>Produk Utama</td>
                        <td>Jumlah Pengunjung</td>
                        <td>Pendapatan/Hari (IDR)</td>
                        <td>Tipe Pengunjung</td>
                        <td>Kapasitas Listrik</td>
                        <td>Kondisi Fisik Outlet</td>
                        <td>Resiko Freezer</td>
                        <td>Spacae 1 x 1.5 m</td>
                        <td>Tipe Freezer</td>
                        <td>Area Penempatan Freezer</td>
                        <td>Visibilitas Freezer di Jalur Utama</td>
                        <td>Layanan Tambahan</td>
                        <td>Target/Bulan (kg)</td>
                        <td>Issue/Follow Up Action</td>
                        


                        
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $no = 1;
                        include ('config/config.php');
                        $query = "SELECT * FROM `distributor`, `focus_freezer_placement` WHERE `focus_freezer_placement`.`id_distributor` = `distributor`.`iddistributor` AND `focus_freezer_placement`.`approved_status` = 'REJECT BY VENDOR'";
                        $data = mysqli_query($koneksi,$query);

                        while ($result = mysqli_fetch_assoc($data)) {

                            $id_placement = $result['id_placement'];
                     ?>
                    <tr>
                        <td>
                            <!-- <div class="table-data-feature"> -->
                                
                            <form action="model/freezerplacement/reject-freezerteam-vendor.php" method="post" class="form-tindaklanjut" id="form-tindaklanjut-<?php echo $result['iddistributor'] ?>">
                                <input type="hidden" id="id" name="id_placement" value="<?php echo $result['id_placement'] ?>">

                                <div class="row">
                                    <div class="col-md-12">
                                        <select name="keterangan" id="namavendor" class="select_database form-control" required>
                                                            <option value="">Pilih Rejection Code</option>

                                                            <?php 
                                                                $query_user = "SELECT * FROM focus_rejection_code";
                                                                $data_user = mysqli_query($koneksi,$query_user);
                                                                while ($result_rejection = mysqli_fetch_assoc($data_user)) {
                                                                    
                                                             ?>
                                                                <option value="<?php echo $result_rejection['ket'] ?>"><?php echo $result_rejection['rejection_code'] ?> - <?php echo $result_rejection['ket'] ?></option>

                                                            <?php } ?>

                                                        </select>
                                    </div>
                                </div>
                                <br>

                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-sm btn-success">
                                            Update
                                        </button>
                                   
                                        <a href="javascript:void();" class="btn btn-sm btn-dark batal-tindaklanjut">Batal</a>
                                    </div>
                                 </div>
                                    
                                </div>
                            </form>
                            

                            
                                 <a href="javascript:void();" class="btn btn-block btn-danger tindaklanjut" id="rejectbtn" data-toggle="tooltip" data-placement="top" title="Reject Placement" acuan="<?php echo $result['id_distributor'] ?>"><i class="fas fa fa-warning"> </i></a> 
                             
                             <!-- approved -->
                             <br>
                             
                            <form action="model/freezerplacement/approve-freezerteam-vendor.php" method="post" class="form-tindaklanjut" id="form-approved-<?php echo $result['iddistributor']?>">
                                <input type="hidden" id="id" name="id_placement" value=" <?php echo $result['id_placement'] ?>">

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <small>Assign Date</small>
                                                    <br>
                                                        <input type="date" id="reassigned_date" name="reassigned_date" class="form-control" value="<?php echo $result['first_assign_date'] ?>" required>
                                                    </div>
                                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <small>Principal</small>
                                    <br>
                                    <select id="principal" name="principal" class="select_bfi form-control" required>
                                            <option disabled selected>Select Principal</option>
                                            <?php
                                                include ('config/config.php');
                                                //cari data semua BFI
                                                $sql = "SELECT * FROM focus_principal ORDER BY id_principal";
                                                $dql = mysqli_query($koneksi,$sql);


                                                while ($rql = mysqli_fetch_assoc($dql)) {
                                            ?>
                                                <option value="<?php echo $rql['principal'] ?>" <?php if($rql['id_principal']==1) echo 'selected'; ?>><?php echo $rql['principal']." - ".$rql['keterangan'] ?></option>

                                            <?php
                                                }

                                             ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <small>Freezer Owner</small>
                                    <br>
                                        <!-- <input type="text" class="freezerowner" name="owner" class="form-control" required> -->

                                        <select name="owner" class="freezerowner" class="form-control" id="freezerowner" acuan="<?php echo $result['iddistributor']; ?>" onChange="rentownplace();" required>
                                            <option selected>Status kepemilikan</option>
                                            <option value="Rent">Rent</option>
                                            <option value="Own">Own</option>
                                        </select>
                                        
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <small>Nama Vendor</small>
                                    <br>
                                        <!-- <input type="text" id="namavendor" name="namavendor" class="form-control" required> -->

                                        <select name="namavendor" id="namavendorselect<?php echo $result['iddistributor'] ?>" class="select_database form-control namavendorselect" onChange="selectvendor(<?php echo $result['iddistributor'] ?>);" required>
                                        <option >Pilih Nama Vendor</option>

                                        <?php 
                                            $query_user = "SELECT * FROM user WHERE role = 'VENDOR' GROUP BY nama";
                                            $data_user = mysqli_query($koneksi,$query_user);
                                            while ($result_user = mysqli_fetch_assoc($data_user)) {
                                                
                                         ?>
                                            <option value="<?php echo $result_user['nama'] ?>"><?php echo $result_user['nama'] ?></option>

                                        <?php } ?>

                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <small>Area Vendor</small>
                                    <br>
                                        
                                        <select name="areavendor" id="areavendor<?php echo $result['iddistributor'] ?>" class="select_database form-control areavendor" required>
                                        

                                    </select>
                                    </div>
                                </div>

                                <!-- <div id="areavendor"></div>  -->

                                <div class="col-12 col-md-9">
                                    <input type="text" name="namavendor_pilih" id="namavendor_pilih<?php echo $result['iddistributor'] ?>" class="form-control namavendor_pilih" hidden>
                                </div>

                                <!-- <div class="row">
                                    <div class="col-md-12">
                                        <small>Area Vendor</small>
                                    <br>
                                        
                                        <select name="areavendor" id="namavendor" class="select_database form-control" required>
                                        <option>Pilih Area</option>

                                        <?php 
                                            $query_user = "SELECT area FROM user WHERE role = 'VENDOR' GROUP BY area";
                                            $data_user = mysqli_query($koneksi,$query_user);
                                            while ($result_user = mysqli_fetch_assoc($data_user)) {
                                                
                                         ?>
                                            <option value="<?php echo $result_user['area'] ?>"><?php echo $result_user['area'] ?></option>

                                        <?php } ?>

                                    </select>
                                    </div>
                                </div> -->

                                <div class="row nomor_mesin" id="nomor_mesin_<?php echo $result['iddistributor'] ?>   ">
                                    <div class="col-md-12">
                                        <small>Nomor Mesin</small>
                                    <br>
                                        <input type="text" name="nomormesin" class="form-control">

                                        

                                    </select>
                                    </div>
                                </div>

                                <!-- <div class="row id_number">
                                    <div class="col-md-12">
                                        <small>IDNumber</small>
                                    <br>

                                        <input type="text" id="idnumber" name="idnumber" class="form-control">

                                        

                                    </select>
                                    </div>
                                </div> -->


                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-sm btn-success">
                                            Update
                                        </button>
                                   
                                        <a href="javascript:void();" class="btn btn-sm btn-dark batal-tindaklanjut">Batal</a>
                                    </div>
                                 </div>
                                    
                                </div>
                            </form>
                            

                            

                            <a href="javascript:void();" id="approved_placement_<?php echo $result['iddistributor']?>" class="btn btn-block btn-primary approved_placement" data-toggle="tooltip" data-placement="top" title="Approve Placement" acuan="<?php echo $result['id_distributor']?>">
                                <i class="fas fa fa-check"></i>
                            </a>
                             
                           
                            
                               <!--  <a href="index.php?mod=freezerplacement&class=placement&id=<?php echo $result['iddistributor'] ?>" id="approved_placement_<?php echo $result['iddistributor']?>" class="btn btn-sm btn-primary approved_placement" data-toggle="tooltip" data-placement="top" title="Approve Placement">
                                <i class="fas fa fa-check"></i>
                            </a> -->
                            <!-- </div> -->
                        </td>
                        <td><?php echo $no ?></td>
                        <td>
                            <?php echo $result['approved_status'] ?>
                        </td>
                        <td>
                            <?php echo $result['keterangan'] ?>
                        </td>
                        <td>
                            <?php 
                            // brand sementara dipakai untuk letak reject date
                            echo $result['brand']; ?>
                        </td>
                        <td>
                            <?php echo $result['custname'] ?>
                        </td>
                        <td>
                            <?php echo $result['custid'] ?>
                        </td>
                        <td>
                            <?php echo $result['distname'] ?>
                        </td>
                        <td>
                            <?php echo $result['coveragearea'] ?>
                        </td>
                       
                        <td>
                            <?php 
                                echo $result['inputdatetime'];
                            ?>
                        </td>

                        <td>
                            
                            <?php echo $result['sales']; ?>
                        </td>
                        <td>
                            <?php 
                                $status = $result['status'];

                                if ($status == 1) {
                                     echo 'Revisit';
                                 }else{
                                    echo 'New Outlet';
                                 } ?>
                        </td>
                        <td>
                            <?php 
                                echo $result['channel'];
                            ?>
                            &nbsp;
                            <!-- <button class="btn btn-dark btn-sm" onClick="show_hidden();"><i class="fas fa fa-edit"></i></button>
                    
                            <div class="hidden-edit">
                                <form action="model/data-review/edit-channel.php" method="post" enctype="multipart/form-data">

                                   
                                    <div class="row">
                                        <div class="col-6 col-md-6">
                                            Edit Channel <br>
                                            <select name="channel" id="select_channel" class="form-control" style="width: 200px">

                                                <?php 
                                                    $query_channel = "SELECT * FROM channel";
                                                        $data_channel = mysqli_query($koneksi,$query_channel);
                                                        while ($result_channel = mysqli_fetch_assoc($data_channel)) {
                                                        
                                                 ?>
                                                    <option value="<?php echo $result_channel['channelname'] ?>"><?php echo $result_channel['channelname'] ?></option>

                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>

                                    <br>

                                    <div class="row form-group" >
                                        <div class="col-12 col-md-9">
                                            <input type="hidden" id="custId" name="id" value="<?php echo $result['id_distributor'] ?>">

                                            <button type="submit" id="btn-editchannel" class="au-btn au-btn--green btn-small">
                                                Update
                                            </button>
                                        </div>
                                    </form>
                            </div> -->
                        </td>

                        <td>
                            <?php echo $result['subchannel'] ?>
                            <!-- <button class="btn btn-dark btn-sm" onClick="show_hidden();"><i class="fas fa fa-edit"></i></button>
                    
                            <div class="hidden-edit">
                                <form action="model/data-review/edit-subchannel.php" method="post" enctype="multipart/form-data">

                                   
                                    <div class="row">
                                        <div class="col-6 col-md-6">
                                            Edit Channel <br>
                                            <select name="channel" id="select_subchannel" class="form-control" style="width: 200px">

                                                <?php 
                                                    $query_channel = "SELECT * FROM subchannel";
                                                        $data_channel = mysqli_query($koneksi,$query_channel);
                                                        while ($result_channel = mysqli_fetch_assoc($data_channel)) {
                                                        
                                                 ?>
                                                    <option value="<?php echo $result_channel['subchannelname'] ?>"><?php echo $result_channel['subchannelname'] ?></option>

                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>

                                    <br>

                                    <div class="row form-group" >
                                        <div class="col-12 col-md-9">
                                            <input type="hidden" id="custId" name="id" value="<?php echo $result['id_distributor'] ?>">

                                            <button type="submit" id="btn-editchannel" class="au-btn au-btn--green btn-small">
                                                Update
                                            </button>
                                        </div>
                                    </form>
                            </div> -->
                        </td>

                        <td>
                            <?php echo $result['segment'] ?>
                        </td>

                        <td>
                            <?php echo $result['nameowner'] ?>
                        </td>

                        <td>
                            <?php echo $result['noktp'] ?>
                        </td>
                        <td>
                            <?php echo $result['telephone'] ?>
                        </td>
                        <td>
                            <?php echo $result['longitude'] ?>
                        </td>
                        <td>
                            <?php echo $result['latitude'] ?>
                        </td>

                        <td>
                            <?php echo $result['address'] ?>
                        </td>

                        <td>
                            <?php echo $result['province'] ?>
                        </td>

                        <td>
                            <?php echo $result['city'] ?>
                        </td>

                        <td>
                            <?php echo $result['kecamatan'] ?>
                        </td>

                        <td>
                            <?php echo $result['kelurahan'] ?>
                        </td>

                        <td>
                            <?php echo $result['kodepos'] ?>
                        </td>

                        <td>
                            <?php echo $result['timeopen'] ?>
                        </td>

                        <td>
                            <?php echo $result['timeclosed'] ?>
                        </td>

                        <td>
                            <?php echo $result['luasbangunan'] ?>
                        </td>

                        <td>
                            <?php echo $result['paketfreezer'] ?>
                        </td>

                        <td>
                            <?php 
                            $photo = 'https://appict.appsbelfoods.com/'.$result['photo'];
                            
                            // echo '<a href="'.$photo.'"><img src="'.$photo.'" width="50px" height="50px">'.$photo.'</a>';
                            echo '<a href="'.$photo.'">View</a>';


                            ?>
                        </td>
                        <td>
                            <?php 
                            $photo = 'https://appict.appsbelfoods.com/'.$result['photo2'];
                            
                            // echo '<a href="'.$photo.'"><img src="'.$photo.'" width="50px" height="50px">'.$photo.'</a>';
                            echo '<a href="'.$photo.'">View</a>';

                            ?>
                        </td>
                        <td>
                            <?php 
                            $photo = 'https://appict.appsbelfoods.com/'.$result['photo3'];
                            
                            // echo '<a href="'.$photo.'"><img src="'.$photo.'" width="50px" height="50px">'.$photo.'</a>';
                            echo '<a href="'.$photo.'">View</a>';

                            ?>
                        </td>

                        <td>
                            <?php echo $result['sebutkanlingkungan'] ?>
                        </td>

                        <td>
                            <?php echo $result['patokan'] ?>
                        </td>
                        <td>
                            <?php echo $result['lokasi1'] ?>
                        </td>
                        <td>
                            <?php echo $result['lokasi2'] ?>
                        </td>
                        <td>
                            <?php echo $result['freezerdalamtoko'] ?>
                        </td>
                        <td>
                            <?php echo $result['merekfreezer'] ?>
                        </td>
                        <td>
                            <?php echo $result['frozenFood'] ?>
                        </td>

                        <td>
                            <?php echo $result['merekfrozenfood'] ?>
                        </td>

                        <td>
                            <?php echo $result['productutama'] ?>
                        </td>
                        <td>
                            <?php echo $result['estimasipengunjung'] ?>
                        </td>
                        <td>
                            <?php echo $result['pendapatan'] ?>
                        </td>

                        <td>
                            <?php echo $result['mayoritas'] ?>
                        </td>
                        <td>
                            <?php echo $result['elektricity'] ?>
                        </td>
                        <td>
                            <?php echo $result['kondisifisik'] ?>
                        </td>

                        <td>
                            <?php echo $result['resikofreezerhilang'] ?>
                        </td>

                        <td>
                            <?php echo $result['tersediatempat'] ?>
                        </td>

                        <td>
                            <?php echo $result['pilihanfreezeroutlet'] ?>
                        </td>

                        <td>
                            <?php echo $result['tempatfreezer'] ?>
                        </td>

                        <td>
                            <?php echo $result['penilaianlokasi'] ?>
                        </td>

                        <td>
                            <?php echo $result['sebutkanlingkungan'] ?>
                        </td>

                        <td>
                            <?php echo $result['targetbulanan'] ?>
                        </td>

                        <td>
                            <?php echo $result['followupaction'] ?>
                        </td>

                        
                        
                    </tr>

                     <?php $no++; } ?>
                </tbody>
            </table>
        </div>


        <!-- <div class="table-responsive table-responsive-data2">
            <table class="table table-data2">
                <thead>
                    <tr>
                        <th>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </th>
                        <th>name</th>
                        <th>email</th>
                        <th>username</th>
                        <th>date created</th>
                        <th>roles</th>
                        <th>password</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="tr-shadow">
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td>Lori Lynch</td>
                        <td>
                            <span class="block-email">lori@gmail.com</span>
                        </td>
                        <td class="desc">Lori345</td>
                        <td>2018-09-17 02:12</td>
                        <td>
                            <span class="role admin">Admin</span>
                        </td>
                        <td>************</td>
                        <td>
                            <div class="table-data-feature">
                                <button class="item" data-toggle="tooltip" data-placement="top" title="View Password">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <i class="zmdi zmdi-delete"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Banned">
                                    <i class="fas fa-ban"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                    <tr class="spacer"></tr>
                    <tr class="tr-shadow">
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td>John Martin</td>
                        <td>
                            <span class="block-email">johnmartin@gmail.com</span>
                        </td>
                        <td class="desc">Martin.jhon</td>
                        <td>2018-09-01 07:57</td>
                        <td>
                            <span class="role stakeholders">Stakeholders</span>
                        </td>
                        <td>************</td>
                        <td>
                            <div class="table-data-feature">
                                <button class="item" data-toggle="tooltip" data-placement="top" title="View Password">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <i class="zmdi zmdi-delete"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Banned">
                                    <i class="fas fa-ban"></i>
                                </button>
                            </div>
                        </td>
                    </tr>

                    <tr class="spacer"></tr>
                    <tr class="tr-shadow">
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td>Rahadian Agus</td>
                        <td>
                            <span class="block-email">rahadianagus@gmail.com</span>
                        </td>
                        <td class="desc">MrAgus.69</td>
                        <td>2018-09-01 06:06</td>
                        <td>
                            <span class="role investors">Investors</span>
                        </td>
                        <td>************</td>
                        <td>
                            <div class="table-data-feature">
                                <button class="item" data-toggle="tooltip" data-placement="top" title="View Password">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <i class="zmdi zmdi-delete"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Banned">
                                    <i class="fas fa-ban"></i>
                                </button>
                            </div>
                        </td>
                    </tr>

                    <tr class="spacer"></tr>
                    <tr class="tr-shadow">
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td>Hasbi</td>
                        <td>
                            <span class="block-email">auskreditsyari@gmail.com</span>
                        </td>
                        <td class="desc">Hasbi9899</td>
                        <td>2018-09-01 05:57</td>
                        <td>
                            <span class="role owner">Owner</span>
                        </td>
                        <td>************</td>
                        <td>
                            <div class="table-data-feature">
                                <button class="item" data-toggle="tooltip" data-placement="top" title="View Password">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <i class="zmdi zmdi-delete"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Banned">
                                    <i class="fas fa-ban"></i>
                                </button>
                            </div>
                        </td>
                    </tr>

                    <tr class="spacer"></tr>
                    <tr class="tr-shadow">
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td>Ahmad Rupandi</td>
                        <td>
                            <span class="block-email">ahmadrupandi@gmail.com</span>
                        </td>
                        <td class="desc">RupandiPejantanTangguh</td>
                        <td>2018-09-01 05:55</td>
                        <td>
                            <span class="role marketing">Marketing</span>
                        </td>
                        <td>************</td>
                        <td>
                            <div class="table-data-feature">
                                <button class="item" data-toggle="tooltip" data-placement="top" title="View Password">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <i class="zmdi zmdi-delete"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Banned">
                                    <i class="fas fa-ban"></i>
                                </button>
                            </div>
                        </td>
                    </tr>

                    <tr class="spacer"></tr>
                    <tr class="tr-shadow">
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td>Supriadi</td>
                        <td>
                            <span class="block-email">supriadi@gmail.com</span>
                        </td>
                        <td class="desc">Supri.Adi31</td>
                        <td>2018-09-01 05:57</td>
                        <td>
                            <span class="role approval">Approval</span>
                        </td>
                        <td>************</td>
                        <td>
                            <div class="table-data-feature">
                                <button class="item" data-toggle="tooltip" data-placement="top" title="View Password">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <i class="zmdi zmdi-delete"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Banned">
                                    <i class="fas fa-ban"></i>
                                </button>
                            </div>
                        </td>
                    </tr>

                </tbody>
            </table>
        </div> -->
        <!-- END DATA TABLE -->
    </div>
</div>
