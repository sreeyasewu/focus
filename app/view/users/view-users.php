<?php 
    if (isset($_GET['status'])) {
        $status = $_GET['status'];

        if ($status == 1) {
            echo '<div class="alert alert-success" role="alert">
                Data user berhasil di hapus
            </div>';
        }elseif ($status == 0) {
            echo '<div class="alert alert-danger" role="alert">
                Terjadi Kesalahan! tidak dapat menghapus data user
            </div>';
        }else{
            echo '<div class="alert alert-warning" role="alert">
                Terjadi kesalahan koneksi internet, silkahkan ulangi kembali
            </div>';
        }
    }

    if (isset($_GET['editstatus'])) {
        $status = $_GET['editstatus'];

        if ($status == 1) {
            echo '<div class="alert alert-success" role="alert">
                Data user berhasil di perbaruhi
            </div>';
        }elseif ($status == 0) {
            echo '<div class="alert alert-danger" role="alert">
                Terjadi Kesalahan! tidak dapat mengubah data user
            </div>';
        }else{
            echo '<div class="alert alert-warning" role="alert">
                Terjadi kesalahan koneksi internet, silkahkan ulangi kembali
            </div>';
        }
    }
 ?>
<div class="row">
<div class="col-md-12">
        <!-- DATA TABLE -->
        <h3 class="title-5 m-b-35">view users</h3>
        <div class="table-data__tool">
            <!-- <div class="table-data__tool-left">
                <div class="rs-select2--light rs-select2--md">
                    <select class="js-select2" name="property">
                        <option selected="selected">Roles</option>
                        <option value="">owner</option>
                        <option value="">stakeholders</option>
                        <option value="">investors</option>
                        <option value="">marketing</option>
                        <option value="">admin</option>
                        <option value="">approval</option>
                    </select>
                    <div class="dropDownSelect2"></div>
                </div>
                <div class="rs-select2--light rs-select2--sm">
                    <select class="js-select2" name="time">
                        <option selected="selected">Sort By</option>
                        <option value="">Name</option>
                        <option value="">Date created</option>
                    </select>
                    <div class="dropDownSelect2"></div>
                </div>
                <button class="au-btn-filter">
                    <i class="zmdi zmdi-filter-list"></i>filters</button>
            </div>
            <div class="table-data__tool-right">
                <button class="au-btn au-btn-icon au-btn--green au-btn--small">
                    <i class="fas fa-print"></i>print</button>
                <div class="rs-select2--dark rs-select2--sm rs-select2--dark2">
                    <select class="js-select2" name="type">
                        <option selected="selected">Export</option>
                        <option value="">PDF</option>
                        <option value="">CSV</option>
                    </select>
                    <div class="dropDownSelect2"></div>
                </div>
            </div> -->
        </div>

        <div class="table-responsive table-data" style="height: 100%">
            <table id="tableok" class="mdl-data-table" style="width:100%">
                <thead>
                    <tr>
                        <td>No</td>
                        <td>NIK</td>
                        <td>Nama</td>
                        <td>Email</td>
                        <td>Area</td>
                        <td>No HP</td>
                        <td>Role</td>
                        <td>Status</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $no = 1;
                        include ('config/config.php');
                        $query = "SELECT * FROM user ";
                        $data = mysqli_query($koneksi,$query);

                        while ($result = mysqli_fetch_assoc($data)) {
                     ?>
                    <tr>
                        <td><?php echo $no ?></td>
                        <td>
                            <?php echo $result['nik'] ?>
                        </td>
                        <td>
                            <?php echo $result['nama'] ?>
                        </td>
                        <td>
                            <?php echo $result['email'] ?>
                        </td>
                        <td>
                            <?php echo $result['area'] ?>
                        </td>
                        <td>
                            <?php echo $result['no_hp'] ?>
                        </td>
                       
                        <td>
                            <?php 
                                $role = $result['role'];
                                if ($role == 'ADMIN') {
                                     echo '<span class="role admin">Admin</span>';
                                 }elseif ($role == 'FREEZER MANAGEMENT') {
                                     echo '<span class="role marketing">Freezer Management</span>';
                                 }elseif ($role == 'VENDOR') {
                                     echo '<span class="role marketing">Vendor</span>';
                                 }elseif ($role == 'ROUTE TO MARKET') {
                                     echo '<span class="role approval">Route to Market</span>';
                                 }elseif ($role == 'COMMAND CENTER') {
                                     echo '<span class="role investors">Command Center</span>';
                                 }
                                 // }elseif ($role == 'STAKEHOLDER') {
                                 //     echo '<span class="role stakeholders">Stakeholder</span>';
                                 // }elseif ( $role == 'SUPERVISOR') {
                                 //     echo '<span class="role approval">Supervisor</span>';
                                 // }elseif ( $role == 'FINANCE') {
                                 //      echo '<span class="role admin">Finance</span>';
                                 // }elseif ( $role == 'OWNER') {
                                 //      echo '<span class="role investors">Owner</span>';
                                 // }elseif ( $role == 'COLLECTION') {
                                 //      echo '<span class="role approval">Collection</span>';
                                 // } 

                            ?>
                        </td>
                        <td><?php echo $result['status'] ?></td>
                        <td>
                            <div class="table-data-feature">
                                
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <a href="index.php?mod=users&class=edit&id=<?php echo $result['id_user']; ?>" ><i class="zmdi zmdi-edit"></i></a>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <a href="index.php?mod=users&class=delete&id=<?php echo $result['id_user'] ?>"><i class="zmdi zmdi-delete"></i></a>
                                </button>
                            </div>
                        </td>
                    </tr>

                     <?php $no++; } ?>
                </tbody>
            </table>
        </div>


        <div class="table-responsive table-responsive-data2">
            <table class="table table-data2">
                <thead>
                    <tr>
                        <th>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </th>
                        <th>name</th>
                        <th>email</th>
                        <th>username</th>
                        <th>date created</th>
                        <th>roles</th>
                        <th>password</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="tr-shadow">
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td>Lori Lynch</td>
                        <td>
                            <span class="block-email">lori@gmail.com</span>
                        </td>
                        <td class="desc">Lori345</td>
                        <td>2018-09-17 02:12</td>
                        <td>
                            <span class="role admin">Admin</span>
                        </td>
                        <td>************</td>
                        <td>
                            <div class="table-data-feature">
                                <button class="item" data-toggle="tooltip" data-placement="top" title="View Password">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <i class="zmdi zmdi-delete"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Banned">
                                    <i class="fas fa-ban"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                    <tr class="spacer"></tr>
                    <tr class="tr-shadow">
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td>John Martin</td>
                        <td>
                            <span class="block-email">johnmartin@gmail.com</span>
                        </td>
                        <td class="desc">Martin.jhon</td>
                        <td>2018-09-01 07:57</td>
                        <td>
                            <span class="role stakeholders">Stakeholders</span>
                        </td>
                        <td>************</td>
                        <td>
                            <div class="table-data-feature">
                                <button class="item" data-toggle="tooltip" data-placement="top" title="View Password">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <i class="zmdi zmdi-delete"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Banned">
                                    <i class="fas fa-ban"></i>
                                </button>
                            </div>
                        </td>
                    </tr>

                    <tr class="spacer"></tr>
                    <tr class="tr-shadow">
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td>Rahadian Agus</td>
                        <td>
                            <span class="block-email">rahadianagus@gmail.com</span>
                        </td>
                        <td class="desc">MrAgus.69</td>
                        <td>2018-09-01 06:06</td>
                        <td>
                            <span class="role investors">Investors</span>
                        </td>
                        <td>************</td>
                        <td>
                            <div class="table-data-feature">
                                <button class="item" data-toggle="tooltip" data-placement="top" title="View Password">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <i class="zmdi zmdi-delete"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Banned">
                                    <i class="fas fa-ban"></i>
                                </button>
                            </div>
                        </td>
                    </tr>

                    <tr class="spacer"></tr>
                    <tr class="tr-shadow">
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td>Hasbi</td>
                        <td>
                            <span class="block-email">auskreditsyari@gmail.com</span>
                        </td>
                        <td class="desc">Hasbi9899</td>
                        <td>2018-09-01 05:57</td>
                        <td>
                            <span class="role owner">Owner</span>
                        </td>
                        <td>************</td>
                        <td>
                            <div class="table-data-feature">
                                <button class="item" data-toggle="tooltip" data-placement="top" title="View Password">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <i class="zmdi zmdi-delete"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Banned">
                                    <i class="fas fa-ban"></i>
                                </button>
                            </div>
                        </td>
                    </tr>

                    <tr class="spacer"></tr>
                    <tr class="tr-shadow">
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td>Ahmad Rupandi</td>
                        <td>
                            <span class="block-email">ahmadrupandi@gmail.com</span>
                        </td>
                        <td class="desc">RupandiPejantanTangguh</td>
                        <td>2018-09-01 05:55</td>
                        <td>
                            <span class="role marketing">Marketing</span>
                        </td>
                        <td>************</td>
                        <td>
                            <div class="table-data-feature">
                                <button class="item" data-toggle="tooltip" data-placement="top" title="View Password">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <i class="zmdi zmdi-delete"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Banned">
                                    <i class="fas fa-ban"></i>
                                </button>
                            </div>
                        </td>
                    </tr>

                    <tr class="spacer"></tr>
                    <tr class="tr-shadow">
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td>Supriadi</td>
                        <td>
                            <span class="block-email">supriadi@gmail.com</span>
                        </td>
                        <td class="desc">Supri.Adi31</td>
                        <td>2018-09-01 05:57</td>
                        <td>
                            <span class="role approval">Approval</span>
                        </td>
                        <td>************</td>
                        <td>
                            <div class="table-data-feature">
                                <button class="item" data-toggle="tooltip" data-placement="top" title="View Password">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <i class="zmdi zmdi-delete"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Banned">
                                    <i class="fas fa-ban"></i>
                                </button>
                            </div>
                        </td>
                    </tr>

                </tbody>
            </table>
        </div>
        <!-- END DATA TABLE -->
    </div>
</div>