<?php 
    if (isset($_GET['status'])) {
        $status = $_GET['status'];

        if ($status == 1) {
            echo '<div class="alert alert-success" role="alert">
                Data updated!
            </div>';
        }elseif ($status == 0) {
            echo '<div class="alert alert-danger" role="alert">
                Terjadi Kesalahan! tidak dapat mengupdate cust code
            </div>';
        }else{
            echo '<div class="alert alert-warning" role="alert">
                Terjadi kesalahan koneksi internet, silkahkan ulangi kembali
            </div>';
        }
    }

    if (isset($_GET['editstatus'])) {
        $status = $_GET['editstatus'];

        if ($status == 1) {
            echo '<div class="alert alert-success" role="alert">
                Data investor berhasil di perbaruhi
            </div>';
        }elseif ($status == 0) {
            echo '<div class="alert alert-danger" role="alert">
                Terjadi Kesalahan! tidak dapat mengubah data user
            </div>';
        }else{
            echo '<div class="alert alert-warning" role="alert">
                Terjadi kesalahan koneksi internet, silkahkan ulangi kembali
            </div>';
        }
    }
 ?>
<div class="row">
<div class="col-md-12">
        <!-- DATA TABLE -->
        <h3 class="title-5 m-b-35">
            <!-- View Deal -->

            <?php 
                //image test
                // echo '<img src="http://appict.belfoods.com/images/distributor/distributor1_2019-04-10_10-37-58.jpg" width="500px" height="500px"> </img>';
             ?>
        </h3>
        <div class="table-data__tool">
            <!-- <div class="table-data__tool-left">
                <div class="rs-select2--light rs-select2--md">
                    <select class="js-select2" name="property">
                        <option selected="selected">Roles</option>
                        <option value="">owner</option>
                        <option value="">stakeholders</option>
                        <option value="">investors</option>
                        <option value="">marketing</option>
                        <option value="">admin</option>
                        <option value="">approval</option>
                    </select>
                    <div class="dropDownSelect2"></div>
                </div>
                <div class="rs-select2--light rs-select2--sm">
                    <select class="js-select2" name="time">
                        <option selected="selected">Sort By</option>
                        <option value="">Name</option>
                        <option value="">Date created</option>
                    </select>
                    <div class="dropDownSelect2"></div>
                </div>
                <button class="au-btn-filter">
                    <i class="zmdi zmdi-filter-list"></i>filters</button>
            </div>
            <div class="table-data__tool-right">
                <button class="au-btn au-btn-icon au-btn--green au-btn--small">
                    <i class="fas fa-print"></i>print</button>
                <div class="rs-select2--dark rs-select2--sm rs-select2--dark2">
                    <select class="js-select2" name="type">
                        <option selected="selected">Export</option>
                        <option value="">PDF</option>
                        <option value="">CSV</option>
                    </select>
                    <div class="dropDownSelect2"></div>
                </div>
            </div> -->
        </div>

        <div class="table-responsive table-data" style="height: auto">
            <table id="tableok" class="mdl-data-table" style="width:100%">
                <thead>
                    <tr>
                        <td>No</td>
                        <td>Nama Outlet</td>
                        <td>BFI Code</td>
                        <td>Cust Code</td>
                        <td>ASM</td>
                        <td>TSS</td>
                        <td>Nama Salesman</td>
                        <td>Area Distributor</td>
                        <td>Nama Distributor</td>
                        <td>Channel</td>
                        <td>Sub-channel</td>
                        <td>Segment</td>
                        <td>Outlet Status</td>
                        <td>Freezer Status</td>
                        <td>Alamat</td>
                        <td>Provinsi</td>
                        <td>Kabupaten/ Kota</td>
                        <td>Kecamatan</td>
                        <td>Kelurahan</td>
                        <td>Kode POS</td>
                        <td>Nama PIC</td>
                        <td>No Telp PIC</td>

                        
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $no = 1;
                        include ('config/config.php');
                        $query = "SELECT * FROM distributor, focus_freezer_placement WHERE `distributor`.`statustoko` = 'DEAL' AND `focus_freezer_placement`.`placement_date` != '0000-00-00' AND `distributor`.`iddistributor` = `focus_freezer_placement`.`id_distributor` ORDER BY iddistributor DESC LIMIT 0,100";
                        $data = mysqli_query($koneksi,$query);

                        while ($result = mysqli_fetch_assoc($data)) {
                     ?>
                    <tr>
                        <td><?php echo $no ?></td>
                        <td>
                            <?php echo $result['custname'] ?>
                        </td>
                        <td>
                            <?php echo $result['custid'] ?>
                        </td>
                        <td>
                            <?php 
                                $iddistributor = $result['iddistributor'];
                                $query_cs = "SELECT * FROM focus_cust_code WHERE id_distributor = '$iddistributor' ORDER BY id_cust_code DESC";
                                $data_cs = mysqli_query($koneksi,$query_cs);
                                $result_cs = mysqli_fetch_assoc($data_cs);

                                echo $result_cs['cust_code'];

                                
                                




                            ?>

                            <form action="model/custcode/update.php" method="post" class="form-tindaklanjut" id="form-approved-<?php echo $result['iddistributor']?>">
                                <input type="hidden" id="id" name="id_distributor" value=" <?php echo $result['iddistributor'] ?>">

                                <input type="hidden" name="bficode" value="<?php echo $result['custid'] ?>">

                                <div class="row">
                                    <div class="col-md-12">
                                        
                                        <input type="text" id="custcode" name="custcode" class="form-control" required>
                                    </div>
                                </div>
                                
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-sm btn-success">
                                            Update
                                        </button>
                                   
                                        <a href="#" class="btn btn-sm btn-dark batal-tindaklanjut">Batal</a>
                                    </div>
                                 </div>
                                    
                                </div>
                            </form>

                            <a href="#" id="approved_placement_<?php echo $result['iddistributor']?>" class="btn btn-sm btn-info approved_placement" data-toggle="tooltip" data-placement="top" title="Edit Customer Code" acuan="<?php echo $result['id_distributor']?>">
                                <i class="fas fa fa-edit"></i>
                            </a>
                        </td>
                        <td>
                            <?php 
                                $distnumber = $result['distnumber'];
                                $query2 = "SELECT * FROM distributormaster WHERE distributornumber = '$distnumber'";
                                $data2 = mysqli_query($koneksi,$query2);

                                $r_dist = mysqli_fetch_assoc($data2);

                                $asm = $r_dist['distributorname'];

                                echo $asm;

                             ?>
                            
                        </td>
                        <td>
                            <?php 
                                echo $result['sales'];
                             ?>
                        </td>
                       
                        <td>
                            <?php echo $result['sales'] ?>
                        </td>

                        
                        <td>
                            <?php echo $result['coveragearea']?>
                        </td>
                        <td>
                            <?php echo $result['distname'] ?>
                        </td>
                        <td><?php echo $result['channel'] ?></td>

                        <td>
                            <?php echo $result['subchannel'] ?>
                        </td>

                        <td>
                            <?php echo $result['segment'] ?>
                        </td>

                        <td>
                            <?php echo $result['statustoko'] ?>
                        </td>

                        <td>
                            <?php 
                                $iddistributor = $result['iddistributor'];
                                $query3 = "SELECT * FROM focus_freezer_placement WHERE id_distributor = '$iddistributor'";
                                $data3 = mysqli_query($koneksi,$query3);
                                $r_dist2 = mysqli_fetch_assoc($data3);

                                echo $r_dist2['freezer_owner'];
                             ?>
                        </td>

                       

                        <td>
                            <?php echo $result['address'] ?>
                        </td>

                        <td>
                            <?php echo $result['province'] ?>
                        </td>

                        <td>
                            <?php echo $result['city'] ?>
                        </td>

                        <td>
                            <?php echo $result['kecamatan'] ?>
                        </td>

                        <td>
                            <?php echo $result['kelurahan'] ?>
                        </td>

                        <td>
                            <?php echo $result['kodepos'] ?>
                        </td>

                        <td>
                            
                            <?php echo $result['nameowner'] ?>
                        </td>


                        

                        <td>
                            <?php echo $result['telephone'] ?>
                        </td>
                        
                    </tr>

                     <?php $no++; } ?>
                </tbody>
            </table>
        </div>


        <!-- <div class="table-responsive table-responsive-data2">
            <table class="table table-data2">
                <thead>
                    <tr>
                        <th>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </th>
                        <th>name</th>
                        <th>email</th>
                        <th>username</th>
                        <th>date created</th>
                        <th>roles</th>
                        <th>password</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="tr-shadow">
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td>Lori Lynch</td>
                        <td>
                            <span class="block-email">lori@gmail.com</span>
                        </td>
                        <td class="desc">Lori345</td>
                        <td>2018-09-17 02:12</td>
                        <td>
                            <span class="role admin">Admin</span>
                        </td>
                        <td>************</td>
                        <td>
                            <div class="table-data-feature">
                                <button class="item" data-toggle="tooltip" data-placement="top" title="View Password">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <i class="zmdi zmdi-delete"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Banned">
                                    <i class="fas fa-ban"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                    <tr class="spacer"></tr>
                    <tr class="tr-shadow">
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td>John Martin</td>
                        <td>
                            <span class="block-email">johnmartin@gmail.com</span>
                        </td>
                        <td class="desc">Martin.jhon</td>
                        <td>2018-09-01 07:57</td>
                        <td>
                            <span class="role stakeholders">Stakeholders</span>
                        </td>
                        <td>************</td>
                        <td>
                            <div class="table-data-feature">
                                <button class="item" data-toggle="tooltip" data-placement="top" title="View Password">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <i class="zmdi zmdi-delete"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Banned">
                                    <i class="fas fa-ban"></i>
                                </button>
                            </div>
                        </td>
                    </tr>

                    <tr class="spacer"></tr>
                    <tr class="tr-shadow">
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td>Rahadian Agus</td>
                        <td>
                            <span class="block-email">rahadianagus@gmail.com</span>
                        </td>
                        <td class="desc">MrAgus.69</td>
                        <td>2018-09-01 06:06</td>
                        <td>
                            <span class="role investors">Investors</span>
                        </td>
                        <td>************</td>
                        <td>
                            <div class="table-data-feature">
                                <button class="item" data-toggle="tooltip" data-placement="top" title="View Password">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <i class="zmdi zmdi-delete"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Banned">
                                    <i class="fas fa-ban"></i>
                                </button>
                            </div>
                        </td>
                    </tr>

                    <tr class="spacer"></tr>
                    <tr class="tr-shadow">
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td>Hasbi</td>
                        <td>
                            <span class="block-email">auskreditsyari@gmail.com</span>
                        </td>
                        <td class="desc">Hasbi9899</td>
                        <td>2018-09-01 05:57</td>
                        <td>
                            <span class="role owner">Owner</span>
                        </td>
                        <td>************</td>
                        <td>
                            <div class="table-data-feature">
                                <button class="item" data-toggle="tooltip" data-placement="top" title="View Password">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <i class="zmdi zmdi-delete"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Banned">
                                    <i class="fas fa-ban"></i>
                                </button>
                            </div>
                        </td>
                    </tr>

                    <tr class="spacer"></tr>
                    <tr class="tr-shadow">
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td>Ahmad Rupandi</td>
                        <td>
                            <span class="block-email">ahmadrupandi@gmail.com</span>
                        </td>
                        <td class="desc">RupandiPejantanTangguh</td>
                        <td>2018-09-01 05:55</td>
                        <td>
                            <span class="role marketing">Marketing</span>
                        </td>
                        <td>************</td>
                        <td>
                            <div class="table-data-feature">
                                <button class="item" data-toggle="tooltip" data-placement="top" title="View Password">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <i class="zmdi zmdi-delete"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Banned">
                                    <i class="fas fa-ban"></i>
                                </button>
                            </div>
                        </td>
                    </tr>

                    <tr class="spacer"></tr>
                    <tr class="tr-shadow">
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td>Supriadi</td>
                        <td>
                            <span class="block-email">supriadi@gmail.com</span>
                        </td>
                        <td class="desc">Supri.Adi31</td>
                        <td>2018-09-01 05:57</td>
                        <td>
                            <span class="role approval">Approval</span>
                        </td>
                        <td>************</td>
                        <td>
                            <div class="table-data-feature">
                                <button class="item" data-toggle="tooltip" data-placement="top" title="View Password">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <i class="zmdi zmdi-delete"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Banned">
                                    <i class="fas fa-ban"></i>
                                </button>
                            </div>
                        </td>
                    </tr>

                </tbody>
            </table>
        </div> -->
        <!-- END DATA TABLE -->
    </div>
</div>