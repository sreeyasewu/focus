<?php 

    //hitung jumlah Customer
    $q_cs = "SELECT count(*) as totalcustomer FROM aus_customer";
    $d_cs = mysqli_query($koneksi, $q_cs);
    $r_cs = mysqli_fetch_assoc($d_cs);

    //hitung jumlah akad
    $q_akad = "SELECT count(*) as totalakad FROM aus_akad";
    $d_akad = mysqli_query($koneksi, $q_akad);
    $r_akad = mysqli_fetch_assoc($d_akad);

    //hitung jumlah investor
    $q_inv = "SELECT count(*) as totalinvestor FROM aus_investors";
    $d_inv = mysqli_query($koneksi,$q_inv);
    $r_inv = mysqli_fetch_assoc($d_inv);

    //hitung total order
    $q_order = "SELECT count(*) as totalorder FROM aus_order";
    $d_order = mysqli_query($koneksi, $q_order);
    $r_order = mysqli_fetch_assoc($d_order);


    
 ?>

<!-- <div class="row m-t-25">
                        <div class="col-sm-6 col-lg-3">
                            <div class="overview-item overview-item--c1">
                                <div class="overview__inner">
                                    <div class="overview-box clearfix">
                                        <div class="icon">
                                            <i class="zmdi zmdi-account-o"></i>
                                        </div>
                                        <div class="text">
                                            <h2><?php echo $r_cs['totalcustomer'] ?></h2>
                                            <span>Customer</span>
                                        </div>
                                    </div>
                                    <div class="overview-chart">
                                        <canvas id="widgetChart1"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="overview-item overview-item--c2">
                                <div class="overview__inner">
                                    <div class="overview-box clearfix">
                                        <div class="icon">
                                            <i class="zmdi zmdi-shopping-cart"></i>
                                        </div>
                                        <div class="text">
                                            <h2><?php echo $r_akad['totalakad'] ?></h2>
                                            <span>Akad</span>
                                        </div>
                                    </div>
                                    <div class="overview-chart">
                                        <canvas id="widgetChart2"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="overview-item overview-item--c3">
                                <div class="overview__inner">
                                    <div class="overview-box clearfix">
                                        <div class="icon">
                                            <i class="zmdi zmdi-calendar-note"></i>
                                        </div>
                                        <div class="text">
                                            <h2><?php echo $r_inv['totalinvestor'] ?></h2>
                                            <span>Investor</span>
                                        </div>
                                    </div>
                                    <div class="overview-chart">
                                        <canvas id="widgetChart3"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="overview-item overview-item--c4">
                                <div class="overview__inner">
                                    <div class="overview-box clearfix">
                                        <div class="icon">
                                            <i class="fa fa-heart"></i>
                                        </div>
                                        <div class="text">
                                            <h2><?php echo $r_order['totalorder'] ?></h2>
                                            <span>Orders</span>
                                        </div>
                                    </div>
                                    <div class="overview-chart">
                                        <canvas id="widgetChart4"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>  -->
                    
                    <br>
            

                    <div class="row">
                       <!--  <div class="col-lg-6">
                            <div class="au-card recent-report">
                                <div class="au-card-inner">
                                    <h3 class="title-2">cash flow</h3>
                                    <div class="chart-info">
                                        <div class="chart-info__left">
                                            <div class="chart-note">
                                                <span class="dot dot--blue"></span>
                                                <span>Pengeluaran</span>
                                            </div>
                                            <div class="chart-note mr-0">
                                                <span class="dot dot--green"></span>
                                                <span>Pendapatan</span>
                                            </div>
                                        </div>
                                        <div class="chart-info__right">
                                            <div class="chart-statis">
                                                <span class="index incre">
                                                    <i class="zmdi zmdi-long-arrow-up"></i>25%</span>
                                                <span class="label">Pengeluaran</span>
                                            </div>
                                            <div class="chart-statis mr-0">
                                                <span class="index decre">
                                                    <i class="zmdi zmdi-long-arrow-down"></i>10%</span>
                                                <span class="label">Pendapatan</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="recent-report__chart">
                                        <canvas id="recent-rep-chart"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        <!-- <div class="col-lg-6">
                            <div class="au-card chart-percent-card">
                                <div class="au-card-inner">
                                    <h3 class="title-2 tm-b-5">Capital Percentage %</h3>
                                    <div class="row no-gutters">
                                        <div class="col-xl-6">
                                            <div class="chart-note-wrap">
                                                <div class="chart-note mr-0 d-block">
                                                    <span class="dot dot--indigo"></span>
                                                    <span>stakeholders</span>
                                                </div>
                                                <div class="chart-note mr-0 d-block">
                                                    <span class="dot dot--pink"></span>
                                                    <span>Investors</span>
                                                </div>
                                                <div class="chart-note mr-0 d-block">
                                                    <span class="dot dot--purple"></span>
                                                    <span>marketing</span>
                                                </div>
                                                <div class="chart-note mr-0 d-block">
                                                    <span class="dot dot--yellow"></span>
                                                    <span>investors</span>
                                                </div>
                                                <div class="chart-note mr-0 d-block">
                                                    <span class="dot dot--green"></span>
                                                    <span>stakeholders</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-6">
                                            <div class="percent-chart">
                                                <canvas id="percent-chart"></canvas>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                    </div> 

                    <div class="row">
                        <div class="col-lg-12">
                            <h2 class="title-1 m-b-25">Angsuran Jatuh Tempo Hari Ini</h2>
                            <div class="table-responsive table--no-card m-b-40">
                                <table id="tableok" class="table table-borderless table-striped table-earning">
                                    <thead>
                                        <tr>
                                            <th>Tanggal Akad</th>
                                            <th>ID Akad</th>
                                            <th>ID Order</th>
                                            <th>Nama Kustomer</th>
                                            <th class="text-right">product</th>
                                            <th class="text-right">Tenor</th>
                                            <th class="text-right">Angsuran</th>
                                        
                                            <th>Keterangan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            $date = date("d");
                                            

                                            $query = "SELECT * FROM aus_akad WHERE tgl_tempo LIKE '%{$date}'";
                                            $data = mysqli_query($koneksi,$query);


                                            while ($result = mysqli_fetch_assoc($data)) {

                                                $id_order = $result['id_order'];

                                                //cari apakah angsuran sudah lunas atau belum 
                                                $q_lns = "SELECT * FROM aus_order_status WHERE id_order = '$id_order'";
                                                $d_lns = mysqli_query($koneksi,$q_lns);
                                                $r_lns = mysqli_fetch_assoc($d_lns);

                                                $lunas = $r_lns['status'];



                                                //$r_row = mysqli_num_rows($r_lns);

                                                //echo $r_row;

                                                //$r_d_row = mysqli_fetch_assoc($r_row);




                                                if ($lunas !== 'LUNAS') {
                                                    //data ada - berarti angsuran sudah lunas

                                                    //cari total angsuran seharusnya


?>

<tr>
                                                    <td>
                                                        <?php 
                                                            

                                                            echo $result['tgl_akad'];
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php  echo "AUS-AK00".$result['id_akad']; ?>
                                                    </td>
                                                    <td><?php echo "AUS-OR00".$id_order; ?></td>
                                                    <td>
                                                        <?php

                                                        //cari nama konsumen dari detail orderannya

                                                        $q_or = "SELECT * FROM aus_order WHERE id_order = '$id_order'";
                                                        $d_or = mysqli_query($koneksi,$q_or);

                                                        $r_or = mysqli_fetch_assoc($d_or);


                                                            $id_customer = $r_or['id_customer'];
                                                            $q_cs = "SELECT * FROM aus_customer WHERE id_customer ='$id_customer'";
                                                            $d_cs = mysqli_query($koneksi,$q_cs);
                                                            $result_cs = mysqli_fetch_assoc($d_cs);

                                                            echo $result_cs['nama_lengkap'];
                                                         ?>

                                                    </td>
                                                    
                                                    <td class="text-right">
                                                        <?php 
                                                            $id_product = $r_or['id_product'];
                                                            $q_pr = "SELECT * FROM aus_products WHERE id_product ='$id_product'";
                                                            $d_pr = mysqli_query($koneksi,$q_pr);
                                                            $result_pr = mysqli_fetch_assoc($d_pr);

                                                            echo $result_pr['name'];
                                                         ?>
                                                    </td>
                                                    <td class="text-right">
                                                        <?php 

                                                        //cari tenor
                                                        $q_tenor = "SELECT * FROM aus_customer_rincian_angsuran WHERE id_order = '$id_order'";
                                                        $d_tenor = mysqli_query($koneksi, $q_tenor);
                                                        $r_tenor = mysqli_fetch_assoc($d_tenor);

                                                        echo $r_tenor['tenor']." Bulan";

                                                        
                                                         ?>

                                                    </td>
                                                    <td class="text-right">
                                                        <?php 
                                                            //echo "<span class='harga_rp'>".$r_tenor['total_bayar']."</span>";

                                                            echo "<span class='harga_rp'>".$r_tenor['angsuran']."</span>";
                                                        ?>
                                                    </td>
                                                    

                                                    <td>
                                                        <?php
                                                            $q_insta =  "SELECT count(id_order) as total FROM aus_installments WHERE id_order = '$id_order' ";
                                                            $d_insta = mysqli_query($koneksi,$q_insta);
                                                            $result_insta = mysqli_fetch_assoc($d_insta);

                                                            if ($r_st['status'] == '') {
                                                                $ke = $result_insta['total'];
                                                                $ke = $ke ;

                                                                if ($ke == '' or $ke == 0) {
                                                                    echo "Uang Muka";
                                                                }else{
                                                                    echo "Pembayaran Ke -  ".$ke;
                                                                }
                                                            }else{
                                                                $ke = $result_insta['total'];
                                                                $ke = $ke + 1 ;
                                                                echo 'Angsuran Telah Lunas';
                                                            }
                                                            
                                                                
                                                            
                                                         ?>
                                                    </td>
                                                </tr>
<?php  
                                                    //echo "do nothing";
                                                }else{

                                                    //echo "tidak ada";
                                                    //data tidak ada - artinya angsuran belum lunas 
                                                    //cari detail angsuran 

                                                    //echo "ini yang belum lunas";
                                                ?>

                                                

                                                
                                        
                                        
                                        <?php } //kurung else 
                                                } //kurung loops  
                                                ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>



                    <div class="row">
                        <div class="col-lg-12">
                            <h2 class="title-1 m-b-25">Angsuran Macet/Telat</h2>
                            <div class="table-responsive table--no-card m-b-40">
                                <table id="tableok2" class="table table-borderless table-striped table-earning">
                                    <thead>
                                        <tr>
                                            <th>Tanggal Akad</th>
                                            <th>ID Akad</th>
                                            <th>ID Order</th>
                                            <th>Nama Kustomer</th>
                                            <th class="text-right">product</th>
                                            <th class="text-right">Tenor</th>
                                            <th class="text-right">Angsuran</th>
                                        
                                            <th>Keterangan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            //$date = date("d");
                                           

                                            $query = "SELECT * FROM aus_akad ORDER BY id_akad DESC";
                                            $data = mysqli_query($koneksi,$query);


                                            while ($result = mysqli_fetch_assoc($data)) {

                                                $id_order = $result['id_order'];

                                                //cari apakah angsuran sudah lunas atau belum 
                                                $q_lns = "SELECT * FROM aus_order_status WHERE id_order = '$id_order' ORDER BY id_order DESC";
                                                $d_lns = mysqli_query($koneksi,$q_lns);
                                                $r_lns = mysqli_fetch_assoc($d_lns);

                                                $lunas = $r_lns['status'];



                                                //$r_row = mysqli_num_rows($r_lns);

                                                //echo $r_row;

                                                //$r_d_row = mysqli_fetch_assoc($r_row);




                                                if ($lunas !== 'LUNAS') {
                                                    //data ada - berarti angsuran sudah lunas

                                                    #cari taotal angsuran yang sudah dibayarkan

                                                    $q_install = "SELECT count(*) as angsuran FROM aus_installments WHERE id_order = '$id_order'";
                                                    $d_install = mysqli_query($koneksi,$q_install);
                                                    $r_install = mysqli_fetch_assoc($d_install);

                                                    $total_angsuran_terbayar = $r_install['angsuran'];

                                                    //total angsuran total dari tenor
                                                    $q_install_total = "SELECT tenor FROM aus_customer_rincian_angsuran WHERE id_order='$id_order'";
                                                    $d_install_total = mysqli_query($koneksi,$q_install_total);
                                                    $r_install_total = mysqli_fetch_assoc($d_install_total);

                                                    $total_angsuran_total = $r_install_total['tenor'];


                                                    //total angsuran seharusnya di bulan ini
                                                    $date = date('Y-m-d');

                                                    //cari tgl akadnya lalu kurangi dgn tanggal hari ini itu adalah total angsuran seharusnya
                                                    $tgl_akad = $result['tgl_akad'];

                                                    $diff = abs(strtotime($date) - strtotime($tgl_akad));

                                                    $years = floor($diff / (365*60*60*24));
                                                    $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                                                    $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

                                                    //printf("%d years, %d months, %d days\n", $years, $months, $days);

                                                    $angsuran_seharusnya = $months;


                                                    $macet = $angsuran_seharusnya - $total_angsuran_terbayar;

                                                    //echo $angsuran_seharusnya;

                                                    if ($macet >= 0) {
                                                        
                                                    




?>

<tr>
                                                    <td>
                                                        <?php 
                                                            

                                                            echo $result['tgl_akad'];
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php  echo "AUS-AK00".$result['id_akad']; ?>
                                                    </td>
                                                    <td><?php echo "AUS-OR00".$id_order; ?></td>
                                                    <td>
                                                        <?php

                                                        //cari nama konsumen dari detail orderannya

                                                        $q_or = "SELECT * FROM aus_order WHERE id_order = '$id_order'";
                                                        $d_or = mysqli_query($koneksi,$q_or);

                                                        $r_or = mysqli_fetch_assoc($d_or);


                                                            $id_customer = $r_or['id_customer'];
                                                            $q_cs = "SELECT * FROM aus_customer WHERE id_customer ='$id_customer'";
                                                            $d_cs = mysqli_query($koneksi,$q_cs);
                                                            $result_cs = mysqli_fetch_assoc($d_cs);

                                                            echo $result_cs['nama_lengkap'];
                                                         ?>

                                                    </td>
                                                    
                                                    <td class="text-right">
                                                        <?php 
                                                            $id_product = $r_or['id_product'];
                                                            $q_pr = "SELECT * FROM aus_products WHERE id_product ='$id_product'";
                                                            $d_pr = mysqli_query($koneksi,$q_pr);
                                                            $result_pr = mysqli_fetch_assoc($d_pr);

                                                            echo $result_pr['name'];
                                                         ?>
                                                    </td>
                                                    <td class="text-right">
                                                        <?php 

                                                        //cari tenor
                                                        $q_tenor = "SELECT * FROM aus_customer_rincian_angsuran WHERE id_order = '$id_order'";
                                                        $d_tenor = mysqli_query($koneksi, $q_tenor);
                                                        $r_tenor = mysqli_fetch_assoc($d_tenor);

                                                        echo $r_tenor['tenor']." Bulan";

                                                        
                                                         ?>

                                                    </td>
                                                    <td class="text-right">
                                                        <?php 
                                                            //echo "<span class='harga_rp'>".$r_tenor['total_bayar']."</span>";

                                                            echo "<span class='harga_rp'>".$r_tenor['angsuran']."</span>";
                                                        ?>
                                                    </td>
                                                    

                                                    <td>
                                                        <?php
                                                            echo "Menuggak ".$macet." Bulan ".$days." Hari";
                                                            
                                                                
                                                            
                                                         ?>
                                                    </td>
                                                </tr>
<?php  }
                                                    //echo "do nothing";
                                                }else{

                                                    //echo "tidak ada";
                                                    //data tidak ada - artinya angsuran belum lunas 
                                                    //cari detail angsuran 

                                                    //echo "ini yang belum lunas";
                                                ?>

                                                

                                                
                                        
                                        
                                        <?php } //kurung else 
                                                } //kurung loops  
                                                ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>



                    

         


    
                    

                    <!-- <div class="row">
                        <div class="col-lg-3">
                            <h2 class="title-1 m-b-25">Top Products</h2>
                            <div class="au-card au-card--bg-blue au-card-top-countries m-b-40">
                                <div class="au-card-inner">
                                    <div class="table-responsive">
                                        <table class="table table-top-countries">
                                            <tbody>
                                                <tr>
                                                    <td>Name</td>
                                                    <td class="text-right">Total</td>
                                                </tr>
                                                <tr>
                                                    <td>Motor Yamaha</td>
                                                    <td class="text-right">32</td>
                                                </tr>
                                                <tr>
                                                    <td>TV Samsung LED 32 Inch</td>
                                                    <td class="text-right">26</td>
                                                </tr>
                                                <tr>
                                                    <td>iPhone X 64GB</td>
                                                    <td class="text-right">25</td>
                                                </tr>
                                                <tr>
                                                    <td>Camera C430W 4k</td>
                                                    <td class="text-right">23</td>
                                                </tr>
                                                <tr>
                                                    <td>Samsung S8 Black</td>
                                                    <td class="text-right">19</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-9">
                            <h2 class="title-1 m-b-25">installment payment</h2>
                            <div class="table-responsive table--no-card m-b-40">
                                <table class="table table-borderless table-striped table-earning">
                                    <thead>
                                        <tr>
                                            <th>date</th>
                                            <th>akad ID</th>
                                            <th>name</th>
                                            <th class="text-right">installment</th>
                                            <th class="text-right">total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>2018-09-17 05:57</td>
                                            <td>100298</td>
                                            <td>Ibnu Firdaus</td>
                                            <td class="text-right">4</td>
                                            <td class="text-right">Rp 675.000</td>
                                        </tr>
                                        <tr>
                                            <td>2018-09-17 01:22</td>
                                            <td>100297</td>
                                            <td>Yoga Permana</td>
                                            <td class="text-right">9</td>
                                            <td class="text-right">Rp 430.000</td>
                                        </tr>
                                        <tr>
                                            <td>2018-09-17 12:12</td>
                                            <td>100156</td>
                                            <td>Taqwa Lidia</td>
                                            <td class="text-right">23</td>
                                            <td class="text-right">Rp 955.000</td>
                                        </tr>
                                        <tr>
                                            <td>2018-09-17 10:06</td>
                                            <td>100235</td>
                                            <td>Suraj</td>
                                            <td class="text-right">12</td>
                                            <td class="text-right">Rp 123.500</td>
                                        </tr>
                                        <tr>
                                            <td>2018-09-17 9:03</td>
                                            <td>100224</td>
                                            <td>Mery Rosyandari</td>
                                            <td class="text-right">11</td>
                                            <td class="text-right">Rp 254.000</td>
                                        </tr>
                                        <tr>
                                            <td>2018-09-17 08:57</td>
                                            <td>100220</td>
                                            <td>Sari Anggraini</td>
                                            <td class="text-right">10</td>
                                            <td class="text-right">Rp 1.275.000</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div> -->
                    </div>