<?php 

    //hitung jumlah Customer
    $area = trim($_SESSION['area']);
    $vendor = $_SESSION['nama'];
    $acc = $vendor.'-'.$area;
    $areaSession = trim($_SESSION['area']);
    $vendorSession = $_SESSION['nama'];
    
 ?>

<div class="row m-t-25">
    <div class="col-md-12">
        <h3 class="title-5 m-b-35">Switch/Removal Request</h3>
        <div class="table-data__tool">
        
        </div>
        <div class="table-responsive table-data" style="height: 100%">
            <table id="tableok" class="mdl-data-table" style="width:100%">
                <thead>
                    <tr>
                        <!-- <td>Stats</td> -->
                        <td>Action</td>
                        <td>Plan Date</td>
                        <td>No</td>
                        <td>Request</td>
			<td>Assign Date</td>
                        <td>BFI Code</td>
                        <td>Outlet Name</td>
                        <td>Distributor Name</td>
                        <!-- <td>Removal/Switch Date</td>
                        <td>Removal/Switch Status</td> -->
                        <td>Freezer Code</td>
                        <td>Type</td>
                        <td>Barcode</td>
                        <!-- <td>Type (New)</td>
                        <td>Barcode (New)</td> -->
                        <td>Supplier Name</td>
                        
                        <td>Removal/Switch Reason</td>
                        <td>Latitude</td>
                        <td>Longitude</td>

                        <td>Nama PIC</td>
                        <td>No Telp PIC</td>
                        <td>Alamat</td>
                        <td>Provinsi</td>
                        <td>Kabupaten / Kota</td>
                        <td>Kecamatan</td>
                        <td>Kelurahan</td>
                        <td>Kode Pos</td>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $no = 1;
                        include ('config/config.php');
                        $acc = $vendor.''.$area;
                        $acc2 = '1{PT'.$vendor.'}-'.$area;
                        $acc3 = '1 {PT'.$vendor.'}-'.$area;
                        $acc4 = '1{PT '.$vendor.'}-'.$area;
                        $acc5 = '1 {PT '.$vendor.'}-'.$area;
                        $acc6 = 'PT'.$vendor.''.$area;
                        $acc7 = 'PT '.$vendor.'-'.$area;
                        $acc8 = 'PT'.$vendor.' '.$area;
                        $acc9 = $vendor.'-'.$area;

                        

                        //ambil data requestnya dulu
                        // $vendor = $_SESSION['nama'];
                        $query = "SELECT * FROM freezer_request WHERE status != 'FAILED' and tgl_action = '' AND (supplier LIKE '%$acc%' or supplier LIKE '%$acc2%' or supplier LIKE '%$acc3%'or supplier LIKE '%$acc4%'or supplier LIKE '%$acc5%'or supplier LIKE '%$acc6%'or supplier LIKE '%$acc7%'or supplier LIKE '%$acc8%' or supplier LIKE '%$acc9%') ORDER BY id_request ";
                        $data = mysqli_query($koneksi,$query);

                        

                            $acc_origin = $r['supplier'];

                            //find if it has a special karakter
                            if (preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $acc_origin)){
                                $acc_origin = str_replace("1","",$acc_origin);
                                $acc_origin = str_replace("{","",$acc_origin);  
                                $acc_origin = str_replace("PT","",$acc_origin);    
                                $acc_origin = str_replace("}","",$acc_origin);
                                $acc_origin = str_replace("-","",$acc_origin);
                                
                            }else{
                                $acc_origin = str_replace("PT","",$acc_origin); 
                                $acc_origin = $acc_origin;
                            }

                            

                        while ($r = mysqli_fetch_assoc($data)) {



                     ?>
                    <tr>
                        <?php 
                            $id_freezer = $r['id_freezer'];
                            $q = "SELECT * FROM focus_freezer_master WHERE id_freezer = '$id_freezer'";
                            $d = mysqli_query($koneksi,$q);
                            $result = mysqli_fetch_assoc($d);
                         ?>
                       
                        <!-- <td> 

                            <?php 
                                if ($r['action'] == 'remove') {
                            ?>

                            <?php 
                            echo '
                            <form action="model/freezermanagement/failure-remove.php" method="post" class="form-tindaklanjut" id="form-tindaklanjut-'.$result['id_freezer'].'">
                                <input type="hidden" id="id" name="id_freezer" value="'.$result['id_freezer'].'">
                                <input type="hidden" id="supplier" name="supplier" value="'.$result['Supplier'].'">
				<input type="hidden" id="id_req" name="id_req" value="'.$r['id_request'].'">


                                <div class="row">
                                    <div class="col-md-12">
                                        <small>Failure Notes</small>
                                    <br>
                                        <input type="text" id="ket-tindaklanjut" name="keterangan" class="form-control" required>
                                    </div>
                                </div>
                                <br>

                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-sm btn-success">
                                            Update
                                        </button>
                                   
                                        <a href="javascript:void();" class="btn btn-sm btn-dark batal-tindaklanjut">Batal</a>
                                    </div>
                                 </div>
                                    
                                </div>
                            </form>
                            
                            <a href="javascript:void();" class="btn btn-sm btn-dark tindaklanjut" id="rejectbtn" data-toggle="tooltip" data-placement="top" title="Failure Remove" acuan="'.$result['id_freezer'].'"><i class="fas fa fa-ban"> </i></a> 
                             ';

                             ?>

                            

                            <?php 

                                }else{

                             ?> 

                             <?php 
                            echo '
                            <form action="model/freezermanagement/failure-switch.php" method="post" class="form-tindaklanjut" id="form-tindaklanjut-'.$result['id_freezer'].'">
                                <input type="hidden" id="id" name="id_freezer" value="'.$result['id_freezer'].'">
                                <input type="hidden" id="supplier" name="supplier" value="'.$result['Supplier'].'">
				<input type="hidden" id="id_req" name="id_req" value="'.$r['id_request'].'">

                               
                                <div class="row">
                                    <div class="col-md-12">
                                        <small>Failure Notes</small>
                                    <br>
                                        <input type="text" id="ket-tindaklanjut" name="keterangan" class="form-control" required>
                                    </div>
                                </div>
                                <br>

                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-sm btn-success">
                                            Update
                                        </button>
                                   
                                        <a href="javascript:void();" class="btn btn-sm btn-dark batal-tindaklanjut">Batal</a>
                                    </div>
                                 </div>
                                    
                                </div>
                            </form>
                            
                            <a href="javascript:void();" class="btn btn-sm btn-dark tindaklanjut" id="rejectbtn" data-toggle="tooltip" data-placement="top" title="Failure Switch" acuan="'.$result['id_freezer'].'"><i class="fas fa fa-ban"> </i></a> 
                             ';

                            } ?>

                           
                            
                        </td> -->

                        <!--  -->

                        <td>
                            <?php 
                                if ($r['action'] == 'remove') {
                            ?>

                            
                            <form action="model/freezermanagement/reject-remove.php" method="post" class="form-tindaklanjut" id="form-tindaklanjut-reject<?php echo $result['id_freezer']?>">
                                <input type="hidden" id="id" name="id_freezer" value="<?php echo $result['id_freezer'] ?>">
                                <input type="hidden" id="supplier" name="supplier" value="'<?php echo $result['Supplier'] ?>">
				<input type="hidden" id="id_req" name="id_req" value="<?php echo $r['id_request'] ?>">


                                <div class="row">
                                    <div class="col-md-12">
                                       <select name="keterangan" class="select_database form-control" required>
                                        <option value="">Pilih Rejection Code</option>

                                        <?php 
                                            $query_user = "SELECT * FROM focus_rejection_code";
                                            $data_user = mysqli_query($koneksi,$query_user);
                                            while ($result_rejection = mysqli_fetch_assoc($data_user)) {
                                                
                                         ?>
                                            <option value="<?php echo $result_rejection['ket'] ?>"><?php echo $result_rejection['rejection_code'] ?> - <?php echo $result_rejection['ket'] ?></option>

                                        <?php } ?>

                                        </select>
                                    </div>
                                </div>
                                <br>


                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-sm btn-success">
                                            Update
                                        </button>
                                   
                                        <a href="javascript:void();" class="btn btn-sm btn-dark batal-tindaklanjut">Batal</a>
                                    </div>
                                 </div>
                                    
                                </div>
                            </form>
                            
                            <a href="javascript:void();" class="btn btn-sm btn-danger tindaklanjut" id="rejectbtn" data-toggle="tooltip" data-placement="top" title="Reject Removal" acuan="reject<?php echo $result['id_freezer'] ?>"><i class="fas fa fa-warning"> </i></a> 

                             

                            <form action="model/freezermanagement/vendor-remove.php" method="post" class="form-tindaklanjut" id="form-approved-<?php echo $r['id_freezer']?>">
                                <input type="hidden" id="id" name="id_freezer" value=" <?php echo $r['id_freezer'] ?>">
                                <input type="hidden" id="id" name="keterangan" value="REMOVED">
                                <input type="hidden" id="id" name="removestatus" value="success">
                                <input type="hidden" id="id" name="BarcodeFANumber" value="<?php echo $result['BarcodeFANumber'] ?>">
                                <input type="hidden" id="id" name="Freezer_Owner" value="<?php echo $result['Freezer_Owner'] ?>">
				<input type="hidden" id="id_req" name="id_req" value="<?php echo $r['id_request'] ?>">
                                

                                <div class="row">
                                    <div class="col-md-12">
                                        <small>Removal Date</small>
                                    <br>
                                        <input type="date" id="idnumber" name="removedate" class="form-control" required>
                                    </div>
                                </div>

                                <?php if($result['Freezer_Owner'] == 'Jbp') { ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <small>Kondisi Freezer</small>
                                    <br>
                                        <select name="kd_condition" id="kd_condition" class="select_bfi form-control" required>
                                            <option value="">kondisi Freezer</option>
                                            <?php 
                                                include ('config/config.php');
                                                //cari data semua BFI
                                                $sql = "SELECT * FROM freezer_condition";

                                                $dql = mysqli_query($koneksi,$sql) or die(mysqli_error($koneksi));
                                                

                                                while ($rql = mysqli_fetch_assoc($dql)) {
                                            ?>
                                                <option value="<?php echo $rql['kd_condition'] ?>"><?php echo strtoupper($rql['nm_condition'])?></option>

                                            <?php 
                                                }


                                             ?>
                                            

                                        </select>               
                                        <!-- <input type="text" id="realisasi_tipe_freezer" name="realisasi_tipe_freezer" class="form-control" required> -->
                                    </div>
                                </div>
                                <?php } ?>
                                
                                <!-- <div class="row">
                                    <div class="col-md-12">
                                        <small>Removal Status</small>
                                    <br>
                                        <input type="text" id="idnumber" name="removestatus" class="form-control" required>
                                    </div>
                                </div> -->
                                                                
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-sm btn-success">
                                            Update
                                        </button>
                                   
                                        <a href="#" class="btn btn-sm btn-dark batal-tindaklanjut">Batal</a>
                                    </div>
                                 </div>
                                    
                                </div>
                            </form>
                            

                            <a href="#" id="approved_placement_<?php echo $r['id_freezer']?>" class="btn btn-sm btn-warning approved_placement" data-toggle="tooltip" data-placement="top" title="Process Remove Freezer" acuan="<?php echo $r['id_freezer']?>">
                                <i class="fas fa fa-minus"></i>
                            </a>

                            <?php 

                                }else{

                             ?> 

                             
                            <form action="model/freezermanagement/reject-switch.php" method="post" class="form-tindaklanjut" id="form-tindaklanjut-reject<?php echo $result['id_freezer'] ?>">
                                <input type="hidden" id="id" name="id_freezer" value="<?php echo $result['id_freezer'] ?>">
                                <input type="hidden" id="supplier" name="supplier" value="<?php echo $result['Supplier'] ?>">
				<input type="hidden" id="id_req" name="id_req" value="<?php echo $r['id_request'] ?>">

                               
                                <div class="row">
                                    <div class="col-md-12">
                                        <select name="keterangan" id="namavendor" class="select_database form-control" required>
                                        <option value="">Pilih Rejection Code</option>

                                        <?php 
                                            $query_user = "SELECT * FROM focus_rejection_code";
                                            $data_user = mysqli_query($koneksi,$query_user);
                                            while ($result_rejection = mysqli_fetch_assoc($data_user)) {
                                                
                                         ?>
                                            <option value="<?php echo $result_rejection['ket'] ?>"><?php echo $result_rejection['rejection_code'] ?> - <?php echo $result_rejection['ket'] ?></option>

                                        <?php } ?>

                                    </select>
                                    </div>
                                </div>
                                <br>

                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-sm btn-success">
                                            Update
                                        </button>
                                   
                                        <a href="javascript:void();" class="btn btn-sm btn-dark batal-tindaklanjut">Batal</a>
                                    </div>
                                 </div>
                                    
                                </div>
                            </form>
                            
                            <a href="javascript:void();" class="btn btn-sm btn-danger tindaklanjut" id="rejectbtn" data-toggle="tooltip" data-placement="top" title="Reject Switch" acuan="reject<?php echo $result['id_freezer'] ?>"><i class="fas fa fa-warning"> </i></a> 
                             

                             <form action="model/freezermanagement/vendor-switch.php" method="post" class="form-tindaklanjut" id="form-approved-<?php echo $r['id_freezer']?>">
                                <input type="hidden" id="id" name="id_freezer" value="<?php echo $r['id_freezer'] ?>">
                                <input type="hidden" id="id" name="keterangan" value="SWITCHED">
                                <input type="hidden" id="id" name="switchstatus" value="success">
                                <input type="hidden" name="tipe_old" value="<?php echo $result['Type'] ?>">
                                <input type="hidden" name="nomormesinold" value="<?php echo $result['ID_Number'] ?>">
				<input type="hidden" id="id_req" name="id_req" value="<?php echo $r['id_request'] ?>">
				<input type="hidden" name="oldbatchpo" value="<?php echo $result['BatchPO'] ?>">
                <input type="hidden" name="assetId_old" value="<?php echo $result['BarcodeFANumber'] ?>">
                <input type="hidden" name="IDBelfoodd" value="<?php echo $result['IDBelfoodd'] ?>">
                <input type="hidden" name="Outletname" value="<?php echo $result['Outletname'] ?>">

                                <div class="row">
                                    <div class="col-md-12">
                                        <small>Switch Date</small>
                                    <br>
                                        <input type="date" id="idnumber" name="switchdate" class="form-control" required>
                                    </div>
                                </div>

                                <!-- <div class="row">
                                    <div class="col-md-12">
                                        <small>Switch Status</small>
                                    <br>
                                        <input type="text" id="idnumber" name="swtichstatus" class="form-control" required>
                                    </div>
                                </div> -->
                                 <?php if($result['Freezer_Owner'] == 'Jbp') { ?>
                                  <div class="row">
                                    <div class="col-md-12">
                                        <small>Tipe Freezer Baru</small>
                                        <br>
                                        
                                        <select name="tipe" id="realisasi_tipe_freezerAsset" class="select_database form-control realisasi_tipe_freezerAsset" required>


                                        </select>
                                    </div>
                                </div>
                                <?php }else{ ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <small>Realisasi Tipe Freezer</small>
                                    <br>
                                        <select name="tipe" class="select_bfi form-control" required>
                                            <option value="">Select Tipe Freezer</option>
                                            <?php 
                                                include ('config/config.php');
                                                //cari data semua BFI
                                                $sql = "SELECT * FROM focus_tipe_freezer ORDER BY id_tipe";
                                                $dql = mysqli_query($koneksi,$sql);
                                                

                                                while ($rql = mysqli_fetch_assoc($dql)) {
                                            ?>
                                                <option value="<?php echo $rql['tipe'] ?>"><?php echo strtoupper($rql['tipe']." - ".$rql['keterangan'])?></option>

                                            <?php 
                                                }


                                             ?>
                                            

                                        </select>               
                                        <!-- <input type="text" id="realisasi_tipe_freezer" name="realisasi_tipe_freezer" class="form-control" required> -->
                                    </div>
                                </div>
                            <?php } ?>
                                <!--
                                <div class="row">
                                    <div class="col-md-12">
                                        <small>Tipe Freezer Baru</small>
                                    <br>
                                    <select name="tipe" class="select_bfi form-control" required>
                                            <option value="">Select Tipe Freezer</option>
                                            <?php 
                                                include ('config/config.php');
                                                //cari data semua BFI
                                                $sql = "SELECT * FROM focus_tipe_freezer ORDER BY id_tipe";
                                                $dql = mysqli_query($koneksi,$sql);
                                                

                                                while ($rql = mysqli_fetch_assoc($dql)) {
                                            ?>
                                                <option value="<?php echo $rql['tipe'] ?>"><?php echo strtoupper($rql['tipe']." - ".$rql['keterangan'])?></option>

                                            <?php 
                                                }


                                             ?>
                                            

                                        </select>   
                                         <input type="text" id="idnumber" name="tipe" class="form-control" required>
                                    </div>
                                </div>
				                -->
				                <div class="row">
                                    <div class="col-md-12">
                                        <small>Batch PO Baru</small>
                                    <br>
                                    <select name="newbatchpo" class="select_bfi form-control" required>
                                            <option disabled selected>Select Batch PO</option>
                                            <?php
                                                include ('config/config.php');
                                                //cari data semua BFI
                                                $sql = "SELECT * FROM focus_batch_po ORDER BY batchpo";
                                                $dql = mysqli_query($koneksi,$sql);


                                                while ($rql = mysqli_fetch_assoc($dql)) {
                                            ?>
                                                <option value="<?php echo $rql['batchpo'] ?>"><?php echo strtoupper($rql['batchpo']." - ".$rql['keterangan'])?></option>

                                            <?php
                                                }


                                             ?>


                                        </select>
                                        <!-- <input type="text" id="idnumber" name="tipe" class="form-control" required> -->
                                    </div>
                                </div>

                                <?php if($result['Freezer_Owner'] == 'Jbp') { ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <small>Aset Freezer</small>
                                    <br>
                                        <select name="assetId" id="assetId" class="select_bfi form-control" onChange="javascript:get_brand()"  required>
                                            <option value="">Select Aset Freezer</option>
                                            <?php 
                                                include ('config/config.php');
                                                //cari data semua BFI
                                                $sql = "SELECT * FROM focus_asset_freezer WHERE vendor = '$vendorSession' AND area = '$areaSession' AND assetStatusId = '1' AND assetId !=''";

                                                $dql = mysqli_query($koneksi,$sql) or die(mysqli_error($koneksi));
                                                

                                                while ($rql = mysqli_fetch_assoc($dql)) {
                                            ?>
                                                <option value="<?php echo $rql['assetId'] ?>"><?php echo strtoupper($rql['assetId'])?></option>

                                            <?php 
                                                }


                                             ?>
                                            

                                        </select>               
                                        <!-- <input type="text" id="realisasi_tipe_freezer" name="realisasi_tipe_freezer" class="form-control" required> -->
                                    </div>
                                </div>
                                <?php } ?>

                                <?php if($result['Freezer_Owner'] == 'Jbp') { ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <small>Brand</small>
                                    <br>
                                        
                                        <select name="brand" id="brandAsset" class="select_database form-control brandAsset" required>
                                        

                                    </select>
                                    </div>
                                </div>
                                <?php }?>

                                <?php if($result['Freezer_Owner'] == 'Jbp') { ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <small>Nomor Mesin Baru</small>
                                            <br>
                                            <select name="nomormesin" id="nomormesinAsset" class="select_database form-control nomormesinAsset" required></select>
                                        </div>
                                    </div>
                                <?php }else{ ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <small>Nomor Mesin Baru</small>
                                            <br>
                                            <input type="text" id="nomormesin" name="nomormesin" class="form-control" required>
                                        </div>
                                    </div>
                                <?php } ?>

                                <br>
                                -----------------
                                <?php if($result['Freezer_Owner'] == 'Jbp') { ?>

                                 <div class="row">
                                    <div class="col-md-12">
                                        <small>Nomor Aset Freezer Lama</small>
                                    <br>
                                         
                                        <input type="text" class="form-control" value="<?php echo $result['BarcodeFANumber']; ?>" readonly>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <small>Kondisi Freezer Lama</small>
                                    <br>
                                        <select name="kd_condition" id="kd_condition" class="select_bfi form-control" required>
                                            <option value="">kondisi Freezer</option>
                                            <?php 
                                                include ('config/config.php');
                                                //cari data semua BFI
                                                $sql = "SELECT * FROM freezer_condition";

                                                $dql = mysqli_query($koneksi,$sql) or die(mysqli_error($koneksi));
                                                

                                                while ($rql = mysqli_fetch_assoc($dql)) {
                                            ?>
                                                <option value="<?php echo $rql['kd_condition'] ?>"><?php echo strtoupper($rql['nm_condition'])?></option>

                                            <?php 
                                                }


                                             ?>
                                            

                                        </select>               
                                        <!-- <input type="text" id="realisasi_tipe_freezer" name="realisasi_tipe_freezer" class="form-control" required> -->
                                    </div>
                                </div>
                                <?php } ?>

                                <br>

                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-sm btn-success">
                                            Update
                                        </button>
                                   
                                        <a href="#" class="btn btn-sm btn-dark batal-tindaklanjut">Batal</a>
                                    </div>
                                 </div>
                                    
                                </div>
                            </form>
                            

                            <a href="#" id="approved_placement_<?php echo $r['id_freezer']?>" class="btn btn-sm btn-success approved_placement" data-toggle="tooltip" data-placement="top" title="Process Switch Freezer" acuan="<?php echo $r['id_freezer']?>">
                                <i class="fas fa fa-exchange-alt"></i>
                            </a>


                        <?php } ?>
                        </td>
                        <td>
                            <?php if ($r['tgl_plan'] == '' or $r['tgl_plan'] == '0000-00-00'): ?>
                                <form action="model/freezermanagement/plandate.php" method="post">
                                <input type="hidden" id="id" name="id_req" value=" <?php echo $r['id_request'] ?>">

                                <!-- <input type="text" name="bficode" value="<?php echo $result['custid'] ?>"> -->

                                <div class="row">
                                    <div class="col-md-12">
                                        
                                        <input type="date" name="plan" class="form-control" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-sm btn-success btn-block">
                                            Update
                                        </button>
                                   
                                        <!-- <a href="#" class="btn btn-sm btn-dark batal-tindaklanjut">Batal</a> -->
                                    </div>
                                 </div>
                                    
                                </div>
                            </form>

                            <?php else: ?>
                                <?php echo $r['tgl_plan'] ?>
                            <?php endif ?>
                        </td>
                    
                        
                        <td><?php echo $no ?></td>
                        <td>
                            <?php echo strtoupper($r['action']) ?>
                        </td>
                        <td>
                            <?php echo $r['tgl_request'] ?>
                        </td>
			<td>
                            <?php echo $result['IDBelfoodd'] ?>
                        </td>
                        <td>
                            <?php echo strtoupper($result['Outletname']) ?>
                        </td>
                        <td>
                            <?php echo $result['DistributorName'] ?>
                        </td>
                        
                        <!-- <td>
                            <?php echo $result['TanggalPenarikan'] ?>
                        </td>
                        <td>
                            <?php echo $result['Keterangan'] ?>
                        </td> -->
                        <td>
                            <?php echo $result['FreezerCode'] ?>
                        </td>
                        <td>
                            <?php echo $result['Type'] ?>
                        </td>
                        <td>
                            <?php echo $result['ID_Number'] ?>
                        </td>
                       <!--  <td>
                            <?php echo $result['Type__Switching'] ?>
                        </td>
                        <td>
                            <?php echo $result['ID_Switching'] ?>
                        </td> -->
                        <td>
                            <?php echo $r['supplier'] ?>
                        </td>
                       
                        <td>
                            <?php echo strtoupper($r['keterangan']) ?>
                        </td>
                        <td>
                            <?php echo $result['Latitude'] ?>
                        </td>
                        <td>
                            <?php echo $result['Longitude'] ?>
                        </td>

                        <td>
                            <?php echo strtoupper($result['PIC']) ?>
                        </td>
                        <td>
                            <?php echo strtoupper($result['Phone']) ?>
                        </td>
                        <td>
                            <?php echo strtoupper($result['Alamat']) ?>
                        </td>
                        <td>
                            <?php echo strtoupper($result['Kelurahan']) ?>
                        </td>
                        <td>
                            <?php echo strtoupper($result['Kecamatan']) ?>
                        </td>
                        <td>
                            <?php echo strtoupper($result['KabKota']) ?>
                        </td>
                        <td>
                            <?php echo strtoupper($result['Propinsi']) ?>
                        </td>
                        <td>
                            <?php echo strtoupper($result['KodePos']) ?>
                        </td>
                        <!-- <td>
                            <div class="table-data-feature">
                                
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <a href="index.php?mod=users&class=edit&id=<?php echo $result['id_user']; ?>" ><i class="zmdi zmdi-edit"></i></a>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <a href="index.php?mod=users&class=delete&id=<?php echo $result['id_user'] ?>"><i class="zmdi zmdi-delete"></i></a>
                                </button>
                            </div>
                        </td> -->
                    </tr>

                     <?php $no++; } ?>
                </tbody>
            </table>
        </div>

    </div>
                        
</div>
