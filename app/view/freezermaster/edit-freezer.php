<?php 
    if (isset($_GET['id'])) {
        $id = $_GET['id'];
        $editsql = "SELECT * FROM focus_tipe_freezer WHERE id_tipe = '$id'";
        $dataedit = mysqli_query($koneksi,$editsql);
        $edit = mysqli_fetch_assoc($dataedit); 
    }
 ?>
<div class="row">
    <div class="col-md-12">
        <h3 class="title-5 m-b-35">edit Freezer</h3>

        <div class="card">
        <div class="card-header">
            <strong>Freezer Form</strong> 
        </div>
        <div class="card-body card-block">

            <form action="model/freezermaster/update.php" method="post" enctype="multipart/form-data" class="form-horizontal">

                <input type="hidden"  name="id" value="<?php echo $id ?>">
                
                
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Tipe</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="text" id="text-input" name="tipe" class="form-control" value="<?php echo $edit['tipe'] ?>" required>
                        
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Keterangan</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="text" id="txt_nama" name="ket" placeholder="" class="form-control" value="<?php echo $edit['keterangan'] ?>" required>
                    </div>
                </div>
                
                


                
                </div>
                <div class="card-footer">
                    <button type="submit" class="au-btn au-btn--green">
                        <i class="fa fa-floppy-o"></i> Save
                    </button>
                </div>



            </form>
        </div>
    </div>
    </div>
</div>