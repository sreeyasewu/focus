<?php 
	if (isset($_GET['mod'])) {
		$mod = $_GET['mod'];

		if ($mod == 'freezer') {
			include ('view/main-freezer.php');
		}elseif($mod == 'customers'){
			include ('view/main-customers.php');
		}elseif($mod == 'freezerdatabase'){
			include ('view/main-freezerdatabase.php');
		}elseif($mod == 'datareview'){
			include ('view/main-data-review.php');
		}elseif($mod == 'users'){
			include ('view/main-users.php');
		}elseif ($mod == 'deal') {
			include ('view/main-deal.php');
		}elseif ($mod == 'postpone') {
			include ('view/main-postpone.php');
		}elseif ($mod == 'reject') {
			include ('view/main-reject.php');
		}elseif ($mod == 'freezerplacement') {
			include ('view/main-freezerplacement.php');
		}elseif ($mod == 'freezerplacementvendor') {
			include ('view/main-freezerplacementvendor.php');
		}elseif ($mod == 'custcodecreation') {
			include ('view/main-custcodecreation.php');
		}elseif ($mod == 'replenishment') {
			include ('view/main-replenishment.php');
		}elseif ($mod == 'freezermaster') {
			include ('view/main-freezermaster.php');
		}elseif ($mod == 'all') {
			include ('view/main-all.php');
		}elseif ($mod == 'rejectioncode') {
			include ('view/main-rejectioncode.php');
		}elseif ($mod == 'manageplacement') {
			include ('view/main-manageplacement.php');
		}elseif ($mod == 'manageremoval') {
			include ('view/main-manageremoval.php');
		}
	}else{
		include ('view/main.php');
	}
 ?>