<header class="header-mobile d-block d-lg-none ">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href=<?php echo BASE_URL ?>>
                            <img src="assets/images/icon/logo-aus.png" alt="AUS" />
                        </a>
                       
                        <div class="header-button-item mr-0 js-sidebar-btn">
                                    <i class="zmdi zmdi-menu"></i>
                                </div>
                    </div>
                </div>
            </div>
           

            <aside class="menu-sidebar2 js-right-sidebar d-block d-lg-none">
                <div class="logo" style="background: white;">
                   <div class="header-button-item mr-0 js-sidebar-btn">
                        <i class="fas fa-close"></i>
                    </div>
                </div>
                <div class="menu-sidebar2__content js-scrollbar2">
                    <div class="account2">
                        <div class="image img-120">
                            <img src="assets/images/icon/logo-aus.png" alt="Logo AUS"/>
                        </div>
                        <!-- <h4 class="name">AUS Survey</h4> -->
                        <!-- <a href="#">Sign out</a> -->
                    </div>
                    <nav class="navbar-sidebar2">
                        <ul class="list-unstyled navbar__list">

                          

                        <li
                         <?php 
                         if (isset($_GET['mod'])) {
                               //do nothing
                            }else{
                                echo "class='active'";
                            }
                        ?>
                        >
                            <a href="index.php">
                            <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                        </li>

                        

                    

                        <li
                        <?php if (isset($_GET['mod'])) {
                            $mod = $_GET['mod'];
                            if ($mod == 'products') {
                                echo "class='active'";
                            }
                        } ?>
                        >
                            <a class="js-arrow" href="#">
                                <i class="fas fa-server"></i>Freezer Management</a>
                                <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <!-- <li>
                                    <a href="index.php?mod=freezer&class=add">Add Freezer</a>
                                </li> -->
                                <li>
                                    <a href="index.php?mod=freezer&class=view">View Freezer</a>
                                </li>
                            </ul>
                        </li>

                        <li
                         <?php if (isset($_GET['mod'])) {
                            $mod = $_GET['mod'];
                            if ($mod == 'investors') {
                                echo "class='active'";
                            }
                        } ?>
                        >
                            <a class="js-arrow" href="#">
                                <i class="fas fa-server"></i>Freezer Database</a>
                                <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <!-- <li>
                                    <a href="index.php?mod=investors&class=add">Add Investor</a>
                                </li> -->
                                <li>
                                    <a href="index.php?mod=freezerdatabase&class=view">View Freezer</a>
                                </li>
                            </ul>
                        </li>

                        <li
                        <?php if (isset($_GET['mod'])) {
                            $mod = $_GET['mod'];
                            if ($mod == 'products') {
                                echo "class='active'";
                            }
                        } ?>
                        >
                            <a class="js-arrow" href="#">
                                <i class="fas fa-building"></i>Placement & Removal</a>
                                <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="index.php?mod=manageplacement">Cancel Placement</a>
                                </li>
                                <li>
                                    <a href="index.php?mod=manageremoval">Cancel Removal</a>
                                </li>
                            </ul>
                            <!-- <a href="index.php?mod=manageplacement"> -->
                            <!-- <i class="fas fa-building"></i>Cancel Placement & Removal</a> -->
                        </li>



                        
                        </ul>
                    </nav>
                </div>
            </aside>
        </header>