<?php
    $email_session = $_SESSION['email'];

    $sql_tss = "SELECT a.TSS_Code, a.TSS_Name, a.nik FROM focus_tss a 
                INNER JOIN MDS_Customerprimary b ON a.TSS_Code = b.TSS_Code 
                INNER JOIN user u ON a.nik = u.nik
                WHERE u.email = '$email_session'";
    
    $query_tss = mysqli_query($koneksi, $sql_tss) or die (mysqli_error());
    $result_tss = mysqli_fetch_assoc($query_tss);

    // mengambil data barang dengan kode paling besar
    $query = mysqli_query($koneksi, "SELECT max(penarikanId) as kodeTerbesar FROM focus_freezer_request_penarikan");
    $data = mysqli_fetch_array($query);
    $kodeBarang = $data['kodeTerbesar'];

// mengambil angka dari kode barang terbesar, menggunakan fungsi substr
// dan diubah ke integer dengan (int)
    $urutan = (int) substr($kodeBarang, 3, 3);

// bilangan yang diambil ini ditambah 1 untuk menentukan nomor urut berikutnya
    $urutan++;

// membentuk kode barang baru
// perintah sprintf("%03s", $urutan); berguna untuk membuat string menjadi 3 karakter
// misalnya perintah sprintf("%03s", 15); maka akan menghasilkan '015'
// angka yang diambil tadi digabungkan dengan kode huruf yang kita inginkan, misalnya BRG 
    $date = date('YmdHis');
    $huruf = $date;
    $penarikanId = $huruf . sprintf("%05s", $urutan);
    echo $penarikanId;

?>
<div class="row">
    <div class="col-md-12">
        <h3 class="title-5 m-b-35">add Freezer</h3>

        <div class="card">
        <div class="card-header">
            <strong>Add new Freezer</strong>
        </div>
        <div class="card-body card-block">

            <form action="model/freezermanagement/request_tarik.php" method="post" enctype="multipart/form-data" class="form-horizontal">
                <input type="hidden" name="penarikanId" id="penarikanId" value="<?php echo $penarikanId ?>">
                <!-- <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Tanggal Pengajuan</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="date" id="text-input" name="tgl_pengajuan" class="form-control" required>
                    </div>
                </div> -->
                <?php  $sql = "SELECT a.TSS_Code, a.TSS_Name, a.nik, b.Code,b.Name FROM focus_tss a 
                                INNER JOIN MDS_Customerprimary b ON a.TSS_Code = b.TSS_Code 
                                INNER JOIN USER u ON a.nik = u.nik 
                                WHERE u.nik = '".$result_tss['nik']."'
                                AND SegmentId_ID = '32'";

                        $sql_reason = "SELECT * FROM focus_rejection_code";

                        $sql_freezer = "SELECT * FROM focus_freezer_master WHERE StatusFreezer = 'IN FIELD'";
                                
                        $dql = mysqli_query($koneksi,$sql);
                        $dql_reason = mysqli_query($koneksi,$sql_reason);
                        $dql_freezer = mysqli_query($koneksi,$sql_freezer);
                ?>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Pilih Distirbutor</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <select name="distributor" id="distributor" class="select_database form-control" required>
                            <?php while ($rql = mysqli_fetch_assoc($dql)) {  ?>
                                <option value="<?php echo $rql['Code'] ?>"><?php echo $rql['Name'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Alasan Penarikan</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <select name="alasan_penarikan" id="alasan_penarikan" class="select_database form-control" required>
                            <?php while ($rql = mysqli_fetch_assoc($dql_reason)) {  ?>
                                <option value="<?php echo $rql['id_rejection'] ?>"><?php echo $rql['ket'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

              
              
               <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class="form-control-label">Pilih BFI</label>
                    </div>
                    <div class="col-12 col-md-9">
                        
                        

                        <select name="bficode" class="form-control freezerplacement" id="freezerplacement">
                            <option value="">- pilih bfi -</option>
                        </select>

                        

                        </select>
                    </div>
                </div>
              
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Foto Form Tarik Freezer Yang Sudah di TTD</label>
                    </div>
                    <div class="col-6 col-md-3">
                            <input type="file" name="file" id="file" class="form-control">
                    </div>
                    <div class="col-6 col-md-3">
                            <input type="button" id="upload" value="Upload File" class="btn btn-success">
                            <input type="text" name="foto" id="foto" class="form-control" style="display: none">
                            <progress id="progressBar" value="0" max="100" style="width:100%; display: none;"></progress>
                            <h3 id="status"></h3>
                            <p id="loaded_n_total"></p>
                    </div>

                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Foto Freezer Sudah Kosong Siap ditarik</label>
                    </div>
                    <div class="col-6 col-md-3">
                            <input type="file" name="file_siap_ditarik" id="file_siap_ditarik" class="form-control">
                    </div>
                    <div class="col-6 col-md-3">
                            <input type="button" id="upload_siap_ditarik" value="Upload File" class="btn btn-success">
                            <input type="text" name="foto_siap_ditarik" id="foto_siap_ditarik" class="form-control" style="display: none">
                            <progress id="progressBar_siap_ditarik" value="0" max="100" style="width:100%; display: none;"></progress>
                            <h3 id="status_siap_ditarik"></h3>
                            <p id="loaded_n_total_siap_ditarik"></p>
                    </div>

                </div> 

                <!-- <div id="areavendor"></div>  -->

                <div class="col-12 col-md-9">
                    <input type="text" name="namavendor_pilih" id="namavendor_pilih" class="form-control" hidden>
                </div>

                <!-- <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Area</label>
                    </div>
                    <div class="col-12 col-md-9">
                            <select name="areavendor" id="namavendor" class="select_database form-control" required>
                                <option>Pilih Area</option>

                                <?php 
                                    $query_user = "SELECT area FROM user WHERE role = 'VENDOR' GROUP BY area";
                                    $data_user = mysqli_query($koneksi,$query_user);
                                    while ($result_user = mysqli_fetch_assoc($data_user)) {
                                        
                                 ?>
                                    <option value="<?php echo $result_user['area'] ?>"><?php echo $result_user['area'] ?></option>

                                <?php } ?>

                            </select>
                        
                    </div>
                </div> -->


                </div>
                
                </div>
                <div class="card-footer">
                    <button type="submit" class="au-btn au-btn--green">
                        <i class="fa fa-floppy-o"></i> Save
                    </button>
                    <button type="reset" class="au-btn au-btn--red">
                        <i class="fa fa-ban"></i> Reset
                    </button>
                </div>

            </form>
        </div>
    </div>
    </div>
</div>
