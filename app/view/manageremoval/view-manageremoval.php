<?php 
    if (isset($_GET['status'])) {
        $status = $_GET['status'];

        if ($status == 1) {
            echo '<div class="alert alert-success" role="alert">
                Proces pembatalan freezer placement berhasil
            </div>';
        }elseif ($status == 0) {
            echo '<div class="alert alert-danger" role="alert">
                Terjadi Kesalahan! tidak dapat memproses request 
            </div>';
        }elseif ($status == 11) {
            echo '<div class="alert alert-success" role="alert">
                Proses pembatalan removal berhasil
            </div>';
        }elseif ($status == 10) {
            echo '<div class="alert alert-success" role="alert">
                Terjadi Kesalahan! tidak dapat melakukan pembatalan 
            </div>';
        }else{
            echo '<div class="alert alert-warning" role="alert">
                Terjadi kesalahan koneksi internet, silkahkan ulangi kembali
            </div>';
        }
        
    }

    $area = $_SESSION['area'];
    $vendor = $_SESSION['nama'];
    $acc = $vendor.'-'.$area;
    $role = $_SESSION['role'];
 ?>


<hr>
<div class="row">
    <div class="col-md-12">
        <!-- <h3 class="title-5 m-b-35">Installments</h3> -->

        <div class="card">
        
        <div class="card-body card-block">

            <div classs="card-title">
                <h3 class="text-center title-2">Removal</h3>
            </div>
            <hr>
            <form method="post" enctype="multipart/form-data" class="form-horizontal">
                
                <div class="form-group">
                    <div class="col col-md-3">
                        <!-- <label for="text-input" class=" form-control-label">Placement Data</label> -->
                    </div>
                    <div>
                        <select name="customer" class="select_database form-control" required>
                        <option disabled selected>Removal History</option>

                        <?php 

                            $acc = $vendor.''.$area;
                            $acc2 = '1{PT'.$vendor.'}-'.$area;
                            $acc3 = '1 {PT'.$vendor.'}-'.$area;
                            $acc4 = '1{PT '.$vendor.'}-'.$area;
                            $acc5 = '1 {PT '.$vendor.'}-'.$area;
                            $acc6 = 'PT'.$vendor.''.$area;
                            $acc7 = 'PT '.$vendor.'-'.$area;
                            $acc8 = 'PT'.$vendor.' '.$area;
                            $acc9 = $vendor.'-'.$area;

                            if ($role == 'VENDOR') {
                                $query = "SELECT * FROM focus_freezer_master, freezer_request WHERE `freezer_request`.`action` ='remove' AND `freezer_request`.`status` = 'success' AND `freezer_request`.`id_freezer` = `focus_freezer_master`.`id_freezer` AND (`freezer_request`.`supplier` LIKE '%$acc%' or `freezer_request`.`supplier` LIKE '%$acc2%' or `freezer_request`.`supplier` LIKE '%$acc3%'or `freezer_request`.`supplier` LIKE '%$acc4%'or `freezer_request`.`supplier` LIKE '%$acc5%'or `freezer_request`.`supplier` LIKE '%$acc6%'or `freezer_request`.`supplier` LIKE '%$acc7%'or `freezer_request`.`supplier` LIKE '%$acc8%' or `freezer_request`.`supplier` LIKE '%$acc9%' ) ORDER BY id_request";
                            }else{
                                $query = "SELECT * FROM focus_freezer_master, freezer_request WHERE `freezer_request`.`action` ='remove' AND `freezer_request`.`status` = 'success' AND `freezer_request`.`id_freezer` = `focus_freezer_master`.`id_freezer` ORDER BY id_request";
                            }

                                $data = mysqli_query($koneksi,$query);
                                while ($result = mysqli_fetch_assoc($data)) {

                                    //cari data customernya

                                
                         ?>
                                <option value="<?php echo $result['id_freezer'] ?>"><?php echo $result['IDBelfoodd']." - ".$result['Outletname']." | Removed by ".$result['Supplier']." (".$result['TanggalPenarikan'].')' ?></option>

                        <?php } ?>
                    </select>
                    <small class="form-text text-muted">Pilih history yang ingin di batalkan. Jika tidak ada data maka belum ada removal yg dilakukan</small>
                    </div>
                </div>

                
                <button id="payment-button" type="submit"  name="submit" class="btn btn-lg btn-info btn-block">
                    <i class="fa fa-lock fa-lg"></i>&nbsp;
                    <span id="payment-button-amount">Check</span>
                    <span id="payment-button-sending" style="display:none;">Sending…</span>
                </button>

            </form>

            
              <?php 
                if (isset($_POST['submit'])) {
                //     include ('config/config.php');
                //     $customer = $_POST['customer'];
                //     // cari data orderan
                //     $q_order = "SELECT * FROM aus_order WHERE id_customer = '$customer' ORDER BY date_order DESC";
                //     $d_order = mysqli_query($koneksi,$q_order);
                //     while ($q_result = mysqli_fetch_assoc($d_order)) {
            ?>
                <br>
                <hr>
                <h4>Removal History</h4>
                <br>
        <div class="table-responsive table-data" style="height: 100%">
            <table id="tableok" class="mdl-data-table" style="width:100%">
                <thead>
                    <tr>
                        <td>Action</td>
                        <td>No</td>
                        <td>Request</td>
                        <td>BFI Code</td>
                        <td>Outlet Name</td>
                        <td>Distributor Name</td>
                        <td>Alamat</td>
                        <td>Kelurahan</td>
                        <td>Kecamatan</td>
                        <td>Kab/Kota</td>
                        <td>Provinsi</td>
                        <td>Placement Date</td>
                        <td>Removal/Switch Date</td>
                        <td>Removal/Switch Plan Date</td>
                        <td>Removal/Switch Status</td>
                        <td>Type (Old)</td>
                        <td>Barcode (Old)</td>
                        <td>Type (New)</td>
                        <td>Barcode (New)</td>
                        <td>Supplier Name</td>
                        <td>BarcodeFA</td>
                        <td>Removal/Switch Reason</td>
                        
                    </tr>
                </thead>

                <tbody>

                     <?php 
                        $no = 1;
                        include ('config/config.php');

                        $id_freezer = $_POST['customer'];
                        $query = "SELECT * FROM freezer_request WHERE id_freezer = '$id_freezer'";
                        $data = mysqli_query($koneksi,$query);

                        while ($r = mysqli_fetch_assoc($data)) {



                     ?>
                    <tr>
                        <?php 
                            $id_freezer = $r['id_freezer'];
                            $q = "SELECT * FROM focus_freezer_master WHERE id_freezer = '$id_freezer'";
                            $d = mysqli_query($koneksi,$q);
                            $result = mysqli_fetch_assoc($d);
                         ?>     
                        
                        <td>
                            <form action="model/manageremoval/cancel-removal.php" method="POST">
                                <input type="hidden" value="<?php echo $result['id_freezer'] ?>" name="id_freezer">

                                <button type="submit" name="cancel" class="btn btn-warning btn-sm">Cancel</button>
                            </form>
                        </td>
                        <td><?php echo $no ?></td>
                        <td>
                            <?php echo strtoupper($r['action']) ?>
                        </td>
                        <td>
                            <?php echo $result['IDBelfoodd'] ?>
                        </td>
                        <td>
                            <?php echo strtoupper($result['Outletname']) ?>
                        </td>
                        <td>
                            <?php echo $result['DistributorName'] ?>
                        </td>
                        <td>
                            <?php echo $result['Alamat'] ?>
                        </td>
                        <td>
                            <?php echo $result['Kelurahan'] ?>
                        </td>
                        <td>
                            <?php echo $result['Kecamatan'] ?>
                        </td>

                        <td>
                            <?php echo $result['KabKota'] ?>
                        </td>
                        <td>
                            <?php echo $result['provinsi'] ?>
                        </td>
                        
                        <td>
                            <?php echo $result['TanggalPasang'] ?>
                        </td>
                        <td>
                            <?php echo $result['TanggalPenarikan'] ?>
                        </td>
                        <td>
                            <?php echo $r['tgl_plan'] ?>
                        </td>
                        <td>
                            <?php 
                                $id_freezer = $result['id_freezer'];
                                $cari = "SELECT action, status from freezer_request WHERE id_freezer = '$id_freezer'";
                                $qc = mysqli_query($koneksi,$cari);
                                $rc = mysqli_fetch_assoc($qc);

                                $status = $rc['status'];
                                $action = $rc['action'];

                                if ($status == 'success') {
                                    if ($action == 'remove') {
                                        echo "Removal Successful";
                                    }elseif($action == 'switch'){
                                        echo "Switch Successful";
                                    }else{
                                        //show nothing
                                    }
                                }elseif ($status == 'FAILED') {
                                    if ($action == 'remove') {
                                        echo "Removal Failed";
                                    }elseif($action == 'switch'){
                                        echo "Switch Failed";
                                    }else{
                                        //show nothing
                                    }
                                }else{
                                    echo 'Waiting for status update';
                                    
                                }
                             ?>
                        </td>
                        <td>
                            <?php echo $result['Type'] ?>
                        </td>
                        <td>
                            <?php echo $result['ID_Number'] ?>
                        </td>
                        <td>
                            <?php echo $result['Type_Switching'] ?>
                        </td>
                        <td>
                            <?php echo $result['ID_Switching'] ?>
                        </td>
                        <td>
                            <?php echo $result['Supplier'] ?>
                        </td>
                        <td>
                            <?php echo $result['BarcodeFANumber'] ?>
                        </td>
                        <td>
                            <?php echo $result['Keterangan'] ?>
                        </td>
                    </tr>

                     <?php $no++; } ?>
                </tbody>
            </table>
            
            <hr>
            <!-- <small class="form-text text-muted">Cancel removal akan mengembalikan freezer ke lokasi customer</small> -->
        </div>

            <?php 
                    };
                //}
             ?>
        </div>
    </div>
    </div>
</div>