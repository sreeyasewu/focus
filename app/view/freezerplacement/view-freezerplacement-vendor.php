<!-- jQuery Library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<?php 
include('config/config.php');
//include('config/mds.php');

$role = $_SESSION['role'];
$placedby = $_SESSION['nama'];
$area = trim($_SESSION['area']);
$vendor = $_SESSION['nama'];
$acc = $vendor.'-'.$area;

    if (isset($_GET['status'])) {
        $status = $_GET['status'];

        if ($status == 1) {
            echo '<div class="alert alert-success" role="alert">
                Data updated
            </div>';
        }elseif ($status == 0) {
            echo '<div class="alert alert-danger" role="alert">
                Terjadi Kesalahan! silahkan coba kembali
            </div>';
        }elseif ($status == 9) {
            echo '<div class="alert alert-danger" role="alert">
                Mohon pilih vendor!
            </div>';
        }elseif ($status == 99) {
            echo '<div class="alert alert-danger" role="alert">
                Mohon pilih status kepemilikan!
            </div>';
        }elseif ($status == 990) {
            echo '<div class="alert alert-danger" role="alert">
                Mohon pilih area vendor!
            </div>';
        }else{
            echo '<div class="alert alert-warning" role="alert">
                Terjadi kesalahan koneksi internet, silkahkan ulangi kembali
            </div>';
        }
    }

    if (isset($_GET['editstatus'])) {
        $status = $_GET['editstatus'];

        if ($status == 1) {
            echo '<div class="alert alert-success" role="alert">
                Data user berhasil di perbaruhi
            </div>';
        }elseif ($status == 0) {
            echo '<div class="alert alert-danger" role="alert">
                Terjadi Kesalahan! tidak dapat mengubah data user
            </div>';
        }else{
            echo '<div class="alert alert-warning" role="alert">
                Terjadi kesalahan koneksi internet, silkahkan ulangi kembali
            </div>';
        }
    }
 ?>

 
     <div class="row">
<div class="col-md-12">
        
        
        <h4>
            View Freezer Placement

        </h4>
        <br>
        <h4>New Placement Request</h4>
        <hr>
		<form action="" method="POST">
		<?php
			if (isset($_POST['customReqDate'])) {
				$tanggalMulai1 = mysqli_escape_string($koneksi,$_POST['tanggal_mulai']);
				$tanggalSelesai1 = mysqli_escape_string($koneksi,$_POST['tanggal_akhir']);
				$tanggalMulai1Edit = date('Y-m-d', strtotime($tanggalMulai1));
				$tanggalSelesai1Edit = date('Y-m-d', strtotime($tanggalSelesai1));
				//$tanggalMulai1= date('Y-m-d', $tanggalMulai1Edit);
				//$tanggalSelesai1= date('Y-m-d', $tanggalSelesai1Edit);
				
				$dateclause = " and approved_date >='$tanggalMulai1Edit' and approved_date <= '$tanggalSelesai1Edit' ";
			} else {
				$tanggalMulai1 = "Mulai";
				$tanggalSelesai1 = "Selesai";
				$dateclause = "";
				
			}
			
		?>
		<div class="row form-group">
			<div class="col-sm-3 col-md-2">
				<label for="text-input" class=" form-control-label"><b>Assign Date</b></label>
			</div>
			<div class="col-sm-3 col-md-2">
				<input type="text" id="tanggal_mulai" name="tanggal_mulai" class="form-control" placeholder="Awal" required>
			</div>
			<div class="col-sm-3 col-md-2">
				<input type="text" id="tanggal_akhir" name="tanggal_akhir" class="form-control" placeholder="Akhir" required>
				
			</div>
			<button type="submit" name="customReqDate" value="custom" class="btn btn-sm btn-primary">
				Search
			</button>
		</div>

		</form>
		<hr>
        <div class="table-responsive table-data" style="height: auto">
            <table id="freezerplacementvendor" class="mdl-data-table" style="width:100%">
                <thead>
                    <tr>
                        <td>Validate</td>
                        <td>Action</td>
                        <td>Assign Date</td>
                        <td>Plan Placement Date</td>
                        <!--<td>No</td>-->
                        <td>Sequence Number</td>
                        <td>Tipe Customer</td>
                        <td>BFI Code</td>
                        <td>Nama Outlet</td>
                        <td>Nama Distributor</td>
                        <td>Tanggal Survey</td>
                        <td>Freezer Owner</td>
                        <td>Nama Vendor</td>
                        <td>Tipe Freezer</td>
                        <td>Email to Vendor Date</td>
                        <td>Placement Date</td>
                        <td>Lead Time</td>
                        <td>Status</td>
                        <td>Tanggal Validasi</td>
                        <td>Freezer Code</td>
                        <td>BatchPO</td>
                        <td>Nomor Mesin</td>
                        <td>Nama Supir</td>
                        <td>Tanggal Pengiriman</td>
                        <td>Realisasi Tipe Freezer</td>
                        <td>Nama PIC</td>
                        <td>No Telp PIC</td>
                        <!--
                        <td>Foto - Deal Form + KTP</td>
                        <td>Foto - Outlet Tampak Depan</td>
                        <td>Kondisi Fisik Outlet</td>
                        -->
                        <!-- <td>Patokan</td> -->
                        <!--
                        <td>Longitude</td>
                        <td>Latitude</td>
                        <td>Vendor Longitude</td>
                        <td>Vendor Latitude</td>
                        <td>Alamat</td>
                        <td>Provinsi</td>
                        <td>Kabupaten / Kota</td>
                        <td>Kecamatan</td>
                        <td>Kelurahan</td>
                        <td>Kode Pos</td>
                        <td>Jam Buka</td>
                        <td>Jam Tutup</td>
                        <td>Updated By</td>
                        -->
                        <!-- <td>Action</td> -->
                    </tr>
                </thead>
               </table>
            </div>
        <!-- END DATA TABLE -->

        <br>

<br>
<h4>Placement</h4>
<hr>
<form action="" method="POST">
<?php
	if (isset($_POST['customPlaceDate'])) {
		$tanggalMulai = mysqli_escape_string($koneksi,$_POST['tanggal_mulai']);
		$tanggalSelesai = mysqli_escape_string($koneksi,$_POST['tanggal_akhir']);
		$tanggalMulaiEdit = date('Y-m-d', strtotime($tanggalMulai));
		$tanggalSelesaiEdit = date('Y-m-d', strtotime($tanggalSelesai));
		//$tanggalMulai= date('Y-m-d', $tanggalMulaiEdit);
		//$tanggalSelesai= date('Y-m-d', $tanggalSelesaiEdit);
		
		$dateclause = " and placement_date >='$tanggalMulaiEdit' and placement_date <= '$tanggalSelesaiEdit' ";
	} else {
		$dateclause = "";
	}
	
?>
<div class="row form-group">
	<div class="col-sm-3 col-md-2">
		<label for="text-input" class=" form-control-label"><b>Placed Sys Date</b></label>
	</div>
	<div class="col-sm-3 col-md-2">
		<input type="text" id="tanggal_mulai" name="tanggal_mulai" class="form-control start_date" placeholder="Awal" required>
	</div>
	<div class="col-sm-3 col-md-2">
		<input type="text" id="tanggal_akhir" name="tanggal_akhir" class="form-control end_date" placeholder="Akhir" required>
		
	</div>
	<button type="submit" name="customPlaceDate" value="custom" class="btn btn-sm btn-primary">
		Search
	</button>
</div>

</form>
<hr>
<div class="row">
    <div class="col-md-12">
        <div class="table-responsive table-data" style="height: auto">
            <table id="tableok" class="mdl-data-table" style="width:100%">
                <thead>
                    <tr>
                        <td>No</td>
                        <td>Assign Date</td>
                        <td>BFI Code</td>
                        <td>Nama Outlet</td>
                        <td>Nama Distributor</td>
                        <td>Tanggal Survey</td>
                        <td>Freezer Owner</td>
                        <td>Nama Vendor</td>
                        <td>Tipe Freezer</td>
                        <td>Email to Vendor Date</td>
                        <td>Placement Date</td>
                        <td>Lead Time</td>
                        <td>Status</td>
                        <td>Tanggal Validasi</td>
                        <td>Plan Placement Date</td>
                        <td>Freezer Code</td>
                        <td>Sequence Number</td>
                        <td>BatchPO</td>
                        <td>Nomor Mesin</td>
                        <td>Nama Supir</td>
                        <td>Tanggal Pengiriman</td>
                        <td>Realisasi Tipe Freezer</td>
                        <td>Nama PIC</td>
                        <td>No Telp PIC</td>
                        <td>Foto - Deal Form + KTP</td>
                        <td>Foto - Outlet Tampak Depan</td>
                        <td>Kondisi Fisik Outlet</td>
                        <!-- <td>Patokan</td> -->
                        <td>Longitude</td>
                        <td>Latitude</td>
                        <td>Vendor Longitude</td>
                        <td>Vendor Latitude</td>
                        <td>Alamat</td>
                        <td>Provinsi</td>
                        <td>Kabupaten / Kota</td>
                        <td>Kecamatan</td>
                        <td>Kelurahan</td>
                        <td>Kode Pos</td>
                        <td>Jam Buka</td>
                        <td>Jam Tutup</td>
                        <td>Updated By</td>
                        <!-- <td>Action</td> -->
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $no = 1;
                        
						if ($area == "") {
							$query = "SELECT * FROM focus_freezer_placement WHERE approved_status = 'APPROVED' and placement_date != '0000-00-00' ".$dateclause." AND nama_vendor LIKE '$acc%' order by IDNumber desc limit 0, 5";
    						} else {
    							$query = "SELECT * FROM focus_freezer_placement WHERE approved_status = 'APPROVED' and placement_date != '0000-00-00' ".$dateclause." AND nama_vendor like '$acc%' order by IDNumber desc limit 0, 3";
    						}
                            $data = mysqli_query($koneksi,$query);

                            while ($result = mysqli_fetch_assoc($data)) {
                     ?>
                    <tr>

                        <!-- cari datangnya dari mana -->
                        <?php 
                                if ($result['id_distributor'] == 0) {
                                    //maka itu artinya bukan dari survey NOO
                                    $id_freezer = $result['id_freezer'];

                                    if ($result['cust_type'] == "Indirect") {
					//karna dia secondary, cari lagi datanya dari tabel secondary mds
                                        $cp2 = "SELECT * FROM MDS_CustomerSecondary WHERE Code = '$id_freezer'";
                                        $dcp2 = mysqli_query($koneksi, $cp2);
                                        $results2 = mysqli_fetch_assoc($dcp2);

                                        $custid = $results2['Code'];
                                        $custname = $results2['Name'];
                                        $distname = $results2['DistName'];

                                        $tipe = "Indirect";
                                        $pic = $results2['PIC'];
                                        $phone = $results2['Phone'];
                                        $latitude = $results2['Latitude'];
                                        $longitude = $results2['Longitude'];
                                        $address = $results2['Alamat'];
                                        $city = $results2['KabKota'];
                                        $province = $results2['Propinsi'];
                                        $kecamatan = $results2['Kecamatan'];
                                        $kelurahan = $results2['Kelurahan'];
                                        $kodepos = $results2['KodePos'];
                                        $tipe_freezer = $result['realisasi_tipe_freezer'];
                                    } else { // primary customer
                                        $cp = "SELECT * FROM MDS_CustomerPrimary WHERE Code = '$id_freezer'";
                                        $dcp = mysqli_query($koneksi, $cp);
                                        $result2 = mysqli_fetch_assoc($dcp);

                                        $tgl_survey = "Tidak tersedia";
                                        $tipe = $result2['MarketId_Name'];
                                        $photo = '';
                                        $photo3 = '';

                                        $custid = $result2['Code'];
                                        $custname = $result2['Name'];
                                        $distname = $result2['System_Distributor_Name'];
                                        $tipe = "Direct";
                                        $pic = $result2['PIC'];
                                        $phone = $result2['Phone'];
                                        $latitude = $result2['Latitude'];
                                        $longitude = $result2['Longitude'];
                                        $address = $result2['Street'];
                                        $city = $result2['City'];
                                        $province = $result2['SalesDistrictName'];
                                        $kecamatan = $result2['SPVillageName'];
                                        $kelurahan = '';
                                        $kodepos = $result2['ZipCode'];
                                        $tipe_freezer = $result['realisasi_tipe_freezer'];
                                    }
                                }else{

                                    $id_distributor = $result['id_distributor'];
                                    $cs = "SELECT * FROM distributor WHERE iddistributor = '$id_distributor'";
                                    $dcs = mysqli_query($koneksi,$cs);
                                    $result3 = mysqli_fetch_assoc($dcs); 

                                    $custid = $result3['custid'];
                                    $custname = $result3['custname'];
                                    $distname = $result3['distname'];
                                    $tgl_survey = $result3['inputdatetime'];
                                    $tipe = 'Indirect';
                                    $photo3 = "https://appict.appsbelfoods.com/".$result3['photo3']."";
                                    $photo = "https://appict.appsbelfoods.com/".$result3['photo'].""; 
                                    $pic = $result3['contactname'];
                                    $phone = $result3['telephone'];
                                    $latitude = $result3['latitude'];
                                    $longitude = $result3['longitude'];
                                    // $tipe = "SECONDARY";
                                    $address = $result3['address'];
                                    $city = $result3['city'];
                                    $province = $result3['province'];
                                    $kecamatan = $result3['kecamatan'];
                                    $kelurahan = $result3['kelurahan'];
                                    $kodepos = $result3['kodepos'];
                                    $tipe_freezer = $result3['pilihanfreezeroutlet'];
                                }
                             ?>

                
                        <td><?php echo $no ?></td>
                        <td><?php echo $result['approved_date'] ?></td>
                        <td>
                            <?php echo $custid ?>
                        </td>
                        <td><?php echo $custname ?></td>
                        <td>
                            <?php echo $distname ?>
                        </td>
                        <td>
                            <?php echo $tgl_survey ?>
                        </td>
                        <td>
                            <?php echo $result['freezer_owner'] ?>
                        </td>
                        <td>
                            <?php echo $result['nama_vendor'] ?>
                        </td>
                        <td>
                            <?php 
                                // if ($result['coming_from'] == 'ADDITIONAL') {
                                //     echo $result['realisasi_tipe_freezer'];
                                // }else{
                                //     echo $result['pilihanfreezeroutlet'];
                                // }

                                echo $tipe_freezer;
                             ?>
                        </td>
                        <td>
                            <?php echo $result['approved_date'] ?>
                        </td>
                        <td>
                            <?php 

                                if ($result['placement_date'] == '0000-00-00') {
                                    echo 'Tanggal belum tersedia';
                                }else{
                                    echo $result['placement_date'];
                                } 
                                ?>
                        </td>
                        <td>
                            <?php
                                echo $result['lead_time'].' Hari';
                            ?>
                        </td>
                        <td>
                            <?php 
                                $status = $result['approved_status'];
                                $tanggal_penempatan = $result['placement_date'];

                                if ($status == 'APPROVED' AND $tanggal_penempatan !== '0000-00-00') {
                                    echo '<span class="role marketing">SUCCESS</span>';
                                }elseif ($status == 'APPROVED' AND $keterangan == 'VALIDATED'){
                                    echo '<span class="role marketing">VALIDATED BY VENDOR</span>';
                                }elseif ($status == 'APPROVED' AND $keterangan == '') {
                                     echo '<span class="role admin">WAITING FOR VENDOR VALIDATION</span>';
                                }else{
                                    echo '<span class="role admin">PROCESSING BY VENDOR</span>';
                                }
                             ?>
                        </td>
                        <td>
                            <?php echo $result['approved_date'] ?>
                        </td>
                        <td>
                            <?php echo $result['plan_placement_date'] ?>
                        </td>
                        <td>
                            <?php echo $result['freezercode'] ?>
                        </td>
                        <td>
                            <?php echo $result['IDNumber'] ?>
                        </td>
                        <td>
                            <?php echo $result['batch_po'] ?>
                        </td>
                        <td>
                            <?php echo $result['placement_notes'] ?>
                        </td>
                        <td>
                            <?php echo $result['nama_supir'] ?>
                        </td>
                        <td>
                            <?php 

                               if ($result['tanggal_pengiriman'] == '0000-00-00') {
                                    echo "Tanggal belum tersedia";
                               }else{
                                    echo $result['tanggal_pengiriman']; 
                               }
                            ?>
                        </td>
                        <td>
                            <?php echo $result['realisasi_tipe_freezer'] ?>
                        </td>
                        <td>
                            <?php echo $pic ?>
                        </td>
                        <td>
                            <?php echo $phone ?>
                        </td>
                        <td>

                            <?php 
                            
                                if ($photo3 == '') {
                                    # code...
                                }else{
                                    echo '<a href="'.$photo.'">View</a>';
                                     // echo '<a href="'.$photo.'"><img src="'.$photo.'" width="50px" height="50px">'.$photo.'</a>';
                                }
                           

                            ?>
                            
                        </td>
                        <td>
                            <?php 
                                if ($photo == '') {
                                    # code...
                                }else{
                                    echo '<a href="'.$photo.'">View</a>';
                                     // echo '<a href="'.$photo.'"><img src="'.$photo.'" width="50px" height="50px">'.$photo.'</a>';
                                }

                            ?>
                        </td>
                        <td>
                            <?php echo $result3['kondisifisik'] ?>
                        </td>
                        <td>
                            <?php echo $longitude ?>
                        </td>
                        <td>
                            <?php echo $latitude ?>
                        </td>
                        <td>
                            <?php echo $result['vendor_longitude'] ?>
                        </td>
                        <td>
                            <?php echo $result['vendor_latitude'] ?>
                        </td>
                        <td>
                            <?php echo $address ?>
                        </td>
                        <td>
                            <?php echo $province ?>
                        </td>
                        <td>
                            <?php echo $city ?>
                        </td>
                        <td>
                            <?php echo $kecamatan ?>
                        </td>
                        <td>
                            <?php echo $kelurahan ?>
                        </td>
                        <td>
                            <?php echo $kodepos ?>
                        </td>
                        <td>
                            <?php echo $result3['timeopen'] ?>
                        </td>
                        <td>
                            <?php echo $result3['timeclosed'] ?>
                        </td>
                        <td>
                            <?php echo $result['placed_by'] ?>
                        </td>
                    </tr>

                     <?php $no++; } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


</div>


<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>

     <form action="model/datareview/action.php" method="post">
                                <input type="hidden" name="id_distributor" value="">

                                <div class="row">
                                    <div class="col-md-12">
                                        <small>Ganti Status</small>
                                    <br>
                                        <select name="action" class="form-control" style="width:200px" required>
                                            <option value="REJECT">REJECT</option>
                                            <option value="POSTPONE">POSTPONE</option>
                                        </select>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-sm btn-success">
                                            Update
                                        </button>
                                   
                                        <a onclick="batal()" class="btn btn-sm btn-dark batal-action">Batal</a>
                                    </div>
                                 </div>
                                    
                                </div>
                            </form>

    
  </div>

</div>

<script type="text/javascript">
$(document).ready(function(){
   $('#freezerplacementvendor').DataTable({
      'processing': true,
      'serverSide': true,
      'serverMethod': 'post',
      'ajax': {
          'url':'API/freezerplacementvendor.php'
      },
      'columns': [
         { data: 'validate' },
         { data: 'action' },
         { data: 'approved_date' },
         { data: 'plan_placement_date' },
         { data: 'IDNumber' },
         { data: 'tipe_customer' },
         { data: 'custid' },
         { data: 'custname' },
         { data: 'distname' },
         { data: 'tgl_survey' },
         { data: 'freezer_owner' },
         { data: 'nama_vendor' },
         { data: 'tipe_freezer' },
         { data: 'approved_date' },
         { data: 'placement_date' },
         { data: 'lead_time' },
         { data: 'span_status' },
         { data: 'approved_date' },
         { data: 'freezercode' },
         { data: 'batch_po' },
         { data: 'placement_notes' },
         { data: 'nama_supir' },
         { data: 'tanggal_pengiriman' },
         { data: 'realisasi_tipe_freezer' },
         { data: 'pic' },
         { data: 'phone'}
      ]
   });
});
</script>

