<?php 
    if (isset($_GET['id'])) {
        $id = $_GET['id'];
        $editsql = "SELECT * FROM user WHERE id_user = '$id'";
        $dataedit = mysqli_query($koneksi,$editsql);
        $edit = mysqli_fetch_assoc($dataedit); 
    }
 ?>
<div class="row">
    <div class="col-md-12">
        <h3 class="title-5 m-b-35">edit Users</h3>

        <div class="card">
        <div class="card-header">
            <strong>User Form</strong> 
        </div>
        <div class="card-body card-block">

            <form action="model/users/update.php" method="post" enctype="multipart/form-data" class="form-horizontal">

                <input type="hidden"  name="id" value="<?php echo $id ?>">
                
                
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">NIK</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="text" id="text-input" name="txt_nik" class="form-control" value="<?php echo $edit['nik'] ?>" required>
                        
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Nama</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="text" id="txt_nama" name="txt_nama" placeholder="Nama Lengkap" class="form-control" value="<?php echo $edit['nama'] ?>" required>
                    </div>
                </div>
                
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="email-input" class=" form-control-label">Email</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="text" id="email-input" name="txt_email" placeholder="email@email.com" class="form-control" value="<?php echo $edit['email'] ?>"required>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="email-input" class=" form-control-label">Area</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="text" id="" name="area" placeholder="Jakarta Pusat" class="form-control" value="<?php echo $edit['area'] ?>" required>
                    </div>
                </div>
                
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="password-input" class=" form-control-label">Password Baru</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="password" id="password-input" name="txt_password" placeholder="Password" class="form-control" >
                    </div>
                </div>
                
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="select" class=" form-control-label">Roles</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <select name="role" id="select" class="form-control" required>
                            <option value="<?php echo $edit['role'] ?>" selected><?php echo strtolower($edit['role']) ?></option>
                            <option>Pilih role pengguna</option>
                            <option value="ADMIN">Admin</option>
                            <option value="FREEZER MANAGEMENT">Freezer Management</option>
                            <option value="ROUTE TO MARKET">Route to Market</option>
                            <option value="VENDOR">Vendor</option>
                            <option value="COMMAND CENTER">Command Center</option>
                        </select>
                    </div>
                </div>


                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="no_hp" class=" form-control-label">No. HP</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="text" id="txt_hp" name="txt_hp" placeholder="No. HP" class="form-control" value="<?php echo $edit['no_hp'] ?>" required>
                    </div>
                </div>


                
                </div>
                <div class="card-footer">
                    <button type="submit" class="au-btn au-btn--green">
                        <i class="fa fa-floppy-o"></i> Save
                    </button>
                </div>



            </form>
        </div>
    </div>
    </div>
</div>