<?php 
    if (isset($_GET['status'])) {
        $status = $_GET['status'];

        if ($status == 1) {
            echo '<div class="alert alert-success" role="alert">
                Berhasil memperbaruhi data
            </div>';
        }elseif ($status == 0) {
            echo '<div class="alert alert-danger" role="alert">
                Gagal memperbaruhi data
            </div>';
        }else{
            echo '<div class="alert alert-warning" role="alert">
                Terjadi kesalahan koneksi internet, silkahkan ulangi kembali
            </div>';
        }
        
    }

    include ('config/config.php');
    $id = $_GET['id'];

    /* Get data survey */
    $q = "SELECT * FROM distributor WHERE iddistributor = '$id'";
    $d = mysqli_query($koneksi,$q);
    $r = mysqli_fetch_assoc($d);

 ?>




<div class="row">
    <div class="col-md-12">
        <h3 class="title-5 m-b-35">Edit data</h3>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
        <div class="card-header">
            <strong>Edit</strong>
        </div>
        <div class="card-body card-block">
    <form id="form1" name="form1" method="post" action="model/datareview/edit-data-review-vendor.php" enctype="multipart/form-data" class="form-horizontal">
        
        <div class="row form-group">
            <div class="col col-md-3">
                <label for="iddistributor" class=" form-control-label">ID Distributor</label>
            </div>
            <div class="col-12 col-md-9">
                <input type="text" name="iddistributordisabled" id="iddistributor" value="<?php echo $r['iddistributor'] ?>" class="form-control" disabled/>
                <input type="hidden" name="iddistributor" value="<?php echo $r['iddistributor']; ?>">
            </div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3">
                <label for="distnumber">Distnumber</label>
            </div>
            <div class="col-12 col-md-9">
                <input type="text" name="distnumberdisabled" id="distnumber" value="<?php echo $r['distnumber'] ?>" class="form-control" disabled/>
                <input type="hidden" name="distnumber" value="<?php echo $r['distnumber']; ?>">
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="distname">Distname</label>
            </div>
            <div class="col-12 col-md-9">
                <input type="text" name="distname" id="distname" value="<?php echo $r['distname'] ?>" class="form-control" />
            </div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3">
                <label for="custid">Custid</label>
            </div>
            <div class="col-12 col-md-9">
                <input type="text" name="custiddisabled" id="custid" value="<?php echo $r['custid'] ?>" class="form-control" disabled/>
                <input type="hidden" name="custid" value="<?php echo $r['custid']; ?>">
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="custname">Custname</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="custname" id="custname" value="<?php echo $r['custname'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="sales">Sales</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="sales" id="sales" value="<?php echo $r['sales'] ?>" class="form-control" />
            </div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3">
                <label for="status">Status</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="status" id="status" value="<?php echo $r['status'] ?>" class="form-control" />
            </div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3">
                <label for="segment">Segment</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="segment" id="segment" value="<?php echo $r['segment'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="channel">Channel</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="channel" id="channel" value="<?php echo $r['channel'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="subchannel">Subchannel</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="subchannel" id="subchannel" value="<?php echo $r['subchannel'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="segmentnumber">Segment Number</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="segmentnumber" id="segmentnumber" value="<?php echo $r['segmentnumber'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="subchannelnumber">Subchannel Number</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="subchannelnumber" id="subchannelnumber" value="<?php echo $r['subchannelnumber'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="account">Account</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="account" id="account" value="<?php echo $r['account'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="address">Address</label>
            </div>
            <div class="col-md-9">
                <textarea name="address" id="address" cols="45" rows="5" class="form-control"><?php echo $r['address'] ?></textarea>
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="city">City</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="city" id="city" value="<?php echo $r['city'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="kodepos">Kodepos</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="kodepos" id="kodepos" value="<?php echo $r['kodepos'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="kelurahan">Kelurahan</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="kelurahan" id="kelurahan" value="<?php echo $r['kelurahan'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="kecamatan">Kecamatan</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="kecamatan" id="kecamatan" value="<?php echo $r['kecamatan'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="province">Province</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="province" id="province" value="<?php echo $r['province'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="telephone">Telephone</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="telephone" id="telephone" value="<?php echo $r['telephone'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="noktp">No KTP</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="noktp" id="noktp" value="<?php echo $r['noktp'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="contactname">Contact Name</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="contactname" id="contactname" value="<?php echo $r['contactname'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="phone">Phone</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="phone" id="phone" value="<?php echo $r['phone'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="celular">Celular</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="celular" id="celular" value="<?php echo $r['celular'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="fax">Fax</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="fax" id="fax" value="<?php echo $r['fax'] ?>" class="form-control" />
            </div>
        </div>

         <div class="row form-group">
            <div class="col col-md-3">
                <label for="email">Email</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="email" id="email" value="<?php echo $r['email'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="npwp">NPWP</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="npwp" id="npwp" value="<?php echo $r['npwp'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="npwpname">NPWP Name</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="npwpname" id="npwpname" value="<?php echo $r['npwpname'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="npwpaddress">NPWP Address</label>
            </div>
            <div class="col-md-9">
                <textarea name="npwpaddress" id="npwpaddress" cols="45" rows="5" class="form-control"><?php echo $r['npwpaddress'] ?></textarea>
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="deliveryname">Delivery Name</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="deliveryname" id="deliveryname" value="<?php echo $r['deliveryname'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="invoiceaccount">Invoice Account</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="invoiceaccount" id="invoiceaccount" value="<?php echo $r['invoiceaccount'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="creditlimit">Creditlimit</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="creditlimit" id="creditlimit" value="<?php echo $r['creditlimit'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="top">Top</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="top" id="top" valiue="<?php echo $r['top'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="regional">Regional</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="regional" id="regional" value="<?php echo $r['regional'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="latitude">Latitude</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="latitude" id="latitude" value="<?php echo $r['latitude'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="longitude">Longitude</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="longitude" id="longitude" value="<?php echo $r['longitude'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="photo">Photo</label>
            </div>
            <div class="col-md-9">
                <img src="<?php echo 'https://appict.appsbelfoods.com/'.$r['photo'] ?>" alt="" width="100px" height="100px">
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="id_sales">Id Sales</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="id_sales" id="id_sales" value="<?php echo $r['id_sales'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="inputdatetime">Input Date Time</label>
            </div>
            <div class="col-md-9">
                <input type="datetime-local" name="inputdatetime" id="inputdatetime" value="<?php $t = $r['inputdatetime']; echo date('Y-m-d\TH:i:s', strtotime($t));?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="nameowner">Name Owner</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="nameowner" id="nameowner" value="<?php echo $r['nameowner'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="jumlahfrezzer">Jumlah Frezzer</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="jumlahfrezzer" id="jumlahfrezzer" value="<?php echo $r['jumlahfrezzer'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="nobarcode">No Barcode</label>
            </div>
            <div class="col-md-9">
                <textarea name="nobarcode" id="nobarcode" cols="45" rows="5" class="form-control"><?php echo $r['nobarcode'] ?></textarea>
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="photo2">Photo2</label>
            </div>
            <div class="col-md-9">
                <img src="<?php echo 'https://appict.appsbelfoods.com/'.$r['photo2'] ?>" alt="" width="100px" height="100px">
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="photo3">Photo3</label>
            </div>
            <div class="col-md-9">
                <img src="<?php echo 'https://appict.appsbelfoods.com/'.$r['photo3'] ?>" alt="photo1" width="100px" height="100px">
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="custrefid">Custrefid</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="custrefid" id="custrefid" value="<?php echo $r['custrefid'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="timestamp">Timestamp</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="timestamp" id="timestamp" value="<?php echo $r['timestamp'] ?>" class="form-control"/>
                <!-- <input type="hidden" name="timestamp" value="<?php echo $r['timestamp']; ?>"> -->

            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="coveragearea">Coveragearea</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="coveragearea" id="coveragearea" value="<?php echo $r['coveragearea'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="patokan">Patokan</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="patokan" id="patokan" value="<?php echo $r['patokan'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="lokasi1">Lokasi1</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="lokasi1" id="lokasi1" value="<?php echo $r['lokasi1'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="lokasi2">Lokasi2</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="lokasi2" id="lokasi2" value="<?php echo $r['lokasi2'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="sebutkanlingkungan">Sebutkan Lingkungan</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="sebutkanlingkungan" id="sebutkanlingkungan" value="<?php echo $r['sebutkanlingkungan'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="timeopen">Time Open</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="timeopen" id="timeopen" value="<?php echo $r['timeopen'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="timeclosed">Time Closed</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="timeclosed" id="timeclosed" value="<?php echo $r['timeclosed'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="luasbangunan">Luas Bangunan</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="luasbangunan" id="luasbangunan" value="<?php echo $r['luasbangunan'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="freezerdalamtoko">Freezer Dalam Toko</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="freezerdalamtoko" id="freezerdalamtoko" value="<?php echo $r['freezerdalamtoko'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="merekfreezer">Merek Freezer</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="merekfreezer" id="merekfreezer" value="<?php echo $r['merekfreezer'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="frozenFood">Frozen Food</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="frozenFood" id="frozenFood" value="<?php echo $r['frozenFood'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="merekfrozenfood">Merekfrozenfood</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="merekfrozenfood" id="merekfrozenfood" value="<?php echo $r['merekfrozenfood'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="productutama">Product Utama</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="productutama" id="productutama" value="<?php echo $r['productutama'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="productutamakode">Product Utama Kode</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="productutamakode" id="productutamakode" value="<?php echo $r['productutamakode'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="estimasipengunjung">Estimasi Pengunjung</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="estimasipengunjung" id="estimasipengunjung" value="<?php echo $r['estimasipengunjung'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="pendapatan">Pendapatan</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="pendapatan" id="pendapatan" value="<?php echo $r['pendapatan'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="mayoritas">Mayoritas</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="mayoritas" id="mayoritas" value="<?php echo $r['mayoritas'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="elektricity">Elektricity</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="elektricity" id="elektricity" value="<?php echo $r['elektricity'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="kondisifisik">Kondisi Fisik</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="kondisifisik" id="kondisifisik" value="<?php echo $r['kondisifisik'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="resikofreezerhilang">Resiko Freezer Hilang</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="resikofreezerhilang" id="resikofreezerhilang" value="<?php echo $r['resikofreezerhilang'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="tersediatempat">Tersedia Tempat</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="tersediatempat" id="tersediatempat" value="<?php echo $r['tersediatempat'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="statustoko">Status Toko</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="statustoko" id="statustoko" value="<?php echo $r['statustoko'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="pilihanfreezeroutlet">Pilihan Freezer Outlet</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="pilihanfreezeroutlet" id="pilihanfreezeroutlet" value="<?php echo $r['pilihanfreezeroutlet'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="tempatfreezer">Tempat Freezer</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="tempatfreezer" id="tempatfreezer" value="<?php echo $r['tempatfreezer'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="penilaianlokasi">Penilaian Lokasi</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="penilaianlokasi" id="penilaianlokasi" value="<?php echo $r['penilaianlokasi'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="serviceadditional">Service Additional</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="serviceadditional" id="serviceadditional" value="<?php echo $r['serviceadditional'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3"> 
                <label for="targetbulanan">Target Bulanan</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="targetbulanan" id="targetbulanan" value="<?php echo $r['targetbulanan'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="notesissue">Notes Issue</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="notesissue" id="notesissue" value="<?php echo $r['notesissue'] ?>" class="form-control" />
            </div>
        </div>

         <div class="row form-group">
            <div class="col col-md-3">
                <label for="alasanissue">Alasan Issue</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="alasanissue" id="alasanissue" value="<?php echo $r['alasanissue'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="followupaction">Follow Up Action</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="followupaction" id="followupaction" value="<?php echo $r['followupaction'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="alasantidaksesuai">Alasantidaksesuai</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="alasantidaksesuai" id="alasantidaksesuai" value="<?php echo $r['alasantidaksesuai'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="followupdate">Follow Update</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="followupdate" id="followupdate" value="<?php echo $r['followupdate'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="paketfreezer">Paket Freezer</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="paketfreezer" id="paketfreezer" value="<?php echo $r['paketfreezer'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="surveydate">Survey Date</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="surveydate" id="surveydate" value="<?php echo $r['surveydate'] ?>" class="form-control" />
            </div>
        </div>

        <div class="card-footer" style="text-align: right">
            <button type="submit" class="au-btn au-btn--green">
                <i class="fa fa-floppy-o"></i> Update
            </button>
            <button type="reset" class="au-btn au-btn--red">
                <i class="fa fa-ban"></i> Reset
            </button>
        </div>
    </form>
</div>
</div>
</div>


<!-- end row -->
