<?php 
    if (isset($_GET['status'])) {
        $status = $_GET['status'];

        if ($status == 1) {
            echo '<div class="alert alert-success" role="alert">
                Proces pembatalan freezer placement berhasil
            </div>';
        }elseif ($status == 0) {
            echo '<div class="alert alert-danger" role="alert">
                Terjadi Kesalahan! tidak dapat memproses request 
            </div>';
        }elseif ($status == 11) {
            echo '<div class="alert alert-success" role="alert">
                Proses pembatalan removal berhasil
            </div>';
        }elseif ($status == 10) {
            echo '<div class="alert alert-success" role="alert">
                Terjadi Kesalahan! tidak dapat melakukan pembatalan 
            </div>';
        }else{
            echo '<div class="alert alert-warning" role="alert">
                Terjadi kesalahan koneksi internet, silkahkan ulangi kembali
            </div>';
        }
        
    }
    $area = $_SESSION['area'];
    $vendor = $_SESSION['nama'];
    $acc = $vendor.'-'.$area;
    $role = $_SESSION['role'];
 ?>


<hr>
<div class="row">
    <div class="col-md-12">
        <!-- <h3 class="title-5 m-b-35">Installments</h3> -->

        <div class="card">
        
        <div class="card-body card-block">

            <div classs="card-title">
                <h3 class="text-center title-2">Placement</h3>
            </div>
            <hr>
            <form method="post" enctype="multipart/form-data" class="form-horizontal">
                
                <div class="form-group">
                    <div class="col col-md-3">
                        <!-- <label for="text-input" class=" form-control-label">Placement Data</label> -->
                    </div>
                    <div>
                        <select name="idnumber" id="idnumber" class="select_database form-control" required>
                        <option disabled selected>Cari Placement</option>

                        <?php 

                                if ($role == 'VENDOR') {
                                    $query = "SELECT * FROM focus_freezer_placement WHERE approved_status = 'APPROVED' and placement_date != '0000-00-00' and nama_vendor = '$acc' ORDER BY id_placement DESC";
                                }else{
                                    $query = "SELECT * FROM focus_freezer_placement WHERE approved_status = 'APPROVED' and placement_date != '0000-00-00' ORDER BY id_placement DESC";
                                }
                            
                                $data = mysqli_query($koneksi,$query);
                                while ($result = mysqli_fetch_assoc($data)) {

                                    //cari data customernya


                                
                         ?>
                                <?php
				/* 
                                if ($result['id_distributor'] == 0) {
                                    //maka itu artinya primary
                                    $id_freezer = $result['id_freezer'];
                                    $cp = "SELECT * FROM focus_freezer_master WHERE id_freezer = '$id_freezer'";
                                    $dcp = mysqli_query($koneksi,$cp);
                                    $result2 = mysqli_fetch_assoc($dcp);

                                    $custid = $result2['IDBelfoodd'];
                                    $custname = $result2['Outletname'];
                                    $distname = $result2['DistributorName'];
                                    $tgl_survey = "Tidak tersedia";
                                    $tipe = $result2['Customer_Type'];
                                    $address = $result2['Alamat'];
                                    $province = $result2['Propinsi'];
                                    $city =$result2['KabKota'];
                                    $kecamatan =$result2['Kecamatan'];
                                    $kelurahan =$result2['Kelurahan'];
                                    $kodepos = $result2['KodePos'];
                                    $photo = '';
                                    $photo3 = '';
                                    $pic = $result2['PIC'];
                                    $phone = $result['phone'];
                                    $latitude = $result2['Latitude'];
                                    $longitude = $result2['Longitude'];

                                    // echo $custname;
                                }else{

                                    $id_distributor = $result['id_distributor'];
                                    $cs = "SELECT * FROM distributor WHERE iddistributor = '$id_distributor'";
                                    $dcs = mysqli_query($koneksi,$cs);
                                    $result3 = mysqli_fetch_assoc($dcs); 

                                    $custid = $result3['custid'];
                                    $custname = $result3['custname'];
                                    $distname = $result3['distname'];
                                    $tgl_survey = $result3['inputdatetime'];
                                    $tipe = 'SECONDARY';
                                    $address = $result3['address'];
                                    $province = $result3['province'];
                                    $city =$result3['city'];
                                    $kecamatan =$result3['kecamatan'];
                                    $kelurahan =$result3['kelurahan'];
                                    $kodepos = $result3['kodepos'];
                                    $photo3 = "http://appict.appsbelfoods.com/".$result3['photo3']."";
                                    $photo = "http://appict.appsbelfoods.com/".$result3['photo'].""; 
                                    $pic = $result2['contactname'];
                                    $phone = $result['telephone'];
                                    $latitude = $result2['latitude'];
                                    $longitude = $result2['longitude'];
                                    $tipe = "SECONDARY";
                                }
                             	*/
                                    $freezercode = $result['freezercode'];
                                    $cp = "SELECT * FROM focus_freezer_master WHERE freezercode = '$freezercode'";
                                    $dcp = mysqli_query($koneksi,$cp);
                                    $result2 = mysqli_fetch_assoc($dcp);

                                    $custid = $result2['IDBelfoodd'];
                                    $custname = $result2['Outletname'];
                                    $distname = $result2['DistributorName'];
                                    $idnumber = $result2['ID_Number'];
 
				
				?>

                                <option value="<?php echo $idnumber ?>"><?php echo $custid." - ".$custname." | Placed by ".$result['placed_by']." (".$result['placement_date'].')' ?></option>

                        <?php } ?>
                    </select>
                    <small class="form-text text-muted">Pilih placement yang ingin di batalkan. Jika tidak ada data maka belum ada placement yg dilakukan</small>
                    </div>
                </div>

                
                <button id="payment-button" type="submit"  name="submit" class="btn btn-lg btn-info btn-block">
                    <i class="fa fa-lock fa-lg"></i>&nbsp;
                    <span id="payment-button-amount">Check</span>
                    <span id="payment-button-sending" style="display:none;">Sending…</span>
                </button>

            </form>

            
              <?php 
                if (isset($_POST['submit'])) {

            ?>
                <br>
                <hr>
                <h4>Detail Placement</h4>
                <br>
        <div class="table-responsive table-data" style="height: 100%">
            <table id="tableok2" class="mdl-data-table" style="width:100%">
                <thead>
                    <tr>
                        <td>Action</td>
                        <td>No</td>
                        <td>BFI Code</td>
                        <td>Nama Outlet</td>
                        <td>Nama Distributor</td>
                        
                        <td>Frrezer Owner</td>
                        <td>Nama Vendor</td>
                        <td>Tipe Freezer</td>
                        <td>Email to Vendor Date</td>
                        <td>Placement Date</td>
                        <td>Lead Time</td>
                        <td>Status</td>
                        <td>Tanggal Validasi</td>
                        <td>Plan Placement Date</td>
                        <td>Freezer Code</td>
                        <td>IDNumber</td>
                        <td>BatchPO</td>
                        <td>Nomor Mesin</td>
                        <td>Nama Supir</td>
                        <td>Tanggal Pengiriman</td>
                        <td>Realisasi Tipe Freezer</td>
                        <td>Nama PIC</td>
                        <td>No Telp PIC</td>
                        <td>Foto - Deal Form + KTP</td>
                        <td>Foto - Outlet Tampak Depan</td>
                        <td>Kondisi Fisik Outlet</td>
                        <!-- <td>Patokan</td> -->
                        <td>Longitude</td>
                        <td>Latitude</td>
                        <td>Vendor Longitude</td>
                        <td>Vendor Latitude</td>
                        <td>Alamat</td>
                        <td>Provinsi</td>
                        <td>Kabupaten / Kota</td>
                        <td>Kecamatan</td>
                        <td>Kelurahan</td>
                        <td>Kode Pos</td>
                        <td>Jam Buka</td>
                        <td>Jam Tutup</td>
                        <td>Updated By</td>
                        <!-- <td>Penyesuaian</td> -->
                        
                    </tr>
                </thead>
                <tbody>

                     <?php 
                        $no = 1;
                        $id_number = $_POST['idnumber'];
                        $query2 = "SELECT * FROM focus_freezer_master WHERE ID_Number = '$id_number'";
                        $data2 = mysqli_query($koneksi,$query2);

                        while ($result2 = mysqli_fetch_assoc($data2)) {

                            $fz = $result2['FreezerCode'];

                            $ww = "SELECT * FROM focus_freezer_placement WHERE freezercode = '$fz'";
                            $dw = mysqli_query($koneksi,$ww);
                            $rw = mysqli_fetch_assoc($dw);
                     ?>
                    <tr>
                        <?php 
                                if ($result['id_distributor'] == 0) {
                                    //maka itu artinya primary

                                    $custid = $result2['IDBelfoodd'];
                                    $custname = $result2['Outletname'];
                                    $distname = $result2['DistributorName'];
                                    $tgl_survey = "Tidak tersedia";
                                    $tipe = $result2['Customer_Type'];
                                    $address = $result2['Alamat'];
                                    $province = $result2['Propinsi'];
                                    $city =$result2['KabKota'];
                                    $kecamatan =$result2['Kecamatan'];
                                    $kelurahan =$result2['Kelurahan'];
                                    $kodepos = $result2['KodePos'];
                                    $photo = '';
                                    $photo3 = '';
                                    $pic = $result2['PIC'];
                                    $phone = $result2['phone'];
                                    $latitude = $result2['Latitude'];
                                    $longitude = $result2['Longitude'];

                                    // echo $custname;
                                }else{

                                    $id_distributor = $rw['id_distributor'];
                                    $cs = "SELECT * FROM distributor WHERE iddistributor = '$id_distributor'";
                                    $dcs = mysqli_query($koneksi,$cs);
                                    $result3 = mysqli_fetch_assoc($dcs); 

                                    $custid = $result3['custid'];
                                    $custname = $result3['custname'];
                                    $distname = $result3['distname'];
                                    $tgl_survey = $result3['inputdatetime'];
                                    $tipe = 'SECONDARY';
                                    $address = $result3['address'];
                                    $province = $result3['province'];
                                    $city =$result3['city'];
                                    $kecamatan =$result3['kecamatan'];
                                    $kelurahan =$result3['kelurahan'];
                                    $kodepos = $result3['kodepos'];
                                    $photo3 = "http://appict.appsbelfoods.com/".$result3['photo3']."";
                                    $photo = "http://appict.appsbelfoods.com/".$result3['photo'].""; 
                                    $pic = $result3['contactname'];
                                    $phone = $result3['telephone'];
                                    $latitude = $result3['latitude'];
                                    $longitude = $result3['longitude'];
                                }
                             ?>
                        <td>
                            <form action="model/manageplacement/cancel-placement.php" method="POST">


                                <input type="hidden" value="<?php echo $rw['id_placement'] ?>" name="id_placement">
                                <input type="hidden" value="<?php echo $rw['freezercode'] ?>" name="freezercode">
                                <input type="hidden" value="<?php echo $result2['id_freezer'] ?>" name="id_freezer">

                                <button type="submit" name="cancel" class="btn btn-warning btn-sm">Cancel</button>
                            </form>
                        </td>
                        <td><?php echo $no ?></td>
                        <td>
                            <?php echo $result2['IDBelfoodd'] ?>
                        </td>
                        <td><?php echo $result2['Outletname'] ?></td>
                        <td>
                            <?php echo $result2['DistributorName'] ?>
                        </td>
                        
                        <td>
                            <?php echo $rw['freezer_owner'] ?>
                        </td>
                        <td>
                            <?php echo $rw['nama_vendor'] ?>
                        </td>
                        <td>
                            <?php echo $result3['pilihanfreezeroutlet'] ?>
                        </td>
                        <td>
                            <?php echo $rw['approved_date'] ?>
                        </td>
                        <td>
                            <?php 

                                if ($rw['placement_date'] == '0000-00-00') {
                                    echo 'Tanggal belum tersedia';
                                }else{
                                    echo $rw['placement_date'];
                                } 
                                ?>
                        </td>
                        <td>
                            <?php
                                echo $rw['lead_time'].' Hari';
                            ?>
                        </td>
                        <td>
                            <?php 
                                $status = $w['approved_status'];
                                $keterangan = $rw['keterangan'];
                                $tanggal_penempatan = $rw['placement_date'];

                                if ($status == 'APPROVED' AND $tanggal_penempatan !== '0000-00-00') {
                                    echo '<span class="role marketing">SUCCESS</span>';
                                }elseif ($status == 'APPROVED' AND $keterangan == 'VALIDATED'){
                                    echo '<span class="role marketing">VALIDATED BY VENDOR</span>';
                                }elseif ($status == 'APPROVED' AND $keterangan == '') {
                                     echo '<span class="role admin">WAITING FOR VENDOR VALIDATION</span>';
                                }else{
                                    echo '<span class="role admin">PROCESSING BY VENDOR</span>';
                                }
                             ?>
                        </td>
                        <td>
                            <?php echo $rw['approved_date'] ?>
                        </td>
                        <td>
                            <?php echo $rw['plan_placement_date'] ?>
                        </td>
                        <td>
                            <?php echo $rw['freezercode'] ?>
                        </td>
                        <td>
                            <?php echo $rw['IDNumber'] ?>
                        </td>
                        <td>
                            <?php echo $rw['batch_po'] ?>
                        </td>
                        <td>
                            <?php echo $rw['placement_notes'] ?>
                        </td>
                        <td>
                            <?php echo $rw['nama_supir'] ?>
                        </td>
                        <td>
                            <?php 

                               if ($rw['tanggal_pengiriman'] == '0000-00-00') {
                                    echo "Tanggal belum tersedia";
                               }else{
                                    echo $rw['tanggal_pengiriman']; 
                               }
                            ?>
                        </td>
                        <td>
                            <?php echo $rw['realisasi_tipe_freezer'] ?>
                        </td>
                        <td>
                            <?php echo $pic ?>
                        </td>
                        <td>
                            <?php echo $phone ?>
                        </td>
                        <td>

                            <?php 
                            
                                if ($photo3 == '') {
                                    # code...
                                }else{
                                     echo '<a href="'.$photo.'"><img src="'.$photo.'" width="50px" height="50px">'.$photo.'</a>';
                                }
                           

                            ?>
                            
                        </td>
                        <td>
                            <?php 
                                if ($photo == '') {
                                    # code...
                                }else{
                                     echo '<a href="'.$photo.'"><img src="'.$photo.'" width="50px" height="50px">'.$photo.'</a>';
                                }

                            ?>
                        </td>
                        <td>
                            <?php echo $result3['kondisifisik'] ?>
                        </td>
                        <td>
                            <?php echo $longitude ?>
                        </td>
                        <td>
                            <?php echo $latitude ?>
                        </td>
                        <td>
                            <?php echo $result['vendor_longitude'] ?>
                        </td>
                        <td>
                            <?php echo $result['vendor_latitude'] ?>
                        </td>
                        <td>
                            <?php echo $address ?>
                        </td>
                        <td>
                            <?php echo $province ?>
                        </td>
                        <td>
                            <?php echo $city ?>
                        </td>
                        <td>
                            <?php echo $kecamatan ?>
                        </td>
                        <td>
                            <?php echo $kelurahan ?>
                        </td>
                        <td>
                            <?php echo $kodepos ?>
                        </td>
                        <td>
                            <?php echo $result3['timeopen'] ?>
                        </td>
                        <td>
                            <?php echo $result3['timeclosed'] ?>
                        </td>
                        <td>
                            <?php echo $rw['placed_by'] ?>
                        </td>
                    </tr>

                     <?php $no++; } ?>
                </tbody>
            </table>
            
            <hr>
            <small class="form-text text-muted">Cancel placement akan mengembalikan freezer ke vendor bersangkutan</small>
        </div>

            <?php 
                    };
                //}
             ?>
        </div>
    </div>
    </div>
</div>
