<?php 
include ('config/config.php');
require_once "library/vendor/autoload.php";

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Csv;

/*
$client = new \Redis();
$client->connect('127.0.0.1', 6379);
$pool = new \Cache\Adapter\Redis\RedisCachePool($client);
$simpleCache = new \Cache\Bridge\SimpleCache\SimpleCacheBridge($pool);

\PhpOffice\PhpSpreadsheet\Settings::setCache($simpleCache);
*/

$spreadsheet = new Spreadsheet();
//$Excel_writer = new Xlsx($spreadsheet);
$Excel_writer = new Csv($spreadsheet);

$spreadsheet->setActiveSheetIndex(0);
$activeSheet = $spreadsheet->getActiveSheet();

$activeSheet->setCellValue('A1', 'Assign Date');
$activeSheet->setCellValue('B1', 'BFI Code');
$activeSheet->setCellValue('C1', 'Nama Outlet');
$activeSheet->setCellValue('D1', 'Nama Distributor');
$activeSheet->setCellValue('E1', 'Tanggal Survey');
$activeSheet->setCellValue('F1', 'Freezer Owner');
$activeSheet->setCellValue('G1', 'Nama Vendor');
$activeSheet->setCellValue('H1', 'Tipe Freezer');
$activeSheet->setCellValue('I1', 'Email to Vendor Date');
$activeSheet->setCellValue('J1', 'Placement System Date');
$activeSheet->setCellValue('K1', 'Lead Time');
$activeSheet->setCellValue('L1', 'Status');
$activeSheet->setCellValue('M1', 'Tanggal Validasi');
$activeSheet->setCellValue('N1', 'Plan Placement Date');
$activeSheet->setCellValue('O1', 'Freezer Code');
$activeSheet->setCellValue('P1', 'ID Number');
$activeSheet->setCellValue('Q1', 'Batch PO');
$activeSheet->setCellValue('R1', 'Nomor Mesin');
$activeSheet->setCellValue('S1', 'Nama Supir');
$activeSheet->setCellValue('T1', 'Tanggal Pengiriman');
$activeSheet->setCellValue('U1', 'Realisasi Tipe Freezer');
$activeSheet->setCellValue('V1', 'Nama PIC');
$activeSheet->setCellValue('W1', 'No Telp PIC');
$activeSheet->setCellValue('X1', 'Foto - Deal Form + KTP');
$activeSheet->setCellValue('Y1', 'Foto - Outlet Tampak Depan');
$activeSheet->setCellValue('Z1', 'Kondisi Fisik Outlet');
$activeSheet->setCellValue('AA1', 'Longitutde');
$activeSheet->setCellValue('AB1', 'Latitude');
$activeSheet->setCellValue('AC1', 'Vendor Longitutde');
$activeSheet->setCellValue('AD1', 'Vendor Latitude');
$activeSheet->setCellValue('AE1', 'Alamat');
$activeSheet->setCellValue('AF1', 'Provinsi');
$activeSheet->setCellValue('AG1', 'Kabupaten / Kota');
$activeSheet->setCellValue('AH1', 'Kecamatan');
$activeSheet->setCellValue('AI1', 'Kelurahan');
$activeSheet->setCellValue('AJ1', 'Kode Pos');
$activeSheet->setCellValue('AK1', 'Jam Buka');
$activeSheet->setCellValue('AL1', 'Jam Tutup');
$activeSheet->setCellValue('AM1', 'Updated BY');
$activeSheet->setCellValue('AN1', 'Notes');



$query = "SELECT * FROM focus_freezer_placement p left join distributor d on p.id_distributor = d.iddistributor where approved_status in ('APPROVED', 'REJECT BY FREEZER TEAM', 'REJECT BY VENDOR')";
$data = mysqli_query($koneksi,$query);
$i = 2;
$placement_date = "";

while ($result = mysqli_fetch_assoc($data)) {

    if ($result['id_distributor'] == 0) {
                                    //maka itu artinya bukan dari survey NOO
                                    $id_freezer = $result['id_freezer'];

                                    if ($result['cust_type'] == "Indirect") {
                    //karna dia secondary, cari lagi datanya dari tabel secondary mds
                                        $cp2 = "SELECT * FROM MDS_CustomerSecondary WHERE Code = '$id_freezer'";
                                        $dcp2 = mysqli_query( $koneksi, $cp2 );
                                        $results2 = mysqli_fetch_assoc($dcp2);

                                        $custid = $results2['Code'];
                                        $custname = $results2['Name'];
                                        $distname = $results2['DistName'];

                                        $tipe = "Indirect";
                                        $pic = $results2['PIC'];
                                        $phone = $results2['Phone'];
                                        $latitude = $results2['Latitude'];
                                        $longitude = $results2['Longitude'];
                                        $address = $results2['Alamat'];
                                        $city = $results2['KabKota'];
                                        $province = $results2['Propinsi'];
                                        $kecamatan = $results2['Kecamatan'];
                                        $kelurahan = $results2['Kelurahan'];
                                        $kodepos = $results2['KodePos'];
                                        $tipe_freezer = $result['realisasi_tipe_freezer'];
                                    } else { // primary customer
                                        $cp = "SELECT * FROM MDS_CustomerPrimary WHERE Code = '$id_freezer'";
                                        $dcp = mysqli_query( $koneksi, $cp );
                                        $result2 = mysqli_fetch_assoc($dcp);

                                        $tgl_survey = "Tidak tersedia";
                                        $tipe = $result2['MarketId_Name'];
                                        $photo = '';
                                        $photo3 = '';

                                        $custid = $result2['Code'];
                                        $custname = $result2['Name'];
                                        $distname = $result2['System_Distributor_Name'];
                                        $tipe = "Direct";
                                        $pic = "";
                                        $phone = $result2['Phone'];
                                        $latitude = $result2['Latitude'];
                                        $longitude = $result2['Longitude'];
                                        $address = $result2['Street'];
                                        $city = $result2['City'];
                                        $province = $result2['SalesDistrictName'];
                                        $kecamatan = $result2['SPVillageName'];
                                        $kelurahan = '';
                                        $kodepos = $result2['ZipCode'];
                                        $tipe_freezer = $result['realisasi_tipe_freezer'];
                                    }
                                }else{

                                    $id_distributor = $result['id_distributor'];

                                    $custid = $result['custid'];
                                    $custname = $result['custname'];
                                    $distname = $result['distname'];
                                    $tgl_survey = $result['inputdatetime'];
                                    $photo3 = "https://appict.appsbelfoods.com/".$result['photo3']."";
                                    $photo = "https://appict.appsbelfoods.com/".$result['photo'].""; 
                                    $pic = $result['contactname'];
                                    $phone = $result['telephone'];
                                    $latitude = $result['latitude'];
                                    $longitude = $result['longitude'];
                                    $tipe = "SECONDARY";
                                    $address = $result['address'];
                                    $city = $result['city'];
                                    $province = $result['province'];
                                    $kecamatan = $result['kecamatan'];
                                    $kelurahan = $result['kelurahan'];
                                    $kodepos = $result['kodepos'];
                                    $tipe_freezer = $result['pilihanfreezeroutlet'];
                                }
    if ($result['placement_date'] == '0000-00-00') {
        $placement_date = 'Tanggal belum tersedia';
    }else{
        $placement_date = $result['placement_date'];
    } 

    $status = $result['approved_status'];
    $keterangan = $result['keterangan'];
    $tanggal_penempatan = $result['placement_date'];

    if ($status == 'APPROVED' AND $tanggal_penempatan <> '0000-00-00') {
            $status_display = 'SUCCESS';
        }else if ($status == 'APPROVED' AND $keterangan == 'VALIDATED'){
            $status_display = 'VALIDATED BY VENDOR';
        }else if ($status == 'APPROVED' AND $keterangan == '') {
            $status_display = 'WAITING FOR VENDOR VALIDATION';
        }else if ($status == 'APPROVED') {
            $status_display = 'PROCESSING BY VENDOR';
        }else if (substr($status, 0, 6) == "REJECT"){
            $status_display = 'REJECT';
        }else {
            $status_display = $status;
    }

    if ($result['tanggal_pengiriman'] == '0000-00-00') {
            $tanggal_pengiriman = "Tanggal belum tersedia";
        }else{
            $tanggal_pengiriman = $result['tanggal_pengiriman']; 
    }

    if ($photo3 == '') {
            $link_foto3 = '';
        }else{
            $link_foto3 = $photo;
                                    
    }

    if ($photo == '') {
            $link_foto = '';
        }else{
            $link_foto = $photo;
                                    
    }

        $activeSheet->setCellValue('A'.$i , $result['approved_date']);
        $activeSheet->setCellValue('B'.$i , $custid);
        $activeSheet->setCellValue('C'.$i , $custname );
        $activeSheet->setCellValue('D'.$i , $distname);
        $activeSheet->setCellValue('E'.$i , $tgl_survey);
        $activeSheet->setCellValue('F'.$i , $result['freezer_owner']);
        $activeSheet->setCellValue('G'.$i , $result['nama_vendor']);
        $activeSheet->setCellValue('H'.$i , $tipe_freezer);
        $activeSheet->setCellValue('I'.$i , $result['approved_date']);
        $activeSheet->setCellValue('J'.$i , $placement_date);
        $activeSheet->setCellValue('K'.$i , $result['lead_time']);
        $activeSheet->setCellValue('L'.$i , $status_display);
        $activeSheet->setCellValue('M'.$i , $result['approved_date']);
        $activeSheet->setCellValue('N'.$i , $result['plan_placement_date']);
        $activeSheet->setCellValue('O'.$i , $result['freezercode']);
        $activeSheet->setCellValue('P'.$i , $result['IDNumber']);
        $activeSheet->setCellValue('Q'.$i , $result['batch_po']);
        $activeSheet->setCellValue('R'.$i , $result['placement_notes']);
        $activeSheet->setCellValue('S'.$i , $result['nama_supir']);
        $activeSheet->setCellValue('T'.$i , $tanggal_pengiriman);
        $activeSheet->setCellValue('U'.$i , $result['realisasi_tipe_freezer']);
        $activeSheet->setCellValue('V'.$i , $pic);
        $activeSheet->setCellValue('W'.$i , $phone);
        $activeSheet->setCellValue('X'.$i , $link_foto3);
        $activeSheet->setCellValue('Y'.$i , $link_foto);
        $activeSheet->setCellValue('Z'.$i , $result['kondisifisik']);
        $activeSheet->setCellValue('AA'.$i , $longitude);
        $activeSheet->setCellValue('AB'.$i , $latitude);
        $activeSheet->setCellValue('AC'.$i , $result['vendor_longitude']);
        $activeSheet->setCellValue('AD'.$i , $result['vendor_latitude']);
        $activeSheet->setCellValue('AE'.$i , $address);
        $activeSheet->setCellValue('AF'.$i , $province);
        $activeSheet->setCellValue('AG'.$i , $city);
        $activeSheet->setCellValue('AH'.$i , $kecamatan);
        $activeSheet->setCellValue('AI'.$i , $kelurahan);
        $activeSheet->setCellValue('AJ'.$i , $kodepos);
        $activeSheet->setCellValue('AK'.$i , $result['timeopen']);
        $activeSheet->setCellValue('AL'.$i , $result['timeclosed']);
        $activeSheet->setCellValue('AM'.$i , $result['placed_by']);
        $activeSheet->setCellValue('AN'.$i , $result['keterangan']);

        $i++;

}

$activeSheet->getColumnDimension('A')->setAutoSize(true);
$activeSheet->getColumnDimension('B')->setAutoSize(true);
$activeSheet->getColumnDimension('C')->setAutoSize(true);
$activeSheet->getColumnDimension('D')->setAutoSize(true);
$activeSheet->getColumnDimension('E')->setAutoSize(true);
$activeSheet->getColumnDimension('F')->setAutoSize(true);
$activeSheet->getColumnDimension('G')->setAutoSize(true);
$activeSheet->getColumnDimension('H')->setAutoSize(true);
$activeSheet->getColumnDimension('I')->setAutoSize(true);
$activeSheet->getColumnDimension('J')->setAutoSize(true);
$activeSheet->getColumnDimension('K')->setAutoSize(true);
$activeSheet->getColumnDimension('L')->setAutoSize(true);
$activeSheet->getColumnDimension('M')->setAutoSize(true);
$activeSheet->getColumnDimension('N')->setAutoSize(true);
$activeSheet->getColumnDimension('O')->setAutoSize(true);
$activeSheet->getColumnDimension('P')->setAutoSize(true);
$activeSheet->getColumnDimension('Q')->setAutoSize(true);
$activeSheet->getColumnDimension('R')->setAutoSize(true);
$activeSheet->getColumnDimension('S')->setAutoSize(true);
$activeSheet->getColumnDimension('T')->setAutoSize(true);
$activeSheet->getColumnDimension('U')->setAutoSize(true);
$activeSheet->getColumnDimension('V')->setAutoSize(true);
$activeSheet->getColumnDimension('W')->setAutoSize(true);
$activeSheet->getColumnDimension('X')->setAutoSize(true);
$activeSheet->getColumnDimension('Y')->setAutoSize(true);
$activeSheet->getColumnDimension('Z')->setAutoSize(true);
$activeSheet->getColumnDimension('AA')->setAutoSize(true);
$activeSheet->getColumnDimension('AB')->setAutoSize(true);
$activeSheet->getColumnDimension('AC')->setAutoSize(true);
$activeSheet->getColumnDimension('AD')->setAutoSize(true);
$activeSheet->getColumnDimension('AE')->setAutoSize(true);
$activeSheet->getColumnDimension('AF')->setAutoSize(true);
$activeSheet->getColumnDimension('AG')->setAutoSize(true);
$activeSheet->getColumnDimension('AH')->setAutoSize(true);
$activeSheet->getColumnDimension('AI')->setAutoSize(true);
$activeSheet->getColumnDimension('AJ')->setAutoSize(true);
$activeSheet->getColumnDimension('AK')->setAutoSize(true);
$activeSheet->getColumnDimension('AL')->setAutoSize(true);
$activeSheet->getColumnDimension('AM')->setAutoSize(true);
$activeSheet->getColumnDimension('AN')->setAutoSize(true);
$activeSheet->getStyle("A1:AN1")->getFont()->setBold(true);

$date = date("d-m-Y");
$filename = 'exportAllPlacement'.$date.'.csv';

$Excel_writer->setDelimiter(';');
$Excel_writer->setEnclosure('"');
$Excel_writer->setLineEnding("\r\n");
$Excel_writer->setSheetIndex(0);

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='. $filename);
header('Cache-Control: max-age=0');
$Excel_writer->save('php://output');
exit();
?>


