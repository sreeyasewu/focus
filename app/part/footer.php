 </div>

    </div>

    <!-- Jquery JS-->
    
    <script src="assets/vendor/jquery-3.2.1.min.js"></script>
    <script src="assets/vendor/jquery-datatables.js"></script>
    <script type="text/javascript" src="assets/js/data_tables/data_tables.js"></script>

    <script src="assets/js/data_tables/material/datatable_material.js"></script>
    <script src="assets/js/data_tables/dataTables.buttons.min.js"></script>
    <script src="assets/js/data_tables/buttons.print.min.js"></script>
    <script src="assets/js/data_tables/buttons.html5.min.js"></script>
    <script src="assets/js/data_tables/vfs_fonts.js"></script>
    <script src="assets/js/data_tables/pdfmake.min.js"></script>
    <script src="assets/js/data_tables/jszip.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="assets/vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="assets/vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="assets/vendor/slick/slick.min.js">
    </script>
    <script src="assets/vendor/wow/wow.min.js"></script>
    <script src="assets/vendor/animsition/animsition.min.js"></script>
    <script src="assets/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="assets/vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="assets/vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="assets/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="assets/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="assets/vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="assets/vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="assets/js/main.js"></script>

    <!-- price -->
    


    <script type="text/javascript" src="assets/js/moment.min.js"></script>
    <script type="text/javascript" src="assets/js/date_picker/lightpick.js"></script>
    <!-- <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/data.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://cdn.rawgit.com/notryanb/highcharts_date_range_grouping/config-date-formats/dist/highcharts_date_range_grouping.min.js"></script>
    <script src="http://code.highcharts.com/stock/highstock.js"></script> -->
    <!-- <script src="https://cdn.datatables.net/responsive/1.0.7/js/dataTables.responsive.min.js"></script> -->

    <script src="assets/js/data_tables/dataTables.responsive.min.js"></script>

    <script src="assets/js/jquery-price.js"></script>

    <script>
        function ambilId(file){
            return document.getElementById(file);
        }

        $(document).ready(function(){
            $("#upload").click(function(){
                console.log('upload');
                
                var file = ambilId("file").files[0];
                var penarikanId = document.getElementById('penarikanId').value;
                console.log(file);
                if (file!=undefined) {
                    ambilId("progressBar").style.display = "block";
                    var formdata = new FormData();
                    formdata.append("file", file);
                    formdata.append("penarikanId", penarikanId);
                    var ajax = new XMLHttpRequest();
                    ajax.upload.addEventListener("progress", progressHandler, false);
                    ajax.addEventListener("load", completeHandler, false);
                    ajax.addEventListener("error", errorHandler, false);
                    ajax.addEventListener("abort", abortHandler, false);
                    ajax.open("POST", "API/uploadImage.php");
                    ajax.send(formdata);
                }else{
                    alert("Silahkan Pilh Foto Terlebih Dahulu");
                    return false;
                }
            });
        });

        function progressHandler(event){
            ambilId("loaded_n_total").innerHTML = "Uploaded "+event.loaded+" bytes of "+event.total;
            var percent = (event.loaded / event.total) * 100;
            ambilId("progressBar").value = Math.round(percent);
            ambilId("status").innerHTML = Math.round(percent)+"% uploaded... please wait";
        }
        function completeHandler(event){
            
            var obj = JSON.parse(event.target.response);
            
            ambilId("status").innerHTML = obj.status;
            //ambilId("file").value = obj.fileName;
            ambilId("foto").value = obj.fileName;
            ambilId("progressBar").value = 0;
        }
        function errorHandler(event){
            ambilId("status").innerHTML = "Upload Failed";
        }
        function abortHandler(event){
            ambilId("status").innerHTML = "Upload Aborted";
        }
    </script>

     <script>
        function ambilIdDitarik(file_siap_ditarik){
            return document.getElementById(file_siap_ditarik);
        }

        $(document).ready(function(){
            $("#upload_siap_ditarik").click(function(){
                console.log('upload_siap_ditarik');
                
                var file_siap_ditarik = ambilIdDitarik("file_siap_ditarik").files[0];
                var penarikanId = document.getElementById('penarikanId').value;

                console.log(file_siap_ditarik);
                if (file_siap_ditarik!=undefined) {
                    ambilIdDitarik("progressBar_siap_ditarik").style.display = "block";
                    var formdata = new FormData();
                    formdata.append("file", file_siap_ditarik);
                    formdata.append("penarikanId", penarikanId);
                    var ajax = new XMLHttpRequest();
                    ajax.upload.addEventListener("progress", progressHandlerSiapDitarik, false);
                    ajax.addEventListener("load", completeHandlerSiapDitarik, false);
                    ajax.addEventListener("error", errorHandlerSiapDitarik, false);
                    ajax.addEventListener("abort", abortHandlerSiapDitarik, false);
                    ajax.open("POST", "API/uploadImage.php");
                    ajax.send(formdata);
                }else{
                    alert("Silahkan Pilh Foto Terlebih Dahulu");
                    return false;
                }
            });
        });

        function progressHandlerSiapDitarik(event){
            ambilIdDitarik("loaded_n_total_siap_ditarik").innerHTML = "Uploaded "+event.loaded+" bytes of "+event.total;
            var percent = (event.loaded / event.total) * 100;
            ambilIdDitarik("progressBar_siap_ditarik").value = Math.round(percent);
            ambilIdDitarik("status_siap_ditarik").innerHTML = Math.round(percent)+"% uploaded... please wait";
        }
        function completeHandlerSiapDitarik(event){
            console.log(event);
            var obj = JSON.parse(event.target.response);
            
            ambilIdDitarik("status_siap_ditarik").innerHTML = obj.status;
            ambilIdDitarik("foto_siap_ditarik").value = obj.fileName;
            ambilIdDitarik("progressBar_siap_ditarik").value = 0;
        }
        function errorHandlerSiapDitarik(event){
            ambilIdDitarik("status_siap_ditarik").innerHTML = "Upload Failed";
        }
        function abortHandlerSiapDitarik(event){
            ambilIdDitarik("status_siap_ditarik").innerHTML = "Upload Aborted";
        }
    </script>

    <script type="text/javascript">
    $(function(){
       $('.freezerplacement').select2({
           minimumInputLength: 3,
           allowClear: true,
           placeholder: 'masukkan kode bfi',
           ajax: {
              dataType: 'json',
              url: 'get/getPlacement.php',
              delay: 800,
              data: function(params) {
                return {
                  search: params.term
                }
              },
              processResults: function (data, page) {
              return {
                results: data
              };
            },
          }
      }).on('select2:select', function (evt) {
         var data = $(".freezerplacement option:selected").text();
         alert("Data yang dipilih adalah "+data);
      });
 });
</script>

    <script>

        // $(document).ready(function(){
        //     alert("Hello, world!");
        // });

        // Persentase pencairan
        // var nominalKas = $('#total').val();
        // //var nominalBank = ; 

        // //console.log(nominalKas);

        // Load data provinsi, kecamatan, kelurahan dan dusun

        // <?php   
        //     echo 'var list1 = document.getElementById("select_provinsi");
            
        //         list1.options.length=0;
        //         list1.options[0] = new Option("Pilih Provinsi","");';

        //     $no = 1;
        //     $query = "SELECT * FROM provinces";
        //     $data = mysqli_query($koneksi,$query);
        //     while ($result = mysqli_fetch_assoc($data)) {
                

        //     echo "list1.options[".$no."] = new Option('".$result['name']."', '".$result['id']."');"; 

        //         $no++; 

        //     };              
        //  ?>

        // function getDataKota(){
        //     var list1 = document.getElementById("select_provinsi");
        //     var list2 = document.getElementById("select_kota");
        //     var list1SelectedValue = list1.options[list1.selectedIndex].value;
        //     console.log(list1SelectedValue);
        //     //createCookie("provinsi", list1SelectedValue, "0");


        //     list2.options.length=0;
        //     list2.options[0] = new Option("Pilih Kota","");

        //      <?php   
        //         // echo 'var list1 = document.getElementById("select_provinsi");
                
        //         //     list1.options.length=0;
        //         //     list1.options[0] = new Option("Pilih Provinsi","");';

        //         $no = 1;
        //         //$id_provinsi = $_COOKIE['provinsi'];

        //         $id_provinsi = '';

        //         echo 'console.log("'.$id_provinsi.'")';

                
        //         $query = "SELECT * FROM regencies WHERE province_id = '$id_provinsi'";
        //         $data = mysqli_query($koneksi,$query);
        //         while ($result = mysqli_fetch_assoc($data)) {
                    

        //         echo "list2.options[".$no."] = new Option('".$result['name']."', '".$result['id']."');"; 

        //             $no++; 

        //     };              
        //  ?>

        // }

        var customer;
        var url;

        $(window).on("load",function(){
          $(".loader-wrapper").fadeOut("slow");
        });


        function hitungOto(){
            var penghasilan = parseInt($("#penghasilan").val());
            var penghasilanLain = parseInt($("#penghasilan_lain_karyawan").val());

            var belanja = parseInt($("#belanja").val());
            var sewa = parseInt($("#sewa_rumah").val());
            var telplistrikair = parseInt($("#telplistrikair").val());
            var transportasi = parseInt($("#transportasi").val());
            var pendidikan = parseInt($("#pendidikan").val());
            var pengeluaranLainnya = parseInt($("#pengeluaran_lainnya").val());
            var angsuran = parseInt($("#total_angsuran_saat_ini").val());


            // var total = (penghasilan + penghasilanLain) - (belanja + sewa + telplistrikair + transportasi + pendidikan + pengeluaranLainnya + angsuran);

            var total = belanja + sewa + telplistrikair + transportasi + pendidikan + pengeluaranLainnya + angsuran;

            var total = isNaN(total) ? "Menghitung..." : total;

            console.log(total);
            
            $("#total_pengeluaran").val(total);

        }

        function rentown(){
            var owning = $('#freezerowner').val();

            console.log(owning);
            console.log("rentown");

            if (owning == 'Rent' || owning == 'RENT') {
                $('.nomor_mesin').hide();
                $('.id_number').show();
            }else{
                $('.nomor_mesin').show();
                $('.id_number').show();
            }
        }

        function rentownplace(a){
            var owning = $('.freezerowner'+a).val();

            console.log(owning);

            if (owning == 'Rent' || owning == 'RENT') {
                $('.nomor_mesin'+a).hide();
                // $('.id_number').show();
            }else{
                $('.nomor_mesin'+a).show();
                // $('.id_number').show();
            }
        }

        function get_brand(){
            var assetId = $('#assetId :selected').val();
            console.log(assetId);

            $.get("get/get_brand.php", {assetId:assetId}, function(data){

                console.log(data);
                $('#brandAsset').html(data);
            });

             $.get("get/get_tipe_freezer.php", {assetId:assetId}, function(data){

                console.log(data);
                $('#realisasi_tipe_freezerAsset').html(data);
            });

             $.get("get/get_nomor_mesin.php", {assetId:assetId}, function(data){

                console.log(data);
                $('#nomormesinAsset').html(data);
            });

        }

        function selectvendor(a){
            // $(".namavendorselect"+).change(function(){
               
                console.log("click vendor"+a);
                $('.areavendor'+a).html("<img src='http://www.broadwaybalancesamerica.com/images/ajax-loader.gif'>"); //Menampilkan loading selama proses pengambilan data kota
                    var id = $('#namavendorselect'+a).val(); //Mengambil id provinsi
                    console.log(id);
                    var list1 = document.getElementById("namavendorselect"+a);
                    var list1SelectedValue = list1.options[list1.selectedIndex].value;
                    // console.log(list1SelectedValue);

                    $('#namavendor_pilih'+a).val(list1SelectedValue);

                    $.get("get/get_vendorarea.php", {id:id}, function(data){
                       
                        $('#areavendor'+a).html(data);
                    });
            // });
        }

         function selectvendorRemove(a){
            // $(".namavendorselect"+).change(function(){
               
                console.log("click vendor"+a);
                $('.areavendorRemove'+a).html("<img src='http://www.broadwaybalancesamerica.com/images/ajax-loader.gif'>"); //Menampilkan loading selama proses pengambilan data kota
                    var id = $('#namavendorremoveselect'+a).val(); //Mengambil id provinsi
                    console.log(id);
                    var list1 = document.getElementById("namavendorremoveselect"+a);
                    var list1SelectedValue = list1.options[list1.selectedIndex].value;
                    // console.log(list1SelectedValue);

                    $.get("get/get_vendorarea.php", {id:id}, function(data){
                        
                        $('#areavendorRemove'+a).html(data);
                    });
            // });
        }

     

        function hitungOtoWira(){
            var penghasilan = parseInt($("#penghasilan_wira").val());
            var penghasilanLain = parseInt($("#penghasilan_lain_karyawan").val());

            var belanja = parseInt($("#belanja_wira").val());
            var sewa = parseInt($("#sewa_rumah_wira").val());
            var telplistrikair = parseInt($("#telplistrikair_wira").val());
            var transportasi = parseInt($("#transportasi_wira").val());
            var pendidikan = parseInt($("#pendidikan_wira").val());
            var pengeluaranLainnya = parseInt($("#pengeluaran_lainnya_wira").val());
            var angsuran = parseInt($("#total_angsuran_saat_ini_wira").val());


            var total = belanja + sewa + telplistrikair + transportasi + pendidikan + pengeluaranLainnya + angsuran;

            var total = isNaN(total) ? "Menghitung..." : total;

            console.log(total);
            
            $("#total_pengeluaran_wira").val(total);

        }


        function hitungOtoUsahaWira(){
            var pendapatan = parseInt($("#pendapatan_usaha_wira").val());
            
            var harga_pokok = parseInt($("#harga_pokok_wira").val());
            
            var sewa_usaha = parseInt($("#biaya_sewa_usaha_wira").val());
            var gaji = parseInt($("#gaji_pegawai_wira").val());
            var telplistrikair_usaha = parseInt($("#telplistrikair_usaha_wira").val());
            var transportasi_usaha = parseInt($("#transportasi_usaha_wira").val());

            var pengeluaran_usaha_lainnya = parseInt($("#pengeluaran_usaha_lainnya_wira").val());

            var total_usaha = harga_pokok + sewa_usaha + gaji + telplistrikair_usaha + transportasi_usaha + pengeluaran_usaha_lainnya;

            var total_usaha = isNaN(total_usaha) ? "Menghitung..." : total_usaha;

            console.log(total_usaha);

            $("#total_pengeluaran_usaha_wira").val(total_usaha);
        }

        function hitungOtoUsaha(){
            var pendapatan = parseInt($("#pendapatan_usaha").val());
            var harga_pokok = parseInt($("#harga_pokok").val());
            var sewa_usaha = parseInt($("#biaya_sewa_usaha").val());
            var gaji = parseInt($("#gaji_pegawai").val());
            var telplistrikair_usaha = parseInt($("#telplistrikair_usaha").val());
            var transportasi_usaha = parseInt($("#transportasi_usaha").val());

            var pengeluaran_usaha_lainnya = parseInt($("#pengeluaran_usaha_lainnya").val());

            var total_usaha = harga_pokok + sewa_usaha + gaji + telplistrikair_usaha + transportasi_usaha + pengeluaran_usaha_lainnya;

            var total_usaha = isNaN(total_usaha) ? "Menghitung..." : total_usaha;

            console.log(total_usaha);

            $("#total_pengeluaran_usaha").val(total_usaha);
        }

        function createCookie(name, value, days){
            var expires;
            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                expires = "; expires=" + date.toGMTString();
            }else{
                expires = "";
            }
            document.cookie = escape(name) + "=" + escape(value) + "; path=/";
        }

        $(function(){ // sama dengan $(document).ready(function(){

          $('#select_provinsi').change(function(){
            $('#kota').html("<img src='http://www.broadwaybalancesamerica.com/images/ajax-loader.gif'>"); //Menampilkan loading selama proses pengambilan data kota
                var id = $(this).val(); //Mengambil id provinsi
                var list1 = document.getElementById("select_provinsi");
                var list1SelectedValue = list1.options[list1.selectedIndex].value;
                console.log(list1SelectedValue);

                $('#provinsi_pilih').val(list1SelectedValue);

                $.get("get/get_kota.php", {id:id}, function(data){
                    $('#kota').html(data);
                });
            });

          $('.form-penyesuaian').hide();
         });


        function getKecamatan(){
            var list1 = document.getElementById("select_kota");
            var list1SelectedValue = list1.options[list1.selectedIndex].value;
            console.log(list1SelectedValue);
            $('#kecamatan').html("<img src='http://www.broadwaybalancesamerica.com/images/ajax-loader.gif'>"); //Menampilkan loading selama proses pengambilan data kota

            $('#kota_pilih').val(list1SelectedValue);

            
                var id = list1SelectedValue; //Mengambil id provinsi
                console.log("kecamatan");
                $.get("get/get_kecamatan.php", {id:id}, function(data){
                    $('#kecamatan').html(data);
                });
        }

        function getKelurahan(){
            var list1 = document.getElementById("select_kecamatan");
            var list1SelectedValue = list1.options[list1.selectedIndex].value;
            $('#kelurahan').html("<img src='http://www.broadwaybalancesamerica.com/images/ajax-loader.gif'>"); //Menampilkan loading selama proses pengambilan data kota

            $('#kecamatan_pilih').val(list1SelectedValue);

                var id = list1SelectedValue; //Mengambil id provinsi
                console.log("kelurahan");
                $.get("get/get_kelurahan.php", {id:id}, function(data){
                    $('#kelurahan').html(data);
                });
        }

        function getKelurahanPilih(){
            var list1 = document.getElementById("select_kelurahan");
            var list1SelectedValue = list1.options[list1.selectedIndex].value;

            $('#kelurahan_pilih').val(list1SelectedValue);
        }

        

        // $("#karyawanForm").hide();
        // $("#wiraswastaForm").hide();
        // $("#karyawanWiraswastaForm").hide();

        $(document).ready(function(){
            $('#submit_noo').hide();
            // $('#save_noo').hide();

            $("#karyawanForm").hide();
            $("#wiraswastaForm").hide();
            $("#karyawanWiraswastaForm").hide();
            $('.hidden-edit').hide();

            $(".select_database").select2();



            // konek mds ajax
            $(".select_mds").select2({
                minimumInputLength: 3,
                allowClear: true,
                placeholder: 'Masukkan Kode BFI atau Nama Outlet',
                ajax: {
                  dataType: 'json',
                  url: function(){return ''+url},
                  delay: 800,
                  data: function(params) {
                    return {
                      search: params.term
                    }
                  },
                  processResults: function (data, page) {
                  return {
                    results: data
                  };
                },
              }
            })

            $(".select_bfi").select2();

            $('.nomor_mesin').hide();
            $('#nomor_mesin').hide();
            $('.id_number').hide();

            // untuk noo
            $("#save_noo").click(function(){
                var list_noo = new Array();
                $("input[name='select_noo[]']:checked").each(function() {
                    list_noo.push($(this).val());
                });

                $('#list_noo').val(list_noo);
                if (list_noo == '') {
                    window.alert("Mohon centang minimal satu outlet yang ingin di register");
                }else{
                    $('#submit_noo').show();
                    $('#save_noo').hide();
                }

                
     
            });

            $("#primaryget").change(function(){
                // alert("click");
                var id = $('option:selected', this).attr('acuan');
                $('#freezerid').val(id);
            });

            $('#select_all').click(function(){

                if (!$(this).prop("checked")) {
                    $(".select_noo").prop("checked", false);
                }else{
                    $(".select_noo").prop("checked", true);
                }
            })

            // $(".namavendorselect").change(function(){
            //     console.log("click vendor");
            //     $('.areavendor').html("<img src='http://www.broadwaybalancesamerica.com/images/ajax-loader.gif'>"); //Menampilkan loading selama proses pengambilan data kota
            //         var id = $(this).val(); //Mengambil id provinsi
            //         var list1 = document.getElementById("namavendorselect");
            //         var list1SelectedValue = list1.options[list1.selectedIndex].value;
            //         console.log(list1SelectedValue);

            //         $('.namavendor_pilih').val(list1SelectedValue);

            //         $.get("get/get_vendorarea.php", {id:id}, function(data){
            //             $('.areavendor').html(data);
            //         });
            // });

            $("#namavendorselectadd").change(function(){
                $('#areavendor').html("<img src='http://www.broadwaybalancesamerica.com/images/ajax-loader.gif'>"); //Menampilkan loading selama proses pengambilan data kota
                    var id = $(this).val(); //Mengambil id provinsi
                    var list1 = document.getElementById("namavendorselectadd");
                    var list1SelectedValue = list1.options[list1.selectedIndex].value;
                    console.log(list1SelectedValue);

                    $('#namavendor_pilih').val(list1SelectedValue);

                    $.get("get/get_vendorareaadd.php", {id:id}, function(data){
                        $('#areavendor').html(data);
                    });
            });

            $("#tipecustomerselect").change(function(){
                // $('#customerall').html("<option>Connecting to MDS</option>......."); //Menampilkan loading selama proses pengambilan data kota
                    var id = $(this).val(); //Mengambil id provinsi
                    var list1 = document.getElementById("tipecustomerselect");
                    var list1SelectedValue = list1.options[list1.selectedIndex].value;
                    console.log(list1SelectedValue);

                    customer = list1SelectedValue;
                    console.log(customer);

                    if (customer == 'PRIMARY') {
                        url = "get/get_customer_mds.php";
                    }else if (customer == 'SECONDARY') {
                        url = "get/get_customer_mds2.php";
                    };




                    $('#tipecustomerselect_pilih').val(list1SelectedValue);

                    // $.get("get/get_customer.php", {id:id}, function(data){
                    //     $('#customerall').html(data);
                    // });
            });

            

        })



        function selectNOO(){
            $('#submit_noo').hide();
            $('#save_noo').show();
        }

        function showForm(){
            var karyawanForm = document.getElementById("karyawanForm");
            var wiraswastaForm = document.getElementById("wiraswastaForm");
            var karyawanWiraswastaForm = document.getElementById("karyawanWiraswastaForm");

            var list1 = document.getElementById("select_pekerjaan_survey");
            var list1SelectedValue = list1.options[list1.selectedIndex].value;

            console.log(list1SelectedValue);
            if (list1SelectedValue == "Karyawan") {
                $("#karyawanForm").show();
                $("#wiraswastaForm").hide();
                $("#karyawanWiraswastaForm").hide();
            }

            if (list1SelectedValue == "Wiraswasta/Professional") {
                $("#wiraswastaForm").show();
                $("#karyawanForm").hide();
                $("#karyawanWiraswastaForm").hide();
            }

            if (list1SelectedValue == "Karyawan dan Wiraswasta/Professional") {
                $("#karyawanWiraswastaForm").show();
                $("#wiraswastaForm").hide();
                $("#karyawanForm").hide();
            }
        }

        //hitung total bayar beli saham
        function getTotalBayar(){
            jumlahSaham = $('#jumlahsaham').val();
            hargaSaham = $('#inputhargasahambeli').val();

            console.log(hargaSaham);
            totalBayar = parseInt(jumlahSaham) * parseInt(hargaSaham);

            console.log(totalBayar);

            $('#totalhargabayarsaham').val(totalBayar);  
            $('#totalhargabayarsahambelijual').val(totalBayar);
            

        }

        $("#sesuaikan").click(function(){
            $('.form-penyesuaian').show();
            $('#sesuaikan').hide();
        });

        $(".tindaklanjut").click(function(){
            $('.nomor_own').hide();

            var acuan = $(this).attr("acuan");

            console.log(acuan);

            $('#form-tindaklanjut-'+acuan).show(300);
            $('.tindaklanjut').hide(300);
            $('.approved_placement').hide(300);
        });

        $(".approved_placement").click(function(){
            $('.nomor_own').hide();

            var acuan = $(this).attr("acuan");

            console.log(acuan);

            $('#form-approved-'+acuan).show(300);
            $('.tindaklanjut').hide(300);
            $('.approved_placement').hide(300);
        });

        // $('.form-tindaklanjut').hide();

        // $('.form-action').hide();

        $(".data-action").click(function(){

            var acuan = $(this).attr("acuan");

            // console.log(acuan);
            // window.alert(acuan);

            $('#form-action-'+acuan).show(300);
            $('.data-action').hide(300);
        });



        $(".batal-tindaklanjut").click(function(){
            $('.form-tindaklanjut').hide(300);
            $('.tindaklanjut').show(300);
            $('.approved_placement').show(300);
        });

        $('#batal-sesuaikan').click(function(){
            $('.form-penyesuaian').hide();
            $('#sesuaikan').show();
        });

        $('.batal-action').click(function(){
            $('.form-action').hide(300);
            $('.data-action').show(200);
        });

        var picker = new Lightpick({ 
            field: document.getElementById('tanggal_mulai') ,
            secondField: document.getElementById('tanggal_akhir'),
            format: 'DD-MM-YYYY',
            repick: true
        });
       
	   var rangeDatePicker = [];
	   
	   function rangeDatePick() {
			var date1 = document.getElementsByClassName("start_date");
			var date2 = document.getElementsByClassName("end_date");
			
			var i;
			for (i = 0; i < date1.length; i++) {
				rangeDatePicker[i] = new Lightpick({ 
					field: date1[i] ,
					secondField: date2[i],
					format: 'DD-MM-YYYY',
					repick: true
					});
			}
		}
		
		rangeDatePick();

        $('.harga_txt').priceFormat({
            prefix: '',
            centsLimit : '0',
            thousandsSeparator: '.'
        });

        $('.harga_rp').priceFormat({
            prefix: 'Rp ',
            centsLimit : '0',
            thousandsSeparator: '.'
        });

        
        $('#tidak_modal').onClick('#notif').hide();

        function notif_close(){
            $('#notif').hide('slow');
        }

        function show_hidden(){
            $('.hidden-edit').toggle();
        }

        function registerNOO(){
            var noo = $('#noo_acuan').val();
            console.log(noo);
            if (noo) {
                console.log("noo ada");
            }else{
                console.log(noo);
            }
        }

        function isiNOO(){
            var isian = $('.cekNOO').val();
            console.log(isian);
            $('#noo_acuan').val(isian);

        }



        // function tindaklanjut(){
        //     $('#rejectbtn').hide();
        //     var acuan = $(this).attr('acuan');

        //     console.log(acuan);

        //     $('#form-tindaklanjut-'+acuan).show();
        //     $('#approved_placement_'+acuan).hide();
        // }
        



        /* add price to anything */

        
        
    



    </script>
    <!-- <script src="assets/js/app.js"></script> -->

</body>

</html>
<!-- end document-->
