<?php 
    if (isset($_GET['status'])) {
        $status = $_GET['status'];

        if ($status == 1) {
            echo '<div class="alert alert-success" role="alert">
                Data stakeholder berhasil di hapus
            </div>';
        }elseif ($status == 0) {
            echo '<div class="alert alert-danger" role="alert">
                Terjadi Kesalahan! tidak dapat menghapus data stakeholder
            </div>';
        }else{
            echo '<div class="alert alert-warning" role="alert">
                Terjadi kesalahan koneksi internet, silkahkan ulangi kembali
            </div>';
        }
    }

    if (isset($_GET['editstatus'])) {
        $status = $_GET['editstatus'];

        if ($status == 1) {
            echo '<div class="alert alert-success" role="alert">
                Data investor berhasil di perbaruhi
            </div>';
        }elseif ($status == 0) {
            echo '<div class="alert alert-danger" role="alert">
                Terjadi Kesalahan! tidak dapat mengubah data user
            </div>';
        }else{
            echo '<div class="alert alert-warning" role="alert">
                Terjadi kesalahan koneksi internet, silkahkan ulangi kembali
            </div>';
        }
    }
 ?>
<div class="row">
<div class="col-md-12">
        <!-- DATA TABLE -->
        <h3 class="title-5 m-b-35">
            View Reject

            <?php 
                //image test
                // echo '<img src="https://appict.appsbelfoods.com/images/distributor/distributor1_2019-04-10_10-37-58.jpg" width="500px" height="500px"> </img>';
             ?>
        </h3>
        <div class="table-data__tool">
            <!-- <div class="table-data__tool-left">
                <div class="rs-select2--light rs-select2--md">
                    <select class="js-select2" name="property">
                        <option selected="selected">Roles</option>
                        <option value="">owner</option>
                        <option value="">stakeholders</option>
                        <option value="">investors</option>
                        <option value="">marketing</option>
                        <option value="">admin</option>
                        <option value="">approval</option>
                    </select>
                    <div class="dropDownSelect2"></div>
                </div>
                <div class="rs-select2--light rs-select2--sm">
                    <select class="js-select2" name="time">
                        <option selected="selected">Filter By</option>
                        <option value="">Name</option>
                        <option value="">Date created</option>
                    </select>
                    <div class="dropDownSelect2"></div>
                </div>
                <button class="au-btn-filter">
                    <i class="zmdi zmdi-filter-list"></i>filters</button>
            </div>
            <div class="table-data__tool-right">
                <button class="au-btn au-btn-icon au-btn--green au-btn--small">
                    <i class="fas fa-print"></i>print</button>
                <div class="rs-select2--dark rs-select2--sm rs-select2--dark2">
                    <select class="js-select2" name="type">
                        <option selected="selected">Export</option>
                        <option value="">PDF</option>
                        <option value="">CSV</option>
                    </select>
                    <div class="dropDownSelect2"></div>
                </div>
            </div> -->
        </div>
        <div>
            <form action="" method="POST">
            <div class="row form-group">
                <div class="col-sm-3 col-md-2">
                    <label for="text-input" class=" form-control-label"><b>Filter by date</b></label>
                </div>
                <div class="col-sm-3 col-md-2">
                    <input type="text" id="tanggal_mulai" name="tanggal_mulai" class="form-control" placeholder="Mulai" required>
                </div>
                <div class="col-sm-3 col-md-2">
                    <input type="text" id="tanggal_akhir" name="tanggal_akhir" class="form-control" placeholder="Akhir" required>
                    
                </div>
                <button type="submit" name="custom" value="custom" class="btn btn-sm btn-primary">
                    Filter
                </button>
            </div>


            </form>
            <hr>
        </div>

        <?php if (isset($_POST['custom'])): ?>
            
            <?php 
                $tanggalMulai = mysqli_escape_string($koneksi,$_POST['tanggal_mulai']);

                $tanggalSelesai = mysqli_escape_string($koneksi,$_POST['tanggal_akhir']);

                $tanggalMulaiEdit = strtotime($tanggalMulai);

                $tanggalMulai= date('Y-m-d', $tanggalMulaiEdit);

                $tanggalSelesaiEdit = strtotime($tanggalSelesai);

                $tanggalSelesai= date('Y-m-d', $tanggalSelesaiEdit);
            ?>

        <div class="table-responsive table-data" style="height: auto">
            <table id="tableok" class="mdl-data-table table table-borderless" style="width:100%">
                <thead>
                    <tr>
                        <!-- <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td> -->
                        <td>No</td>
                        <td>Nama Outlet</td>
                        <td>BFI Code</td>
                        <td>Nama Distributor</td>
                        <td>Area Distributor</td>
                        <td>Tgl Survey</td>
                        <td>Nama Surveyor</td>
                        <td>Status Kunjungan</td>
                        <td>Channel</td>
                        <td>Sub-Channel</td>
                        <td>Segment</td>
                        <td>Nama PIC</td>
                        <td>No KTP</td>
                        <td>No Telp PIC</td>
                        <td>Longitude</td>
                        <td>Latitude</td>
                        <td>Alamat</td>
                        <td>Provinsi</td>
                        <td>Kabupaten/Kota</td>
                        <td>Kecamatan</td>
                        <td>Keluarahan</td>
                        <td>Kode Pos</td>
                        <td>Jam Buka (WIB)</td>
                        <td>Jam Tutup (WIB)</td>
                        <td>Luas Bangunan</td>
                        <td>Paket Awal</td>
                        <td>Foto - Outlet Tampak Depan</td>
                        <td>Foto - Daerah Depan Sekitar Toko</td>
                        <td>Foto - Deal Form + KTP</td>
                        <td>Lokasi Sekitar</td>
                        <td>Patokan</td>
                        <td>Lokasi (1)</td>
                        <td>Lokasi (2)</td>
                        <td>Freezer Es Krim</td>
                        <td>Merk Freezer Es Krim</td>
                        <td>Freezer Makanan Beku</td>
                        <td>Merk Freezer Makanan Beku</td>
                        <td>Produk Utama</td>
                        <td>Jumlah Pengunjung</td>
                        <td>Pendapatan/Hari (IDR)</td>
                        <td>Tipe Pengunjung</td>
                        <td>Kapasitas Listrik</td>
                        <td>Kondisi Fisik Outlet</td>
                        <td>Resiko Freezer</td>
                        <td>Spacae 1 x 1.5 m</td>
                        <td>Tipe Freezer</td>
                        <td>Area Penempatan Freezer</td>
                        <td>Visibilitas Freezer di Jalur Utama</td>
                        <td>Layanan Tambahan</td>
                        <td>Target/Bulan (kg)</td>
                        <td>Issue/Follow Up Action</td>
                        <td>Status</td>


                        
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $no = 1;
                        include ('config/config.php');
                        $query = "SELECT * FROM distributor WHERE statustoko = 'REJECT' AND inputdatetime >= '$tanggalMulai' and inputdatetime <= '$tanggalSelesai' ORDER BY iddistributor DESC";
                        $data = mysqli_query($koneksi,$query);

                        while ($result = mysqli_fetch_assoc($data)) {
                     ?>
                    <tr>
                        <!-- <td>
                        <label class="au-checkbox">
                        <input type="checkbox">
                        <span class="au-checkmark"></span>
                        </label>
                        </td> -->
                        <td><?php echo $no ?></td>
                        <td>
                            <?php echo $result['custname'] ?>
                        </td>
                        <td>
                            <?php echo $result['custid'] ?>
                        </td>
                        <td>
                            <?php echo $result['distname'] ?>
                        </td>
                        <td>
                            <?php echo $result['coveragearea'] ?>
                        </td>
                       
                        <td>
                            <?php 
                                echo $result['inputdatetime'];
                            ?>
                        </td>

                        <td>
                            
                            <?php echo $result['sales']; ?>
                        </td>
                        <td>
                            <?php 
                                $status = $result['status'];

                                if ($status == 1) {
                                     echo 'Revisit';
                                 }else{
                                    echo 'New Outlet';
                                 } ?>
                        </td>
                        <td>
                            <?php echo $result['channel'] ?>
                        </td>

                        <td>
                            <?php echo $result['subchannel'] ?>
                        </td>

                        <td>
                            <?php echo $result['segment'] ?>
                        </td>

                        <td>
                            <?php echo $result['nameowner'] ?>
                        </td>

                        <td>
                            <?php echo $result['noktp'] ?>
                        </td>
                        <td>
                            <?php echo $result['telephone'] ?>
                        </td>
                        <td>
                            <?php echo $result['longitude'] ?>
                        </td>
                        <td>
                            <?php echo $result['latitude'] ?>
                        </td>

                        <td>
                            <?php echo $result['address'] ?>
                        </td>

                        <td>
                            <?php echo $result['province'] ?>
                        </td>

                        <td>
                            <?php echo $result['city'] ?>
                        </td>

                        <td>
                            <?php echo $result['kecamatan'] ?>
                        </td>

                        <td>
                            <?php echo $result['kelurahan'] ?>
                        </td>

                        <td>
                            <?php echo $result['kodepos'] ?>
                        </td>

                        <td>
                            <?php echo $result['timeopen'] ?>
                        </td>

                        <td>
                            <?php echo $result['timeclosed'] ?>
                        </td>

                        <td>
                            <?php echo $result['luasbangunan'] ?>
                        </td>

                        <td>
                            <?php echo $result['paketfreezer'] ?>
                        </td>

                        <td>
                            <?php 
                            $photo = 'https://appict.appsbelfoods.com/'.$result['photo'];
                            
                            // echo '<a href="'.$photo.'"><img src="'.$photo.'" width="50px" height="50px"></a>';
                            echo '<a href="'.$photo.'">'.$photo.'</a>';

                            ?>
                        </td>
                        <td>
                            <?php 
                            $photo = 'https://appict.appsbelfoods.com/'.$result['photo2'];
                            
                            // echo '<a href="'.$photo.'"><img src="'.$photo.'" width="50px" height="50px"></a>';
                            echo '<a href="'.$photo.'">'.$photo.'</a>';

                            ?>
                        </td>
                        <td>
                            <?php 
                            $photo = 'https://appict.appsbelfoods.com/'.$result['photo3'];
                            
                            // echo '<a href="'.$photo.'"><img src="'.$photo.'" width="50px" height="50px"></a>';
                            echo '<a href="'.$photo.'">'.$photo.'</a>';

                            ?>
                        </td>

                        <td>
                            <?php echo $result['sebutkanlingkungan'] ?>
                        </td>

                        <td>
                            <?php echo $result['patokan'] ?>
                        </td>
                        <td>
                            <?php echo $result['lokasi1'] ?>
                        </td>
                        <td>
                            <?php echo $result['lokasi2'] ?>
                        </td>
                        <td>
                            <?php echo $result['freezerdalamtoko'] ?>
                        </td>
                        <td>
                            <?php echo $result['merekfreezer'] ?>
                        </td>
                        <td>
                            <?php echo $result['frozenFood'] ?>
                        </td>

                        <td>
                            <?php echo $result['merekfrozenfood'] ?>
                        </td>

                        <td>
                            <?php echo $result['productutama'] ?>
                        </td>
                        <td>
                            <?php echo $result['estimasipengunjung'] ?>
                        </td>
                        <td>
                            <?php echo $result['pendapatan'] ?>
                        </td>

                        <td>
                            <?php echo $result['mayoritas'] ?>
                        </td>
                        <td>
                            <?php echo $result['elektricity'] ?>
                        </td>
                        <td>
                            <?php echo $result['kondisifisik'] ?>
                        </td>

                        <td>
                            <?php echo $result['resikofreezerhilang'] ?>
                        </td>

                        <td>
                            <?php echo $result['tersediatempat'] ?>
                        </td>

                        <td>
                            <?php echo $result['pilihanfreezeroutlet'] ?>
                        </td>

                        <td>
                            <?php echo $result['tempatfreezer'] ?>
                        </td>

                        <td>
                            <?php echo $result['penilaianlokasi'] ?>
                        </td>

                        <td>
                            <?php echo $result['sebutkanlingkungan'] ?>
                        </td>

                        <td>
                            <?php echo $result['targetbulanan'] ?>
                        </td>

                        <td>
                            <?php echo $result['followupaction'] ?>
                        </td>

                        <td>
                            <?php echo $result['statustoko'] ?>
                        </td>
                        
                    </tr>

                     <?php $no++; } ?>
                </tbody>
            </table>
        </div>
        <?php else: ?>

        <div class="table-responsive table-data" style="height: auto">
            <table id="tableok" class="mdl-data-table table table-borderless" style="width:100%">
                <thead>
                    <tr>
                        <!-- <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td> -->
                        <td>No</td>
                        <td>Nama Outlet</td>
                        <td>BFI Code</td>
                        <td>Nama Distributor</td>
                        <td>Area Distributor</td>
                        <td>Tgl Survey</td>
                        <td>Nama Surveyor</td>
                        <td>Status Kunjungan</td>
                        <td>Channel</td>
                        <td>Sub-Channel</td>
                        <td>Segment</td>
                        <td>Nama PIC</td>
                        <td>No KTP</td>
                        <td>No Telp PIC</td>
                        <td>Longitude</td>
                        <td>Latitude</td>
                        <td>Alamat</td>
                        <td>Provinsi</td>
                        <td>Kabupaten/Kota</td>
                        <td>Kecamatan</td>
                        <td>Keluarahan</td>
                        <td>Kode Pos</td>
                        <td>Jam Buka (WIB)</td>
                        <td>Jam Tutup (WIB)</td>
                        <td>Luas Bangunan</td>
                        <td>Paket Awal</td>
                        <td>Foto - Outlet Tampak Depan</td>
                        <td>Foto - Daerah Depan Sekitar Toko</td>
                        <td>Foto - Deal Form + KTP</td>
                        <td>Lokasi Sekitar</td>
                        <td>Patokan</td>
                        <td>Lokasi (1)</td>
                        <td>Lokasi (2)</td>
                        <td>Freezer Es Krim</td>
                        <td>Merk Freezer Es Krim</td>
                        <td>Freezer Makanan Beku</td>
                        <td>Merk Freezer Makanan Beku</td>
                        <td>Produk Utama</td>
                        <td>Jumlah Pengunjung</td>
                        <td>Pendapatan/Hari (IDR)</td>
                        <td>Tipe Pengunjung</td>
                        <td>Kapasitas Listrik</td>
                        <td>Kondisi Fisik Outlet</td>
                        <td>Resiko Freezer</td>
                        <td>Spacae 1 x 1.5 m</td>
                        <td>Tipe Freezer</td>
                        <td>Area Penempatan Freezer</td>
                        <td>Visibilitas Freezer di Jalur Utama</td>
                        <td>Layanan Tambahan</td>
                        <td>Target/Bulan (kg)</td>
                        <td>Issue/Follow Up Action</td>
                        <td>Status</td>


                        
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $no = 1;
                        include ('config/config.php');
                        $query = "SELECT * FROM distributor WHERE statustoko = 'REJECT' ORDER BY iddistributor DESC LIMIT 0,1000";
                        $data = mysqli_query($koneksi,$query);

                        while ($result = mysqli_fetch_assoc($data)) {
                     ?>
                    <tr>
                        <!-- <td>
                        <label class="au-checkbox">
                        <input type="checkbox">
                        <span class="au-checkmark"></span>
                        </label>
                        </td> -->
                        <td><?php echo $no ?></td>
                        <td>
                            <?php echo $result['custname'] ?>
                        </td>
                        <td>
                            <?php echo $result['custid'] ?>
                        </td>
                        <td>
                            <?php echo $result['distname'] ?>
                        </td>
                        <td>
                            <?php echo $result['coveragearea'] ?>
                        </td>
                       
                        <td>
                            <?php 
                                echo $result['inputdatetime'];
                            ?>
                        </td>

                        <td>
                            
                            <?php echo $result['sales']; ?>
                        </td>
                        <td>
                            <?php 
                                $status = $result['status'];

                                if ($status == 1) {
                                     echo 'Revisit';
                                 }else{
                                    echo 'New Outlet';
                                 } ?>
                        </td>
                        <td>
                            <?php echo $result['channel'] ?>
                        </td>

                        <td>
                            <?php echo $result['subchannel'] ?>
                        </td>

                        <td>
                            <?php echo $result['segment'] ?>
                        </td>

                        <td>
                            <?php echo $result['nameowner'] ?>
                        </td>

                        <td>
                            <?php echo $result['noktp'] ?>
                        </td>
                        <td>
                            <?php echo $result['telephone'] ?>
                        </td>
                        <td>
                            <?php echo $result['longitude'] ?>
                        </td>
                        <td>
                            <?php echo $result['latitude'] ?>
                        </td>

                        <td>
                            <?php echo $result['address'] ?>
                        </td>

                        <td>
                            <?php echo $result['province'] ?>
                        </td>

                        <td>
                            <?php echo $result['city'] ?>
                        </td>

                        <td>
                            <?php echo $result['kecamatan'] ?>
                        </td>

                        <td>
                            <?php echo $result['kelurahan'] ?>
                        </td>

                        <td>
                            <?php echo $result['kodepos'] ?>
                        </td>

                        <td>
                            <?php echo $result['timeopen'] ?>
                        </td>

                        <td>
                            <?php echo $result['timeclosed'] ?>
                        </td>

                        <td>
                            <?php echo $result['luasbangunan'] ?>
                        </td>

                        <td>
                            <?php echo $result['paketfreezer'] ?>
                        </td>

                        <td>
                            <?php 
                            $photo = 'https://appict.appsbelfoods.com/'.$result['photo'];
                            
                            // echo '<a href="'.$photo.'"><img src="'.$photo.'" width="50px" height="50px"></a>';
                            echo '<a href="'.$photo.'">'.$photo.'</a>';

                            ?>
                        </td>
                        <td>
                            <?php 
                            $photo = 'https://appict.appsbelfoods.com/'.$result['photo2'];
                            
                            // echo '<a href="'.$photo.'"><img src="'.$photo.'" width="50px" height="50px"></a>';
                            echo '<a href="'.$photo.'">'.$photo.'</a>';

                            ?>
                        </td>
                        <td>
                            <?php 
                            $photo = 'https://appict.appsbelfoods.com/'.$result['photo3'];
                            
                            // echo '<a href="'.$photo.'"><img src="'.$photo.'" width="50px" height="50px"></a>';
                            echo '<a href="'.$photo.'">'.$photo.'</a>';

                            ?>
                        </td>

                        <td>
                            <?php echo $result['sebutkanlingkungan'] ?>
                        </td>

                        <td>
                            <?php echo $result['patokan'] ?>
                        </td>
                        <td>
                            <?php echo $result['lokasi1'] ?>
                        </td>
                        <td>
                            <?php echo $result['lokasi2'] ?>
                        </td>
                        <td>
                            <?php echo $result['freezerdalamtoko'] ?>
                        </td>
                        <td>
                            <?php echo $result['merekfreezer'] ?>
                        </td>
                        <td>
                            <?php echo $result['frozenFood'] ?>
                        </td>

                        <td>
                            <?php echo $result['merekfrozenfood'] ?>
                        </td>

                        <td>
                            <?php echo $result['productutama'] ?>
                        </td>
                        <td>
                            <?php echo $result['estimasipengunjung'] ?>
                        </td>
                        <td>
                            <?php echo $result['pendapatan'] ?>
                        </td>

                        <td>
                            <?php echo $result['mayoritas'] ?>
                        </td>
                        <td>
                            <?php echo $result['elektricity'] ?>
                        </td>
                        <td>
                            <?php echo $result['kondisifisik'] ?>
                        </td>

                        <td>
                            <?php echo $result['resikofreezerhilang'] ?>
                        </td>

                        <td>
                            <?php echo $result['tersediatempat'] ?>
                        </td>

                        <td>
                            <?php echo $result['pilihanfreezeroutlet'] ?>
                        </td>

                        <td>
                            <?php echo $result['tempatfreezer'] ?>
                        </td>

                        <td>
                            <?php echo $result['penilaianlokasi'] ?>
                        </td>

                        <td>
                            <?php echo $result['sebutkanlingkungan'] ?>
                        </td>

                        <td>
                            <?php echo $result['targetbulanan'] ?>
                        </td>

                        <td>
                            <?php echo $result['followupaction'] ?>
                        </td>

                        <td>
                            <?php echo $result['statustoko'] ?>
                        </td>
                        
                    </tr>

                     <?php $no++; } ?>
                </tbody>
            </table>
        </div>


        <!-- <div class="table-responsive table-responsive-data2">
            <table class="table table-data2">
                <thead>
                    <tr>
                        <th>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </th>
                        <th>name</th>
                        <th>email</th>
                        <th>username</th>
                        <th>date created</th>
                        <th>roles</th>
                        <th>password</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="tr-shadow">
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td>Lori Lynch</td>
                        <td>
                            <span class="block-email">lori@gmail.com</span>
                        </td>
                        <td class="desc">Lori345</td>
                        <td>2018-09-17 02:12</td>
                        <td>
                            <span class="role admin">Admin</span>
                        </td>
                        <td>************</td>
                        <td>
                            <div class="table-data-feature">
                                <button class="item" data-toggle="tooltip" data-placement="top" title="View Password">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <i class="zmdi zmdi-delete"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Banned">
                                    <i class="fas fa-ban"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                    <tr class="spacer"></tr>
                    <tr class="tr-shadow">
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td>John Martin</td>
                        <td>
                            <span class="block-email">johnmartin@gmail.com</span>
                        </td>
                        <td class="desc">Martin.jhon</td>
                        <td>2018-09-01 07:57</td>
                        <td>
                            <span class="role stakeholders">Stakeholders</span>
                        </td>
                        <td>************</td>
                        <td>
                            <div class="table-data-feature">
                                <button class="item" data-toggle="tooltip" data-placement="top" title="View Password">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <i class="zmdi zmdi-delete"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Banned">
                                    <i class="fas fa-ban"></i>
                                </button>
                            </div>
                        </td>
                    </tr>

                    <tr class="spacer"></tr>
                    <tr class="tr-shadow">
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td>Rahadian Agus</td>
                        <td>
                            <span class="block-email">rahadianagus@gmail.com</span>
                        </td>
                        <td class="desc">MrAgus.69</td>
                        <td>2018-09-01 06:06</td>
                        <td>
                            <span class="role investors">Investors</span>
                        </td>
                        <td>************</td>
                        <td>
                            <div class="table-data-feature">
                                <button class="item" data-toggle="tooltip" data-placement="top" title="View Password">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <i class="zmdi zmdi-delete"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Banned">
                                    <i class="fas fa-ban"></i>
                                </button>
                            </div>
                        </td>
                    </tr>

                    <tr class="spacer"></tr>
                    <tr class="tr-shadow">
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td>Hasbi</td>
                        <td>
                            <span class="block-email">auskreditsyari@gmail.com</span>
                        </td>
                        <td class="desc">Hasbi9899</td>
                        <td>2018-09-01 05:57</td>
                        <td>
                            <span class="role owner">Owner</span>
                        </td>
                        <td>************</td>
                        <td>
                            <div class="table-data-feature">
                                <button class="item" data-toggle="tooltip" data-placement="top" title="View Password">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <i class="zmdi zmdi-delete"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Banned">
                                    <i class="fas fa-ban"></i>
                                </button>
                            </div>
                        </td>
                    </tr>

                    <tr class="spacer"></tr>
                    <tr class="tr-shadow">
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td>Ahmad Rupandi</td>
                        <td>
                            <span class="block-email">ahmadrupandi@gmail.com</span>
                        </td>
                        <td class="desc">RupandiPejantanTangguh</td>
                        <td>2018-09-01 05:55</td>
                        <td>
                            <span class="role marketing">Marketing</span>
                        </td>
                        <td>************</td>
                        <td>
                            <div class="table-data-feature">
                                <button class="item" data-toggle="tooltip" data-placement="top" title="View Password">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <i class="zmdi zmdi-delete"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Banned">
                                    <i class="fas fa-ban"></i>
                                </button>
                            </div>
                        </td>
                    </tr>

                    <tr class="spacer"></tr>
                    <tr class="tr-shadow">
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td>Supriadi</td>
                        <td>
                            <span class="block-email">supriadi@gmail.com</span>
                        </td>
                        <td class="desc">Supri.Adi31</td>
                        <td>2018-09-01 05:57</td>
                        <td>
                            <span class="role approval">Approval</span>
                        </td>
                        <td>************</td>
                        <td>
                            <div class="table-data-feature">
                                <button class="item" data-toggle="tooltip" data-placement="top" title="View Password">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <i class="zmdi zmdi-delete"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Banned">
                                    <i class="fas fa-ban"></i>
                                </button>
                            </div>
                        </td>
                    </tr>

                </tbody>
            </table>
        </div> -->

        <?php endif ?>
        <!-- END DATA TABLE -->
    </div>
</div>