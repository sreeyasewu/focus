<?php
## Database configuration
include '../config/config.php';

## Read value
$draw = $_POST['draw'];
$row = $_POST['start'];
$rowperpage = $_POST['length']; // Rows display per page
$columnIndex = $_POST['order'][0]['column']; // Column index
$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
$searchValue = mysqli_real_escape_string($koneksi,$_POST['search']['value']); // Search value

## Search 
$tanggal_mulai = $_POST['tanggal_mulai'];
$tanggal_akhir = $_POST['tanggal_akhir'];

$date_start = new DateTime($tanggal_mulai);
$date_end = new DateTime($tanggal_akhir);


$tgl_mulai = $date_start->format('Y-m-d');
$tgl_akhir = $date_end->format('Y-m-d');

$searchQuery = " ";

if($tanggal_mulai != ''){
 $searchQuery .= " and inputdatetime >= '".$tgl_mulai."' ";
}
if($tanggal_akhir != ''){
 
  $searchQuery .= " and inputdatetime <= '".$tgl_akhir."' ";
}

if($searchValue != ''){
   $searchQuery = " and (custname like '%".$searchValue."%' or 
        custid like '%".$searchValue."%' or 
        distname like'%".$searchValue."%' ) ";
}

## Total number of records without filtering
$sel = mysqli_query($koneksi,"select count(*) as allcount from distributor");
$records = mysqli_fetch_assoc($sel);
$totalRecords = $records['allcount'];

## Total number of record with filtering
$sel = mysqli_query($koneksi,"select count(*) as allcount from distributor WHERE 1 ".$searchQuery);
$records = mysqli_fetch_assoc($sel);
$totalRecordwithFilter = $records['allcount'];

if($rowperpage != '-1'){

    $empQuery = "select * from distributor WHERE 1 ".$searchQuery." order by iddistributor ".$columnSortOrder." limit ".$row.",".$rowperpage;

}else{

    $empQuery = "select * from distributor WHERE 1 ".$searchQuery." order by iddistributor ".$columnSortOrder." LIMIT 0,1000";

}
## Fetch records

$empRecords = mysqli_query($koneksi, $empQuery);
$data = array();

while ($row = mysqli_fetch_assoc($empRecords)) {

   if ($row['status'] == 1) {
     $status = 'Revisit';
   }else{
     $status = 'New Outlet';
  }

  $query2 = "SELECT id_distributor FROM focus_freezer_placement";
  $data2 = mysqli_query($koneksi,$query2);
  $r2 = mysqli_fetch_assoc($data2);

  $current = $row['iddistributor'];
  $noo = $r2['id_distributor'];

  if ($current == $noo) {

    $is_noo = 'YES';
  }else{
    $is_noo = 'NO';
  }

   $data[] = array( 
      "custname"=>$row['custname'],
      "custid"=>$row['custid'],
      "distname"=>$row['distname'],
      "coveragearea"=>$row['coveragearea'],
      "inputdatetime"=>$row['inputdatetime'],
      "sales"=>$row['sales'],
      "status"=>$status,
      "channel"=>$row['channel'],
      "subchannel"=>$row['subchannel'],
      "segment"=>$row['segment'],
      "nameowner"=>$row['nameowner'],
      "noktp"=>$row['noktp'],
      "telephone"=>$row['telephone'],
      "longitude"=>$row['longitude'],
      "latitude"=>$row['latitude'],
      "address"=>$row['address'],
      "province"=>$row['province'],
      "city"=>$row['city'],
      "kecamatan"=>$row['kecamatan'],
      "kelurahan"=>$row['kelurahan'],
      "kodepos"=>$row['kodepos'],
      "timeopen"=>$row['timeopen'],
      "timeclosed"=>$row['timeclosed'],
      "luasbangunan"=>$row['luasbangunan'],
      "paketfreezer"=>$row['paketfreezer'],
      "photo"=>'<a href="'.$row['photo'].'">'.$row['photo'].'</a>',
      "photo2"=>'<a href="'.$row['photo2'].'">'.$row['photo2'].'</a>',
      "photo3"=>'<a href="'.$row['photo3'].'">'.$row['photo3'].'</a>',
      "photo4"=>'<a href="'.$row['photo4'].'">'.$row['photo4'].'</a>',
      "sebutkanlingkungan"=>$row['sebutkanlingkungan'],
      "patokan"=>$row['patokan'],
      "lokasi1"=>$row['lokasi1'],
      "lokasi2"=>$row['lokasi2'],
      "freezerdalamtoko"=>$row['freezerdalamtoko'],
      "merekfreezer"=>$row['merekfreezer'],
      "frozenFood"=>$row['frozenFood'],
      "merekfrozenfood"=>$row['merekfrozenfood'],
      "productutama"=>$row['productutama'],
      "estimasipengunjung"=>$row['estimasipengunjung'],
      "pendapatan"=>$row['pendapatan'],
      "mayoritas"=>$row['mayoritas'],
      "elektricity"=>$row['elektricity'],
      "kondisifisik"=>$row['kondisifisik'],
      "resikofreezerhilang"=>$row['resikofreezerhilang'],
      "tersediatempat"=>$row['tersediatempat'],
      "pilihanfreezeroutlet"=>$row['pilihanfreezeroutlet'],
      "tempatfreezer"=>$row['tempatfreezer'],
      "penilaianlokasi"=>$row['penilaianlokasi'],
      "sebutkanlingkungan"=>$row['sebutkanlingkungan'],
      "targetbulanan"=>$row['targetbulanan'],
      "followupaction"=>$row['followupaction'],
      "alasanissue"=>$row['alasanissue'],
      "statustoko"=>$row['statustoko'],
      "is_noo"=>$is_noo
   );
}

## Response
$response = array(
  "draw" => intval($draw),
  "iTotalRecords" => $totalRecords,
  "iTotalDisplayRecords" => $totalRecordwithFilter,
  "aaData" => $data
);

echo json_encode($response);