<aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href=<?php echo BASE_URL  ?>>
                    <img src="assets/images/icon/logo-aus.png" alt="Cool Admin" />
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">

                        <li
                         <?php 
                         if (isset($_GET['mod'])) {
                               //do nothing
                            }else{
                                echo "class='active'";
                            }
                        ?>
                        >
                            <a href="index.php">
                            <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                        </li>

                        <!-- <li
                         <?php if (isset($_GET['mod'])) {
                            $mod = $_GET['mod'];
                            if ($mod == 'deal') {
                                echo "class='active'";
                            }
                        } ?>
                        >
                            <a href="index.php?mod=deal">
                                <i class="fas fa-heart" style="color:#fc466b"></i>Deal</a>
                        </li>

                        <li
                         <?php if (isset($_GET['mod'])) {
                            $mod = $_GET['mod'];
                            if ($mod == 'postpone') {
                                echo "class='active'";
                            }
                        } ?>
                        >
                            <a href="index.php?mod=postpone">
                                <i class="fas fa-hand-paper"></i>Postpone</a>
                        </li>

                        <li
                         <?php if (isset($_GET['mod'])) {
                            $mod = $_GET['mod'];
                            if ($mod == 'reject') {
                                echo "class='active'";
                            }
                        } ?>
                        >
                            <a href="index.php?mod=reject">
                                <i class="fas fa-times"></i>Reject</a>
                        </li> -->

                        <li
                        <?php if (isset($_GET['mod'])) {
                            $mod = $_GET['mod'];
                            if ($mod == 'deal' or $mod == 'postpone' or $mod == 'reject') {
                                echo "class='active'";
                            }
                        } ?>
                        >
                            <a class="js-arrow" href="#">
                                <i class="fas fa-book"></i>Data Survey</a>
                                <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                     <a href="index.php?mod=deal">
                                    <i class="fas fa-heart" style="color:#fc466b"></i>Deal</a>
                                </li>
                                <li>
                                    <a href="index.php?mod=postpone">
                                <i class="fas fa-hand-paper"></i>Postpone</a>
                                </li>
                                <li>
                                    <a href="index.php?mod=reject">
                                <i class="fas fa-times"></i>Reject</a>
                                </li>
                                <li>
                                    <a href="index.php?mod=all">
                                <i class="fas fa-database"></i>All data</a>
                                </li>
                            </ul>
                        </li>

                        <br><br><br>

                        <li
                         <?php if (isset($_GET['mod'])) {
                            $mod = $_GET['mod'];
                            if ($mod == 'customers') {
                                echo "class='active'";
                            }
                        } ?>
                        >
                            <a class="js-arrow" href="#">
                                <i class="fas fa-users"></i>Customer Master</a>
                                <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <!-- <li>
                                    <a href="index.php?mod=customers&class=add">Add Customer</a>
                                </li> -->
                                <li>
                                    <a href="index.php?mod=customers&class=view">View Customers</a>
                                </li>
                            </ul>
                        </li>

                        <li
                         <?php if (isset($_GET['mod'])) {
                            $mod = $_GET['mod'];
                            if ($mod == 'investors') {
                                echo "class='active'";
                            }
                        } ?>
                        >
                            <a class="js-arrow" href="#">
                                <i class="fas fa-server"></i>Freezer Database</a>
                                <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <!-- <li>
                                    <a href="index.php?mod=investors&class=add">Add Investor</a>
                                </li> -->
                                <li>
                                    <a href="index.php?mod=freezerdatabase&class=view">View Freezer</a>
                                </li>
                            </ul>
                        </li>


                        <!-- <li>
                            <a href="#">
                                <i class="fas fa-calendar-alt"></i>Calendar</a>
                        </li>
                        <li>
                            <a href="map.html">
                                <i class="fas fa-map-marker-alt"></i>Maps</a>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-copy"></i>Pages</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="login.html">Login</a>
                                </li>
                                <li>
                                    <a href="register.html">Register</a>
                                </li>
                                <li>
                                    <a href="forget-pass.html">Forget Password</a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-desktop"></i>UI Elements</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="button.html">Button</a>
                                </li>
                                <li>
                                    <a href="badge.html">Badges</a>
                                </li>
                                <li>
                                    <a href="tab.html">Tabs</a>
                                </li>
                                <li>
                                    <a href="card.html">Cards</a>
                                </li>
                                <li>
                                    <a href="alert.html">Alerts</a>
                                </li>
                                <li>
                                    <a href="progress-bar.html">Progress Bars</a>
                                </li>
                                <li>
                                    <a href="modal.html">Modals</a>
                                </li>
                                <li>
                                    <a href="switch.html">Switchs</a>
                                </li>
                                <li>
                                    <a href="grid.html">Grids</a>
                                </li>
                                <li>
                                    <a href="fontawesome.html">Fontawesome Icon</a>
                                </li>
                                <li>
                                    <a href="typo.html">Typography</a>
                                </li>
                            </ul>
                        </li> -->
                    </ul>
                </nav>
            </div>
        </aside>

