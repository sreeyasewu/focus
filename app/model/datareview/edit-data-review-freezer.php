<?php 

ob_start();
error_reporting(E_ALL);
ini_set('display_errors', 1);

include ('../../config/config.php');

date_default_timezone_set("Asia/Jakarta");


//Post Params 
$iddistributor = mysqli_escape_string($koneksi,$_POST['iddistributor']);  
$distnumber = mysqli_escape_string($koneksi,$_POST['distnumber']);  
$distname = mysqli_escape_string($koneksi,$_POST['distname']);  
$custid = mysqli_escape_string($koneksi,$_POST['custid']);  
$custname = mysqli_escape_string($koneksi,$_POST['custname']);  
$sales = mysqli_escape_string($koneksi,$_POST['sales']);  
$status = mysqli_escape_string($koneksi,$_POST['status']);  
$segment = mysqli_escape_string($koneksi,$_POST['segment']);  
$channel = mysqli_escape_string($koneksi,$_POST['channel']);  
$subchannel = mysqli_escape_string($koneksi,$_POST['subchannel']);  
$segmentnumber = mysqli_escape_string($koneksi,$_POST['segmentnumber']);  
$subchannelnumber = mysqli_escape_string($koneksi,$_POST['subchannelnumber']);  
$account = mysqli_escape_string($koneksi,$_POST['account']);  
$address = mysqli_escape_string($koneksi,$_POST['address']);  
$city = mysqli_escape_string($koneksi,$_POST['city']);  
$kodepos = mysqli_escape_string($koneksi,$_POST['kodepos']);  
$kelurahan = mysqli_escape_string($koneksi,$_POST['kelurahan']);  
$kecamatan = mysqli_escape_string($koneksi,$_POST['kecamatan']);  
$province = mysqli_escape_string($koneksi,$_POST['province']);  
$telephone = mysqli_escape_string($koneksi,$_POST['telephone']);  
$noktp = mysqli_escape_string($koneksi,$_POST['noktp']);  
$contactname = mysqli_escape_string($koneksi,$_POST['contactname']);  
$phone = mysqli_escape_string($koneksi,$_POST['phone']);  
$celular = mysqli_escape_string($koneksi,$_POST['celular']);  
$fax = mysqli_escape_string($koneksi,$_POST['fax']);  
$email = mysqli_escape_string($koneksi,$_POST['email']);  
$npwp = mysqli_escape_string($koneksi,$_POST['npwp']);  
$npwpname = mysqli_escape_string($koneksi,$_POST['npwpname']);  
$npwpaddress = mysqli_escape_string($koneksi,$_POST['npwpaddress']);  
$deliveryname = mysqli_escape_string($koneksi,$_POST['deliveryname']);  
$invoiceaccount = mysqli_escape_string($koneksi,$_POST['invoiceaccount']);  
$creditlimit = mysqli_escape_string($koneksi,$_POST['creditlimit']);  
$top = mysqli_escape_string($koneksi,$_POST['top']);  
$regional = mysqli_escape_string($koneksi,$_POST['regional']);  
$latitude = mysqli_escape_string($koneksi,$_POST['latitude']);  
$longitude = mysqli_escape_string($koneksi,$_POST['longitude']);  
// $photo = mysqli_escape_string($koneksi,$_POST['photo']);  
$id_sales = mysqli_escape_string($koneksi,$_POST['id_sales']); 

$inputdatetime = mysqli_escape_string($koneksi,$_POST['inputdatetime']);  
$inputdatetime = date('Y-m-d H:i:s',strtotime($inputdatetime));

$nameowner = mysqli_escape_string($koneksi,$_POST['nameowner']);  
$jumlahfrezzer = mysqli_escape_string($koneksi,$_POST['jumlahfrezzer']);  
$nobarcode = mysqli_escape_string($koneksi,$_POST['nobarcode']);  
// $photo2 = mysqli_escape_string($koneksi,$_POST['photo2']);  
// $photo3 = mysqli_escape_string($koneksi,$_POST['photo3']);  
$custrefid = mysqli_escape_string($koneksi,$_POST['custrefid']); 
$timestamp = $_POST['timestamp'];

// $timestamp = mysqli_escape_string($koneksi,$_POST['timestamp']);  
$coveragearea = mysqli_escape_string($koneksi,$_POST['coveragearea']);  
$patokan = mysqli_escape_string($koneksi,$_POST['patokan']);  
$lokasi1 = mysqli_escape_string($koneksi,$_POST['lokasi1']);  
$lokasi2 = mysqli_escape_string($koneksi,$_POST['lokasi2']);  
$sebutkanlingkungan = mysqli_escape_string($koneksi,$_POST['sebutkanlingkungan']);  
$timeopen = mysqli_escape_string($koneksi,$_POST['timeopen']);  
$timeclosed = mysqli_escape_string($koneksi,$_POST['timeclosed']);  
$luasbangunan = mysqli_escape_string($koneksi,$_POST['luasbangunan']);  
$freezerdalamtoko = mysqli_escape_string($koneksi,$_POST['freezerdalamtoko']);  
$merekfreezer = mysqli_escape_string($koneksi,$_POST['merekfreezer']);  
$frozenFood = mysqli_escape_string($koneksi,$_POST['frozenFood']);  
$merekfrozenfood = mysqli_escape_string($koneksi,$_POST['merekfrozenfood']);  
$productutama = mysqli_escape_string($koneksi,$_POST['productutama']);  
$productutamakode = mysqli_escape_string($koneksi,$_POST['productutamakode']);  
$estimasipengunjung = mysqli_escape_string($koneksi,$_POST['estimasipengunjung']);  
$pendapatan = mysqli_escape_string($koneksi,$_POST['pendapatan']);  
$mayoritas = mysqli_escape_string($koneksi,$_POST['mayoritas']);  
$elektricity = mysqli_escape_string($koneksi,$_POST['elektricity']);  
$kondisifisik = mysqli_escape_string($koneksi,$_POST['kondisifisik']);  
$resikofreezerhilang = mysqli_escape_string($koneksi,$_POST['resikofreezerhilang']);  
$tersediatempat = mysqli_escape_string($koneksi,$_POST['tersediatempat']);  
$statustoko = mysqli_escape_string($koneksi,$_POST['statustoko']);  
$pilihanfreezeroutlet = mysqli_escape_string($koneksi,$_POST['pilihanfreezeroutlet']);  
$tempatfreezer = mysqli_escape_string($koneksi,$_POST['tempatfreezer']);  
$penilaianlokasi = mysqli_escape_string($koneksi,$_POST['penilaianlokasi']);  
$serviceadditional = mysqli_escape_string($koneksi,$_POST['serviceadditional']);  
$targetbulanan = mysqli_escape_string($koneksi,$_POST['targetbulanan']);  
$notesissue = mysqli_escape_string($koneksi,$_POST['notesissue']);  
$alasanissue = mysqli_escape_string($koneksi,$_POST['alasanissue']);  
$followupaction = mysqli_escape_string($koneksi,$_POST['followupaction']);  
$alasantidaksesuai = mysqli_escape_string($koneksi,$_POST['alasantidaksesuai']);  
$followupdate = mysqli_escape_string($koneksi,$_POST['followupdate']);  
$paketfreezer = mysqli_escape_string($koneksi,$_POST['paketfreezer']);  
// $surveydate = mysqli_escape_string($koneksi,$_POST['surveydate']);  

echo $iddistributor;



 //UPDATE 
 $query = " UPDATE distributor SET distnumber = '$distnumber',  distname = '$distname',  custid = '$custid',  custname = '$custname',  sales = '$sales',  status = '$status',  segment = '$segment',  channel = '$channel',  subchannel = '$subchannel',  segmentnumber = '$segmentnumber',  subchannelnumber = '$subchannelnumber',  account = '$account',  address = '$address',  city = '$city',  kodepos = '$kodepos',  kelurahan = '$kelurahan',  kecamatan = '$kecamatan',  province = '$province',  telephone = '$telephone',  noktp = '$noktp',  contactname = '$contactname',  phone = '$phone',  celular = '$celular',  fax = '$fax',  email = '$email',  npwp = '$npwp',  npwpname = '$npwpname',  npwpaddress = '$npwpaddress',  deliveryname = '$deliveryname',  invoiceaccount = '$invoiceaccount',  creditlimit = '$creditlimit',  top = '$top',  regional = '$regional',  latitude = '$latitude',  longitude = '$longitude', id_sales = '$id_sales', nameowner = '$nameowner',  jumlahfrezzer = '$jumlahfrezzer',  nobarcode = '$nobarcode', custrefid = '$custrefid', coveragearea = '$coveragearea',  patokan = '$patokan',  lokasi1 = '$lokasi1',  lokasi2 = '$lokasi2',  sebutkanlingkungan = '$sebutkanlingkungan',  timeopen = '$timeopen',  timeclosed = '$timeclosed',  luasbangunan = '$luasbangunan',  freezerdalamtoko = '$freezerdalamtoko',  merekfreezer = '$merekfreezer',  frozenFood = '$frozenFood',  merekfrozenfood = '$merekfrozenfood',  productutama = '$productutama',  productutamakode = '$productutamakode',  estimasipengunjung = '$estimasipengunjung',  pendapatan = '$pendapatan',  mayoritas = '$mayoritas',  elektricity = '$elektricity',  kondisifisik = '$kondisifisik',  resikofreezerhilang = '$resikofreezerhilang',  tersediatempat = '$tersediatempat',  statustoko = '$statustoko',  pilihanfreezeroutlet = '$pilihanfreezeroutlet',  tempatfreezer = '$tempatfreezer',  penilaianlokasi = '$penilaianlokasi',  serviceadditional = '$serviceadditional',  targetbulanan = '$targetbulanan',  notesissue = '$notesissue',  alasanissue = '$alasanissue',  followupaction = '$followupaction',  alasantidaksesuai = '$alasantidaksesuai',  followupdate = '$followupdate',  paketfreezer = '$paketfreezer', inputdatetime = '$inputdatetime', timestamp = '$timestamp' WHERE iddistributor = '$iddistributor'"; 

 $result = mysqli_query($koneksi,$query); 

 if( $result )
 {
 	header("location:../../index.php?mod=datareview&class=reject_freezer&status=11"); 
 }
 else
 {
 	header("location:../../index.php?mod=datareview&class=reject_freezer&status=0"); 
 }



?>