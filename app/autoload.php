<?php 

	include ('config/config.php');

    session_start();
    if(!isset($_SESSION["email"]))
    {
        header("Location: login.php");
        exit(); 
    }else{
        include ('part/header.php');
        include ('part/mobile-header.php');
        include ('part/sidebar.php');

        echo '<div class="page-container">';
        include ('part/topbar.php');
        echo '<div class="main-content">';
        include ('route/main.php');
        echo '</div></div>';

        include ('part/footer.php');
    }
    
?>

<script>

    //$(document).ready(function() {

        $('#tableok').DataTable( {
        columnDefs: [
            {
                targets: [ 0, 1, 2 ],
                className: 'mdl-data-table__cell--non-numeric'
            }
        ],

        "deferRender": true,

        

        "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
            // if ( aData[52] == "YES" )
            // {
            //     $('td', nRow).css('background-color', '#007bff');
            //     $('td', nRow).css('color', 'White');
            //     $('td', nRow).css('font-weight', 'bold');
            // }
            if ( aData[51] == "REJECT")
            {
                $('td', nRow).css('background-color', '#9e9e9e');
                $('td', nRow).css('color', 'White');
                $('td', nRow).css('font-weight', 'bold');
            }
            if ( aData[51] == "POSTPONE")
            {
                $('td', nRow).css('background-color', 'background-color: rgba(244, 67, 54, 0.66)');
                $('td', nRow).css('color', 'White');
                $('td', nRow).css('font-weight', 'bold');
            }
            if ( aData[51] == "DEAL")
            {
                $('td', nRow).css('background-color', 'rgba(76, 175, 80, 0.65)');
                $('td', nRow).css('color', 'White');
                $('td', nRow).css('font-weight', 'bold');
            }
            else if ( aData[7] == "Lunas" )
            {
                $('td', nRow).css('background-color', '#007bff');
                $('td', nRow).css('color', 'White');
                $('td', nRow).css('font-weight', 'bold');
            }else if ( aData[12] == "canceled") 
            {
                $('td', nRow).css('background-color', '#FF9800');
                $('td', nRow).css('color', 'White');
                $('td', nRow).css('font-weight', 'bold');
            }
            else if ( aData[53] == "OVERDUE") 
            {
                $('td', nRow).css('background-color', '#FFC107');
                $('td', nRow).css('color', 'White');
                $('td', nRow).css('font-weight', 'bold');
            }
        },

        dom: 'Bfrtip',

        buttons: [
            'copy',
            {
                extend: 'excel',
                messageTop: 'The information in this table is copyright to PT. Sierad Produce Tbk.'
            },
            // {
            //     extend: 'pdf',
            //     messageBottom: null
            // },
            {
                extend: 'print',
                  text: '<i class="fa fa-print"></i> Print',
                  title: '<center>Laporan '+ $('h2').text() + '<hr>',
                  exportOptions: {
                    columns: ':not(.no-print)'
                  },
                  footer: true,
                  autoPrint: true
            }
        ]

    } );

    $('#tableok2').DataTable( {
        columnDefs: [
            {
                targets: [ 0, 1, 2 ],
                className: 'mdl-data-table__cell--non-numeric'
            }
        ],

        "deferRender": true,

        "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
            if ( aData[9] == "cancel" )
            {
                $('td', nRow).css('background-color', '#ff4b5a');
                $('td', nRow).css('color', 'White');
                $('td', nRow).css('font-weight', 'bold');
            }
            else if ( aData[9] == "ongoing" )
            {
                $('td', nRow).css('background-color', '#007bff');
                $('td', nRow).css('color', 'White');
                $('td', nRow).css('font-weight', 'bold');
            }
        },

        dom: 'Bfrtip',

        buttons: [
            'copy',
            {
                extend: 'excel',
                messageTop: 'The information in this table is copyright to PT. Sierad Produce Tbk.'
            },
            // {
            //     extend: 'pdf',
            //     messageBottom: null
            // },
            {
                extend: 'print',
                  text: '<i class="fa fa-print"></i> Print',
                  title: '<center>Laporan '+ $('h2').text() + '<hr>',
                  exportOptions: {
                    columns: ':not(.no-print)'
                  },
                  footer: true,
                  autoPrint: true
            }
        ]

    } );

    // $('#tableok2').DataTable( {
    //     columnDefs: [
    //         {
    //             targets: [ 0, 1, 2 ],
    //             className: 'mdl-data-table__cell--non-numeric'
    //         }
    //     ],

    //     "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
    //         if ( aData[9] == "cancel" )
    //         {
    //             $('td', nRow).css('background-color', '#ff4b5a');
    //             $('td', nRow).css('color', 'White');
    //             $('td', nRow).css('font-weight', 'bold');
    //         }
    //         else if ( aData[9] == "ongoing" )
    //         {
    //             $('td', nRow).css('background-color', '#007bff');
    //             $('td', nRow).css('color', 'White');
    //             $('td', nRow).css('font-weight', 'bold');
    //         }
    //     },

    //     dom: 'Bfrtip',

    //     buttons: [
    //         'copy',
    //         {
    //             extend: 'excel',
    //             messageTop: 'The information in this table is copyright to PT. Sierad Produce Tbk.'
    //         },
    //         // {
    //         //     extend: 'pdf',
    //         //     messageBottom: null
    //         // },
    //         {
    //             extend: 'print',
    //               text: '<i class="fa fa-print"></i> Print',
    //               title: '<center>Laporan '+ $('h2').text() + '<hr>',
    //               exportOptions: {
    //                 columns: ':not(.no-print)'
    //               },
    //               footer: true,
    //               autoPrint: true
    //         }
    //     ]

    // } );
    //});
    
   
    

</script>