<?php 
include ('config/config.php');
require_once "library/vendor/autoload.php";

function clean($string) {
    $string = str_replace(' ', ' ', $string);
    $string = preg_replace('/[^A-Za-z0-9\-ığşçöüÖÇŞİıĞ]/', ' ', $string);

    return preg_replace('/-+/', '-', $string);
}

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Style\Style;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Cell\AdvancedValueBinder;


$spreadsheet = new Spreadsheet();
$Excel_writer = new Xls($spreadsheet);

$spreadsheet->setActiveSheetIndex(0);
$activeSheet = $spreadsheet->getActiveSheet();

$activeSheet->setCellValue('A1', 'ID Freezer');
$activeSheet->setCellValue('B1', 'Outletname');
$activeSheet->setCellValue('C1', 'IDBelfoodd');
$activeSheet->setCellValue('D1', 'Outlet Status');
$activeSheet->setCellValue('E1', 'Distributor ID');
$activeSheet->setCellValue('F1', 'Distirbutor Name');
$activeSheet->setCellValue('G1', 'RSM Name');
$activeSheet->setCellValue('H1', 'Area');
$activeSheet->setCellValue('I1', 'PIC');
$activeSheet->setCellValue('J1', 'Phone');
$activeSheet->setCellValue('K1', 'Alamat');
$activeSheet->setCellValue('L1', 'Kelurahan');
$activeSheet->setCellValue('M1', 'Kecamatan');
$activeSheet->setCellValue('N1', 'Kab Kota');
$activeSheet->setCellValue('O1', 'Propinsi');
$activeSheet->setCellValue('P1', 'Kode Pos');
$activeSheet->setCellValue('Q1', 'Freezer Owner');
$activeSheet->setCellValue('R1', 'Supplier');
$activeSheet->setCellValue('S1', 'Brand');
$activeSheet->setCellValue('T1', 'Batch PO');
$activeSheet->setCellValue('U1', 'Type');
$activeSheet->setCellValue('V1', 'ID Number');
$activeSheet->setCellValue('W1', 'Type Switching');
$activeSheet->setCellValue('X1', 'ID Switching');
$activeSheet->setCellValue('Y1', 'Freezer Code');
$activeSheet->setCellValue('Z1', 'Barcode FA Number');
$activeSheet->setCellValue('AA1', 'Status Freezer');
$activeSheet->setCellValue('AB1', 'Tanggal Pasang');
$activeSheet->setCellValue('AC1', 'Tanggal Penarikan');
$activeSheet->setCellValue('AD1', 'Keterangan');
$activeSheet->setCellValue('AE1', 'Customer Type');
$activeSheet->setCellValue('AF1', 'Principal');
$activeSheet->setCellValue('AG1', 'IDNumber');

$query = "SELECT * FROM focus_freezer_master";
$data = mysqli_query($koneksi,$query);
$i = 2;
$customer_type = "";
$IDNumber = "";
$RSM_NAME = "";

while ($result = mysqli_fetch_assoc($data)) {

    $cari = "SELECT IDNumber FROM distributor INNER JOIN focus_freezer_placement ON distributor.iddistributor=focus_freezer_placement.id_distributor WHERE distributor.custid = '".$result['IDBelfoodd']."'";
    $d = mysqli_query($koneksi,$cari);
    $r = mysqli_fetch_assoc($d);

    $cari_rsm = "SELECT RSM_NAME FROM MDS_CustomerPrimary p WHERE p.Code = '".$result['DistributorID']."'";
    $result_rsm = mysqli_query($koneksi,$cari_rsm);
    $row_rsm = mysqli_fetch_assoc($result_rsm);


    if(isset($r['IDNumber']))
    {
        $IDNumber = $r['IDNumber'];
    }
    else
    {
        $IDNumber = '';
    }

    if(isset($row_rsm['RSM_NAME']))
    {
        $RSM_NAME = $row_rsm['RSM_NAME'];
    }
    else
    {
        $RSM_NAME = '';
    }

    if ($result['Customer_Type'] == '01 {DIRECT}') {
        $customer_type = 'PRIMARY';
    }else{
        $customer_type = "SECONDARY";
    }

      
        $activeSheet->setCellValue('A'.$i , ''.$result['id_freezer'].'');
        $activeSheet->setCellValue('B'.$i , ''.$result['Outletname'].'');
        $activeSheet->setCellValue('C'.$i , ''.$result['IDBelfoodd'].'');
        $activeSheet->setCellValue('D'.$i , ''.$result['Outlet_Status'].'');
        $activeSheet->setCellValue('E'.$i , ''.$result['DistributorID'].'');
        $activeSheet->setCellValue('F'.$i , ''.$result['DistributorName'].'');
        $activeSheet->setCellValue('G'.$i , ''.$RSM_NAME.'');
        $activeSheet->setCellValue('H'.$i , ''.$result['Area'].'');
        $activeSheet->setCellValue('I'.$i , ''.$result['PIC'].'');
        $activeSheet->setCellValue('J'.$i , "".clean($result['Phone'])."");
        $activeSheet->setCellValue('K'.$i , ''.clean($result['Alamat']).'');
        $activeSheet->setCellValue('L'.$i , ''.$result['Kelurahan'].'');
        $activeSheet->setCellValue('M'.$i , ''.$result['Kecamatan'].'');
        $activeSheet->setCellValue('N'.$i , ''.$result['KabKota'].'');
        $activeSheet->setCellValue('O'.$i , ''.$result['Propinsi'].'');
        $activeSheet->setCellValue('P'.$i , ''.$result['KodePos'].'');
        $activeSheet->setCellValue('Q'.$i , ''.$result['Freezer_Owner'].'');
        $activeSheet->setCellValue('R'.$i , ''.$result['Supplier'].'');
        $activeSheet->setCellValue('S'.$i , ''.$result['Brand'].'');
        $activeSheet->setCellValue('T'.$i , ''.$result['BatchPO'].'');
        $activeSheet->setCellValue('U'.$i , ''.$result['Type'].'');
        $activeSheet->setCellValueExplicit("V".$i , "".$result['ID_Number']."",DataType::TYPE_STRING);
        $activeSheet->setCellValue('W'.$i , ''.$result['Type_Switching'].'');
        $activeSheet->setCellValue('X'.$i , ''.$result['ID_Switching'].'');
        $activeSheet->setCellValue('Y'.$i , ''.$result['FreezerCode'].'');
        $activeSheet->setCellValue('Z'.$i , ''.$result['BarcodeFANumber'].'');
        $activeSheet->setCellValue('AA'.$i , ''.$result['StatusFreezer'].'');
        $activeSheet->setCellValue('AB'.$i , ''.$result['TanggalPasang'].'');
        $activeSheet->setCellValue('AC'.$i , ''.$result['TanggalPenarikan'].'');
        $activeSheet->setCellValue('AD'.$i , ''.$result['Keterangan'].'');
        $activeSheet->setCellValue('AE'.$i , ''.$customer_type.'');
        $activeSheet->setCellValue('AF'.$i , ''.$result['principal'].'');
        $activeSheet->setCellValue('AG'.$i , ''.$IDNumber.'');

        //$activeSheet->getStyle('V2')->getNumberFormat() ->setFormatCode('0000');

        $i++;

}

/*
$activeSheet->getColumnDimension('A')->setAutoSize(true);
$activeSheet->getColumnDimension('B')->setAutoSize(true);
$activeSheet->getColumnDimension('C')->setAutoSize(true);
$activeSheet->getColumnDimension('D')->setAutoSize(true);
$activeSheet->getColumnDimension('E')->setAutoSize(true);
$activeSheet->getColumnDimension('F')->setAutoSize(true);
$activeSheet->getColumnDimension('G')->setAutoSize(true);
$activeSheet->getColumnDimension('H')->setAutoSize(true);
$activeSheet->getColumnDimension('I')->setAutoSize(true);
$activeSheet->getColumnDimension('J')->setAutoSize(true);
$activeSheet->getColumnDimension('K')->setAutoSize(true);
$activeSheet->getColumnDimension('L')->setAutoSize(true);
$activeSheet->getColumnDimension('M')->setAutoSize(true);
$activeSheet->getColumnDimension('N')->setAutoSize(true);
$activeSheet->getColumnDimension('O')->setAutoSize(true);
$activeSheet->getColumnDimension('P')->setAutoSize(true);
$activeSheet->getColumnDimension('Q')->setAutoSize(true);
$activeSheet->getColumnDimension('R')->setAutoSize(true);
$activeSheet->getColumnDimension('S')->setAutoSize(true);
$activeSheet->getColumnDimension('T')->setAutoSize(true);
$activeSheet->getColumnDimension('U')->setAutoSize(true);
$activeSheet->getColumnDimension('V')->setAutoSize(true);
$activeSheet->getColumnDimension('W')->setAutoSize(true);
$activeSheet->getColumnDimension('X')->setAutoSize(true);
$activeSheet->getColumnDimension('Y')->setAutoSize(true);
$activeSheet->getColumnDimension('Z')->setAutoSize(true);
$activeSheet->getColumnDimension('AA')->setAutoSize(true);
$activeSheet->getColumnDimension('AB')->setAutoSize(true);
$activeSheet->getColumnDimension('AC')->setAutoSize(true);
$activeSheet->getColumnDimension('AD')->setAutoSize(true);
$activeSheet->getColumnDimension('AE')->setAutoSize(true);
$activeSheet->getColumnDimension('AF')->setAutoSize(true);
$activeSheet->getColumnDimension('AG')->setAutoSize(true);
$activeSheet->getStyle("A1:AG1")->getFont()->setBold(true);
*/

$date = date("d-m-Y");
$filename = 'exportAllFreezer'.$date.'.xls';

/*
$Excel_writer->setDelimiter(';');
$Excel_writer->setEnclosure('"');
$Excel_writer->setLineEnding("\r\n");
$Excel_writer->setSheetIndex(0);
*/
header('Content-Type: application/vnd.ms-excel');
//header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment;filename='. $filename);
header('Cache-Control: max-age=0');
//$Excel_writer->setUseBOM(true);
//$Excel_writer->setPreCalculateFormulas(false);
$Excel_writer->save('php://output');
exit();
?>
