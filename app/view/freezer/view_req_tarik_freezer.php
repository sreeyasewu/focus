<?php 
    $IDNumber = '';
    if (isset($_GET['status'])) {
        $status = $_GET['status'];

        if ($status == 1) {
            echo '<div class="alert alert-success" role="alert">
                Request telah dikirim ke vendor
            </div>';
        }elseif ($status == 0) {
            echo '<div class="alert alert-danger" role="alert">
                Terjadi Kesalahan! tidak dapat menghapus mengirim request ke vendor
            </div>';
        }else{
            echo '<div class="alert alert-warning" role="alert">
                Terjadi kesalahan koneksi internet, silkahkan ulangi kembali
            </div>';
        }
    }

    // if (isset($_GET['editstatus'])) {
    //     $status = $_GET['editstatus'];

    //     if ($status == 1) {
    //         echo '<div class="alert alert-success" role="alert">
    //             Data product berhasil di perbaruhi
    //         </div>';
    //     }elseif ($status == 0) {
    //         echo '<div class="alert alert-danger" role="alert">
    //             Terjadi Kesalahan! tidak dapat mengubah data product
    //         </div>';
    //     }else{
    //         echo '<div class="alert alert-warning" role="alert">
    //             Terjadi kesalahan koneksi internet, silkahkan ulangi kembali
    //         </div>';
    //     }
    // }
 ?>
<div class="row">
<div class="col-md-12">
        <!-- DATA TABLE -->
        <h3 class="title-5 m-b-35">view Freezers</h3>
        <div class="table-data__tool">
          
        </div>

        <div>
            <form action="" method="POST">
            <div class="row form-group">
                <div class="col-sm-3 col-md-2">
                    <label for="text-input" class=" form-control-label"><b>Filter by date</b></label>
                </div>
                <div class="col-sm-3 col-md-2">
                    <input type="text" id="tanggal_mulai" name="tanggal_mulai" class="form-control" placeholder="Mulai" required>
                </div>
                <div class="col-sm-3 col-md-2">
                    <input type="text" id="tanggal_akhir" name="tanggal_akhir" class="form-control" placeholder="Akhir" required>
                    
                </div>
                <button type="submit" name="custom" value="custom" class="btn btn-sm btn-primary">
                    Filter
                </button>
            </div>


            </form>
            <hr>

            <form action="" method="POST">
            <div class="row form-group">
                <div class="col-sm-3 col-md-2">
                    <label for="text-input" class=" form-control-label"><b>Full Search</b></label>
                </div>
                <div class="col-sm-3 col-md-2">
                    <input type="text" name="search" class="form-control" placeholder="By nama outlet atau kode bfi" required>
                </div>
                <div class="col-sm-3 col-md-2">
                <button type="submit" name="fullsearch" value="custom" class="btn btn-sm btn-primary form-control">
                    Filter
                </button>
                </div>
            </div>
            <hr>
            <br>
            </form>
        </div>

        <?php if (isset($_POST['custom'])): ?>
            
            <?php 
                $tanggalMulai = mysqli_escape_string($koneksi,$_POST['tanggal_mulai']);

                $tanggalSelesai = mysqli_escape_string($koneksi,$_POST['tanggal_akhir']);

                $tanggalMulaiEdit = strtotime($tanggalMulai);

                $tanggalMulai= date('Y-m-d', $tanggalMulaiEdit);

                $tanggalSelesaiEdit = strtotime($tanggalSelesai);

                $tanggalSelesai= date('Y-m-d', $tanggalSelesaiEdit);
             ?>
        <div class="table-responsive table-data" style="height: auto">
            <table id="tableok" class="mdl-data-table" style="width:100%">
                <thead>
                    <tr>
                       
                        <td>Action</td>
                        <td>No</td>
                        <td>Outlet Name</td>
                        <td>BFI Code</td>
                        <td>Freezer Code</td>
                        <td>ID Number</td>
                        <td>Freezer Status</td>
                        <td>Distributor Name</td>
                        <td>Alamat</td>
                        <td>Kelurahan</td>
                        <td>Kecamatan</td>
                        <td>Kab/Kota</td>
                        <td>Provinsi</td>
                        <td>Email to Vendor Date</td>
                        <td>Placement Date</td>
                        <td>Removal/Switch Date</td>
                        <td>Removal/Switch Status</td>
                        <td>Type (Current)</td>
                        <td>Barcode (Current)</td>
                        <td>Type (Old)</td>
                        <td>Barcode (Old)</td>
                        <td>Supplier Name</td>
                        <td>BarcodeFA</td>
                        <td>Removal/Switch Reason</td>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $no = 1;
                        include ('config/config.php');
                        $query = "SELECT * FROM focus_freezer_request_penarikan WHERE tanggalPasang >= '$tanggalMulai' and tanggalPasang <= '$tanggalSelesai' GROUP BY FreezerCode ORDER BY id_freezer DESC";
                        $data = mysqli_query($koneksi,$query);

                        while ($result = mysqli_fetch_assoc($data)) {
                     ?>
                    <tr>

                        <?php 
                            if ($_SESSION['role'] == 'VENDOR') {
                                
                                $query = "SELECT * FROM focus_freezer_placement, distributor WHERE focus_freezer_placement.id_distributor = distributor.iddistributor AND distributor.custid = '".$result['IDBelfoodd']."' ORDER BY id_distributor DESC LIMIT 0,1";
                                $dq1 = mysqli_query($koneksi,$query);
                                $result_q = mysqli_fetch_assoc($dq1);

                                if($result_q['IDNumber'] == '')
                                {
                                    $IDNumber = '';
                                }
                                else
                                {
                                    $IDNumber = $result_q['IDNumber']; 
                                }

                            }else{
                         ?>
                        <td>

                            <?php 
                                //cari apakah requst sudah dibuat 
                                $id_freezer = $result['id_freezer'];
                                $q = "SELECT * FROM freezer_request WHERE id_freezer = '$id_freezer' ORDER BY id_request DESC LIMIT 0,1";
                                $dq = mysqli_query($koneksi,$q);
                                $rq = mysqli_fetch_assoc($dq);

                                $query = "SELECT * FROM focus_freezer_placement, distributor WHERE focus_freezer_placement.id_distributor = distributor.iddistributor AND distributor.custid = '".$result['IDBelfoodd']."' ORDER BY id_distributor DESC LIMIT 0,1";
                                $dq1 = mysqli_query($koneksi,$query);
                                $result_q = mysqli_fetch_assoc($dq1);

                                if($result_q['IDNumber'] == '')
                                {
                                    $IDNumber = '';
                                }
                                else
                                {
                                    $IDNumber = $result_q['IDNumber']; 
                                }

                                if ($rq['id_freezer'] == '') {
                            ?>
                             
                            <form action="model/freezermanagement/switch.php" method="post" class="form-tindaklanjut" id="form-tindaklanjut-<?php echo $result['id_freezer'] ?>">
                                            <input type="hidden" id="id" name="id_freezer" value="<?php echo $result['id_freezer'] ?>">
                                           
                                            <h5>Vendor</h5>

                                            
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <small>Keterangan (masukkan jika ada)</small>
                                                <br>
                                                    <input type="text" id="ket-tindaklanjut" name="keterangan" class="form-control" required>
                                                </div>
                                            </div>
                                            

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <small>Nama Vendor</small>
                                                <br>
                                                    <!-- <input type="text" id="namavendor" name="namavendor" class="form-control" required> -->

                                                    <select name="namavendor" id="namavendorselect<?php echo $result['id_freezer'] ?>" class="select_database form-control namavendorselect" onChange="selectvendor(<?php echo $result['id_freezer'] ?>);" required>
                                                    <option >Pilih Nama Vendor</option>

                                                    <?php 
                                                        $query_user = "SELECT * FROM user WHERE role = 'VENDOR' GROUP BY nama";
                                                        $data_user = mysqli_query($koneksi,$query_user);
                                                        while ($result_user = mysqli_fetch_assoc($data_user)) {
                                                            
                                                     ?>
                                                        <option value="<?php echo $result_user['nama'] ?>"><?php echo $result_user['nama'] ?></option>

                                                    <?php } ?>

                                                    </select>
                                                </div>
                                            </div>

                                            <br>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <select name="areavendor" id="areavendor<?php echo $result['id_freezer'] ?>" class="select_database form-control areavendor" required>
                                                </select>
                                                    <!--
                                                    <select name="areavendor" id="namavendor" class="select_database form-control" required>
                                                    <option>Pilih Area</option>

                                                    <?php 

                                                        $acc_origin = $result['Supplier'];
                                                        $acc_origin = str_replace("1","",$acc_origin);
                                                        $acc_origin = str_replace("2","",$acc_origin);
                                                        $acc_origin = str_replace(" ","",$acc_origin);
                                                        $acc_origin = str_replace("{","",$acc_origin);  
                                                        $acc_origin = str_replace("PT","",$acc_origin);    
                                                        $acc_origin = str_replace("}","",$acc_origin);

                                                        $query_user = "SELECT area FROM user WHERE role = 'VENDOR' AND nama = '$acc_origin' GROUP BY area";
                                                        $data_user = mysqli_query($koneksi,$query_user);
                                                        while ($result_user = mysqli_fetch_assoc($data_user)) {
                                                            
                                                     ?>
                                                        <option value="<?php echo $result_user['area'] ?>"><?php echo $result_user['area'] ?></option>

                                                    <?php } ?>

                                                </select>
                                                -->
                                                
                                                </div>
                                            </div>
                                            <br>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button type="submit" class="btn btn-sm btn-success">
                                                        Send Request
                                                    </button>
                                               
                                                    <a href="javascript:void();" class="btn btn-sm btn-dark batal-tindaklanjut">Batal</a>
                                                </div>
                                             </div>
                                                
                                            </div>
                                        </form>
                                        
                                        <a href="javascript:void();" class="btn btn-sm btn-success tindaklanjut" id="rejectbtn" data-toggle="tooltip" data-placement="top" title="Switch Freezer" acuan="<?php echo $result['id_freezer'] ?>"><i class="fas fa fa-exchange-alt"> </i></a> 
                                         
                                         <!-- approved -->
                                         
                                         
                                        <form action="model/freezermanagement/remove.php" method="post" class="form-tindaklanjut" id="form-approved-<?php echo $result['id_freezer']?>">
                                            <input type="hidden" id="id" name="id_freezer" value="<?php echo $result['id_freezer'] ?>">
                                            <input type="hidden" id="supplier" name="supplier" value="<?php echo $result['Supplier'] ?>">
                                            <h5>Vendor</h5>

                                           
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <small>Keterangan (masukkan jika ada)</small>
                                                <br>
                                                    <input type="text" id="idnumber" name="keterangan" class="form-control" required>
                                                </div>
                                            </div>
                                            

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <small>Nama Vendor</small>
                                                <br>
                                                    <!-- <input type="text" id="namavendor" name="namavendor" class="form-control" required> -->

                                                    <select name="namavendorRemove" id="namavendorremoveselect<?php echo $result['id_freezer'] ?>" class="select_database form-control namavendorremoveselect" onChange="selectvendorRemove(<?php echo $result['id_freezer'] ?>);" required>
                                                    <option >Pilih Nama Vendor</option>

                                                    <?php 
                                                        $query_user = "SELECT * FROM user WHERE role = 'VENDOR' GROUP BY nama";
                                                        $data_user = mysqli_query($koneksi,$query_user);
                                                        while ($result_user = mysqli_fetch_assoc($data_user)) {
                                                            
                                                     ?>
                                                        <option value="<?php echo $result_user['nama'] ?>"><?php echo $result_user['nama'] ?></option>

                                                    <?php } ?>

                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <small>Area Vendor</small>
                                                <br>
                                                    
                                                   <select name="areavendorRemove" id="areavendorRemove<?php echo $result['id_freezer'] ?>" class="select_database form-control areavendorRemove" required>
                                                    </select>
                                                </div>
                                            </div>

                                            <!-- <div id="areavendor"></div>  -->

                                         
                                            <!--
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <select name="areavendor" id="namavendor" class="select_database form-control" required>
                                                    <option>Pilih Area</option>

                                                    <?php 
                                                        $acc_origin = $result['Supplier'];
                                                        $acc_origin = str_replace("1","",$acc_origin);
                                                        $acc_origin = str_replace("2","",$acc_origin);
                                                        $acc_origin = str_replace(" ","",$acc_origin);
                                                        $acc_origin = str_replace("{","",$acc_origin);  
                                                        $acc_origin = str_replace("PT","",$acc_origin);    
                                                        $acc_origin = str_replace("}","",$acc_origin);

                                                        $query_user = "SELECT area FROM user WHERE role = 'VENDOR' AND nama = '$acc_origin' GROUP BY area";
                                                        $data_user = mysqli_query($koneksi,$query_user);
                                                        while ($result_user = mysqli_fetch_assoc($data_user)) {
                                                            
                                                     ?>
                                                        <option value="<?php echo $result_user['area'] ?>"><?php echo $result_user['area'] ?></option>

                                                    <?php } ?>

                                                </select>
                                                </div>
                                            </div>
                                            -->                         
                                            <br>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button type="submit" class="btn btn-sm btn-success">
                                                        Send Request
                                                    </button>
                                               
                                                    <a href="javascript:void();" class="btn btn-sm btn-dark batal-tindaklanjut">Batal</a>
                                                </div>
                                             </div>
                                                
                                            </div>
                                        </form>
                                        

                                        <a href="javascript:void();" id="approved_placement_<?php echo $result['id_freezer']?>" class="btn btn-sm btn-warning approved_placement" data-toggle="tooltip" data-placement="top" title="Remove Freezer" acuan="<?php echo $result['id_freezer']?>">
                                            <i class="fas fa fa-minus"></i>
                                        </a>

                            <?php 
                                }else{

                                //cari lagi apakah sudah di proses atau belum
                                    if ($rq['status'] == 'success' or $rq['status'] ==  'FAILED') {

                                        if ($rq['action'] == 'remove' AND $rq['status'] == 'success') {
                                            echo '<a href="index.php?mod=freezer&class=add&id='.$result['IDBelfoodd'].'&freezerid='.$result['id_freezer'].'" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Add Freezer"><i class="fas fa fa-plus"> </i></a>';
                                        }else{
                                            ?>
                                            <form action="model/freezermanagement/switch.php" method="post" class="form-tindaklanjut" id="form-tindaklanjut-<?php echo $result['id_freezer'] ?>">
                                            <input type="hidden" id="id" name="id_freezer" value="<?php echo $result['id_freezer'] ?>">
                                            <input type="hidden" id="supplier" name="supplier" value="<?php echo $result['Supplier'] ?>">
                                            <h5>Vendor</h5>

                                           
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <small>Keterangan (masukkan jika ada)</small>
                                                <br>
                                                    <input type="text" id="ket-tindaklanjut" name="keterangan" class="form-control" required>
                                                </div>
                                            </div>
                                          

                                             <div class="row">
                                    <div class="col-md-12">
                                        <small>Nama Vendor</small>
                                    <br>
                                        <!-- <input type="text" id="namavendor" name="namavendor" class="form-control" required> -->

                                        <select name="namavendor" id="namavendorselect<?php echo $result['id_freezer'] ?>" class="select_database form-control namavendorselect" onChange="selectvendor(<?php echo $result['id_freezer'] ?>);" required>
                                        <option >Pilih Nama Vendor</option>

                                        <?php 
                                            $query_user = "SELECT * FROM user WHERE role = 'VENDOR' GROUP BY nama";
                                            $data_user = mysqli_query($koneksi,$query_user);
                                            while ($result_user = mysqli_fetch_assoc($data_user)) {
                                                
                                         ?>
                                            <option value="<?php echo $result_user['nama'] ?>"><?php echo $result_user['nama'] ?></option>

                                        <?php } ?>

                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <small>Area Vendor</small>
                                    <br>
                                        
                                        <select name="areavendor" id="areavendor<?php echo $result['id_freezer'] ?>" class="select_database form-control areavendor" required>
                                        </select>
                                    </div>
                                </div>

                                <!-- <div id="areavendor"></div>  -->

                                <div class="col-12 col-md-9">
                                    <input type="text" name="namavendor_pilih" id="namavendor_pilih<?php echo $result['id_freezer'] ?>" class="form-control namavendor_pilih" hidden>
                                </div>

                                            <!--
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <select name="areavendor" id="namavendor" class="select_database form-control" required>
                                                    <option>Pilih Area</option>

                                                    <?php 
                                                        $acc_origin = $result['Supplier'];
                                                        $acc_origin = str_replace("1","",$acc_origin);
                                                        $acc_origin = str_replace("2","",$acc_origin);
                                                        $acc_origin = str_replace(" ","",$acc_origin);
                                                        $acc_origin = str_replace("{","",$acc_origin);  
                                                        $acc_origin = str_replace("PT","",$acc_origin);    
                                                        $acc_origin = str_replace("}","",$acc_origin);

                                                        $query_user = "SELECT area FROM user WHERE role = 'VENDOR' AND nama = '$acc_origin' GROUP BY area";
                                                        $data_user = mysqli_query($koneksi,$query_user);
                                                        while ($result_user = mysqli_fetch_assoc($data_user)) {
                                                            
                                                     ?>
                                                        <option value="<?php echo $result_user['area'] ?>"><?php echo $result_user['area'] ?></option>

                                                    <?php } ?>

                                                </select>
                                                </div>
                                            </div>
                                            -->
                                            <br>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button type="submit" class="btn btn-sm btn-success">
                                                        Send Request
                                                    </button>
                                               
                                                    <a href="javascript:void();" class="btn btn-sm btn-dark batal-tindaklanjut">Batal</a>
                                                </div>
                                             </div>
                                                
                                            </div>
                                        </form>
                                        
                                        <a href="javascript:void();" class="btn btn-sm btn-success tindaklanjut" id="rejectbtn" data-toggle="tooltip" data-placement="top" title="Switch Freezer" acuan="<?php echo $result['id_freezer'] ?>"><i class="fas fa fa-exchange-alt"> </i></a> 
                                         
                                         <!-- approved -->
                                         
                                         
                                        <form action="model/freezermanagement/remove.php" method="post" class="form-tindaklanjut" id="form-approved-<?php echo $result['id_freezer']?>">
                                            <input type="hidden" id="id" name="id_freezer" value="<?php echo $result['id_freezer'] ?>">
                                            <input type="hidden" id="supplier" name="supplier" value="<?php echo $result['Supplier'] ?>">
                                            <h5>Vendor</h5>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <small>Keterangan (masukkan jika ada)</small>
                                                <br>
                                                    <input type="text" id="idnumber" name="keterangan" class="form-control" required>
                                                </div>
                                            </div>

                                           <div class="row">
                                                <div class="col-md-12">
                                                    <small>Nama Vendor</small>
                                                <br>
                                                    <!-- <input type="text" id="namavendor" name="namavendor" class="form-control" required> -->

                                                    <select name="namavendorRemove" id="namavendorremoveselect<?php echo $result['id_freezer'] ?>" class="select_database form-control namavendorremoveselect" onChange="selectvendorRemove(<?php echo $result['id_freezer'] ?>);" required>
                                                    <option >Pilih Nama Vendor</option>

                                                    <?php 
                                                        $query_user = "SELECT * FROM user WHERE role = 'VENDOR' GROUP BY nama";
                                                        $data_user = mysqli_query($koneksi,$query_user);
                                                        while ($result_user = mysqli_fetch_assoc($data_user)) {
                                                            
                                                     ?>
                                                        <option value="<?php echo $result_user['nama'] ?>"><?php echo $result_user['nama'] ?></option>

                                                    <?php } ?>

                                                    </select>
                                                </div>
                                            </div>

                                            <br>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <select name="areavendorRemove" id="areavendorRemove<?php echo $result['id_freezer'] ?>" class="select_database form-control areavendorRemove" required>
                                                </select>
                                                    <!--
                                                    <select name="areavendor" id="namavendor" class="select_database form-control" required>
                                                    <option>Pilih Area</option>

                                                    <?php 

                                                        $acc_origin = $result['Supplier'];
                                                        $acc_origin = str_replace("1","",$acc_origin);
                                                        $acc_origin = str_replace("2","",$acc_origin);
                                                        $acc_origin = str_replace(" ","",$acc_origin);
                                                        $acc_origin = str_replace("{","",$acc_origin);  
                                                        $acc_origin = str_replace("PT","",$acc_origin);    
                                                        $acc_origin = str_replace("}","",$acc_origin);

                                                        $query_user = "SELECT area FROM user WHERE role = 'VENDOR' AND nama = '$acc_origin' GROUP BY area";
                                                        $data_user = mysqli_query($koneksi,$query_user);
                                                        while ($result_user = mysqli_fetch_assoc($data_user)) {
                                                            
                                                     ?>
                                                        <option value="<?php echo $result_user['area'] ?>"><?php echo $result_user['area'] ?></option>

                                                    <?php } ?>

                                                </select>
                                                -->
                                                
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button type="submit" class="btn btn-sm btn-success">
                                                        Send Request
                                                    </button>
                                               
                                                    <a href="javascript:void();" class="btn btn-sm btn-dark batal-tindaklanjut">Batal</a>
                                                </div>
                                             </div>
                                                
                                            </div>
                                        </form>
                                        

                                        <a href="javascript:void();" id="approved_placement_<?php echo $result['id_freezer']?>" class="btn btn-sm btn-warning approved_placement" data-toggle="tooltip" data-placement="top" title="Remove Freezer" acuan="<?php echo $result['id_freezer']?>">
                                            <i class="fas fa fa-minus"></i>
                                        </a>

                                        
                                        

                                <?php  
                                        }

                                    }else{

                                        echo $rq['status'];
                                    }
                                }


                             ?>
                            
                        </td>
                        <?php 
                            }
                         ?>
                        
                        <td><?php echo $no ?></td>
                        <td>
                            <?php echo $result['Outletname'] ?>
                        </td>
                        <td>
                            <?php echo $result['IDBelfoodd'] ?>
                        </td>
                        <td>
                            <?php echo $result['FreezerCode'] ?>
                        </td>
                        <td>
                            <?php echo $IDNumber ?>
                        </td>
                        <td>
                            <?php echo $result['StatusFreezer'] ?>
                        </td>
                        <td>
                            <?php echo $result['DistributorName'] ?>
                        </td>
                        <td>
                            <?php echo $result['Alamat'] ?>
                        </td>
                        <td>
                            <?php echo $result['Kelurahan'] ?>
                        </td>
                        <td>
                            <?php echo $result['Kecamatan'] ?>
                        </td>

                        <td>
                            <?php echo $result['KabKota'] ?>
                        </td>
                        <td>
                            <?php echo $result['provinsi'] ?>
                        </td>
                        <td>
                            <?php echo $result['TanggalPasang'] ?>
                        </td>
                        <td>
                            <?php echo $result['TanggalPasang'] ?>
                        </td>
                        <td>
                            <?php echo $result['TanggalPenarikan'] ?>
                        </td>
                        <td>
                            <?php 
                                $id_freezer = $result['id_freezer'];
                                $cari = "SELECT action, status from freezer_request WHERE id_freezer = '$id_freezer' ORDER BY id_request DESC LIMIT 0,1";
                                $qc = mysqli_query($koneksi,$cari);
                                $rc = mysqli_fetch_assoc($qc);

                                $status = $rc['status'];
                                $action = $rc['action'];

                                if ($status == 'success') {
                                    if ($action == 'remove') {
                                        echo "Removal Successful";
                                    }elseif($action == 'switch'){
                                        echo "Switch Successful";
                                    }else{
                                        //show nothing
                                    }
                                }elseif ($status == 'FAILED') {
                                    if ($action == 'remove') {
                                        echo "Removal Failed";
                                    }elseif($action == 'switch'){
                                        echo "Switch Failed";
                                    }else{
                                        //show nothing
                                    }
                                }else{
                                    echo $status;
                                }
                             ?>
                        </td>
                        <td>
                            <?php echo $result['Type'] ?>
                        </td>
                        <td>
                            <?php echo $result['ID_Number'] ?>
                        </td>
                        <td>
                            <?php echo $result['Type_Switching'] ?>
                        </td>
                        <td>
                            <?php echo $result['ID_Switching'] ?>
                        </td>
                        <td>
                            <?php echo $result['Supplier'] ?>
                        </td>
                        <td>
                            <?php echo $result['BarcodeFANumber'] ?>
                        </td>
                        <td>
                            <?php echo $result['Keterangan'] ?>
                        </td>
                        <!-- <td>
                            <div class="table-data-feature">
                                
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <a href="index.php?mod=users&class=edit&id=<?php echo $result['id_user']; ?>" ><i class="zmdi zmdi-edit"></i></a>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <a href="index.php?mod=users&class=delete&id=<?php echo $result['id_user'] ?>"><i class="zmdi zmdi-delete"></i></a>
                                </button>
                            </div>
                        </td> -->
                    </tr>

                     <?php $no++; } ?>
                </tbody>
            </table>
        </div>
        <?php else: ?>

             <?php if (isset($_POST['fullsearch'])): ?>
            <div class="table-responsive table-data" style="height: auto">
            <table id="tableok" class="mdl-data-table" style="width:100%">
                <thead>
                    <tr>
                         <?php 
                            if ($_SESSION['role'] == 'VENDOR') {
                                //do nothing
                                $query = "SELECT * FROM focus_freezer_placement, distributor WHERE focus_freezer_placement.id_distributor = distributor.iddistributor AND distributor.custid = '".$result['IDBelfoodd']."' ORDER BY id_distributor DESC LIMIT 0,1";
                                $dq1 = mysqli_query($koneksi,$query);
                                $result_q = mysqli_fetch_assoc($dq1);

                                if($result_q['IDNumber'] == '')
                                {
                                    $IDNumber = '';
                                }
                                else
                                {
                                    $IDNumber = $result_q['IDNumber']; 
                                }
                            }else{
                         ?>
                        <td>Action</td>
                    <?php } ?>
                       
                        <td>No</td>
                        <td>Outlet Name</td>
                        <td>BFI Code</td>
                        <td>Freezer Code</td>
                        <td>ID Number</td>
                        <td>Freezer Status</td>
                        <td>Distributor Name</td>
                        <td>Alamat</td>
                        <td>Kelurahan</td>
                        <td>Kecamatan</td>
                        <td>Kab/Kota</td>
                        <td>Provinsi</td>
                        <td>Email to Vendor Date</td>
                        <td>Placement Date</td>
                        <td>Removal/Switch Date</td>
                        <td>Removal/Switch Status</td>
                        <td>Type (Current)</td>
                        <td>Barcode (Current)</td>
                        <td>Type (Old)</td>
                        <td>Barcode (Old)</td>
                        <td>Supplier Name</td>
                        <td>BarcodeFA</td>
                        <td>Removal/Switch Reason</td>
                    </tr>
                </thead>
                <tbody>
                    <?php 

                        $search = $_POST['search'];
                        $no = 1;
                        include ('config/config.php');
                        $query = "SELECT * FROM focus_freezer_master WHERE Outletname LIKE '%$search%' OR IDBelfoodd LIKE '%$search%' GROUP BY FreezerCode";
                        $data = mysqli_query($koneksi,$query);

                        while ($result = mysqli_fetch_assoc($data)) {
                     ?>
                    <tr>

                        <?php 
                            if ($_SESSION['role'] == 'VENDOR') {
                                //do nothing
                                $query = "SELECT * FROM focus_freezer_placement, distributor WHERE focus_freezer_placement.id_distributor = distributor.iddistributor AND distributor.custid = '".$result['IDBelfoodd']."' ORDER BY id_distributor DESC LIMIT 0,1";
                                $dq1 = mysqli_query($koneksi,$query);
                                $result_q = mysqli_fetch_assoc($dq1);

                                if($result_q['IDNumber'] == '')
                                {
                                    $IDNumber = '';
                                }
                                else
                                {
                                    $IDNumber = $result_q['IDNumber']; 
                                }

                            }else{
                         ?>
                        <td>

                            <?php 
                                //cari apakah requst sudah dibuat 
                                $id_freezer = $result['id_freezer'];
                                $q = "SELECT * FROM freezer_request WHERE id_freezer = '$id_freezer' ORDER BY id_request DESC LIMIT 0,1";
                                $dq = mysqli_query($koneksi,$q);
                                $rq = mysqli_fetch_assoc($dq);

                                $query = "SELECT * FROM focus_freezer_placement, distributor WHERE focus_freezer_placement.id_distributor = distributor.iddistributor AND distributor.custid = '".$result['IDBelfoodd']."' ORDER BY id_distributor DESC LIMIT 0,1";
                                $dq1 = mysqli_query($koneksi,$query);
                                $result_q = mysqli_fetch_assoc($dq1);

                                if($result_q['IDNumber'] == '')
                                {
                                    $IDNumber = '';
                                }
                                else
                                {
                                    $IDNumber = $result_q['IDNumber']; 
                                }

                                if ($rq['id_freezer'] == '') {
                            ?>

                            <form action="model/freezermanagement/switch.php" method="post" class="form-tindaklanjut" id="form-tindaklanjut-<?php echo $result['id_freezer'] ?>">
                                            <input type="hidden" id="id" name="id_freezer" value="<?php echo $result['id_freezer'] ?>">
                                            <input type="hidden" id="supplier" name="supplier" value="<?php echo $result['Supplier'] ?>">
                                            <h5>Vendor</h5>

                                           
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <small>Keterangan (masukkan jika ada)</small>
                                                <br>
                                                    <input type="text" id="ket-tindaklanjut" name="keterangan" class="form-control" required>
                                                </div>
                                            </div>
                                           

                                           <div class="row">
                                                <div class="col-md-12">
                                                    <small>Nama Vendor</small>
                                                <br>
                                                    <!-- <input type="text" id="namavendor" name="namavendor" class="form-control" required> -->

                                                    <select name="namavendor" id="namavendorselect<?php echo $result['id_freezer'] ?>" class="select_database form-control namavendorselect" onChange="selectvendor(<?php echo $result['id_freezer'] ?>);" required>
                                                    <option >Pilih Nama Vendor</option>

                                                    <?php 
                                                        $query_user = "SELECT * FROM user WHERE role = 'VENDOR' GROUP BY nama";
                                                        $data_user = mysqli_query($koneksi,$query_user);
                                                        while ($result_user = mysqli_fetch_assoc($data_user)) {
                                                            
                                                     ?>
                                                        <option value="<?php echo $result_user['nama'] ?>"><?php echo $result_user['nama'] ?></option>

                                                    <?php } ?>

                                                    </select>
                                                </div>
                                            </div>

                                            <br>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <select name="areavendor" id="areavendor<?php echo $result['id_freezer'] ?>" class="select_database form-control areavendor" required>
                                                </select>
                                                    <!--
                                                    <select name="areavendor" id="namavendor" class="select_database form-control" required>
                                                    <option>Pilih Area</option>

                                                    <?php 

                                                        $acc_origin = $result['Supplier'];
                                                        $acc_origin = str_replace("1","",$acc_origin);
                                                        $acc_origin = str_replace("2","",$acc_origin);
                                                        $acc_origin = str_replace(" ","",$acc_origin);
                                                        $acc_origin = str_replace("{","",$acc_origin);  
                                                        $acc_origin = str_replace("PT","",$acc_origin);    
                                                        $acc_origin = str_replace("}","",$acc_origin);

                                                        $query_user = "SELECT area FROM user WHERE role = 'VENDOR' AND nama = '$acc_origin' GROUP BY area";
                                                        $data_user = mysqli_query($koneksi,$query_user);
                                                        while ($result_user = mysqli_fetch_assoc($data_user)) {
                                                            
                                                     ?>
                                                        <option value="<?php echo $result_user['area'] ?>"><?php echo $result_user['area'] ?></option>

                                                    <?php } ?>

                                                </select>
                                                -->
                                                
                                                </div>
                                            </div>
                                            <br>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button type="submit" class="btn btn-sm btn-success">
                                                        Send Request
                                                    </button>
                                               
                                                    <a href="javascript:void();" class="btn btn-sm btn-dark batal-tindaklanjut">Batal</a>
                                                </div>
                                             </div>
                                                
                                            </div>
                                        </form>
                                        
                                        <a href="javascript:void();" class="btn btn-sm btn-success tindaklanjut" id="rejectbtn" data-toggle="tooltip" data-placement="top" title="Switch Freezer" acuan="<?php echo $result['id_freezer'] ?>"><i class="fas fa fa-exchange-alt"> </i></a> 
                                         
                                         <!-- approved -->
                                         
                                         
                                        <form action="model/freezermanagement/remove.php" method="post" class="form-tindaklanjut" id="form-approved-<?php echo $result['id_freezer']?>">
                                            <input type="hidden" id="id" name="id_freezer" value="<?php echo $result['id_freezer'] ?>">
                                            <input type="hidden" id="supplier" name="supplier" value="<?php echo $result['Supplier'] ?>">
                                            <h5>Vendor</h5>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <small>Keterangan (masukkan jika ada)</small>
                                                <br>
                                                    <input type="text" id="idnumber" name="keterangan" class="form-control" required>
                                                </div>
                                            </div>

                                           <div class="row">
                                                <div class="col-md-12">
                                                    <small>Nama Vendor</small>
                                                <br>
                                                    <!-- <input type="text" id="namavendor" name="namavendor" class="form-control" required> -->

                                                    <select name="namavendorRemove" id="namavendorremoveselect<?php echo $result['id_freezer'] ?>" class="select_database form-control namavendorremoveselect" onChange="selectvendorRemove(<?php echo $result['id_freezer'] ?>);" required>
                                                    <option >Pilih Nama Vendor</option>

                                                    <?php 
                                                        $query_user = "SELECT * FROM user WHERE role = 'VENDOR' GROUP BY nama";
                                                        $data_user = mysqli_query($koneksi,$query_user);
                                                        while ($result_user = mysqli_fetch_assoc($data_user)) {
                                                            
                                                     ?>
                                                        <option value="<?php echo $result_user['nama'] ?>"><?php echo $result_user['nama'] ?></option>

                                                    <?php } ?>

                                                    </select>
                                                </div>
                                            </div>

                                            <br>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <select name="areavendorRemove" id="areavendorRemove<?php echo $result['id_freezer'] ?>" class="select_database form-control areavendorRemove" required>
                                                </select>
                                                    <!--
                                                    <select name="areavendor" id="namavendor" class="select_database form-control" required>
                                                    <option>Pilih Area</option>

                                                    <?php 

                                                        $acc_origin = $result['Supplier'];
                                                        $acc_origin = str_replace("1","",$acc_origin);
                                                        $acc_origin = str_replace("2","",$acc_origin);
                                                        $acc_origin = str_replace(" ","",$acc_origin);
                                                        $acc_origin = str_replace("{","",$acc_origin);  
                                                        $acc_origin = str_replace("PT","",$acc_origin);    
                                                        $acc_origin = str_replace("}","",$acc_origin);

                                                        $query_user = "SELECT area FROM user WHERE role = 'VENDOR' AND nama = '$acc_origin' GROUP BY area";
                                                        $data_user = mysqli_query($koneksi,$query_user);
                                                        while ($result_user = mysqli_fetch_assoc($data_user)) {
                                                            
                                                     ?>
                                                        <option value="<?php echo $result_user['area'] ?>"><?php echo $result_user['area'] ?></option>

                                                    <?php } ?>

                                                </select>
                                                -->
                                                
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button type="submit" class="btn btn-sm btn-success">
                                                        Send Request
                                                    </button>
                                               
                                                    <a href="javascript:void();" class="btn btn-sm btn-dark batal-tindaklanjut">Batal</a>
                                                </div>
                                             </div>
                                                
                                            </div>
                                        </form>
                                        

                                        <a href="javascript:void();" id="approved_placement_<?php echo $result['id_freezer']?>" class="btn btn-sm btn-warning approved_placement" data-toggle="tooltip" data-placement="top" title="Remove Freezer" acuan="<?php echo $result['id_freezer']?>">
                                            <i class="fas fa fa-minus"></i>
                                        </a>

                            <?php 
                                }else{

                                //cari lagi apakah sudah di proses atau belum
                                    if ($rq['status'] == 'success' or $rq['status'] ==  'FAILED') {

                                        if ($rq['action'] == 'remove' AND $rq['status'] == 'success') {
                                            echo '<a href="index.php?mod=freezer&class=add&id='.$result['IDBelfoodd'].'&freezerid='.$result['id_freezer'].'" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Add Freezer"><i class="fas fa fa-plus"> </i></a>';
                                        }else{

                                    ?>
                                            <form action="model/freezermanagement/switch.php" method="post" class="form-tindaklanjut" id="form-tindaklanjut-<?php echo $result['id_freezer'] ?>">
                                            <input type="hidden" id="id" name="id_freezer" value="<?php echo $result['id_freezer'] ?>">
                                            <input type="hidden" id="supplier" name="supplier" value="<?php echo $result['Supplier'] ?>">
                                            <h5>Vendor</h5>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <small>Keterangan (masukkan jika ada)</small>
                                                <br>
                                                    <input type="text" id="ket-tindaklanjut" name="keterangan" class="form-control" required>
                                                </div>
                                            </div>

<div class="row">
                                                <div class="col-md-12">
                                                    <small>Nama Vendor</small>
                                                <br>
                                                    <!-- <input type="text" id="namavendor" name="namavendor" class="form-control" required> -->

                                                    <select name="namavendor" id="namavendorselect<?php echo $result['id_freezer'] ?>" class="select_database form-control namavendorselect" onChange="selectvendor(<?php echo $result['id_freezer'] ?>);" required>
                                                    <option >Pilih Nama Vendor</option>

                                                    <?php 
                                                        $query_user = "SELECT * FROM user WHERE role = 'VENDOR' GROUP BY nama";
                                                        $data_user = mysqli_query($koneksi,$query_user);
                                                        while ($result_user = mysqli_fetch_assoc($data_user)) {
                                                            
                                                     ?>
                                                        <option value="<?php echo $result_user['nama'] ?>"><?php echo $result_user['nama'] ?></option>

                                                    <?php } ?>

                                                    </select>
                                                </div>
                                            </div>

                                            <br>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <select name="areavendor" id="areavendor<?php echo $result['id_freezer'] ?>" class="select_database form-control areavendor" required>
                                                </select>
                                                    <!--
                                                    <select name="areavendor" id="namavendor" class="select_database form-control" required>
                                                    <option>Pilih Area</option>

                                                    <?php 

                                                        $acc_origin = $result['Supplier'];
                                                        $acc_origin = str_replace("1","",$acc_origin);
                                                        $acc_origin = str_replace("2","",$acc_origin);
                                                        $acc_origin = str_replace(" ","",$acc_origin);
                                                        $acc_origin = str_replace("{","",$acc_origin);  
                                                        $acc_origin = str_replace("PT","",$acc_origin);    
                                                        $acc_origin = str_replace("}","",$acc_origin);

                                                        $query_user = "SELECT area FROM user WHERE role = 'VENDOR' AND nama = '$acc_origin' GROUP BY area";
                                                        $data_user = mysqli_query($koneksi,$query_user);
                                                        while ($result_user = mysqli_fetch_assoc($data_user)) {
                                                            
                                                     ?>
                                                        <option value="<?php echo $result_user['area'] ?>"><?php echo $result_user['area'] ?></option>

                                                    <?php } ?>

                                                </select>
                                                -->
                                                
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button type="submit" class="btn btn-sm btn-success">
                                                        Send Request
                                                    </button>
                                               
                                                    <a href="javascript:void();" class="btn btn-sm btn-dark batal-tindaklanjut">Batal</a>
                                                </div>
                                             </div>
                                                
                                            </div>
                                        </form>
                                        
                                        <a href="javascript:void();" class="btn btn-sm btn-success tindaklanjut" id="rejectbtn" data-toggle="tooltip" data-placement="top" title="Switch Freezer" acuan="<?php echo $result['id_freezer'] ?>"><i class="fas fa fa-exchange-alt"> </i></a> 
                                         
                                         <!-- approved -->
                                         
                                         
                                        <form action="model/freezermanagement/remove.php" method="post" class="form-tindaklanjut" id="form-approved-<?php echo $result['id_freezer']?>">
                                            <input type="hidden" id="id" name="id_freezer" value="<?php echo $result['id_freezer'] ?>">
                                            <input type="hidden" id="supplier" name="supplier" value="<?php echo $result['Supplier'] ?>">
                                            <h5>Vendor</h5>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <small>Keterangan (masukkan jika ada)</small>
                                                <br>
                                                    <input type="text" id="idnumber" name="keterangan" class="form-control" required>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <small>Nama Vendor</small>
                                                <br>
                                                    <!-- <input type="text" id="namavendor" name="namavendor" class="form-control" required> -->

                                                    <select name="namavendorRemove" id="namavendorremoveselect<?php echo $result['id_freezer'] ?>" class="select_database form-control namavendorremoveselect" onChange="selectvendorRemove(<?php echo $result['id_freezer'] ?>);" required>
                                                    <option >Pilih Nama Vendor</option>

                                                    <?php 
                                                        $query_user = "SELECT * FROM user WHERE role = 'VENDOR' GROUP BY nama";
                                                        $data_user = mysqli_query($koneksi,$query_user);
                                                        while ($result_user = mysqli_fetch_assoc($data_user)) {
                                                            
                                                     ?>
                                                        <option value="<?php echo $result_user['nama'] ?>"><?php echo $result_user['nama'] ?></option>

                                                    <?php } ?>

                                                    </select>
                                                </div>
                                            </div>

                                            <br>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <select name="areavendorRemove" id="areavendorRemove<?php echo $result['id_freezer'] ?>" class="select_database form-control areavendorRemove" required>
                                                </select>
                                                    <!--
                                                    <select name="areavendor" id="namavendor" class="select_database form-control" required>
                                                    <option>Pilih Area</option>

                                                    <?php 

                                                        $acc_origin = $result['Supplier'];
                                                        $acc_origin = str_replace("1","",$acc_origin);
                                                        $acc_origin = str_replace("2","",$acc_origin);
                                                        $acc_origin = str_replace(" ","",$acc_origin);
                                                        $acc_origin = str_replace("{","",$acc_origin);  
                                                        $acc_origin = str_replace("PT","",$acc_origin);    
                                                        $acc_origin = str_replace("}","",$acc_origin);

                                                        $query_user = "SELECT area FROM user WHERE role = 'VENDOR' AND nama = '$acc_origin' GROUP BY area";
                                                        $data_user = mysqli_query($koneksi,$query_user);
                                                        while ($result_user = mysqli_fetch_assoc($data_user)) {
                                                            
                                                     ?>
                                                        <option value="<?php echo $result_user['area'] ?>"><?php echo $result_user['area'] ?></option>

                                                    <?php } ?>

                                                </select>
                                                -->
                                                
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button type="submit" class="btn btn-sm btn-success">
                                                        Send Request
                                                    </button>
                                               
                                                    <a href="javascript:void();" class="btn btn-sm btn-dark batal-tindaklanjut">Batal</a>
                                                </div>
                                             </div>
                                                
                                            </div>
                                        </form>
                                        

                                        <a href="javascript:void();" id="approved_placement_<?php echo $result['id_freezer']?>" class="btn btn-sm btn-warning approved_placement" data-toggle="tooltip" data-placement="top" title="Remove Freezer" acuan="<?php echo $result['id_freezer']?>">
                                            <i class="fas fa fa-minus"></i>
                                        </a>

                                        
                                        

                                <?php  
                                        }

                                    }else{

                                        echo $rq['status'];
                                    }
                                }


                             ?>
                            
                        </td>
                        <?php 
                            }
                         ?>
                        
                        <td><?php echo $no ?></td>
                        <td>
                            <?php echo $result['Outletname'] ?>
                        </td>
                        <td>
                            <?php echo $result['IDBelfoodd'] ?>
                        </td>
                        <td>
                            <?php echo $result['FreezerCode'] ?>
                        </td>
                        <td>
                            <?php echo $IDNumber ?>
                        </td>
                        <td>
                            <?php echo $result['StatusFreezer'] ?>
                        </td>
                        <td>
                            <?php echo $result['DistributorName'] ?>
                        </td>
                        <td>
                            <?php echo $result['Alamat'] ?>
                        </td>
                        <td>
                            <?php echo $result['Kelurahan'] ?>
                        </td>
                        <td>
                            <?php echo $result['Kecamatan'] ?>
                        </td>

                        <td>
                            <?php echo $result['KabKota'] ?>
                        </td>
                        <td>
                            <?php echo $result['provinsi'] ?>
                        </td>
                        <td>
                            <?php echo $result['TanggalPasang'] ?>
                        </td>
                        <td>
                            <?php echo $result['TanggalPasang'] ?>
                        </td>
                        <td>
                            <?php echo $result['TanggalPenarikan'] ?>
                        </td>
                        <td>
                            <?php 
                                $id_freezer = $result['id_freezer'];
                                $cari = "SELECT action, status from freezer_request WHERE id_freezer = '$id_freezer' ORDER BY id_request DESC LIMIT 0,1";
                                $qc = mysqli_query($koneksi,$cari);
                                $rc = mysqli_fetch_assoc($qc);

                                $status = $rc['status'];
                                $action = $rc['action'];

                                if ($status == 'success') {
                                    if ($action == 'remove') {
                                        echo "Removal Successful";
                                    }elseif($action == 'switch'){
                                        echo "Switch Successful";
                                    }else{
                                        //show nothing
                                    }
                                }elseif ($status == 'FAILED') {
                                    if ($action == 'remove') {
                                        echo "Removal Failed";
                                    }elseif($action == 'switch'){
                                        echo "Switch Failed";
                                    }else{
                                        //show nothing
                                    }
                                }else{
                                    echo $status;
                                }
                             ?>
                        </td>
                        <td>
                            <?php echo $result['Type'] ?>
                        </td>
                        <td>
                            <?php echo $result['ID_Number'] ?>
                        </td>
                        <td>
                            <?php echo $result['Type_Switching'] ?>
                        </td>
                        <td>
                            <?php echo $result['ID_Switching'] ?>
                        </td>
                        <td>
                            <?php echo $result['Supplier'] ?>
                        </td>
                        <td>
                            <?php echo $result['BarcodeFANumber'] ?>
                        </td>
                        <td>
                            <?php echo $result['Keterangan'] ?>
                        </td>
                        <!-- <td>
                            <div class="table-data-feature">
                                
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <a href="index.php?mod=users&class=edit&id=<?php echo $result['id_user']; ?>" ><i class="zmdi zmdi-edit"></i></a>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <a href="index.php?mod=users&class=delete&id=<?php echo $result['id_user'] ?>"><i class="zmdi zmdi-delete"></i></a>
                                </button>
                            </div>
                        </td> -->
                    </tr>

                     <?php $no++; } ?>
                </tbody>
            </table>
        </div>
        <?php else: ?>
            <div class="table-responsive table-data" style="height: auto">
            <table id="tableok" class="mdl-data-table" style="width:100%">
                <thead>
                    <tr>
                        
                        <td>Action</td>
                    
                        <td>No</td>
                        <td>Outlet Name</td>
                        <td>BFI Code</td>
                        <td>Freezer Code</td>
                        <td>ID Number</td>
                        <td>Freezer Status</td>
                        <td>Distributor Name</td>
                        <td>Alamat</td>
                        <td>Kelurahan</td>
                        <td>Kecamatan</td>
                        <td>Kab/Kota</td>
                        <td>Provinsi</td>
                        <td>Email to Vendor Date</td>
                        <td>Placement Date</td>
                        <td>Removal/Switch Date</td>
                        <td>Removal/Switch Status</td>
                        <td>Type (Current)</td>
                        <td>Barcode (Current)</td>
                        <td>Type (Old)</td>
                        <td>Barcode (Old)</td>
                        <td>Supplier Name</td>
                        <td>BarcodeFA</td>
                        <td>Removal/Switch Reason</td>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $no = 1;
                        include ('config/config.php');
                        $query = "SELECT * FROM focus_freezer_request_penarikan as a 
                        INNER JOIN focus_freezer_master as b ON a.id_freezer=b.id_freezer 
                        INNER JOIN focus_rejection_code as c ON a.id_rejection=c.id_rejection
                        GROUP BY FreezerCode ORDER BY a.id_freezer DESC  LIMIT 0,100";
                        $data = mysqli_query($koneksi,$query);

                        while ($result = mysqli_fetch_assoc($data)) {

                     ?>
                    <tr>

                       
                        <td>

                                
                            <form action="model/freezermanagement/switch.php" method="post" class="form-tindaklanjut" id="form-tindaklanjut-<?php echo $result['id_freezer'] ?>">
                                <input type="hidden" id="id" name="id_freezer" value="<?php echo $result['id_freezer'] ?>">
                                <input type="hidden" id="penarikanId" name="penarikanId" value="<?php echo $result['penarikanId'] ?>">

                                <h5>Vendor</h5>
                                
                                <div class="row">
                                    <div class="col-md-12">
                                        <small>Keterangan (masukkan jika ada)</small>
                                    <br>
                                        <input type="text" id="ket-tindaklanjut" name="keterangan" class="form-control" value="<?php echo $result['ket'] ?>" required>
                                    </div>
                                </div>
                                

                               <div class="row">
                                    <div class="col-md-12">
                                        <small>Nama Vendor</small>
                                    <br>
                                        <!-- <input type="text" id="namavendor" name="namavendor" class="form-control" required> -->

                                        <select name="namavendor" id="namavendorselect<?php echo $result['id_freezer'] ?>" class="select_database form-control namavendorselect" onChange="selectvendor(<?php echo $result['id_freezer'] ?>);" required>
                                        <option >Pilih Nama Vendor</option>

                                        <?php 
                                            $query_user = "SELECT * FROM user WHERE role = 'VENDOR' GROUP BY nama";
                                            $data_user = mysqli_query($koneksi,$query_user);
                                            while ($result_user = mysqli_fetch_assoc($data_user)) {
                                                
                                         ?>
                                            <option value="<?php echo $result_user['nama'] ?>"><?php echo $result_user['nama'] ?></option>

                                        <?php } ?>

                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <small>Area Vendor</small>
                                    <br>
                                        
                                       <select name="areavendor" id="areavendor<?php echo $result['id_freezer'] ?>" class="select_database form-control areavendor" required>
                                        </select>
                                    </div>
                                </div>

                                <!-- <div id="areavendor"></div>  -->

                                <div class="col-12 col-md-9">
                                    <input type="text" name="namavendor_pilih" id="namavendor_pilih<?php echo $result['id_freezer'] ?>" class="form-control namavendor_pilih" hidden>
                                </div>

                             
                                <br>

                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-sm btn-success">
                                            Send Request
                                        </button>
                                   
                                        <a href="javascript:void();" class="btn btn-sm btn-dark batal-tindaklanjut">Batal</a>
                                    </div>
                                 </div>
                                    
                                </div>
                            </form>
                            
                            <a href="javascript:void();" class="btn btn-sm btn-success tindaklanjut" id="rejectbtn" data-toggle="tooltip" data-placement="top" title="Switch Freezer" acuan="<?php echo $result['id_freezer'] ?>"><i class="fas fa fa-exchange-alt"> </i></a> 
                             
                             <!-- approved -->
                             
                             
                            <form action="model/freezermanagement/remove_req_tarik.php" method="post" class="form-tindaklanjut" id="form-approved-<?php echo $result['id_freezer']?>">
                                <input type="hidden" id="id" name="id_freezer" value="<?php echo $result['id_freezer'] ?>">
                                <input type="hidden" id="supplier" name="supplier" value="<?php echo $result['Supplier'] ?>">

                                 <input type="hidden" id="penarikanId" name="penarikanId" value="<?php echo $result['penarikanId'] ?>">
                                 
                                <h5>Vendor</h5>
                                <div class="row">
                                                <div class="col-md-12">
                                                    <small>Keterangan (masukkan jika ada)</small>
                                                <br>
                                                    <input type="text" id="idnumber" name="keterangan" class="form-control" value="<?php echo $result['ket'] ?>" required>
                                                </div>
                                            </div>

                                <div class="row">
                                                <div class="col-md-12">
                                                    <small>Nama Vendor</small>
                                                <br>
                                                    <!-- <input type="text" id="namavendor" name="namavendor" class="form-control" required> -->

                                                    <select name="namavendorRemove" id="namavendorremoveselect<?php echo $result['id_freezer'] ?>" class="select_database form-control namavendorremoveselect" onChange="selectvendorRemove(<?php echo $result['id_freezer'] ?>);" required>
                                                    <option >Pilih Nama Vendor</option>

                                                    <?php 
                                                        $query_user = "SELECT * FROM user WHERE role = 'VENDOR' GROUP BY nama";
                                                        $data_user = mysqli_query($koneksi,$query_user);
                                                        while ($result_user = mysqli_fetch_assoc($data_user)) {
                                                            
                                                     ?>
                                                        <option value="<?php echo $result_user['nama'] ?>"><?php echo $result_user['nama'] ?></option>

                                                    <?php } ?>

                                                    </select>
                                                </div>
                                            </div>

                                            <br>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <select name="areavendorRemove" id="areavendorRemove<?php echo $result['id_freezer'] ?>" class="select_database form-control areavendorRemove" required>
                                                </select>
                                                    <!--
                                                    <select name="areavendor" id="namavendor" class="select_database form-control" required>
                                                    <option>Pilih Area</option>

                                                    <?php 

                                                        $acc_origin = $result['Supplier'];
                                                        $acc_origin = str_replace("1","",$acc_origin);
                                                        $acc_origin = str_replace("2","",$acc_origin);
                                                        $acc_origin = str_replace(" ","",$acc_origin);
                                                        $acc_origin = str_replace("{","",$acc_origin);  
                                                        $acc_origin = str_replace("PT","",$acc_origin);    
                                                        $acc_origin = str_replace("}","",$acc_origin);

                                                        $query_user = "SELECT area FROM user WHERE role = 'VENDOR' AND nama = '$acc_origin' GROUP BY area";
                                                        $data_user = mysqli_query($koneksi,$query_user);
                                                        while ($result_user = mysqli_fetch_assoc($data_user)) {
                                                            
                                                     ?>
                                                        <option value="<?php echo $result_user['area'] ?>"><?php echo $result_user['area'] ?></option>

                                                    <?php } ?>

                                                </select>
                                                -->
                                                
                                                </div>
                                            </div>
                                            <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-sm btn-success">
                                            Send Request
                                        </button>
                                   
                                        <a href="javascript:void();" class="btn btn-sm btn-dark batal-tindaklanjut">Batal</a>
                                    </div>
                                 </div>
                                    
                                </div>
                            </form>
                            

                            <a href="javascript:void();" id="approved_placement_<?php echo $result['id_freezer']?>" class="btn btn-sm btn-warning approved_placement" data-toggle="tooltip" data-placement="top" title="Remove Freezer" acuan="<?php echo $result['id_freezer']?>">
                                <i class="fas fa fa-minus"></i>
                            </a>

                          
                            
                        </td>
                      
                        <td><?php echo $no ?></td>
                        <td>
                            <?php echo $result['Outletname'] ?>
                        </td>
                        <td>
                            <?php echo $result['IDBelfoodd'] ?>
                        </td>
                        <td>
                            <?php echo $result['FreezerCode'] ?>
                        </td>
                        <td>
                            <?php echo $IDNumber ?>
                        </td>
                        <td>
                            <?php echo $result['StatusFreezer'] ?>
                        </td>
                        <td>
                            <?php echo $result['DistributorName'] ?>
                        </td>
                        <td>
                            <?php echo $result['Alamat'] ?>
                        </td>
                        <td>
                            <?php echo $result['Kelurahan'] ?>
                        </td>
                        <td>
                            <?php echo $result['Kecamatan'] ?>
                        </td>

                        <td>
                            <?php echo $result['KabKota'] ?>
                        </td>
                        <td>
                            <?php echo $result['Propinsi'] ?>
                        </td>
                        <td>
                            <?php echo $result['TanggalPasang'] ?>
                        </td>
                        <td>
                            <?php echo $result['TanggalPasang'] ?>
                        </td>
                        <td>
                            <?php echo $result['TanggalPenarikan'] ?>
                        </td>
                        <td>
                           
                        </td>
                        <td>
                            <?php echo $result['Type'] ?>
                        </td>
                        <td>
                            <?php echo $result['ID_Number'] ?>
                        </td>
                        <td>
                            <?php echo $result['Type_Switching'] ?>
                        </td>
                        <td>
                            <?php echo $result['ID_Switching'] ?>
                        </td>
                        <td>
                            <?php echo $result['Supplier'] ?>
                        </td>
                        <td>
                            <?php echo $result['BarcodeFANumber'] ?>
                        </td>
                        <td>
                            <?php echo $result['Keterangan'] ?>
                        </td>
                        <!-- <td>
                            <div class="table-data-feature">
                                
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <a href="index.php?mod=users&class=edit&id=<?php echo $result['id_user']; ?>" ><i class="zmdi zmdi-edit"></i></a>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <a href="index.php?mod=users&class=delete&id=<?php echo $result['id_user'] ?>"><i class="zmdi zmdi-delete"></i></a>
                                </button>
                            </div>
                        </td> -->
                    </tr>

                     <?php $no++; } ?>
                </tbody>
            </table>
        </div>
        <?php endif ?>
        <!-- end of full search -->
        
        <?php endif ?>
        <!-- END DATA TABLE -->
    </div>
</div>
