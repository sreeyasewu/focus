<?php 
    if (isset($_GET['status'])) {
        $status = $_GET['status'];

        if ($status == 1) {
            echo '<div class="alert alert-success" role="alert">
                Request telah dikirim ke vendor
            </div>';
        }elseif ($status == 0) {
            echo '<div class="alert alert-danger" role="alert">
                Terjadi Kesalahan! tidak dapat menghapus mengirim request ke vendor
            </div>';
        }else{
            echo '<div class="alert alert-warning" role="alert">
                Terjadi kesalahan koneksi internet, silkahkan ulangi kembali
            </div>';
        }
        
    }
 ?>
<div class="row">
    <div class="col-md-12">
        <h3 class="title-5 m-b-35">add Freezer</h3>

        <div class="card">
        <div class="card-header">
            <strong>Add new Freezer</strong>
        </div>
        <div class="card-body card-block">

            <form action="model/freezermanagement/add.php" method="post" enctype="multipart/form-data" class="form-horizontal">

                <!-- <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Tanggal Pengajuan</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="date" id="text-input" name="tgl_pengajuan" class="form-control" required>
                    </div>
                </div> -->

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Tipe Customer</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <select name="tipecustomer" id="tipecustomerselect" class="select_database form-control" required>
                            <option disabled selected>Pilih Customer Tipe</option>
                            <option value="PRIMARY">PRIMARY</option>
                            <option value="SECONDARY">SECONDARY</option>
                        </select>
                    </div>
                </div>

                <!-- <div id="customeralladdisi                    
                </div>  -->

                 <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Pilih Customer</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <span id="customerloading"></span>
                        
                        <select name="customer" id="customerall" class="select_mds form-control" required>
                        <option>Pilih Customer</option>

                        

                        </select>
                    </div>
                </div>

                <div class="col-12 col-md-9">
                    <input type="text" name="tipecustomer_pilih" id="tipecustomer_pilih" class="form-control" hidden>
                </div>
                <!-- 

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">BFI Code</label>
                    </div>
                    <div class="col-12 col-md-9">

                        <?php if (isset($_GET['id'])): ?>
                                <input type="text" name="kode" class="form-control" value="<?php echo $_GET['id'] ?>" required>
                                <input type="hidden" value="<?php echo $_GET['freezerid'] ?>" name="freezeridisi">
                                <input type="hidden" value="add" name="addisi">

                            <?php else: ?>
                                <select name="kode" class="select_bfi form-control" id="primaryget" required>
                                <option disabled selected>Select BFI Code</option>
                                <?php 
                                    include ('config/config.php');
                                    //cari data semua BFI
                                    $sql = "SELECT * FROM focus_freezer_master GROUP BY Outletname ORDER BY id_freezer DESC ";
                                    $dql = mysqli_query($koneksi,$sql);
                                    

                                    while ($rql = mysqli_fetch_assoc($dql)) {
                                ?>
                                    <?php 
                                        $cs = $rql['Customer_Type'];

                                        if ($cs == '01 {DIRECT}') {
                                            $css = 'PRIMARY';
                                        }else{
                                            $css = 'SECONDARY';
                                        }
                                     ?>
                                    <option value="<?php echo $rql['IDBelfoodd'] ?>" acuan="<?php echo $rql['id_freezer'] ?>"><?php echo strtoupper($rql['IDBelfoodd']." - ".$rql['Outletname']." - ".$css) ?></option>

                                <?php 
                                    }

                                 ?>
                                

                            </select>
                            <small>BFI Code, Outlet Name, Distributor Name</small>

                            <input type="hidden" value="kosong" id="freezerid" name="freezerid">


                        <?php endif ?>
                            
                       
                    </div>
                </div> -->

                
                <!-- <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Nama Distributor</label>
                    </div>
                    <div class="col-12 col-md-9">
                            <select name="distributor" class="form-control" required>
                                <option disabled selected>Select Distributor Name</option>
                                

                            </select>
                        
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Nama Outlet</label>
                    </div>
                    <div class="col-12 col-md-9">
                            <select name="distributor" class="form-control" required>
                                <option disabled selected>Select Outlet</option>
                                

                            </select>
                       
                    </div>
                </div> -->

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Type</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <select name="realisasi_tipe_freezer" class="select_bfi form-control" required>
                            <option value="">Select Tipe Freezer</option>
                            <?php 
                                include ('config/config.php');
                                //cari data semua BFI
                                $sql = "SELECT * FROM focus_tipe_freezer ORDER BY id_tipe";
                                $dql = mysqli_query($koneksi,$sql);
                                

                                while ($rql = mysqli_fetch_assoc($dql)) {
                            ?>
                                <option value="<?php echo $rql['tipe'] ?>"><?php echo strtoupper($rql['tipe']." - ".$rql['keterangan'])?></option>

                            <?php 
                                }


                             ?>
                            

                        </select> 
                            <!-- <input type="text" name="tipe" class="form-control" placeholder="Tipe Freezer"> -->
                        
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Principal</label>
                    </div>
	             <div class="col-12 col-md-9">
                        <select name="principal" class="select_bfi form-control" required>
                            <option disabled selected>Select Principal</option>
                            <?php
                                include ('config/config.php');
                                //cari data semua BFI
                                $sql = "SELECT * FROM focus_principal ORDER BY id_principal";
                                $dql = mysqli_query($koneksi,$sql);


                                while ($rql = mysqli_fetch_assoc($dql)) {
                            ?>
                                <option value="<?php echo $rql['principal'] ?>" <?php if ($rql['id_principal']==1) echo 'selected'; ?>><?php echo $rql['principal']." - ".$rql['keterangan']?></option>

                            <?php
                                }


                             ?>


                        </select>
                            <!-- <input type="text" name="tipe" class="form-control" placeholder="Tipe Freezer"> -->

                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Freezer Owner</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <select name="owner" id="freezerowner" class="form-control" onChange="rentown();" required>
                            <option selected>Status kepemilikan</option>
                            <option value="Rent">Rent</option>
                            <option value="Own">Own</option>
                            <option value="Jbp">Jbp</option>
                        </select>
                        
                    </div>
                </div>

                <div class="row form-group nomor_mesin">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Nomor Mesin</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="text" id="nomormesin" name="nomormesin" class="form-control">
                        
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Vendor</label>
                    </div>
                    <div class="col-12 col-md-9">
                            <select name="namavendor" id="namavendorselectadd" class="select_database form-control" required>
                                <option disabled selected>Nama Vendor</option>

                                <?php 
                                    $query_user = "SELECT * FROM user WHERE role = 'VENDOR' GROUP BY nama";
                                    $data_user = mysqli_query($koneksi,$query_user);
                                    while ($result_user = mysqli_fetch_assoc($data_user)) {
                                        
                                 ?>
                                    <option value="<?php echo $result_user['nama'] ?>"><?php echo $result_user['nama'] ?></option>

                                <?php } ?>

                            </select>
                        
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Area</label>
                    </div>
                    <div class="col-12 col-md-9">
                            <select name="areavendor" id="areavendor" class="select_database form-control" required>
                                

                            </select>
                        
                    </div>
                </div> 

                <!-- <div id="areavendor"></div>  -->

                <div class="col-12 col-md-9">
                    <input type="text" name="namavendor_pilih" id="namavendor_pilih" class="form-control" hidden>
                </div>

                <!-- <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Area</label>
                    </div>
                    <div class="col-12 col-md-9">
                            <select name="areavendor" id="namavendor" class="select_database form-control" required>
                                <option>Pilih Area</option>

                                <?php 
                                    $query_user = "SELECT area FROM user WHERE role = 'VENDOR' GROUP BY area";
                                    $data_user = mysqli_query($koneksi,$query_user);
                                    while ($result_user = mysqli_fetch_assoc($data_user)) {
                                        
                                 ?>
                                    <option value="<?php echo $result_user['area'] ?>"><?php echo $result_user['area'] ?></option>

                                <?php } ?>

                            </select>
                        
                    </div>
                </div> -->


                </div>
                
                </div>
                <div class="card-footer">
                    <button type="submit" class="au-btn au-btn--green">
                        <i class="fa fa-floppy-o"></i> Save
                    </button>
                    <button type="reset" class="au-btn au-btn--red">
                        <i class="fa fa-ban"></i> Reset
                    </button>
                </div>

            </form>
        </div>
    </div>
    </div>
</div>
