<?php 
    include ('config/config.php');
 ?>
<header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            <div class="">
                                <span class="badge badge-pill badge-primary">Welcome <?php echo $_SESSION['nama'] ?></span> 
                                <!-- <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button> -->
                            </div>
                            <div class="header-button">
                                <div class="noti-wrap">

                                    <?php if ($_SESSION['role'] == 'FREEZER MANAGEMENT' or $_SESSION['role'] == 'VENDOR'): ?>
                                        
                                        <?php else: ?>
                                            <!-- <a href="index.php?mod=datareview&class=view" class="btn btn-dark" data-toggle="tooltip" data-placement="bottom" title="DATA REVIEW </br> DONE : 5 </br> PENDING : 2 </br> OVERDUE : 1" data-html="true"><i class="fas fa-eye"></i></a> -->

                                            <small><center><a href="index.php?mod=datareview&class=view">Data Review</a></center></small>

                                    &nbsp;
                                    &nbsp;
                                    <?php endif ?>

                                    


                                    <!-- <a href="index.php?mod=freezerplacement&class=view" class="btn btn-dark" data-toggle="tooltip" data-placement="bottom" title="FREEZER PLACEMENT"><i class="fas fa-server"></i></a> -->

                                    <?php if ($_SESSION['role'] == 'FREEZER MANAGEMENT' or $_SESSION['role'] == 'VENDOR' or $_SESSION['role'] == 'ADMIN'): ?>
                                        <?php 

                                            if ($_SESSION['role'] == 'VENDOR') {
                                                
                                                $area = trim($_SESSION['area']);
                                                $vendor = $_SESSION['nama'];
                                                $acc = $vendor.'-'.$area;
                                                $q_n = "SELECT count(*) as notif FROM focus_freezer_placement WHERE approved_status = 'APPROVED' AND nama_vendor LIKE '$acc%' AND placement_date= '0000-00-00'";
                                                $d_n = mysqli_query($koneksi,$q_n);
                                                $r = mysqli_fetch_assoc($d_n);
                                                $notif = $r['notif'];
                                            }else{

                                                $q_n = "SELECT count(*) as notif FROM focus_freezer_placement WHERE approved_status = 'REJECT BY VENDOR'";
                                                $d_n = mysqli_query($koneksi,$q_n);
                                                $r = mysqli_fetch_assoc($d_n);
                                                $notif = $r['notif'];

                                                $q_n2 = "SELECT count(*) as notif FROM focus_freezer_placement WHERE approved_status = 'REGISTERED' AND id_distributor !=0 AND id_freezer = ''";
                                                $d_n2 = mysqli_query($koneksi,$q_n2);
                                                $r2 = mysqli_fetch_assoc($d_n2);
                                                $notif2 = $r2['notif'];

                                                $notif = $notif + $notif2;
                                            }
                                        
                                         ?>
                                        
                                            <?php if ($_SESSION['role'] == 'VENDOR') { ?>
                                             <small><center><a href="index.php?mod=freezerplacement&class=view">Freezer Placement <span class="badge badge-danger"><?php echo $notif ?></span></a></center></small>

                                            <?php } else { ?>
                                                <small><center><a href="index.php?mod=freezerplacement&class=view">Freezer Placement <span class="badge badge-danger"><?php echo $notif ?></span></a></center></small>
                                            <?php } ?>
                                    &nbsp;
                                    &nbsp;
                                        <?php else: ?>

                                        
                                    <?php endif ?>
                                    
                                    <?php if ($_SESSION['role'] == 'FREEZER MANAGEMENT' or $_SESSION['role'] == 'VENDOR'): ?>
                                        
                                        <?php else: ?>

                                   <!--  <a href="index.php?mod=custcodecreation&class=view" class="btn btn-dark" data-toggle="tooltip" data-placement="bottom" title="CUST CODE CREATION"><i class="fas fa-code"></i></a> -->

                                   <small><center><a href="index.php?mod=custcodecreation&class=view">Cust Code Creation</a></center></small>
                                    
                                    &nbsp;
                                    &nbsp;

                                    <!-- <a href="index.php?mod=replenishment&class=view" class="btn btn-dark" data-toggle="tooltip" data-placement="bottom" title="REPLENISHMENT MONITORING"><i class="fas fa-boxes"></i></a> -->

                                    <small><center><a href="index.php?mod=replenishment&class=view">Replenishment Monitoring</a></center></small>
                                    
                                    &nbsp;
                                    &nbsp;
                                    &nbsp;
                                    &nbsp;

                                    <?php endif ?>

                                    <!-- <div class="noti__item js-item-menu">
                                        <i class="zmdi zmdi-comment-more"></i>
                                        <span class="quantity">1</span>
                                        <div class="mess-dropdown js-dropdown">
                                            <div class="mess__title">
                                                <p>You have 2 news message</p>
                                            </div>
                                            <div class="mess__item">
                                                <div class="image img-cir img-40">
                                                    <img src="assets/images/icon/avatar-06.jpg" alt="Michelle Moreno" />
                                                </div>
                                                <div class="content">
                                                    <h6>Michelle Moreno</h6>
                                                    <p>Have sent a photo</p>
                                                    <span class="time">3 min ago</span>
                                                </div>
                                            </div>
                                            <div class="mess__item">
                                                <div class="image img-cir img-40">
                                                    <img src="assets/images/icon/avatar-04.jpg" alt="Diane Myers" />
                                                </div>
                                                <div class="content">
                                                    <h6>Diane Myers</h6>
                                                    <p>You are now connected on message</p>
                                                    <span class="time">Yesterday</span>
                                                </div>
                                            </div>
                                            <div class="mess__footer">
                                                <a href="#">View all messages</a>
                                            </div>
                                        </div>
                                    </div> -->
                                    <!-- <div class="noti__item js-item-menu">
                                        <i class="zmdi zmdi-email"></i>
                                        <span class="quantity">1</span>
                                        <div class="email-dropdown js-dropdown">
                                            <div class="email__title">
                                                <p>You have 3 New Emails</p>
                                            </div>
                                            <div class="email__item">
                                                <div class="image img-cir img-40">
                                                    <img src="assets/images/icon/avatar-06.jpg" alt="Cynthia Harvey" />
                                                </div>
                                                <div class="content">
                                                    <p>Meeting about new dashboard...</p>
                                                    <span>Cynthia Harvey, 3 min ago</span>
                                                </div>
                                            </div>
                                            <div class="email__item">
                                                <div class="image img-cir img-40">
                                                    <img src="assets/images/icon/avatar-05.jpg" alt="Cynthia Harvey" />
                                                </div>
                                                <div class="content">
                                                    <p>Meeting about new dashboard...</p>
                                                    <span>Cynthia Harvey, Yesterday</span>
                                                </div>
                                            </div>
                                            <div class="email__item">
                                                <div class="image img-cir img-40">
                                                    <img src="assets/images/icon/avatar-04.jpg" alt="Cynthia Harvey" />
                                                </div>
                                                <div class="content">
                                                    <p>Meeting about new dashboard...</p>
                                                    <span>Cynthia Harvey, April 12,,2018</span>
                                                </div>
                                            </div>
                                            <div class="email__footer">
                                                <a href="#">See all emails</a>
                                            </div>
                                        </div>
                                    </div> -->
                                    <div class="noti__item js-item-menu">
                                        <!-- <i class="zmdi zmdi-notifications"></i>
                                        <span class="quantity">3</span>
                                        <div class="notifi-dropdown js-dropdown">
                                            <div class="notifi__title">
                                                <p>You have 3 Notifications</p>
                                            </div>
                                            <div class="notifi__item">
                                                <div class="bg-c1 img-cir img-40">
                                                    <i class="zmdi zmdi-email-open"></i>
                                                </div>
                                                <div class="content">
                                                    <p>A new order from Arifin Mahmud</p>
                                                    <span class="date">September 17, 2018 06:50</span>
                                                </div>
                                            </div>
                                            <div class="notifi__item">
                                                <div class="bg-c2 img-cir img-40">
                                                    <i class="zmdi zmdi-account-box"></i>
                                                </div>
                                                <div class="content">
                                                    <p>Admin account last log</p>
                                                    <span class="date">September 17, 2018 06:50</span>
                                                </div>
                                            </div>
                                            <div class="notifi__item">
                                                <div class="bg-c3 img-cir img-40">
                                                    <i class="zmdi zmdi-file-text"></i>
                                                </div>
                                                <div class="content">
                                                    <p>New report for September generated</p>
                                                    <span class="date">September 17, 2018 06:50</span>
                                                </div>
                                            </div>
                                            <div class="notifi__footer">
                                                <a href="#">All notifications</a>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        <div class="image">
                                            <img src="assets/images/icon/avatar-01.png" alt="Owner" style="border-radius: 360px; width: 50px; height: 45px" />
                                        </div>
                                        <div class="content">
                                            <a class="js-acc-btn" href="#"><?php echo $_SESSION['role']; ?></a>
                                        </div>
                                        <div class="account-dropdown js-dropdown">
                                            <div class="info clearfix">
                                                <div class="image">
                                                    <a href="#">
                                                        <img src="assets/images/icon/avatar-01.png" alt="Owner" />
                                                    </a>
                                                </div>
                                                <div class="content">
                                                    <h5 class="name">
                                                        <a href="#"><?php echo $_SESSION['nama']; ?> - <?php echo $_SESSION['area']; ?></a>
                                                    </h5>
                                                    <span class="email"><?php echo $_SESSION['email']; ?></span>
                                                </div>
                                            </div>
                                            <!-- <div class="account-dropdown__body">
                                             -->    <!-- <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-account"></i>Account</a>
                                                </div>
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-settings"></i>Setting</a>
                                                </div> -->
                                                <!-- <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-money-box"></i>Billing</a>
                                                </div> -->
                                           <!--  </div> -->
                                            <div class="account-dropdown__footer">
                                                <a href="logout.php">
                                                    <i class="zmdi zmdi-power"></i>Logout</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
