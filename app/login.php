<?php 
include ('part/header.php');
 ?>

<div class="page-wrapper">
        <div class="page-content--bge5">
            <div class="container">
                <div class="login-wrap">
                
                    <?php 
                        if(isset($_GET['pesan'])){
                            if($_GET['pesan'] == "gagal"){
                                echo '<div class="alert alert-danger" role="alert">
                                    Wrong Email and Password Combination
                                </div>';
                            }else if($_GET['pesan'] == "logout"){
                                echo '<div class="alert alert-info" role="success">
                                    You\'re logout, please login again to access dashboard
                                </div>';
                            }else if($_GET['pesan'] == "belum_login"){
                                echo "Anda harus login untuk mengakses halaman admin";
                            }
                        }   
                    ?>

                    <div class="login-content">
                        <div class="login-logo">
                            <a href="#">
                                <img src="assets/images/icon/logo-aus.png" alt="PGN" style="width: 100%; height: 100%">
                            </a>
                        </div>
                        <div class="login-form">
                            <form action="config/auth.php" method="post">
                                <div class="form-group">
                                    <label>Email Address</label>
                                    <input class="au-input au-input--full" type="email" name="txt_email" placeholder="Email" required>
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input class="au-input au-input--full" type="password" name="txt_password" placeholder="Password" required>
                                </div>
                                <div class="login-checkbox">
                                    <label>
                                        <input type="checkbox" name="remember">Remember Me
                                    </label>
                                    
                                </div>
                                <button class="btn btn-block btn-dark btn-lg" type="submit" name="login">Log In</button>
                                
                            </form>
                            
                        </div>

                       <!--  <span>
                            <a href="login-key.php" >USE KEY</a>
                        </span>

                        or
                        <span>
                            <a href="token-generation.php" >GENERATE KEY</a>
                        </span> -->
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php  
    include ('part/footer.php');
?>