<?php 
    if (isset($_GET['status'])) {
        $status = $_GET['status'];

        if ($status == 1) {
            echo '<div class="alert alert-success" role="alert">
                Data berhasil diperbaruhi
            </div>';
        }elseif ($status == 0) {
            echo '<div class="alert alert-danger" role="alert">
                Terjadi Kesalahan! tidak dapat memperbaruhi data
            </div>';
        }else{
            echo '<div class="alert alert-warning" role="alert">
                Terjadi kesalahan koneksi internet, silkahkan ulangi kembali
            </div>';
        }
    }

    if (isset($_GET['editstatus'])) {
        $status = $_GET['editstatus'];

        if ($status == 1) {
            echo '<div class="alert alert-success" role="alert">
                Data customer berhasil di perbaruhi
            </div>';
        }elseif ($status == 0) {
            echo '<div class="alert alert-danger" role="alert">
                Terjadi Kesalahan! tidak dapat mengubah data customer
            </div>';
        }else{
            echo '<div class="alert alert-warning" role="alert">
                Terjadi kesalahan koneksi internet, silkahkan ulangi kembali
            </div>';
        }
    }
 ?>
<div class="row">
<div class="col-md-12">
        <!-- DATA TABLE -->
        <h3 class="title-5 m-b-35">view customers</h3>
        <div class="table-data__tool">
            <!-- <div class="table-data__tool-left">
                <div class="rs-select2--light rs-select2--md">
                    <select class="js-select2" name="property">
                        <option selected="selected">Roles</option>
                        <option value="">owner</option>
                        <option value="">stakeholders</option>
                        <option value="">investors</option>
                        <option value="">marketing</option>
                        <option value="">admin</option>
                        <option value="">approval</option>
                    </select>
                    <div class="dropDownSelect2"></div>
                </div>
                <div class="rs-select2--light rs-select2--sm">
                    <select class="js-select2" name="time">
                        <option selected="selected">Sort By</option>
                        <option value="">Name</option>
                        <option value="">Date created</option>
                    </select>
                    <div class="dropDownSelect2"></div>
                </div>
                <button class="au-btn-filter">
                    <i class="zmdi zmdi-filter-list"></i>filters</button>
            </div>
            <div class="table-data__tool-right">
                <button class="au-btn au-btn-icon au-btn--green au-btn--small">
                    <i class="fas fa-print"></i>print</button>
                <div class="rs-select2--dark rs-select2--sm rs-select2--dark2">
                    <select class="js-select2" name="type">
                        <option selected="selected">Export</option>
                        <option value="">PDF</option>
                        <option value="">CSV</option>
                    </select>
                    <div class="dropDownSelect2"></div>
                </div>
            </div> -->
        </div>

        <div>
            <form action="" method="POST">
            <div class="row form-group">
                <div class="col-sm-3 col-md-2">
                    <label for="text-input" class=" form-control-label"><b>Sort by date</b></label>
                </div>
                <div class="col-sm-3 col-md-2">
                    <input type="text" id="tanggal_mulai" name="tanggal_mulai" class="form-control" placeholder="Mulai" required>
                </div>
                <div class="col-sm-3 col-md-2">
                    <input type="text" id="tanggal_akhir" name="tanggal_akhir" class="form-control" placeholder="Akhir" required>
                    
                </div>
                <button type="submit" name="custom" value="custom" class="btn btn-sm btn-primary">
                    Sort
                </button>
            </div>


            </form>
            <hr>

            <form action="" method="POST">
            <div class="row form-group">
                <div class="col-sm-3 col-md-2">
                    <label for="text-input" class=" form-control-label"><b>Full Search</b></label>
                </div>
                <div class="col-sm-3 col-md-2">
                    <input type="text" name="search" class="form-control" placeholder="By nama outlet atau kode bfi" required>
                </div>
                <div class="col-sm-3 col-md-2">
                <button type="submit" name="fullsearch" value="custom" class="btn btn-sm btn-primary form-control">
                    Filter
                </button>
                </div>
            </div>
            <hr>
            <br>
            </form>
        </div>

        

        <?php if (isset($_POST['custom'])): ?>
            
            <?php 
                $tanggalMulai = mysqli_escape_string($koneksi,$_POST['tanggal_mulai']);

                $tanggalSelesai = mysqli_escape_string($koneksi,$_POST['tanggal_akhir']);

                $tanggalMulaiEdit = strtotime($tanggalMulai);

                $tanggalMulai= date('Y-m-d', $tanggalMulaiEdit);

                $tanggalSelesaiEdit = strtotime($tanggalSelesai);

                $tanggalSelesai= date('Y-m-d', $tanggalSelesaiEdit);
            ?>

        <div class="table-responsive table-data" style="height: auto">
            <table id="tableok" class="mdl-data-table" style="width:100%">
                <thead>
                    <tr>
                        <td>Action</td>
                        <td>No</td>
                        <td>Nama Outlet</td>
                        <td>BFI Code</td>
                        <td>Cust Code</td>
                        <td>Nama Salesman</td>
                        <!-- <td>Salesman Code</td> -->
                        <!-- <td>ASM</td>
                        <td>TSS</td> -->
                        <td>Hari Kunjungan</td>
                        <td>Week 1</td>
                        <td>Week 2</td>
                        <td>Week 3</td>
                        <td>Week 4</td>
                        <td>Area Distributor</td>
                        <td>Nama Distributor</td>
                        <td>Channel</td>
                        <td>Sub-channel</td>
                        <td>Segment</td>
                        <td>Outlet Status</td>
                        <td>Freezer Status</td>
                        <td>Alamat</td>
                        <td>Provinsi</td>
                        <td>Kabupaten/Kota</td>
                        <td>Kecamatan</td>
                        <td>Kelurahan</td>
                        <td>Kode POS</td>
                        <td>Nama PIC</td>
                        <td>No Telp PIC</td>
                        <td>Bill To</td>
                        <td>Ship To</td>
                        <td>Flag DU</td>
                        <td>Distributor BFI Code</td>
                        <td>Freezer/Non-Freezer</td>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $no = 1;
                        include ('config/config.php');
                        $query = "SELECT * FROM focus_freezer_master WHERE tanggalPasang >= '$tanggalMulai' and tanggalPasang <= '$tanggalSelesai' GROUP BY FreezerCode ORDER BY id_freezer DESC LIMIT 0,1000";
                        $data = mysqli_query($koneksi,$query);

                        while ($result = mysqli_fetch_assoc($data)) {
                     ?>
                    <tr>
                        <td>
                            <form action="model/customers/dayvisit.php" method="post" class="form-tindaklanjut" id="form-approved-<?php echo $result['id_freezer']?>">
                                <input type="hidden" id="id" name="id_freezer" value=" <?php echo $result['id_freezer'] ?>">

                                <div class="row">
                                    <div class="col-md-12">

                                        <label>
                                            Day Visit
                                        </label>

                                        <select name="dayvisit" class="form-control" required>
                                            <option value="Minggu">Minggu</option>
                                            <option value="Senin">Senin</option>
                                            <option value="Selasa">Selasa</option>
                                            <option value="Rabu">Rabu</option>
                                            <option value="Kamis">Kamis</option>
                                            <option value="Jumat">Jumat</option>
                                            <option value="Sabtu">Sabtu</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">

                                        <label>
                                            Minggu ke - 1
                                        </label>

                                        <select name="minggu1" class="form-control" required>
                                            <option value="1">Ya</option>
                                            <option value="0">Tidak</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">

                                        <label>
                                            Minggu ke - 2
                                        </label>

                                        <select name="minggu2" class="form-control" required>
                                            <option value="1">Ya</option>
                                            <option value="0">Tidak</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">

                                        <label>
                                            Minggu ke - 3
                                        </label>

                                        <select name="minggu3" class="form-control" required>
                                            <option value="1">Ya</option>
                                            <option value="0">Tidak</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">

                                        <label>
                                            Minggu ke - 4
                                        </label>

                                        <select name="minggu4" class="form-control" required>
                                            <option value="1">Ya</option>
                                            <option value="0">Tidak</option>
                                        </select>
                                    </div>
                                </div>
                                
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-sm btn-success">
                                            Update
                                        </button>
                                   
                                        <a href="javascript:void();" class="btn btn-sm btn-dark batal-tindaklanjut">Batal</a>
                                    </div>
                                 </div>
                                    
                                </div>
                            </form>

                            <a href="javascript:void();" id="approved_placement_<?php echo $result['id_freezer']?>" class="btn btn-sm btn-warning approved_placement" data-toggle="tooltip" data-placement="top" title="Edit Day Visit & Week (1-4)" acuan="<?php echo $result['id_freezer']?>">
                                <i class="fas fa fa-calendar"></i>
                            </a>

                        </td>
                        <td><?php echo $no ?></td>
                        <td>
                            <?php echo $result['Outletname'] ?>
                        </td>
                        <td>
                            <?php echo $result['IDBelfoodd'] ?>
                        </td>
                        <td>
                            <?php echo $result['ID_PJP'] ?>
                        </td>
                        <td>
                            <?php echo $result['SalesMan_Distributor'] ?>
                        </td>
                        <!-- <td>
                            <?php echo "Salesman code" ?>
                        </td>
                        <td>
                            <?php 
                                echo 'ASM'
                             ?>
                        </td>
                        <td><?php echo 'TSS' ?></td> -->
                        <td>
                            <?php 
                              
                                echo $result['Day_Visit'];

                            ?>

                        </td>
                        <td>
                            <?php 
                                $m1 = $result['Minggu_1'];
                                if ($m1 == 1 or $m1 == '1' or $m1 == '01') {
                                    echo 'YA';
                                }else{
                                    echo 'TIDAK';
                                }
                            ?>
                        </td>
                        <td>
                            <?php 
                                $m2 = $result['Minggu_2'];
                                if ($m2 == 1 or $m2 == '1' or $m2 == '01') {
                                    echo 'YA';
                                }else{
                                    echo 'TIDAK';
                                }
                            ?>
                        </td>
                        <td>
                            <?php 
                                $m3 = $result['Minggu_3'];
                                if ($m3 == 1 or $m3 == '1' or $m3 == '01') {
                                    echo 'YA';
                                }else{
                                    echo 'TIDAK';
                                }
                            ?>        
                        </td>
                        <td>
                            <?php 
                                $m4 = $result['Minggu_4'];
                                if ($m4 == 1 or $m4 == '1' or $m4 == '01') {
                                    echo 'YA';
                                }else{
                                    echo 'TIDAK';
                                }
                            ?>
                        </td>
                        <td>
                            <?php echo $result['Area'] ?>
                        </td>
                        <td>
                            <?php echo $result['DistributorName'] ?>
                        </td>
                        <td>
                            <?php 
                                //get the distributor ID
                                $distID = $result['DistributorID'];

                                $ds = "SELECT * FROM distributor WHERE distnumber = '$distID'";
                                $q_ds = mysqli_query($koneksi,$ds);
                                $r_ds = mysqli_fetch_assoc($q_ds);

                                echo $r_ds['channel']
                             ?>
                        </td>
                        <td>
                            <?php echo $r_ds['subchannel'] ?>
                        </td>
                        <td>
                            <?php echo $r_ds['segment'] ?>
                        </td>
                        <td>
                            <?php echo $result['Outlet_Status'] ?>
                        </td>
                        <td>
                            <?php echo $result['Status_Freezer'] ?>
                        </td>
                        <td>
                            <?php echo $result['Alamat'] ?>
                        </td>
                        <td>
                            <?php echo $result['Propinsi'] ?>
                        </td>
                        <td>
                            <?php echo $result['KabKota'] ?>
                        </td>
                        <td>
                            <?php echo $result['Kecamatan'] ?>
                        </td>
                        <td>
                            <?php echo $result['Kelurahan'] ?>
                        </td>
                        <td>
                            <?php echo $result['KodePos'] ?>
                        </td>
                        <td>
                            <?php echo $result['PIC'] ?>
                        </td>
                        <td>
                            <?php echo $result['Phone'] ?>
                        </td>
                        <td>
                            <?php echo $result['IDBelfoodd'] ?>
                        </td>
                        <td>
                            <?php echo $result['IDBelfoodd'] ?>
                        </td>
                        <td>
                            <?php echo $result['Flag_DU'] ?>
                        </td>
                        <td>
                            <?php echo $result['IDBelfoodd'] ?>
                        </td>
                        <td>
                            <?php 

                                //find freezer
                                if ($result['Freezer_Owner'] == '') {
                                    echo 'Non-Freezer';
                                }else{
                                    echo 'Freezer';
                                }

                             ?>
                        </td>
                        
                    </tr>

                     <?php $no++; } ?>
                </tbody>
            </table>
        </div>

        <?php else: ?>
            <?php if (isset($_POST['fullsearch'])): ?>
            <div class="table-responsive table-data" style="height: auto">
            <table id="tableok" class="mdl-data-table" style="width:100%">
                <thead>
                    <tr>
                        <td>Action</td>
                        <td>No</td>
                        <td>Nama Outlet</td>
                        <td>BFI Code</td>
                        <td>Cust Code</td>
                        <td>Nama Salesman</td>
                        <!-- <td>Salesman Code</td> -->
                        <!-- <td>ASM</td>
                        <td>TSS</td> -->
                        <td>Hari Kunjungan</td>
                        <td>Week 1</td>
                        <td>Week 2</td>
                        <td>Week 3</td>
                        <td>Week 4</td>
                        <td>Area Distributor</td>
                        <td>Nama Distributor</td>
                        <td>Channel</td>
                        <td>Sub-channel</td>
                        <td>Segment</td>
                        <td>Outlet Status</td>
                        <td>Freezer Status</td>
                        <td>Alamat</td>
                        <td>Provinsi</td>
                        <td>Kabupaten/Kota</td>
                        <td>Kecamatan</td>
                        <td>Kelurahan</td>
                        <td>Kode POS</td>
                        <td>Nama PIC</td>
                        <td>No Telp PIC</td>
                        <td>Bill To</td>
                        <td>Ship To</td>
                        <td>Flag DU</td>
                        <td>Distributor BFI Code</td>
                        <td>Freezer/Non-Freezer</td>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $search = $_POST['search'];
                        $no = 1;
                        include ('config/config.php');
                        $query = "SELECT * FROM focus_freezer_master WHERE Outletname LIKE '%$search%' OR IDBelfoodd LIKE '%$search%' GROUP BY FreezerCode";
                        $data = mysqli_query($koneksi,$query);

                        while ($result = mysqli_fetch_assoc($data)) {
                     ?>
                    <tr>
                        <td>
                            <form action="model/customers/dayvisit.php" method="post" class="form-tindaklanjut" id="form-approved-<?php echo $result['id_freezer']?>">
                                <input type="hidden" id="id" name="id_freezer" value=" <?php echo $result['id_freezer'] ?>">

                                <div class="row">
                                    <div class="col-md-12">

                                        <label>
                                            Day Visit
                                        </label>

                                        <select name="dayvisit" class="form-control" required>
                                            <option value="Minggu">Minggu</option>
                                            <option value="Senin">Senin</option>
                                            <option value="Selasa">Selasa</option>
                                            <option value="Rabu">Rabu</option>
                                            <option value="Kamis">Kamis</option>
                                            <option value="Jumat">Jumat</option>
                                            <option value="Sabtu">Sabtu</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">

                                        <label>
                                            Minggu ke - 1
                                        </label>

                                        <select name="minggu1" class="form-control" required>
                                            <option value="1">Ya</option>
                                            <option value="0">Tidak</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">

                                        <label>
                                            Minggu ke - 2
                                        </label>

                                        <select name="minggu2" class="form-control" required>
                                            <option value="1">Ya</option>
                                            <option value="0">Tidak</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">

                                        <label>
                                            Minggu ke - 3
                                        </label>

                                        <select name="minggu3" class="form-control" required>
                                            <option value="1">Ya</option>
                                            <option value="0">Tidak</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">

                                        <label>
                                            Minggu ke - 4
                                        </label>

                                        <select name="minggu4" class="form-control" required>
                                            <option value="1">Ya</option>
                                            <option value="0">Tidak</option>
                                        </select>
                                    </div>
                                </div>
                                
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-sm btn-success">
                                            Update
                                        </button>
                                   
                                        <a href="javascript:void();" class="btn btn-sm btn-dark batal-tindaklanjut">Batal</a>
                                    </div>
                                 </div>
                                    
                                </div>
                            </form>

                            <a href="javascript:void();" id="approved_placement_<?php echo $result['id_freezer']?>" class="btn btn-sm btn-warning approved_placement" data-toggle="tooltip" data-placement="top" title="Edit Day Visit & Week (1-4)" acuan="<?php echo $result['id_freezer']?>">
                                <i class="fas fa fa-calendar"></i>
                            </a>

                        </td>
                        <td><?php echo $no ?></td>
                        <td>
                            <?php echo $result['Outletname'] ?>
                        </td>
                        <td>
                            <?php echo $result['IDBelfoodd'] ?>
                        </td>
                        <td>
                            <?php echo $result['ID_PJP'] ?>
                        </td>
                        <td>
                            <?php echo $result['SalesMan_Distributor'] ?>
                        </td>
                        <!-- <td>
                            <?php echo "Salesman code" ?>
                        </td>
                        <td>
                            <?php 
                                echo 'ASM'
                             ?>
                        </td> -->
                        <!-- <td><?php echo 'TSS' ?></td> -->
                        <td>
                            <?php 
                              
                                echo $result['Day_Visit'];

                            ?>

                        </td>
                        <td>
                            <?php 
                                $m1 = $result['Minggu_1'];
                                if ($m1 == 1 or $m1 == '1' or $m1 == '01') {
                                    echo 'YA';
                                }else{
                                    echo 'TIDAK';
                                }
                            ?>
                        </td>
                        <td>
                            <?php 
                                $m2 = $result['Minggu_2'];
                                if ($m2 == 1 or $m2 == '1' or $m2 == '01') {
                                    echo 'YA';
                                }else{
                                    echo 'TIDAK';
                                }
                            ?>
                        </td>
                        <td>
                            <?php 
                                $m3 = $result['Minggu_3'];
                                if ($m3 == 1 or $m3 == '1' or $m3 == '01') {
                                    echo 'YA';
                                }else{
                                    echo 'TIDAK';
                                }
                            ?>        
                        </td>
                        <td>
                            <?php 
                                $m4 = $result['Minggu_4'];
                                if ($m4 == 1 or $m4 == '1' or $m4 == '01') {
                                    echo 'YA';
                                }else{
                                    echo 'TIDAK';
                                }
                            ?>
                        </td>
                        <td>
                            <?php echo $result['Area'] ?>
                        </td>
                        <td>
                            <?php echo $result['DistributorName'] ?>
                        </td>
                        <td>
                            <?php 
                                //get the distributor ID
                                $distID = $result['DistributorID'];

                                $ds = "SELECT * FROM distributor WHERE distnumber = '$distID'";
                                $q_ds = mysqli_query($koneksi,$ds);
                                $r_ds = mysqli_fetch_assoc($q_ds);

                                echo $r_ds['channel']
                             ?>
                        </td>
                        <td>
                            <?php echo $r_ds['subchannel'] ?>
                        </td>
                        <td>
                            <?php echo $r_ds['segment'] ?>
                        </td>
                        <td>
                            <?php echo $result['Outlet_Status'] ?>
                        </td>
                        <td>
                            <?php echo $result['Status_Freezer'] ?>
                        </td>
                        <td>
                            <?php echo $result['Alamat'] ?>
                        </td>
                        <td>
                            <?php echo $result['Propinsi'] ?>
                        </td>
                        <td>
                            <?php echo $result['KabKota'] ?>
                        </td>
                        <td>
                            <?php echo $result['Kecamatan'] ?>
                        </td>
                        <td>
                            <?php echo $result['Kelurahan'] ?>
                        </td>
                        <td>
                            <?php echo $result['KodePos'] ?>
                        </td>
                        <td>
                            <?php echo $result['PIC'] ?>
                        </td>
                        <td>
                            <?php echo $result['Phone'] ?>
                        </td>
                        <td>
                            <?php echo $result['IDBelfoodd'] ?>
                        </td>
                        <td>
                            <?php echo $result['IDBelfoodd'] ?>
                        </td>
                        <td>
                            <?php echo $result['Flag_DU'] ?>
                        </td>
                        <td>
                            <?php echo $result['IDBelfoodd'] ?>
                        </td>
                        <td>
                            <?php 

                                //find freezer
                                if ($result['Freezer_Owner'] == '') {
                                    echo 'Non-Freezer';
                                }else{
                                    echo 'Freezer';
                                }

                             ?>
                        </td>
                        
                    </tr>

                     <?php $no++; } ?>
                </tbody>
            </table>
        </div>
        <?php else: ?>
            <div class="table-responsive table-data" style="height: auto">
            <table id="tableok" class="mdl-data-table" style="width:100%">
                <thead>
                    <tr>
                        <td>Action</td>
                        <td>No</td>
                        <td>Nama Outlet</td>
                        <td>BFI Code</td>
                        <td>Cust Code</td>
                        <td>Nama Salesman</td>
                        <!-- <td>Salesman Code</td>
                        <td>ASM</td>
                        <td>TSS</td> -->
                        <td>Hari Kunjungan</td>
                        <td>Week 1</td>
                        <td>Week 2</td>
                        <td>Week 3</td>
                        <td>Week 4</td>
                        <td>Area Distributor</td>
                        <td>Nama Distributor</td>
                        <td>Channel</td>
                        <td>Sub-channel</td>
                        <td>Segment</td>
                        <td>Outlet Status</td>
                        <td>Freezer Status</td>
                        <td>Alamat</td>
                        <td>Provinsi</td>
                        <td>Kabupaten/Kota</td>
                        <td>Kecamatan</td>
                        <td>Kelurahan</td>
                        <td>Kode POS</td>
                        <td>Nama PIC</td>
                        <td>No Telp PIC</td>
                        <td>Bill To</td>
                        <td>Ship To</td>
                        <td>Flag DU</td>
                        <td>Distributor BFI Code</td>
                        <td>Freezer/Non-Freezer</td>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $no = 1;
                        include ('config/config.php');
                        $query = "SELECT * FROM focus_freezer_master GROUP BY FreezerCode ORDER BY id_freezer DESC LIMIT 0,100";
                        $data = mysqli_query($koneksi,$query);

                        while ($result = mysqli_fetch_assoc($data)) {
                     ?>
                    <tr>
                        <td>
                            <form action="model/customers/dayvisit.php" method="post" class="form-tindaklanjut" id="form-approved-<?php echo $result['id_freezer']?>">
                                <input type="hidden" id="id" name="id_freezer" value=" <?php echo $result['id_freezer'] ?>">

                                <div class="row">
                                    <div class="col-md-12">

                                        <label>
                                            Day Visit
                                        </label>

                                        <select name="dayvisit" class="form-control" required>
                                            <option value="Minggu">Minggu</option>
                                            <option value="Senin">Senin</option>
                                            <option value="Selasa">Selasa</option>
                                            <option value="Rabu">Rabu</option>
                                            <option value="Kamis">Kamis</option>
                                            <option value="Jumat">Jumat</option>
                                            <option value="Sabtu">Sabtu</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">

                                        <label>
                                            Minggu ke - 1
                                        </label>

                                        <select name="minggu1" class="form-control" required>
                                            <option value="1">Ya</option>
                                            <option value="0">Tidak</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">

                                        <label>
                                            Minggu ke - 2
                                        </label>

                                        <select name="minggu2" class="form-control" required>
                                            <option value="1">Ya</option>
                                            <option value="0">Tidak</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">

                                        <label>
                                            Minggu ke - 3
                                        </label>

                                        <select name="minggu3" class="form-control" required>
                                            <option value="1">Ya</option>
                                            <option value="0">Tidak</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">

                                        <label>
                                            Minggu ke - 4
                                        </label>

                                        <select name="minggu4" class="form-control" required>
                                            <option value="1">Ya</option>
                                            <option value="0">Tidak</option>
                                        </select>
                                    </div>
                                </div>
                                
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-sm btn-success">
                                            Update
                                        </button>
                                   
                                        <a href="javascript:void();" class="btn btn-sm btn-dark batal-tindaklanjut">Batal</a>
                                    </div>
                                 </div>
                                    
                                </div>
                            </form>

                            <a href="javascript:void();" id="approved_placement_<?php echo $result['id_freezer']?>" class="btn btn-sm btn-warning approved_placement" data-toggle="tooltip" data-placement="top" title="Edit Day Visit & Week (1-4)" acuan="<?php echo $result['id_freezer']?>">
                                <i class="fas fa fa-calendar"></i>
                            </a>

                        </td>
                        <td><?php echo $no ?></td>
                        <td>
                            <?php echo $result['Outletname'] ?>
                        </td>
                        <td>
                            <?php echo $result['IDBelfoodd'] ?>
                        </td>
                        <td>
                            <?php echo $result['ID_PJP'] ?>
                        </td>
                        <td>
                            <?php echo $result['SalesMan_Distributor'] ?>
                        </td>
                        <!-- <td>
                            <?php echo "Salesman code" ?>
                        </td>
                        <td>
                            <?php 
                                echo 'ASM'
                             ?>
                        </td>
                        <td><?php echo 'TSS' ?></td> -->
                        <td>
                            <?php 
                              
                                echo $result['Day_Visit'];

                            ?>

                        </td>
                        <td>
                            <?php 
                                $m1 = $result['Minggu_1'];
                                if ($m1 == 1 or $m1 == '1' or $m1 == '01') {
                                    echo 'YA';
                                }else{
                                    echo 'TIDAK';
                                }
                            ?>
                        </td>
                        <td>
                            <?php 
                                $m2 = $result['Minggu_2'];
                                if ($m2 == 1 or $m2 == '1' or $m2 == '01') {
                                    echo 'YA';
                                }else{
                                    echo 'TIDAK';
                                }
                            ?>
                        </td>
                        <td>
                            <?php 
                                $m3 = $result['Minggu_3'];
                                if ($m3 == 1 or $m3 == '1' or $m3 == '01') {
                                    echo 'YA';
                                }else{
                                    echo 'TIDAK';
                                }
                            ?>        
                        </td>
                        <td>
                            <?php 
                                $m4 = $result['Minggu_4'];
                                if ($m4 == 1 or $m4 == '1' or $m4 == '01') {
                                    echo 'YA';
                                }else{
                                    echo 'TIDAK';
                                }
                            ?>
                        </td>
                        <td>
                            <?php echo $result['Area'] ?>
                        </td>
                        <td>
                            <?php echo $result['DistributorName'] ?>
                        </td>
                        <td>
                            <?php 
                                //get the distributor ID
                                $distID = $result['DistributorID'];

                                $ds = "SELECT * FROM distributor WHERE distnumber = '$distID'";
                                $q_ds = mysqli_query($koneksi,$ds);
                                $r_ds = mysqli_fetch_assoc($q_ds);

                                echo $r_ds['channel']
                             ?>
                        </td>
                        <td>
                            <?php echo $r_ds['subchannel'] ?>
                        </td>
                        <td>
                            <?php echo $r_ds['segment'] ?>
                        </td>
                        <td>
                            <?php echo $result['Outlet_Status'] ?>
                        </td>
                        <td>
                            <?php echo $result['Status_Freezer'] ?>
                        </td>
                        <td>
                            <?php echo $result['Alamat'] ?>
                        </td>
                        <td>
                            <?php echo $result['Propinsi'] ?>
                        </td>
                        <td>
                            <?php echo $result['KabKota'] ?>
                        </td>
                        <td>
                            <?php echo $result['Kecamatan'] ?>
                        </td>
                        <td>
                            <?php echo $result['Kelurahan'] ?>
                        </td>
                        <td>
                            <?php echo $result['KodePos'] ?>
                        </td>
                        <td>
                            <?php echo $result['PIC'] ?>
                        </td>
                        <td>
                            <?php echo $result['Phone'] ?>
                        </td>
                        <td>
                            <?php echo $result['IDBelfoodd'] ?>
                        </td>
                        <td>
                            <?php echo $result['IDBelfoodd'] ?>
                        </td>
                        <td>
                            <?php echo $result['Flag_DU'] ?>
                        </td>
                        <td>
                            <?php echo $result['IDBelfoodd'] ?>
                        </td>
                        <td>
                            <?php 

                                //find freezer
                                if ($result['Freezer_Owner'] == '') {
                                    echo 'Non-Freezer';
                                }else{
                                    echo 'Freezer';
                                }

                             ?>
                        </td>
                        
                    </tr>

                     <?php $no++; } ?>
                </tbody>
            </table>
        </div>
        <?php endif ?>
        
        <?php endif ?>
        <!-- END DATA TABLE -->
    </div>
</div>
