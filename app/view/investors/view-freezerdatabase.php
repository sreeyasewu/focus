<?php 
    if (isset($_GET['status'])) {
        $status = $_GET['status'];

        if ($status == 11) {
            echo '<div class="alert alert-success" role="alert">
                Data berhasil diperbaruhi
            </div>';
        }elseif ($status == 0) {
            echo '<div class="alert alert-danger" role="alert">
                Terjadi Kesalahan! tidak dapat memperbaruhi data
            </div>';
        }else{
            echo '<div class="alert alert-warning" role="alert">
                Terjadi kesalahan koneksi internet, silkahkan ulangi kembali
            </div>';
        }
    }

    if (isset($_GET['editstatus'])) {
        $status = $_GET['editstatus'];

        if ($status == 1) {
            echo '<div class="alert alert-success" role="alert">
                Data investor berhasil di perbaruhi
            </div>';
        }elseif ($status == 0) {
            echo '<div class="alert alert-danger" role="alert">
                Terjadi Kesalahan! tidak dapat mengubah data user
            </div>';
        }else{
            echo '<div class="alert alert-warning" role="alert">
                Terjadi kesalahan koneksi internet, silkahkan ulangi kembali
            </div>';
        }
    }

    include ('config/config.php');
    $role = $_SESSION['role'];
    $vendor = $_SESSION['nama'];

 ?>
<div class="row">
<div class="col-md-12">
        <!-- DATA TABLE -->
        <h3 class="title-5 m-b-35">view freezers</h3>
	<a href="export-freezer.php" class="btn btn-dark btn-sm">Download All Freezer</a>
        <div class="table-data__tool">
            <!-- <div class="table-data__tool-left">
                <div class="rs-select2--light rs-select2--md">
                    <select class="js-select2" name="property">
                        <option selected="selected">Roles</option>
                        <option value="">owner</option>
                        <option value="">stakeholders</option>
                        <option value="">investors</option>
                        <option value="">marketing</option>
                        <option value="">admin</option>
                        <option value="">approval</option>
                    </select>
                    <div class="dropDownSelect2"></div>
                </div>
                <div class="rs-select2--light rs-select2--sm">
                    <select class="js-select2" name="time">
                        <option selected="selected">Filter By</option>
                        <option value="">Name</option>
                        <option value="">Date created</option>
                    </select>
                    <div class="dropDownSelect2"></div>
                </div>
                <button class="au-btn-filter">
                    <i class="zmdi zmdi-filter-list"></i>filters</button>
            </div>
            <div class="table-data__tool-right">
                <button class="au-btn au-btn-icon au-btn--green au-btn--small">
                    <i class="fas fa-print"></i>print</button>
                <div class="rs-select2--dark rs-select2--sm rs-select2--dark2">
                    <select class="js-select2" name="type">
                        <option selected="selected">Export</option>
                        <option value="">PDF</option>
                        <option value="">CSV</option>
                    </select>
                    <div class="dropDownSelect2"></div>
                </div>
            </div> -->
        </div>
        <div>
            <form action="" method="POST">
            <div class="row form-group">
                <div class="col-sm-3 col-md-2">
                    <label for="text-input" class=" form-control-label"><b>Filter by date</b></label>
                </div>
                <div class="col-sm-3 col-md-2">
                    <input type="text" id="tanggal_mulai" name="tanggal_mulai" class="form-control" placeholder="Mulai" required>
                </div>
                <div class="col-sm-3 col-md-2">
                    <input type="text" id="tanggal_akhir" name="tanggal_akhir" class="form-control" placeholder="Akhir" required>
                    
                </div>
                <button type="submit" name="custom" value="custom" class="btn btn-sm btn-primary">
                    Filter
                </button>
            </div>


            </form>
            <hr>

            <form action="" method="POST">
            <div class="row form-group">
                <div class="col-sm-3 col-md-2">
                    <label for="text-input" class=" form-control-label"><b>Full Search</b></label>
                </div>
                <div class="col-sm-3 col-md-2">
                    <input type="text" name="search" class="form-control" placeholder="By nama outlet atau kode bfi" required>
                </div>
                <div class="col-sm-3 col-md-2">
                <button type="submit" name="fullsearch" value="custom" class="btn btn-sm btn-primary form-control">
                    Filter
                </button>
                </div>
            </div>
            <hr>
            <br>
            </form>
        </div>


        <?php if (isset($_POST['custom'])): ?>
            
            <?php 
                $tanggalMulai = mysqli_escape_string($koneksi,$_POST['tanggal_mulai']);

                $tanggalSelesai = mysqli_escape_string($koneksi,$_POST['tanggal_akhir']);

                $tanggalMulaiEdit = strtotime($tanggalMulai);

                $tanggalMulai= date('Y-m-d', $tanggalMulaiEdit);

                $tanggalSelesaiEdit = strtotime($tanggalSelesai);

                $tanggalSelesai= date('Y-m-d', $tanggalSelesaiEdit);
            ?>

        <div class="table-responsive table-data" style="height: auto">
            <table id="tableok" class="mdl-data-table" style="width:100%">
                <thead>
                    <tr>
                        <td>No</td>
                        <td>Nama Outlet</td>
                        <td>BFI Code</td>
                        <td>Freezer Code</td>
                        <td>Supplier</td>
                        <td>Brand</td>
                        <td>Type</td>
                        <td>BarcodeFANumber</td>
                        <td>Nomor Mesin</td>
                        <td>BatchPO</td>
                        <td>Status</td>
                        <td>Freezer Owner</td>
                        <td>Latitude</td>
                        <td>Longitude</td>
                        <td>Placement Date</td>
                        <td>Tanggal Tarik/Tukar</td>
                        <td>Customer Type</td>

                        <?php 

                                if ($role == 'VENDOR') {
                                    
                                }else{
                                    echo '<td>Action</td>';
                                }
                            ?>
                        
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $no = 1;
                        

                        if ($role == 'VENDOR') {
                            $query = "SELECT * FROM focus_freezer_master WHERE tanggalPasang >= '$tanggalMulai' and tanggalPasang <= '$tanggalSelesai' WHERE Supplier LIKE '%$vendor%' GROUP BY FreezerCode ORDER BY id_freezer DESC";
                        }else{
                            $query = "SELECT * FROM focus_freezer_master WHERE tanggalPasang >= '$tanggalMulai' and tanggalPasang <= '$tanggalSelesai' GROUP BY FreezerCode ORDER BY id_freezer DESC";
                        }

                        $data = mysqli_query($koneksi,$query);

                        while ($result = mysqli_fetch_assoc($data)) {
                     ?>
                    <tr>
                        <td><?php echo $no ?></td>
                        <td>
                            <?php echo $result['Outletname'] ?>
                        </td>
                        <td>
                            <?php echo $result['IDBelfoodd'] ?>
                        </td>
                        <td>
                            <?php echo $result['FreezerCode'] ?>
                        </td>
                        <td>
                            <?php echo $result['Supplier'] ?>
                        </td>
                        <td>
                            <?php echo $result['Brand'] ?>
                        </td>
                        <td>
                            <?php echo $result['Type'] ?>
                        </td>
                        <td>
                            <?php echo $result['BarcodeFANumber'] ?>
                        </td>
                        <td>
                            <?php 


                                $id_number = $result['ID_Number'];

                                // if ($id_number == '') {
                                //     $cari = "SELECT placement_notes FROM focus_freezer_placement WHERE IDNumber = '$id_number'";
                                //     $d = mysqli_query($koneksi,$cari);
                                //     $r = mysqli_fetch_assoc($d);

                                //     echo $r['placement_notes'];
                                // }else{
                                    echo $id_number;
                                // }

                                
                            ?>
                        </td>
                        <td>
                            <?php echo $result['BatchPO'] ?>
                        </td>
                        <td>
                            <?php echo $result['Keterangan'] ?>
                        </td>
                        <td>
                            <?php echo $result['Freezer_Owner'] ?>
                        </td>
                        <td>
                            <?php echo $result['Latitude'] ?>
                        </td>
                        <td>
                            <?php echo $result['Longitude'] ?>
                        </td>
                        <td>
                            <?php echo $result['TanggalPasang'] ?>
                        </td>
                        <td>
                            <?php echo $result['TanggalPenarikan'] ?>
                        </td>
                        <td>
                            <?php echo $result['Customer_Type'] ?>
                        </td>
                         <?php 
                             

                                if ($role == 'VENDOR') {
                                    
                                }else{
                                    echo '<td><button class="btn btn-dark" data-toggle="tooltip" data-placement="top" title="Edit">
                                <a href="index.php?mod=freezerdatabase&class=edit&id='.$result['id_freezer'].'" style="color:white"><i class="zmdi zmdi-edit"></i></a>
                            </button></td>';
                                }
                            ?>
                        
                    </tr>

                     <?php $no++; } ?>
                </tbody>
            </table>
        </div>

        <?php else: ?>
            <?php if (isset($_POST['fullsearch'])): ?>
            <div class="table-responsive table-data" style="height: auto">
            <table id="tableok" class="mdl-data-table" style="width:100%">
                <thead>
                    <tr>
                        <td>No</td>
                        <td>Nama Outlet</td>
                        <td>BFI Code</td>
                        <td>Freezer Code</td>
                        <td>ID NUMBER</td>
                        <td>Supplier</td>
                        <td>Brand</td>
                        <td>Type</td>
                        <td>BarcodeFANumber</td>
                        <td>Nomor Mesin</td>
                        <td>BatchPO</td>
                        <td>Status</td>
                        <td>Freezer Owner</td>
                        <td>Latitude</td>
                        <td>Longitude</td>
                        <td>Placement Date</td>
                        <td>Tanggal Tarik/Tukar</td>
                        <td>Customer Type</td>
                        <?php 
                                $vendor = $_SESSION['role'];

                                if ($vendor == 'VENDOR') {
                                    
                                }else{
                                    echo '<td>Action</td>';
                                }
                            ?>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $search = $_POST['search'];
                        $no = 1;
                        
                        if ($role == 'VENDOR') {
                            $query = "SELECT * FROM focus_freezer_master WHERE Outletname LIKE '%$search%' OR IDBelfoodd LIKE '%$search%' AND Supplier LIKE '%$vendor%' GROUP BY FreezerCode ";
                        }else{
                            $query = "SELECT *,focus_freezer_master.id_freezer as id_freezer FROM focus_freezer_master LEFT JOIN distributor ON focus_freezer_master.IDBelfoodd=distributor.custid LEFT JOIN focus_freezer_placement ON distributor.iddistributor=focus_freezer_placement.id_distributor WHERE Outletname LIKE '%$search%' OR IDBelfoodd LIKE '%$search%'";
                        }
                        
                        $data = mysqli_query($koneksi,$query);

                        while ($result = mysqli_fetch_assoc($data)) {
                     ?>
                    <tr>
                        <td><?php echo $no ?></td>
                        <td>
                            <?php echo $result['Outletname'] ?>
                        </td>
                        <td>
                            <?php echo $result['IDBelfoodd'] ?>
                        </td>
                        <td>
                            <?php echo $result['FreezerCode'] ?>
                        </td>
                        <td>
                            <?php echo $result['IDNumber'] ?>
                        </td>
                        <td>
                            <?php echo $result['Supplier'] ?>
                        </td>
                        <td>
                            <?php echo $result['Brand'] ?>
                        </td>
                        <td>
                            <?php echo $result['Type'] ?>
                        </td>
                        <td>
                            <?php echo $result['BarcodeFANumber'] ?>
                        </td>
                        <td>
                            <?php 
                                $id_number = $result['ID_Number'];

                                // if ($id_number == '') {
                                //     $cari = "SELECT placement_notes FROM focus_freezer_placement WHERE IDNumber = '$id_number'";
                                //     $d = mysqli_query($koneksi,$cari);
                                //     $r = mysqli_fetch_assoc($d);

                                //     echo $r['placement_notes'];
                                // }else{
                                    echo $id_number;
                            ?>
                        </td>
                        <td>
                            <?php echo $result['BatchPO'] ?>
                        </td>
                        <td>
                            <?php echo $result['Keterangan'] ?>
                        </td>
                        <td>
                            <?php echo $result['Freezer_Owner'] ?>
                        </td>
                        <td>
                            <?php echo $result['Latitude'] ?>
                        </td>
                        <td>
                            <?php echo $result['Longitude'] ?>
                        </td>
                        <td>
                            <?php echo $result['TanggalPasang'] ?>
                        </td>
                        <td>
                            <?php echo $result['TanggalPenarikan'] ?>
                        </td>
                        <td>
                            <?php 

                                $tipe = $result['Customer_Type'];

                                if ($tipe == '01 {DIRECT}') {
                                    echo 'PRIMARY';
                                }else{
                                    echo "SECONDARY";
                                }
                            ?>
                        </td>
                        
                            <?php 
                                $vendor = $_SESSION['role'];

                                if ($vendor == 'VENDOR') {
                                    
                                }else{
                                    echo '<td><button class="btn btn-dark" data-toggle="tooltip" data-placement="top" title="Edit">
                                <a href="index.php?mod=freezerdatabase&class=edit&id='.$result['id_freezer'].'" style="color:white"><i class="zmdi zmdi-edit"></i></a>
                            </button></td>';
                                }
                            ?>
                        
                        
                    </tr>

                     <?php $no++; } ?>
                </tbody>
            </table>
        </div>
        <?php else: ?>
            <div class="table-responsive table-data" style="height: auto">
            <table id="tableok" class="mdl-data-table" style="width:100%">
                <thead>
                    <tr>
                        <td>No</td>
                        <td>Nama Outlet</td>
                        <td>BFI Code</td>
                        <td>Freezer Code</td>
                        <td>ID Number</td>
                        <td>Supplier</td>
                        <td>Brand</td>
                        <td>Type</td>
                        <td>BarcodeFANumber</td>
                        <td>Nomor Mesin</td>
                        <td>BatchPO</td>
                        <td>Status</td>
                        <td>Freezer Owner</td>
                        <td>Latitude</td>
                        <td>Longitude</td>
                        <td>Placement Date</td>
                        <td>Tanggal Tarik/Tukar</td>
                        <td>Customer Type</td>
                        <?php 
                                

                                if ($role == 'VENDOR') {
                                    
                                }else{
                                    echo '<td>Action</td>';
                                }
                            ?>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $no = 1;
                        
                        if ($role == 'VENDOR') {
                            $query = "SELECT * FROM focus_freezer_master WHERE Supplier LIKE '%$vendor%' GROUP BY FreezerCode  ORDER BY id_freezer DESC LIMIT 0,100";
                        }else{
                            $query = "SELECT *,focus_freezer_master.id_freezer as id_freezer FROM focus_freezer_master INNER JOIN distributor ON focus_freezer_master.IDBelfoodd=distributor.custid LEFT JOIN focus_freezer_placement ON distributor.iddistributor=focus_freezer_placement.id_distributor LIMIT 0,100";
                        }
                        
                        $data = mysqli_query($koneksi,$query);

                        while ($result = mysqli_fetch_assoc($data)) {
                     ?>
                    <tr>
                        <td><?php echo $no ?></td>
                        <td>
                            <?php echo $result['Outletname'] ?>
                        </td>
                        <td>
                            <?php echo $result['IDBelfoodd'] ?>
                        </td>
                        <td>
                            <?php echo $result['FreezerCode'] ?>
                        </td>
                        <td>
                            <?php echo $result['IDNumber'] ?>
                        </td>
                        <td>
                            <?php echo $result['Supplier'] ?>
                        </td>
                        <td>
                            <?php echo $result['Brand'] ?>
                        </td>
                        <td>
                            <?php echo $result['Type'] ?>
                        </td>
                        <td>
                            <?php echo $result['BarcodeFANumber'] ?>
                        </td>
                        <td>
                            <?php 
                                $id_number = $result['ID_Number'];

                                // if ($id_number == '') {
                                //     $cari = "SELECT placement_notes FROM focus_freezer_placement WHERE IDNumber = '$id_number'";
                                //     $d = mysqli_query($koneksi,$cari);
                                //     $r = mysqli_fetch_assoc($d);

                                //     echo $r['placement_notes'];
                                // }else{
                                    echo $id_number;
                            ?>
                        </td>
                        <td>
                            <?php echo $result['BatchPO'] ?>
                        </td>
                        <td>
                            <?php echo $result['Keterangan'] ?>
                        </td>
                        <td>
                            <?php echo $result['Freezer_Owner'] ?>
                        </td>
                        <td>
                            <?php echo $result['Latitude'] ?>
                        </td>
                        <td>
                            <?php echo $result['Longitude'] ?>
                        </td>
                        <td>
                            <?php echo $result['TanggalPasang'] ?>
                        </td>
                        <td>
                            <?php echo $result['TanggalPenarikan'] ?>
                        </td>
                        <td>
                            <?php 
                                $tipe = $result['Customer_Type'];

                                if ($tipe == '01 {DIRECT}') {
                                    echo 'PRIMARY';
                                }else{
                                    echo "SECONDARY";
                                }
                                 ?>
                        </td>
                         <?php 
                                

                                if ($role == 'VENDOR') {
                                    
                                }else{
                                    echo '<td><button class="btn btn-dark" data-toggle="tooltip" data-placement="top" title="Edit">
                                <a href="index.php?mod=freezerdatabase&class=edit&id='.$result['id_freezer'].'" style="color:white"><i class="zmdi zmdi-edit"></i></a>
                            </button></td>';
                                }
                            ?>
                        
                    </tr>

                     <?php $no++; } ?>
                </tbody>
            </table>
        </div>
        <?php endif ?>
        


        <!-- <div class="table-responsive table-responsive-data2">
            <table class="table table-data2">
                <thead>
                    <tr>
                        <th>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </th>
                        <th>name</th>
                        <th>email</th>
                        <th>username</th>
                        <th>date created</th>
                        <th>roles</th>
                        <th>password</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="tr-shadow">
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td>Lori Lynch</td>
                        <td>
                            <span class="block-email">lori@gmail.com</span>
                        </td>
                        <td class="desc">Lori345</td>
                        <td>2018-09-17 02:12</td>
                        <td>
                            <span class="role admin">Admin</span>
                        </td>
                        <td>************</td>
                        <td>
                            <div class="table-data-feature">
                                <button class="item" data-toggle="tooltip" data-placement="top" title="View Password">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <i class="zmdi zmdi-delete"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Banned">
                                    <i class="fas fa-ban"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                    <tr class="spacer"></tr>
                    <tr class="tr-shadow">
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td>John Martin</td>
                        <td>
                            <span class="block-email">johnmartin@gmail.com</span>
                        </td>
                        <td class="desc">Martin.jhon</td>
                        <td>2018-09-01 07:57</td>
                        <td>
                            <span class="role stakeholders">Stakeholders</span>
                        </td>
                        <td>************</td>
                        <td>
                            <div class="table-data-feature">
                                <button class="item" data-toggle="tooltip" data-placement="top" title="View Password">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <i class="zmdi zmdi-delete"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Banned">
                                    <i class="fas fa-ban"></i>
                                </button>
                            </div>
                        </td>
                    </tr>

                    <tr class="spacer"></tr>
                    <tr class="tr-shadow">
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td>Rahadian Agus</td>
                        <td>
                            <span class="block-email">rahadianagus@gmail.com</span>
                        </td>
                        <td class="desc">MrAgus.69</td>
                        <td>2018-09-01 06:06</td>
                        <td>
                            <span class="role investors">Investors</span>
                        </td>
                        <td>************</td>
                        <td>
                            <div class="table-data-feature">
                                <button class="item" data-toggle="tooltip" data-placement="top" title="View Password">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <i class="zmdi zmdi-delete"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Banned">
                                    <i class="fas fa-ban"></i>
                                </button>
                            </div>
                        </td>
                    </tr>

                    <tr class="spacer"></tr>
                    <tr class="tr-shadow">
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td>Hasbi</td>
                        <td>
                            <span class="block-email">auskreditsyari@gmail.com</span>
                        </td>
                        <td class="desc">Hasbi9899</td>
                        <td>2018-09-01 05:57</td>
                        <td>
                            <span class="role owner">Owner</span>
                        </td>
                        <td>************</td>
                        <td>
                            <div class="table-data-feature">
                                <button class="item" data-toggle="tooltip" data-placement="top" title="View Password">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <i class="zmdi zmdi-delete"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Banned">
                                    <i class="fas fa-ban"></i>
                                </button>
                            </div>
                        </td>
                    </tr>

                    <tr class="spacer"></tr>
                    <tr class="tr-shadow">
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td>Ahmad Rupandi</td>
                        <td>
                            <span class="block-email">ahmadrupandi@gmail.com</span>
                        </td>
                        <td class="desc">RupandiPejantanTangguh</td>
                        <td>2018-09-01 05:55</td>
                        <td>
                            <span class="role marketing">Marketing</span>
                        </td>
                        <td>************</td>
                        <td>
                            <div class="table-data-feature">
                                <button class="item" data-toggle="tooltip" data-placement="top" title="View Password">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <i class="zmdi zmdi-delete"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Banned">
                                    <i class="fas fa-ban"></i>
                                </button>
                            </div>
                        </td>
                    </tr>

                    <tr class="spacer"></tr>
                    <tr class="tr-shadow">
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox">
                                <span class="au-checkmark"></span>
                            </label>
                        </td>
                        <td>Supriadi</td>
                        <td>
                            <span class="block-email">supriadi@gmail.com</span>
                        </td>
                        <td class="desc">Supri.Adi31</td>
                        <td>2018-09-01 05:57</td>
                        <td>
                            <span class="role approval">Approval</span>
                        </td>
                        <td>************</td>
                        <td>
                            <div class="table-data-feature">
                                <button class="item" data-toggle="tooltip" data-placement="top" title="View Password">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <i class="zmdi zmdi-delete"></i>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Banned">
                                    <i class="fas fa-ban"></i>
                                </button>
                            </div>
                        </td>
                    </tr>

                </tbody>
            </table>
        </div> -->
        <?php endif ?>
        <!-- END DATA TABLE -->
    </div>
</div>
