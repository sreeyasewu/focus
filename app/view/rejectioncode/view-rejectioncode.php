<?php 
    if (isset($_GET['status'])) {
        $status = $_GET['status'];

        if ($status == 1) {
            echo '<div class="alert alert-success" role="alert">
                Data berhasil di hapus
            </div>';
        }elseif ($status == 0) {
            echo '<div class="alert alert-danger" role="alert">
                Terjadi Kesalahan! tidak dapat menghapus data
            </div>';
        }else{
            echo '<div class="alert alert-warning" role="alert">
                Terjadi kesalahan koneksi internet, silkahkan ulangi kembali
            </div>';
        }
    }

    if (isset($_GET['editstatus'])) {
        $status = $_GET['editstatus'];

        if ($status == 1) {
            echo '<div class="alert alert-success" role="alert">
                Data berhasil di perbaruhi
            </div>';
        }elseif ($status == 0) {
            echo '<div class="alert alert-danger" role="alert">
                Terjadi Kesalahan! tidak dapat mengubah data 
            </div>';
        }else{
            echo '<div class="alert alert-warning" role="alert">
                Terjadi kesalahan koneksi internet, silkahkan ulangi kembali
            </div>';
        }
    }
 ?>
<div class="row">
<div class="col-md-12">
        <!-- DATA TABLE -->
        <h3 class="title-5 m-b-35">tipe freezer</h3>
        <div class="table-data__tool">
            <!-- <div class="table-data__tool-left">
                <div class="rs-select2--light rs-select2--md">
                    <select class="js-select2" name="property">
                        <option selected="selected">Roles</option>
                        <option value="">owner</option>
                        <option value="">stakeholders</option>
                        <option value="">investors</option>
                        <option value="">marketing</option>
                        <option value="">admin</option>
                        <option value="">approval</option>
                    </select>
                    <div class="dropDownSelect2"></div>
                </div>
                <div class="rs-select2--light rs-select2--sm">
                    <select class="js-select2" name="time">
                        <option selected="selected">Sort By</option>
                        <option value="">Name</option>
                        <option value="">Date created</option>
                    </select>
                    <div class="dropDownSelect2"></div>
                </div>
                <button class="au-btn-filter">
                    <i class="zmdi zmdi-filter-list"></i>filters</button>
            </div>
            <div class="table-data__tool-right">
                <button class="au-btn au-btn-icon au-btn--green au-btn--small">
                    <i class="fas fa-print"></i>print</button>
                <div class="rs-select2--dark rs-select2--sm rs-select2--dark2">
                    <select class="js-select2" name="type">
                        <option selected="selected">Export</option>
                        <option value="">PDF</option>
                        <option value="">CSV</option>
                    </select>
                    <div class="dropDownSelect2"></div>
                </div>
            </div> -->
        </div>

        <div class="table-responsive table-data" style="height: auto">
            <table id="tableok" class="mdl-data-table" style="width:100%">
                <thead>
                    <tr>
                        <td>No</td>
                        <td>Code</td>
                        <td>Keterangan</td>
                        <td>Date</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $no = 1;
                        include ('config/config.php');
                        $query = "SELECT * FROM focus_rejection_code ";
                        $data = mysqli_query($koneksi,$query);

                        while ($result = mysqli_fetch_assoc($data)) {
                     ?>
                    <tr>
                        <td><?php echo $no ?></td>
                        <td>
                            <?php echo $result['rejection_code'] ?>
                        </td>
                        <td>
                            <?php echo $result['ket'] ?>
                        </td>
                        <td>
                            <?php echo $result['date'] ?>
                        </td>
                        
                        <td>
                            <div class="table-data-feature">
                                
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <a href="index.php?mod=rejectioncode&class=edit&id=<?php echo $result['id_rejection']; ?>" ><i class="zmdi zmdi-edit"></i></a>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <a href="index.php?mod=rejectioncode&class=delete&id=<?php echo $result['id_rejection'] ?>"><i class="zmdi zmdi-delete"></i></a>
                                </button>
                            </div>
                        </td>
                    </tr>

                     <?php $no++; } ?>
                </tbody>
            </table>
        </div>

        <!-- END DATA TABLE -->
    </div>
</div>