<?php 

include ('../config/config.php');

if (isset($_GET['class'])) {
    $class = $_GET['class'];
    if ($class == 'view') {
        include ('view/rejectioncode/view-rejectioncode.php');
    }elseif ($class == 'add') {
        include ('view/rejectioncode/add-rejectioncode.php');
    }elseif ($class == 'edit') {
        include ('view/rejectioncode/edit-rejectioncode.php');
    }elseif ($class == 'delete') {
        $hapus = true;
        if ($hapus == true) {
            if (isset($_GET['id'])) {
                $id = $_GET['id'];
                $query = "SELECT * FROM focus_rejection_code WHERE id_rejection = '$id'";
                $sql = mysqli_query($koneksi,$query);
                $data = mysqli_fetch_assoc($sql);
            }
                 echo '<div class="alert alert-danger animated fadeInDownBig" role="alert"  id="notif" style="z-index:9999;position:absolute;margin:auto;background:#333;color:white;margin-left:20%;margin-top:15%;">
                        <center>
                            <h4 class="alert-heading">Warning!</h4>
                            <p>Yakin ingin menghapus rejection code dengan keterangan '.$data['ket'].' ?</p>
                            <p style="font-style:italic;font-size:12px;color:#dc3545"><i class="fa fa-warning"></i> Data yang dihapus tidak dapat dikembalikan lagi</p>
                            <a class="btn btn-warning btn-lg" href="model/rejectioncode/delete.php?id='.$id.'" id="ya" id="hapus">YA</a>
                            <button class="btn btn-secondary btn-lg" id="tidak" onclick="notif_close();">TIDAK</button>
                        </center>
                        </div>';
            }
            include ('view/rejectioncode/view-rejectioncode.php');
    }else{
        include ('view/rejectioncode/view-rejectioncode.php');
    }
}else{
    include ('view/rejectioncode/view-rejectioncode.php');
}
?>