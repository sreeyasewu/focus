<?php 

    //hitung jumlah Customer
    $q_cs = "SELECT count(*) as totalcustomer FROM distributor WHERE statustoko = 'DEAL'";
    $d_cs = mysqli_query($koneksi, $q_cs);
    $r_cs = mysqli_fetch_assoc($d_cs);

    //hitung jumlah akad
    $q_akad = "SELECT count(*) as rent FROM focus_freezer_master WHERE Freezer_Owner = 'Rent'";
    $d_akad = mysqli_query($koneksi, $q_akad);
    $r_akad = mysqli_fetch_assoc($d_akad);

    //hitung jumlah investor
    $q_inv = "SELECT count(*) as own FROM focus_freezer_master WHERE Freezer_Owner = 'Own'";
    $d_inv = mysqli_query($koneksi,$q_inv);
    $r_inv = mysqli_fetch_assoc($d_inv);

    //hitung total order
    $q_order = "SELECT count(*) as deal FROM distributor WHERE statustoko = 'DEAL'";
    $d_order = mysqli_query($koneksi, $q_order);
    $r_order = mysqli_fetch_assoc($d_order);


    
 ?>

<div class="row m-t-25">
                        <div class="col-sm-6 col-lg-3">
                            <div class="overview-item overview-item--c1">
                                <div class="overview__inner">
                                    <div class="overview-box clearfix">
                                        <div class="icon">
                                            <i class="fas fa fa-group"></i>
                                        </div>
                                        <div class="text">
                                            <h2><?php echo $r_cs['totalcustomer'] ?></h2>
                                            <span>Customer</span>
                                        </div>
                                    </div>
                                    <br>
                                    <!-- <div class="overview-chart">
                                        <canvas id="widgetChart1"></canvas>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="overview-item overview-item--c2">
                                <div class="overview__inner">
                                    <div class="overview-box clearfix">
                                        <div class="icon">
                                            <i class="fas fa fa-server"></i>
                                        </div>
                                        <div class="text">
                                            <h2><?php echo $r_inv['own'] ?></h2>
                                            <span>Freezer Own</span>
                                        </div>
                                    </div>
                                    <br>
                                    <!-- <div class="overview-chart">
                                        <canvas id="widgetChart2"></canvas>
                                    </div>
 -->                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="overview-item overview-item--c3">
                                <div class="overview__inner">
                                    <div class="overview-box clearfix">
                                        <div class="icon">
                                            <i class="fas fa fa-user"></i>
                                        </div>
                                        <div class="text">
                                            <h2><?php echo $r_akad['rent'] ?></h2>
                                            <span>Frezer Rent</span>
                                        </div>
                                    </div>
                                    <br>
                                    <!-- <div class="overview-chart">
                                        <canvas id="widgetChart3"></canvas>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="overview-item overview-item--c4">
                                <div class="overview__inner">
                                    <div class="overview-box clearfix">
                                        <div class="icon">
                                            <i class="fa fa-heart"></i>
                                        </div>
                                        <div class="text">
                                            <h2><?php echo $r_order['deal'] ?></h2>
                                            <span>Deal</span>
                                        </div>
                                    </div>
                                    <br>
                                    <!-- <div class="overview-chart">
                                        <canvas id="widgetChart4"></canvas>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div> 
                     
    
                    <!-- <div class="row">
                        <div class="col-lg-12">
                            <h2 class="title-1 m-b-25">Latest Akad</h2>
                            <div class="table-responsive table--no-card m-b-40">
                                <table class="table table-borderless table-striped table-earning">
                                    <thead>
                                        <tr>
                                            <th>Tanggal Akad</th>
                                            <th>ID Akad</th>
                                            <th>Nama Kustomer</th>
                                            <th class="text-right">product</th>
                                            <th class="text-right">Tenor</th>
                                            <th class="text-right">total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            
                                            $query = "SELECT * FROM aus_order WHERE status = 'approved' and approval_date != '0000-00-00' LIMIT 0,20";
                                            $data = mysqli_query($koneksi,$query);


                                            while ($result = mysqli_fetch_assoc($data)) {
                                         ?>
                                        <tr>
                                             <?php 
                                                $id_order = $result['id_order'];
                                                $q_akad = "SELECT * FROM aus_akad WHERE id_order = '$id_order'";
                                                $d_akad = mysqli_query($koneksi,$q_akad);
                                                $result_akad = mysqli_fetch_assoc($d_akad);

                                                
                                             ?>
                                            <td>
                                                <?php 
                                                    $tgl_akad = $result_akad['tgl_akad']; 
                                                    $tgl_akad = strtotime($tgl_akad);
                                                    $tgl_akad = date('d F Y',$tgl_akad);

                                                    echo $tgl_akad;
                                                ?>
                                            </td>
                                            <td>
                                                <?php  echo "AUS-AK00".$result_akad['id_akad']; ?>
                                            </td>
                                            <td>
                                                <?php
                                                    $id_customer = $result['id_customer'];
                                                    $q_cs = "SELECT * FROM aus_customer WHERE id_customer ='$id_customer'";
                                                    $d_cs = mysqli_query($koneksi,$q_cs);
                                                    $result_cs = mysqli_fetch_assoc($d_cs);

                                                    echo $result_cs['nama_lengkap'];
                                                 ?>

                                            </td>
                                            <td class="text-right">
                                                <?php 
                                                    $id_product = $result['id_product'];
                                                    $q_pr = "SELECT * FROM aus_products WHERE id_product ='$id_product'";
                                                    $d_pr = mysqli_query($koneksi,$q_pr);
                                                    $result_pr = mysqli_fetch_assoc($d_pr);

                                                    echo $result_pr['name'];
                                                 ?>
                                            </td>
                                            <td class="text-right">
                                                <?php 

                                                //cari tenor
                                                $q_tenor = "SELECT * FROM aus_customer_rincian_angsuran WHERE id_order = '$id_order'";
                                                $d_tenor = mysqli_query($koneksi, $q_tenor);
                                                $r_tenor = mysqli_fetch_assoc($d_tenor);

                                                echo $r_tenor['tenor']." Bulan";

                                                 ?>

                                            </td>
                                            <td class="text-right">
                                                <?php echo "<span class='harga_rp'>".$r_tenor['total_bayar']."</span>" ?>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>  -->

                    <!-- <div class="row">
                        <div class="col-lg-3">
                            <h2 class="title-1 m-b-25">Top Products</h2>
                            <div class="au-card au-card--bg-blue au-card-top-countries m-b-40">
                                <div class="au-card-inner">
                                    <div class="table-responsive">
                                        <table class="table table-top-countries">
                                            <tbody>
                                                <tr>
                                                    <td>Name</td>
                                                    <td class="text-right">Total</td>
                                                </tr>
                                                <tr>
                                                    <td>Motor Yamaha</td>
                                                    <td class="text-right">32</td>
                                                </tr>
                                                <tr>
                                                    <td>TV Samsung LED 32 Inch</td>
                                                    <td class="text-right">26</td>
                                                </tr>
                                                <tr>
                                                    <td>iPhone X 64GB</td>
                                                    <td class="text-right">25</td>
                                                </tr>
                                                <tr>
                                                    <td>Camera C430W 4k</td>
                                                    <td class="text-right">23</td>
                                                </tr>
                                                <tr>
                                                    <td>Samsung S8 Black</td>
                                                    <td class="text-right">19</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-9">
                            <h2 class="title-1 m-b-25">installment payment</h2>
                            <div class="table-responsive table--no-card m-b-40">
                                <table class="table table-borderless table-striped table-earning">
                                    <thead>
                                        <tr>
                                            <th>date</th>
                                            <th>akad ID</th>
                                            <th>name</th>
                                            <th class="text-right">installment</th>
                                            <th class="text-right">total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>2018-09-17 05:57</td>
                                            <td>100298</td>
                                            <td>Ibnu Firdaus</td>
                                            <td class="text-right">4</td>
                                            <td class="text-right">Rp 675.000</td>
                                        </tr>
                                        <tr>
                                            <td>2018-09-17 01:22</td>
                                            <td>100297</td>
                                            <td>Yoga Permana</td>
                                            <td class="text-right">9</td>
                                            <td class="text-right">Rp 430.000</td>
                                        </tr>
                                        <tr>
                                            <td>2018-09-17 12:12</td>
                                            <td>100156</td>
                                            <td>Taqwa Lidia</td>
                                            <td class="text-right">23</td>
                                            <td class="text-right">Rp 955.000</td>
                                        </tr>
                                        <tr>
                                            <td>2018-09-17 10:06</td>
                                            <td>100235</td>
                                            <td>Suraj</td>
                                            <td class="text-right">12</td>
                                            <td class="text-right">Rp 123.500</td>
                                        </tr>
                                        <tr>
                                            <td>2018-09-17 9:03</td>
                                            <td>100224</td>
                                            <td>Mery Rosyandari</td>
                                            <td class="text-right">11</td>
                                            <td class="text-right">Rp 254.000</td>
                                        </tr>
                                        <tr>
                                            <td>2018-09-17 08:57</td>
                                            <td>100220</td>
                                            <td>Sari Anggraini</td>
                                            <td class="text-right">10</td>
                                            <td class="text-right">Rp 1.275.000</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div> -->
                    </div>