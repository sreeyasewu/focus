<?php 

    //hitung jumlah Customer
    $q_cs = "SELECT count(*) as totaltransaksi FROM aus_margin";
    $d_cs = mysqli_query($koneksi, $q_cs);
    $r_cs = mysqli_fetch_assoc($d_cs);

    //hitung jumlah pendapatan keseluruhan
    $q_margin = "SELECT sum(margin_stakeholders) as totalprofit FROM aus_margin";
    $d_margin = mysqli_query($koneksi, $q_margin);
    $r_margin = mysqli_fetch_assoc($d_margin);

    $total_profit = $r_margin['totalprofit'];
   

    $user = $_SESSION['nama'];

    //total stok
    $q_ss = "SELECT sum(stok) as stok FROM aus_stakeholders";
    $d_ss = mysqli_query($koneksi,$q_ss);
    $result_ss = mysqli_fetch_assoc($d_ss);

    $stok_keseluruhan = $result_ss['stok'];

    //cari stok yang dimiliki
    $q_st = "SELECT * FROM aus_stakeholders WHERE stakeholders_name = '$user'";
    $d_st = mysqli_query($koneksi,$q_st);

    $result_st = mysqli_fetch_assoc($d_st);

    //total pengeluaran 
    $q_with = "SELECT sum(jumlah) as jumlah FROM aus_withdraw";
    $d_With = mysqli_query($koneksi,$q_with);
    $r_with = mysqli_fetch_assoc($d_With);

    $pengeluaran = $r_with['jumlah']; 

    //pengurangan margin
    //diskon margin bank
    $q_diskon_margin_bank = "SELECT sum(pengurangan_margin) as diskon_margin_bank FROM aus_customer_order_adjustments WHERE metode= 'BANK'";
    $d_diskon_margin_bank = mysqli_query($koneksi,$q_diskon_margin_bank);
    $r_diskon_margin_bank = mysqli_fetch_assoc($d_diskon_margin_bank);

    //diskon margin kas
    $q_diskon_margin_kas = "SELECT sum(pengurangan_margin) as diskon_margin_kas FROM aus_customer_order_adjustments WHERE metode= 'KAS'";
    $d_diskon_margin_kas = mysqli_query($koneksi,$q_diskon_margin_kas);
    $r_diskon_margin_kas = mysqli_fetch_assoc($d_diskon_margin_kas);

    $diskon_margin_bank = $r_diskon_margin_bank['diskon_margin_bank'];
    $diskon_margin_kas = $r_diskon_margin_kas['diskon_margin_kas'];
    $diskon_margin = $diskon_margin_bank + $diskon_margin_kas;

    $diskon_margin_stakeholer = $diskon_margin/2;

    //kurangi profit dengan margin

    //$total_profit = $total_profit - $diskon_margin;

    $stok = $result_st['stok'];

    $pengurangan_pengeluaran = (($pengeluaran/2)/$stok_keseluruhan) * $stok;

    $pengurangan_margin = ($stok/$stok_keseluruhan) * $diskon_margin_stakeholer;


    $profit_final = ($stok/$stok_keseluruhan) * $total_profit;

    $profit_final = $profit_final - $pengurangan_pengeluaran - $pengurangan_margin;

    $profit_final = round($profit_final,0)

    
 ?>

<div class="row m-t-25">
                        <div class="col-sm-4 col-lg-4">
                            <div class="overview-item overview-item--c1">
                                <div class="overview__inner">
                                    <div class="overview-box clearfix">
                                        <div class="icon">
                                            <i class="zmdi zmdi-account-o"></i>
                                        </div>
                                        <div class="text">
                                            <h2><?php echo $r_cs['totaltransaksi'] ?></h2>
                                            <span>Transaction</span>
                                        </div>
                                    </div>
                                    <div class="overview-chart">
                                        <br>
                                        <br>
                                        <br>
                                        <!-- <canvas id="widgetChart1"></canvas> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-lg-4">
                            <div class="overview-item overview-item--c2">
                                <div class="overview__inner">
                                    <div class="overview-box clearfix">
                                        <div class="icon">
                                            <i class="zmdi zmdi-shopping-cart"></i>
                                        </div>
                                        <div class="text">
                                            <h2 class="harga_rp"><?php echo $profit_final ?></h2>
                                            <span>Profit</span>
                                        </div>
                                    </div>
                                    <div class="overview-chart">
                                        <br>
                                        <br>
                                        <br>
                                        <!-- <canvas id="widgetChart2"></canvas> -->
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4 col-lg-4">
                            <div class="overview-item overview-item--c3">
                                <div class="overview__inner">
                                    <div class="overview-box clearfix">
                                        <div class="icon">
                                            <i class="zmdi zmdi-calendar-note"></i>
                                        </div>
                                        <div class="text">
                                            <h2><?php echo $stok ?></h2>
                                            <span>Lot yang dimiliki</span>
                                        </div>
                                    </div>
                                    <div class="overview-chart">
                                        <br><br><br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                    
            

                    <div class="row">
                       <!--  <div class="col-lg-6">
                            <div class="au-card recent-report">
                                <div class="au-card-inner">
                                    <h3 class="title-2">cash flow</h3>
                                    <div class="chart-info">
                                        <div class="chart-info__left">
                                            <div class="chart-note">
                                                <span class="dot dot--blue"></span>
                                                <span>Pengeluaran</span>
                                            </div>
                                            <div class="chart-note mr-0">
                                                <span class="dot dot--green"></span>
                                                <span>Pendapatan</span>
                                            </div>
                                        </div>
                                        <div class="chart-info__right">
                                            <div class="chart-statis">
                                                <span class="index incre">
                                                    <i class="zmdi zmdi-long-arrow-up"></i>25%</span>
                                                <span class="label">Pengeluaran</span>
                                            </div>
                                            <div class="chart-statis mr-0">
                                                <span class="index decre">
                                                    <i class="zmdi zmdi-long-arrow-down"></i>10%</span>
                                                <span class="label">Pendapatan</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="recent-report__chart">
                                        <canvas id="recent-rep-chart"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        <!-- <div class="col-lg-6">
                            <div class="au-card chart-percent-card">
                                <div class="au-card-inner">
                                    <h3 class="title-2 tm-b-5">Capital Percentage %</h3>
                                    <div class="row no-gutters">
                                        <div class="col-xl-6">
                                            <div class="chart-note-wrap">
                                                <div class="chart-note mr-0 d-block">
                                                    <span class="dot dot--indigo"></span>
                                                    <span>stakeholders</span>
                                                </div>
                                                <div class="chart-note mr-0 d-block">
                                                    <span class="dot dot--pink"></span>
                                                    <span>Investors</span>
                                                </div>
                                                <div class="chart-note mr-0 d-block">
                                                    <span class="dot dot--purple"></span>
                                                    <span>marketing</span>
                                                </div>
                                                <div class="chart-note mr-0 d-block">
                                                    <span class="dot dot--yellow"></span>
                                                    <span>investors</span>
                                                </div>
                                                <div class="chart-note mr-0 d-block">
                                                    <span class="dot dot--green"></span>
                                                    <span>stakeholders</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-6">
                                            <div class="percent-chart">
                                                <canvas id="percent-chart"></canvas>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                    </div> 

                    

                    <div class="row">
                        <div class="col-lg-12">
                            <h2 class="title-1 m-b-25">Latest Akad</h2>
                            <div class="table-responsive table--no-card m-b-40">
                                <table class="table table-borderless table-striped table-earning">
                                    <thead>
                                        <tr>
                                            <th>Tanggal Akad</th>
                                            <th>ID Akad</th>
                                            <th>Nama Kustomer</th>
                                            <th class="text-right">product</th>
                                            <th class="text-right">Tenor</th>
                                            <th class="text-right">total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            
                                            $query = "SELECT * FROM aus_order WHERE status = 'approved' and approval_date != '0000-00-00' ORDER BY id_order DESC LIMIT 0,10";
                                            $data = mysqli_query($koneksi,$query);


                                            while ($result = mysqli_fetch_assoc($data)) {
                                         ?>
                                        <tr>
                                             <?php 
                                                $id_order = $result['id_order'];
                                                $q_akad = "SELECT * FROM aus_akad WHERE id_order = '$id_order'";
                                                $d_akad = mysqli_query($koneksi,$q_akad);
                                                $result_akad = mysqli_fetch_assoc($d_akad);

                                                
                                             ?>
                                            <td>
                                                <?php 
                                                    $tgl_akad = $result_akad['tgl_akad']; 
                                                    $tgl_akad = strtotime($tgl_akad);
                                                    $tgl_akad = date('d F Y',$tgl_akad);

                                                    echo $tgl_akad;
                                                ?>
                                            </td>
                                            <td>
                                                <?php  echo "AUS-AK00".$result_akad['id_akad']; ?>
                                            </td>
                                            <td>
                                                <?php
                                                    $id_customer = $result['id_customer'];
                                                    $q_cs = "SELECT * FROM aus_customer WHERE id_customer ='$id_customer'";
                                                    $d_cs = mysqli_query($koneksi,$q_cs);
                                                    $result_cs = mysqli_fetch_assoc($d_cs);

                                                    echo $result_cs['nama_lengkap'];
                                                 ?>

                                            </td>
                                            <td class="text-right">
                                                <?php 
                                                    $id_product = $result['id_product'];
                                                    $q_pr = "SELECT * FROM aus_products WHERE id_product ='$id_product'";
                                                    $d_pr = mysqli_query($koneksi,$q_pr);
                                                    $result_pr = mysqli_fetch_assoc($d_pr);

                                                    echo $result_pr['name'];
                                                 ?>
                                            </td>
                                            <td class="text-right">
                                                <?php 

                                                //cari tenor
                                                $q_tenor = "SELECT * FROM aus_customer_rincian_angsuran WHERE id_order = '$id_order'";
                                                $d_tenor = mysqli_query($koneksi, $q_tenor);
                                                $r_tenor = mysqli_fetch_assoc($d_tenor);

                                                echo $r_tenor['tenor']." Bulan";

                                                 ?>

                                            </td>
                                            <td class="text-right">
                                                <?php echo "<span class='harga_rp'>".$r_tenor['total_bayar']."</span>" ?>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div> 

                    <!-- <div class="row">
                        <div class="col-lg-3">
                            <h2 class="title-1 m-b-25">Top Products</h2>
                            <div class="au-card au-card--bg-blue au-card-top-countries m-b-40">
                                <div class="au-card-inner">
                                    <div class="table-responsive">
                                        <table class="table table-top-countries">
                                            <tbody>
                                                <tr>
                                                    <td>Name</td>
                                                    <td class="text-right">Total</td>
                                                </tr>
                                                <tr>
                                                    <td>Motor Yamaha</td>
                                                    <td class="text-right">32</td>
                                                </tr>
                                                <tr>
                                                    <td>TV Samsung LED 32 Inch</td>
                                                    <td class="text-right">26</td>
                                                </tr>
                                                <tr>
                                                    <td>iPhone X 64GB</td>
                                                    <td class="text-right">25</td>
                                                </tr>
                                                <tr>
                                                    <td>Camera C430W 4k</td>
                                                    <td class="text-right">23</td>
                                                </tr>
                                                <tr>
                                                    <td>Samsung S8 Black</td>
                                                    <td class="text-right">19</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-9">
                            <h2 class="title-1 m-b-25">installment payment</h2>
                            <div class="table-responsive table--no-card m-b-40">
                                <table class="table table-borderless table-striped table-earning">
                                    <thead>
                                        <tr>
                                            <th>date</th>
                                            <th>akad ID</th>
                                            <th>name</th>
                                            <th class="text-right">installment</th>
                                            <th class="text-right">total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>2018-09-17 05:57</td>
                                            <td>100298</td>
                                            <td>Ibnu Firdaus</td>
                                            <td class="text-right">4</td>
                                            <td class="text-right">Rp 675.000</td>
                                        </tr>
                                        <tr>
                                            <td>2018-09-17 01:22</td>
                                            <td>100297</td>
                                            <td>Yoga Permana</td>
                                            <td class="text-right">9</td>
                                            <td class="text-right">Rp 430.000</td>
                                        </tr>
                                        <tr>
                                            <td>2018-09-17 12:12</td>
                                            <td>100156</td>
                                            <td>Taqwa Lidia</td>
                                            <td class="text-right">23</td>
                                            <td class="text-right">Rp 955.000</td>
                                        </tr>
                                        <tr>
                                            <td>2018-09-17 10:06</td>
                                            <td>100235</td>
                                            <td>Suraj</td>
                                            <td class="text-right">12</td>
                                            <td class="text-right">Rp 123.500</td>
                                        </tr>
                                        <tr>
                                            <td>2018-09-17 9:03</td>
                                            <td>100224</td>
                                            <td>Mery Rosyandari</td>
                                            <td class="text-right">11</td>
                                            <td class="text-right">Rp 254.000</td>
                                        </tr>
                                        <tr>
                                            <td>2018-09-17 08:57</td>
                                            <td>100220</td>
                                            <td>Sari Anggraini</td>
                                            <td class="text-right">10</td>
                                            <td class="text-right">Rp 1.275.000</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div> -->
                    </div>