<?php
## Database configuration
include '../config/config.php';

        /*
        $row = 1;
        $rowperpage = 10; // Rows display per page
        $columnSortOrder = 'desc'; // asc or desc
        $searchValue = mysqli_real_escape_string($koneksi,''); // Search value
        */
        
        $draw = $_POST['draw'];
        $row = $_POST['start'];
        $rowperpage = $_POST['length']; // Rows display per page
        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
        $searchValue = mysqli_real_escape_string($koneksi,$_POST['search']['value']); // Search value
        

        $tanggal_mulai = $_POST['tanggal_mulai'];
        $tanggal_akhir = $_POST['tanggal_akhir'];

        ## Search 
        $searchQuery = " ";
       
        if($tanggal_mulai != ''){
           $searchQuery .= " and inputdatetime >= ".$tanggal_mulai." ";
        }
        if($tanggal_akhir != ''){
           $searchQuery .= " and inputdatetime <= ".$tanggal_akhir." ";
        }

        if($searchValue != ''){
           $searchQuery = " and (custname like '%".$searchValue."%' or 
                custid like '%".$searchValue."%' or 
                distname like'%".$searchValue."%' or 
                coveragearea like'%".$searchValue."%' or 
                inputdatetime like'%".$searchValue."%' or 
                sales like'%".$searchValue."%' or 
                status like'%".$searchValue."%' or 
                channel like'%".$searchValue."%' or 
                subchannel like'%".$searchValue."%' or 
                segment like'%".$searchValue."%' or 
                nameowner like'%".$searchValue."%' or 
                noktp like'%".$searchValue."%' or 
                address like'%".$searchValue."%' or 
                province like'%".$searchValue."%' or 
                city like'%".$searchValue."%' or 
                kecamatan like'%".$searchValue."%' or 
                kelurahan like'%".$searchValue."%' or 
                kodepos like'%".$searchValue."%') ";
        }

        ## Total number of records without filtering
        $sel = mysqli_query($koneksi,"select count(*) as allcount from `distributor`, `focus_freezer_placement` WHERE `focus_freezer_placement`.`id_distributor` = `distributor`.`iddistributor` AND `focus_freezer_placement`.`approved_status` = 'CLOSED'");
        $records = mysqli_fetch_assoc($sel);
        $totalRecords = $records['allcount'];

        ## Total number of record with filtering
        $sel = mysqli_query($koneksi,"select count(*) as allcount from `distributor`, `focus_freezer_placement` WHERE `focus_freezer_placement`.`id_distributor` = `distributor`.`iddistributor` AND `focus_freezer_placement`.`approved_status` = 'CLOSED' ".$searchQuery."");
        $records = mysqli_fetch_assoc($sel);
        $totalRecordwithFilter = $records['allcount'];

        ## Fetch records
        $empQuery = "SELECT * FROM `distributor`, `focus_freezer_placement` WHERE `focus_freezer_placement`.`id_distributor` = `distributor`.`iddistributor` AND `focus_freezer_placement`.`approved_status` = 'CLOSED' ".$searchQuery." ORDER by iddistributor ".$columnSortOrder." LIMIT ".$row.",".$rowperpage;
        $empRecords = mysqli_query($koneksi, $empQuery);
        $data = array();
     

        while ($row = mysqli_fetch_assoc($empRecords)) {


           $action = '<a href="index.php?mod=datareview&class=edit&id='.$row['iddistributor'].'" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Edit data">
                                <i class="fas fa fa-edit"></i>
                            </a>

                             
                           

                            <a href="javascript:void();" id="data-action-'.$row['iddistributor'].'" class="btn btn-sm btn-primary data-action" data-toggle="tooltip" data-placement="top" title="Ubah Status" acuan="'.$row['iddistributor'].'">
                                <i class="fas fa fa-hand-point-down"></i>
                            </a>';
           $checkbox = '<input type="checkbox" name="select_noo[]" id="select_noo" class="select_noo" value="'.$row['iddistributor'].'" onClick="selectNOO();">';

           if($row['status'] == 1)
           {
              $status = 'Revisit';
           }
           else
           {
              $status = 'New Outlet';
           }

           $paddr = $row['photo'];
              if (substr($paddr, 0, 4) == "http") {
                $photo = $paddr;
              } else {
                $photo = 'https://appict.appsbelfoods.com/'.$paddr;
              }

           $paddr2 = $row['photo2'];
              if (substr($paddr2, 0, 4) == "http") {
                $photo2 = $paddr2;
              } else {
                $photo2 = 'https://appict.appsbelfoods.com/'.$paddr;
              }

           $paddr3 = $row['photo3'];
              if (substr($paddr3, 0, 4) == "http") {
                $photo3 = $paddr3;
              } else {
                $photo3 = 'https://appict.appsbelfoods.com/'.$paddr;
              }
                            

           $link_photo = "<a href=".$photo.">View</a>";
           $link_photo2 = "<a href=".$photo2.">View</a>";
           $link_photo3 = "<a href=".$photo3.">View</a>";

           $surveydate = $row['inputdatetime'];
           $today = date('Y-m-d');
           $diff = abs(strtotime($today) - strtotime($surveydate));
           $years = floor($diff / (365*60*60*24));
           $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
           $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
           $jaraknya = $days;

                if ($jaraknya > 7) {
                    $sla = 'OVERDUE';
                }else{
                    $sla = 'PENDING';
                }
                $action = '<a href="model/datareview/resubmitnoo-freezer-close.php?id='.$row['id_placement'].'" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Register as NOO">
                <i class="fas fa fa-arrow-alt-circle-up"></i>
                </a>
                <a href="index.php?mod=datareview&class=edit-freezer&id='.$row['iddistributor'].'" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Edit data">
                <i class="fas fa fa-edit"></i>
                </a>';

           $data[] = array( 
              "action"=>$action,
              "iddistributor"=>$row['iddistributor'],
              "custname"=>$row['custname'],
              "custid"=>$row['custid'],
              "distname"=>$row['distname'],
              "coveragearea"=>$row['coveragearea'],
              "inputdatetime"=>$row['inputdatetime'],
              "sales"=>$row['sales'],
              "status"=>$status,
              "channel"=>$row['channel'],
              "subchannel"=>$row['subchannel'],
              "segment"=>$row['segment'],
              "nameowner"=>$row['nameowner'],
              "noktp"=>$row['noktp'],
              "telephone"=>$row['telephone'],
              "longitude"=>$row['longitude'],
              "latitude"=>$row['latitude'],
              "address"=>$row['address'],
              "province"=>$row['province'],
              "city"=>$row['city'],
              "kecamatan"=>$row['kecamatan'],
              "kelurahan"=>$row['kelurahan'],
              "kodepos"=>$row['kodepos'],
              "timeopen"=>$row['timeopen'],
              "timeclosed"=>$row['timeclosed'],
              "luasbangunan"=>$row['luasbangunan'],
              "paketfreezer"=>$row['paketfreezer'],
              "link_photo"=>$link_photo,
              "link_photo2"=>$link_photo2,
              "link_photo3"=>$link_photo3,
              "sebutkanlingkungan"=>$row['sebutkanlingkungan'],
              "patokan"=>$row['patokan'],
              "lokasi1"=>$row['lokasi1'],
              "lokasi2"=>$row['lokasi2'],
              "freezerdalamtoko"=>$row['freezerdalamtoko'],
              "merekfreezer"=>$row['merekfreezer'],
              "frozenFood"=>$row['frozenFood'],
              "merekfrozenfood"=>$row['merekfrozenfood'],
              "productutama"=>$row['productutama'],
              "estimasipengunjung"=>$row['estimasipengunjung'],
              "pendapatan"=>$row['pendapatan'],
              "mayoritas"=>$row['mayoritas'],
              "elektricity"=>$row['elektricity'],
              "kondisifisik"=>$row['kondisifisik'],
              "resikofreezerhilang"=>$row['resikofreezerhilang'],
              "tersediatempat"=>$row['tersediatempat'],
              "pilihanfreezeroutlet"=>$row['pilihanfreezeroutlet'],
              "tempatfreezer"=>$row['tempatfreezer'],
              "penilaianlokasi"=>$row['penilaianlokasi'],
              "sebutkanlingkungan"=>$row['sebutkanlingkungan'],
              "targetbulan"=>$row['targetbulanan'],
              "followupaction"=>$row['followupaction'],
              "approved_status"=>$row['approved_status'],
              "keterangan"=>$row['keterangan']

           );
        }

        ## Response
        $response = array(
          "draw" => intval($draw),
          "iTotalRecords" => $totalRecords,
          "iTotalDisplayRecords" => $totalRecordwithFilter,
          "aaData" => $data
        );

        //var_dump(count((array)$data[0]));
        echo json_encode($response);
?>
