<?php 
ob_start();
error_reporting(E_ALL);
ini_set('display_errors', 1);
include('../../config/config.php');

$id_placement = $_POST['id_placement'];
$id = $_POST['id_distributor'];
$idnumber = $_POST['idnumber'];
$batchpo = $_POST['batchpo'];
$freezer = $_POST['assetId'];
$nomormesin = $_POST['nomormesin'];
// $placement_notes = $_POST['placement_notes'];
$namasupir = $_POST['namasupir'];
$tanggal_pengiriman = $_POST['tanggal_pengiriman'];
$realisasi = $_POST['realisasi_tipe_freezer'];
$v_longitude = $_POST['v_longitude'];
$v_latitude = $_POST['v_latitude'];

$owner = $_POST['owner'];

$placementdate = date('Y-m-d');
$placedby = $_POST['placedby'];
$ket = "Freezer telah berhasil diletakkan oleh vendor";

$brand = $_POST['brand'];

$time = date('h:i:s');

$vendor = $_POST['vendor'];


// find leadtime
$emailtovendor = $_POST['emailtovendor'];
$diff = abs(strtotime($placementdate) - strtotime($emailtovendor));

$years = floor($diff / (365*60*60*24));
$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

$leadtime = $days;


// if ($owner == 'RENT' or $owner == 'Rent') {
if ($nomormesin <> '') {
	$lastdigit = substr($nomormesin, -4);
	$year = date('y');
	$freezercode = 'FZ.'.$year.'.'.$lastdigit.'.'.$idnumber;


	$statement = "UPDATE focus_freezer_placement SET brand='$brand', freezercode= '$freezercode', batch_po = '$batchpo', placement_notes = '$nomormesin', nama_supir = '$namasupir', tanggal_pengiriman = '$tanggal_pengiriman', placement_date = '$placementdate', lead_time = '$leadtime', realisasi_tipe_freezer = '$realisasi', vendor_longitude = '$v_longitude', vendor_latitude= '$v_latitude', placed_by = '$placedby', keterangan = '$ket' WHERE id_placement = '$id_placement'";
} else {
	$statement = "UPDATE focus_freezer_placement SET brand='$brand', batch_po = '$batchpo', nama_supir = '$namasupir', tanggal_pengiriman = '$tanggal_pengiriman', placement_date = '$placementdate', lead_time = '$leadtime', realisasi_tipe_freezer = '$realisasi', vendor_longitude = '$v_longitude', vendor_latitude= '$v_latitude', placed_by = '$placedby', keterangan = '$ket' WHERE id_placement = '$id_placement'";
}



if ($update = mysqli_query($koneksi,$statement)) {


	$sql = "INSERT INTO notif_placement VALUES ('','$idnumber','$placedby','RTM','$time','$placementdate','0')";

	if ($notif = mysqli_query($koneksi,$sql)) {

		//cari data customernya dulu
		$cs = "SELECT * FROM distributor WHERE iddistributor = '$id'";
		$dcs = mysqli_query($koneksi,$cs);
		$rcs = mysqli_fetch_assoc($dcs);

		//cari data placement
		$qp = "SELECT * FROM focus_freezer_placement WHERE id_placement = '$id_placement'";
		$dqp = mysqli_query($koneksi,$qp);
		$rqp = mysqli_fetch_assoc($dqp);

		$outletname = $rcs['custname'];

		// echo $outletname;
		// echo $id;

		$idbelfood = $rcs['custid'];
		$sales = $rcs['sales'];
		$idpjp = '';
		$idsup = '';
		$outletstatus = "Active";
		$dayvisit = '';
		// $sales = '';
		$call = '';
		$m1 = '';
		$m2 = '';
		$m3 = '';
		$m4 = '';
		$distid = $rcs['distnumber'];
		$distname = $rcs['distname'];
		$flagdu = 'Distributor Urban';
		$area = $rcs['coveragearea'];
		$lat = $rcs['latitude'];
		$long = $rcs['longitude'];
		$segment = $rcs['segmentnumber'];
		$pic = $rcs['nameowner'];
		$phone = $rcs['telephone'];
		$alamat = $rcs['address'];
		$kel = $rcs['kelurahan'];
		$kec = $rcs['kecamatan'];
		$kota = $rcs['city'];
		$prov = $rcs['province'];
		$kodepos = $rcs ['kodepos'];
		$fowner = $rqp['freezer_owner'];
		//$supp = '1 {PT. BIG}';
		// $brand = '';
		$batchpo = $batchpo;
		$type = $realisasi;
		$idnumber = $idnumber;
		$freezercode = $rqp['freezercode'];
		$nomormesin = $rqp['placement_notes'];
		$principal = $rqp['principal'];


		//insert into freezer database 
		//idnumber disini adalah nomor mesin
		$q_freezer = "REPLACE INTO focus_freezer_master VALUES ('','".mysqli_real_escape_string($koneksi,$outletname)."','$idbelfood','$idpjp','$idsup','$outletstatus','".mysqli_real_escape_string($koneksi,$dayvisit)."','$sales','$call','$m1','$m2','$m3','$m4','$distid','$distname','$flagdu','$area','$lat','$long','$segment','".mysqli_real_escape_string($koneksi,$pic)."','".mysqli_real_escape_string($koneksi,$phone)."','".mysqli_real_escape_string($koneksi,$alamat)."','$kel','$kec','$kota','$prov','$kodepos','$fowner','$vendor','$brand','$batchpo','$type','$nomormesin','','','$freezercode','$freezer','IN FIELD','$tanggal_pengiriman','','','Indirect','$principal')";
		$d_freezer = mysqli_query($koneksi,$q_freezer) or die(mysqli_error($koneksi));

		
		$updateAsset = "UPDATE focus_asset_freezer SET assetStatusId='2', IDBelfood = '$idbelfood', OutletName = '$outletname' WHERE assetId = '$freezer'";


		$updateAssetQuery = mysqli_query($koneksi,$updateAsset) or die(mysqli_error($koneksi));
		

		if ($d_freezer) {
			header('location:../../index.php?mod=freezerplacement&class=view&status=1');
		}else{
			$statement = "INSERT INTO focus_error_log VALUES (NULL, CURRENT_TIMESTAMP, '".mysqli_real_escape_string($koneksi,"".$_SERVER['PHP_SELF'])."', '".mysqli_real_escape_string($koneksi,"".mysqli_error($koneksi))."', '".mysqli_real_escape_string($koneksi,$q_freezer)."', '".mysqli_real_escape_string($koneksi,"".$_SESSION['email'])."')";
			$update = mysqli_query($koneksi,$statement);
                        $statement = "UPDATE focus_freezer_placement SET placement_date = '0000-00-00'  WHERE id_placement = '$id_placement'";
			$update = mysqli_query($koneksi,$statement);
			echo 'ERROR 500. Contact Admin';
		}
		
	}
    
}else{
    header('location:../../index.php?mod=freezerplacement&class=view&status=0');
}
 ?>
