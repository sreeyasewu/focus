<?php 
include ('config/config.php');
require_once "library/vendor/autoload.php";

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$spreadsheet = new Spreadsheet();
$Excel_writer = new Xlsx($spreadsheet);

$spreadsheet->setActiveSheetIndex(0);
$activeSheet = $spreadsheet->getActiveSheet();

$activeSheet->setCellValue('A1', 'Request');
$activeSheet->setCellValue('B1', 'Date Assign');
$activeSheet->setCellValue('C1', 'Date Plan by Vendor');
$activeSheet->setCellValue('D1', 'Date Process');
$activeSheet->setCellValue('E1', 'BFI Code');
$activeSheet->setCellValue('F1', 'Outlet Name');
$activeSheet->setCellValue('G1', 'Distributor Name');
$activeSheet->setCellValue('H1', 'Alamat');
$activeSheet->setCellValue('I1', 'Kelurahan');
$activeSheet->setCellValue('J1', 'Kecamatan');
$activeSheet->setCellValue('K1', 'Kabupaten/Kota');
$activeSheet->setCellValue('L1', 'Provinsi');
$activeSheet->setCellValue('M1', 'Placement Date');
$activeSheet->setCellValue('N1', 'Removal/Switch Date');
$activeSheet->setCellValue('O1', 'Removal/Switch Plan Date');
$activeSheet->setCellValue('P1', 'Removal/Switch Status');
$activeSheet->setCellValue('Q1', 'Type (Old)');
$activeSheet->setCellValue('R1', 'Barcode (Old)');
$activeSheet->setCellValue('S1', 'Type (New)');
$activeSheet->setCellValue('T1', 'Barcode (New)');
$activeSheet->setCellValue('U1', 'Supplier Name');
$activeSheet->setCellValue('V1', 'BarcodeFA');
$activeSheet->setCellValue('W1', 'Removal/Switch Reason');

$query = "SELECT * FROM freezer_request as r INNER JOIN focus_freezer_master as m ON r.id_freezer=m.id_freezer  ORDER BY id_request DESC";
$data = mysqli_query($koneksi,$query);
$i = 2;
$customer_type = "";
$date_assign = "";
$switch_date = "";

while ($result = mysqli_fetch_assoc($data)) {

    $dp = $result['tgl_plan'];
    $ss = $result['status'];
    if ($dp == '0000-00-00' and $ss == 'success') {
        $date_assign = "Vendor tidak memasukkan tanggal plan";
    }elseif ($dp == '0000-00-00') {
        $date_assign = "Vendor belum memasukkan tanggal plan";
    }else{
        $date_assign = $dp; 
    }

    if ($result['status'] == 'success') {
                   $switch_date = $result['tgl_action'];
    }
    else {
                    $switch_date = "";
    }

        $activeSheet->setCellValue('A'.$i , strtoupper($result['action']));
        $activeSheet->setCellValue('B'.$i , $result['tgl_request']);
        $activeSheet->setCellValue('C'.$i , $date_assign );
        $activeSheet->setCellValue('D'.$i , $result['tgl_action']);
        $activeSheet->setCellValue('E'.$i , $result['IDBelfoodd']);
        $activeSheet->setCellValue('F'.$i , strtoupper($result['Outletname']));
        $activeSheet->setCellValue('G'.$i , $result['DistributorName']);
        $activeSheet->setCellValue('H'.$i , $result['Alamat']);
        $activeSheet->setCellValue('I'.$i , $result['Kelurahan']);
        $activeSheet->setCellValue('J'.$i , $result['Kecamatan']);
        $activeSheet->setCellValue('K'.$i , $result['KabKota']);
        $activeSheet->setCellValue('L'.$i , $result['Propinsi']);
        $activeSheet->setCellValue('M'.$i , $result['TanggalPasang']);
        $activeSheet->setCellValue('N'.$i , $switch_date);
        $activeSheet->setCellValue('O'.$i , $result['tgl_plan']);
        $activeSheet->setCellValue('P'.$i , $result['status']);
        $activeSheet->setCellValue('Q'.$i , $result['Type_Switching']);
        $activeSheet->setCellValue('R'.$i , $result['ID_Switching']);
        $activeSheet->setCellValue('S'.$i , $result['Type']);
        $activeSheet->setCellValue('T'.$i , $result['ID_Number']);
        $activeSheet->setCellValue('U'.$i , $result['supplier']);
        $activeSheet->setCellValue('V'.$i , $result['BarcodeFANumber']);
        $activeSheet->setCellValue('W'.$i , $result['keterangan']);

        $i++;

}

$activeSheet->getColumnDimension('A')->setAutoSize(true);
$activeSheet->getColumnDimension('B')->setAutoSize(true);
$activeSheet->getColumnDimension('C')->setAutoSize(true);
$activeSheet->getColumnDimension('D')->setAutoSize(true);
$activeSheet->getColumnDimension('E')->setAutoSize(true);
$activeSheet->getColumnDimension('F')->setAutoSize(true);
$activeSheet->getColumnDimension('G')->setAutoSize(true);
$activeSheet->getColumnDimension('H')->setAutoSize(true);
$activeSheet->getColumnDimension('I')->setAutoSize(true);
$activeSheet->getColumnDimension('J')->setAutoSize(true);
$activeSheet->getColumnDimension('K')->setAutoSize(true);
$activeSheet->getColumnDimension('L')->setAutoSize(true);
$activeSheet->getColumnDimension('M')->setAutoSize(true);
$activeSheet->getColumnDimension('N')->setAutoSize(true);
$activeSheet->getColumnDimension('O')->setAutoSize(true);
$activeSheet->getColumnDimension('P')->setAutoSize(true);
$activeSheet->getColumnDimension('Q')->setAutoSize(true);
$activeSheet->getColumnDimension('R')->setAutoSize(true);
$activeSheet->getColumnDimension('S')->setAutoSize(true);
$activeSheet->getColumnDimension('T')->setAutoSize(true);
$activeSheet->getColumnDimension('U')->setAutoSize(true);
$activeSheet->getColumnDimension('V')->setAutoSize(true);
$activeSheet->getColumnDimension('W')->setAutoSize(true);

$activeSheet->getStyle("A1:W1")->getFont()->setBold(true);

$date = date("d-m-Y");
$filename = 'exportAllSwitchRemoval'.$date.'.xlsx';


header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='. $filename);
header('Cache-Control: max-age=0');
$Excel_writer->save('php://output');
exit();
?>


