<?php 
include ('config/config.php');

    if (isset($_GET['status'])) {
        $status = $_GET['status'];

        if ($status == 'success') {
            echo '<div class="alert alert-success" role="alert">
                Investor baru berhasil di tambahkan
                <a href="index.php?mod=investors&class=view" class="alert-link">view investor</a>.
            </div>';
        }elseif ($status == 'failed') {
            echo '<div class="alert alert-danger" role="alert">
                Investor sudah terdaftar
            </div>';
        }else{
            echo '<div class="alert alert-warning" role="alert">
                Terjadi kesalahan koneksi internet, silkahkan ulangi kembali
            </div>';
        }
        
    }


    if (isset($_GET['id'])) {
        $id = $_GET['id'];
        $query = "SELECT * FROM distributor";
        $data = mysqli_query($koneksi,$query);
    }
 ?>
<div class="row">
    <div class="col-md-12">
        <h3 class="title-5 m-b-35">new placement</h3>

        <div class="card">
        <!-- <div class="card-header">
            <strong>Create new vendor request</strong>
        </div> -->
        <div class="card-body card-block">

            <form action="model/freezerplacement/approve_placement.php" method="post" enctype="multipart/form-data" class="form-horizontal">

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Nama Outlet</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="text" id="text-input" name="outlet" class="form-control" required>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Freezer Owner</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="text" id="text-input" name="owner_freezer" placeholder="Freezer Owner Name" class="form-control" required>
                    </div>
                </div>
                
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Freezer Owner</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="text" id="text-input" name="owner_freezer" placeholder="Freezer Owner Name" class="form-control" required>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="email-input" class=" form-control-label">Nama Vendor</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="text" id="email-input" name="nama_vendor" placeholder="Nama Vendor" class="form-control" required>
                    </div>
                </div>
                
                </div>
                <div class="card-footer" style="text-align: right">
                    <button type="submit" class="au-btn au-btn--green">
                        <i class="fa fa-floppy-o"></i> Process
                    </button>
                    <button type="reset" class="au-btn au-btn--red">
                        <i class="fa fa-ban"></i> Reset
                    </button>
                </div>

            </form>
        </div>
    </div>
    </div>
</div>