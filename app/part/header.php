
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Bellfoods">
    <meta name="author" content="sieradproduce">
    <meta name="keywords" content="Bellfood, Nugget, Chicken, Food">

    <!-- Title Page-->
    <title>Bellfoods Focus System</title>

    <!-- Fontfaces CSS-->

    <!-- color CSS -->
    <!-- <link rel=stylesheet href="https://s3-us-west-2.amazonaws.com/colors-css/2.2.0/colors.min.css"> -->
    <!-- color CSS -->



        
        <link href="assets/css/font-face.css" rel="stylesheet" media="all">
        <link href="assets/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
        <link href="assets/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
        <link href="assets/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

        <!-- Bootstrap CSS-->
        <link href="assets/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

        <!-- Vendor CSS-->
        <!--<link href="assets/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">-->
        <link href="assets/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
        <link href="assets/vendor/wow/animate.css" rel="stylesheet" media="all">
        <link href="assets/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
        <link href="assets/vendor/slick/slick.css" rel="stylesheet" media="all">
        <link href="assets/vendor/select2/select2.min.css" rel="stylesheet" media="all">
        <link href="assets/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">


        <link rel="stylesheet" href="assets/css/data_tables/data_tables.css">
        <link rel="stylesheet" href="assets/css/data_tables/material/datatable_material.css">
        <link rel="stylesheet" href="assets/css/data_tables/material/material.css">
        <link rel="stylesheet" href="assets/css/data_tables/buttons.dataTables.min.css">
        <!-- Main CSS-->
        <link href="assets/css/theme.css" rel="stylesheet" media="all">
        <link rel="stylesheet" href="assets/css/lightpick.css">



    <style>
        
        div.dataTables_wrapper {
            width: 100%;
        }

        table.dataTable tbody td {
            word-break: break-word;
            vertical-align: top;
        }

        .form-tindaklanjut{
           display:none;
        }

        .form-action{
           display:none;
        }

        .loader-wrapper {
          width: 100%;
          height: 100%;
          position: absolute;
          top: 0;
          left: 0;
          background-color: #242f3f;
          display:flex;
          justify-content: center;
          align-items: center;
          z-index: 999999;
        }
        .loader {
          display: inline-block;
          width: 30px;
          height: 30px;
          position: relative;
          border: 4px solid #Fff;
          animation: loader 2s infinite ease;
        }
        .loader-inner {
          vertical-align: top;
          display: inline-block;
          width: 100%;
          background-color: #fff;
          animation: loader-inner 2s infinite ease-in;
        }
        @keyframes loader {
          0% { transform: rotate(0deg);}
          25% { transform: rotate(180deg);}
          50% { transform: rotate(180deg);}
          75% { transform: rotate(360deg);}
          100% { transform: rotate(360deg);}
        }
        @keyframes loader-inner {
          0% { height: 0%;}
          25% { height: 0%;}
          50% { height: 100%;}
          75% { height: 100%;}
          100% { height: 0%;}
        }

        /*.tindaklanjut{
           display:visible;
        }*/
    </style>

    <!-- Data Tables -->
    
    <!-- <link rel="stylesheet" href="assets/css/data_tables/bootstrap.data_tables.css"> -->

    <!-- End Data Tables -->

    

</head>

<body class="animsition">

    <div class="page-wrapper">

    <div class="loader-wrapper">
      <span class="loader"><span class="loader-inner"></span></span>
    </div>