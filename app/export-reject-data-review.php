<?php 
include ('config/config.php');
require_once "library/vendor/autoload.php";

function clean($string) {
    $string = str_replace(' ', ' ', $string);
    $string = preg_replace('/[^A-Za-z0-9\-ığşçöüÖÇŞİıĞ]/', ' ', $string);

    return preg_replace('/-+/', '-', $string);
}

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$spreadsheet = new Spreadsheet();
$Excel_writer = new Xlsx($spreadsheet);

$spreadsheet->setActiveSheetIndex(0);
$activeSheet = $spreadsheet->getActiveSheet();

$activeSheet->setCellValue('A1', 'Nama Outlet');
$activeSheet->setCellValue('B1', 'BFI Code');
$activeSheet->setCellValue('C1', 'Nama Distributor');
$activeSheet->setCellValue('D1', 'Area Distributor');
$activeSheet->setCellValue('E1', 'Tgl Survey');
$activeSheet->setCellValue('F1', 'Nama Surveyor');
$activeSheet->setCellValue('G1', 'Status Kunjungan');
$activeSheet->setCellValue('H1', 'Channel');
$activeSheet->setCellValue('I1', 'SubChannel');
$activeSheet->setCellValue('J1', 'Segment');
$activeSheet->setCellValue('K1', 'Nama PIC');
$activeSheet->setCellValue('L1', 'No KTP');
$activeSheet->setCellValue('M1', 'No Telp PIC');
$activeSheet->setCellValue('N1', 'Alamat');
$activeSheet->setCellValue('O1', 'Provinsi');
$activeSheet->setCellValue('P1', 'Kecamatan');
$activeSheet->setCellValue('Q1', 'Kelurahan');
$activeSheet->setCellValue('R1', 'Kode Pos');
$activeSheet->setCellValue('S1', 'Jam Buka');
$activeSheet->setCellValue('T1', 'Jam Tutup');
$activeSheet->setCellValue('U1', 'Luas Bangunan');
$activeSheet->setCellValue('V1', 'Paket Awal');
$activeSheet->setCellValue('W1', 'Lokasi Sekitar');
$activeSheet->setCellValue('X1', 'Patokan');
$activeSheet->setCellValue('Y1', 'Freezer Es Krim');
$activeSheet->setCellValue('Z1', 'Merk Freezer Es Krim');
$activeSheet->setCellValue('AA1', 'Freezer Makanan Beku');
$activeSheet->setCellValue('AB1', 'Merk Freezer Makanan Beku');
$activeSheet->setCellValue('AC1', 'Produk Utama');
$activeSheet->setCellValue('AD1', 'Jumlah Pengunjung');
$activeSheet->setCellValue('AE1', 'Pendapatan/Hari (IDR)');
$activeSheet->setCellValue('AF1', 'Tipe Pengunjung');
$activeSheet->setCellValue('AG1', 'Kapasitas Listrik');
$activeSheet->setCellValue('AH1', 'Kondisi Fisik Outlet');
$activeSheet->setCellValue('AI1', 'Resiko Freezer');
$activeSheet->setCellValue('AJ1', 'Space 1x1.5 m');
$activeSheet->setCellValue('AK1', 'Tipe Freezer');
$activeSheet->setCellValue('AL1', 'Area Penempatan Freezer');
$activeSheet->setCellValue('AM1', 'Visibilitas Freezer di Jalur Utama');
$activeSheet->setCellValue('AN1', 'Layanan Tambahan');
$activeSheet->setCellValue('AO1', 'Target/Bulan (KG)');
$activeSheet->setCellValue('AP1', 'Issue/Followup Action');
$activeSheet->setCellValue('AQ1', 'Status');
$activeSheet->setCellValue('AR1', 'Keterangan');



$query = "SELECT * FROM `distributor`, `focus_freezer_placement` WHERE `focus_freezer_placement`.`id_distributor` = `distributor`.`iddistributor` AND `focus_freezer_placement`.`approved_status` = 'REJECT BY FREEZER TEAM'";
$data = mysqli_query($koneksi,$query);
$i = 2;
$customer_type = "";

while ($result = mysqli_fetch_assoc($data)) {

        if($result['status'] == 1)
           {
              $status = 'Revisit';
           }
           else
           {
              $status = 'New Outlet';
           }

            $surveydate = $result['inputdatetime'];
           $today = date('Y-m-d');
           $diff = abs(strtotime($today) - strtotime($surveydate));
           $years = floor($diff / (365*60*60*24));
           $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
           $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
           $jaraknya = $days;

                if ($jaraknya > 7) {
                    $sla = 'OVERDUE';
                }else{
                    $sla = 'PENDING';
                }

        $activeSheet->setCellValue('A'.$i , ''.$result['custname'].'');
        $activeSheet->setCellValue('B'.$i , ''.$result['custid'].'');
        $activeSheet->setCellValue('C'.$i , ''.$result['distname'].'');
        $activeSheet->setCellValue('D'.$i , ''.$result['coveragearea'].'');
        $activeSheet->setCellValue('E'.$i , ''.$result['inputdatetime'].'');
        $activeSheet->setCellValue('F'.$i , ''.$result['sales'].'');
        $activeSheet->setCellValue('G'.$i , ''.$status.'');
        $activeSheet->setCellValue('H'.$i , ''.$result['channel'].'');
        $activeSheet->setCellValue('I'.$i , "".clean($result['subchannel'])."");
        $activeSheet->setCellValue('J'.$i , ''.clean($result['segment']).'');
        $activeSheet->setCellValue('K'.$i , ''.$result['nameowner'].'');
        $activeSheet->setCellValue('L'.$i , ''.$result['noktp'].'');
        $activeSheet->setCellValue('M'.$i , ''.$result['telephone'].'');
        $activeSheet->setCellValue('N'.$i , ''.$result['address'].'');
        $activeSheet->setCellValue('O'.$i , ''.$result['province'].'');
        $activeSheet->setCellValue('P'.$i , ''.$result['kecamatan'].'');
        $activeSheet->setCellValue('Q'.$i , ''.$result['kelurahan'].'');
        $activeSheet->setCellValue('R'.$i , ''.$result['kodepos'].'');
        $activeSheet->setCellValue('S'.$i , ''.$result['timeopen'].'');
        $activeSheet->setCellValue('T'.$i , ''.$result['timeclosed'].'');
        $activeSheet->setCellValue('U'.$i , ''.$result['luasbangunan'].'');
        $activeSheet->setCellValue('V'.$i , ''.$result['paketfreezer'].'');
        $activeSheet->setCellValue('W'.$i , ''.$result['sebutkanlingkungan'].'');
        $activeSheet->setCellValue('X'.$i , ''.$result['patokan'].'');
        $activeSheet->setCellValue('Y'.$i , ''.$result['freezerdalamtoko'].'');
        $activeSheet->setCellValue('Z'.$i , ''.$result['merekfreezer'].'');
        $activeSheet->setCellValue('AA'.$i , ''.$result['frozenFood'].'');
        $activeSheet->setCellValue('AB'.$i , ''.$result['merekfrozenfood'].'');
        $activeSheet->setCellValue('AC'.$i , ''.$result['productutama'].'');
        $activeSheet->setCellValue('AD'.$i , ''.$result['estimasipengunjung'].'');

        $activeSheet->setCellValue('AE'.$i , ''.$result['pendapatan'].'');
        $activeSheet->setCellValue('AF'.$i , ''.$result['mayoritas'].'');
        $activeSheet->setCellValue('AG'.$i , ''.$result['elektricity'].'');
        $activeSheet->setCellValue('AH'.$i , ''.$result['kondisifisik'].'');
        $activeSheet->setCellValue('AI'.$i , ''.$result['resikofreezerhilang'].'');
        $activeSheet->setCellValue('AJ'.$i , ''.$result['tersediatempat'].'');
        $activeSheet->setCellValue('AK'.$i , ''.$result['pilihanfreezeroutlet'].'');
        $activeSheet->setCellValue('AL'.$i , ''.$result['tempatfreezer'].'');
        $activeSheet->setCellValue('AM'.$i , ''.$result['penilaianlokasi'].'');
        $activeSheet->setCellValue('AN'.$i , ''.$result['sebutkanlingkungan'].'');
        $activeSheet->setCellValue('AO'.$i , ''.$result['targetbulanan'].'');
        $activeSheet->setCellValue('AP'.$i , ''.$result['followupaction'].'');
        $activeSheet->setCellValue('AQ'.$i , ''.$result['approved_status'].'');
        $activeSheet->setCellValue('AR'.$i , ''.$result['keterangan'].'');
        $i++;

}

$activeSheet->getColumnDimension('A')->setAutoSize(true);
$activeSheet->getColumnDimension('B')->setAutoSize(true);
$activeSheet->getColumnDimension('C')->setAutoSize(true);
$activeSheet->getColumnDimension('D')->setAutoSize(true);
$activeSheet->getColumnDimension('E')->setAutoSize(true);
$activeSheet->getColumnDimension('F')->setAutoSize(true);
$activeSheet->getColumnDimension('G')->setAutoSize(true);
$activeSheet->getColumnDimension('H')->setAutoSize(true);
$activeSheet->getColumnDimension('I')->setAutoSize(true);
$activeSheet->getColumnDimension('J')->setAutoSize(true);
$activeSheet->getColumnDimension('K')->setAutoSize(true);
$activeSheet->getColumnDimension('L')->setAutoSize(true);
$activeSheet->getColumnDimension('M')->setAutoSize(true);
$activeSheet->getColumnDimension('N')->setAutoSize(true);
$activeSheet->getColumnDimension('O')->setAutoSize(true);
$activeSheet->getColumnDimension('P')->setAutoSize(true);
$activeSheet->getColumnDimension('Q')->setAutoSize(true);
$activeSheet->getColumnDimension('R')->setAutoSize(true);
$activeSheet->getColumnDimension('S')->setAutoSize(true);
$activeSheet->getColumnDimension('T')->setAutoSize(true);
$activeSheet->getColumnDimension('U')->setAutoSize(true);
$activeSheet->getColumnDimension('V')->setAutoSize(true);
$activeSheet->getColumnDimension('W')->setAutoSize(true);
$activeSheet->getColumnDimension('X')->setAutoSize(true);
$activeSheet->getColumnDimension('Y')->setAutoSize(true);
$activeSheet->getColumnDimension('Z')->setAutoSize(true);
$activeSheet->getColumnDimension('AA')->setAutoSize(true);
$activeSheet->getColumnDimension('AB')->setAutoSize(true);
$activeSheet->getColumnDimension('AC')->setAutoSize(true);
$activeSheet->getColumnDimension('AD')->setAutoSize(true);
$activeSheet->getColumnDimension('AE')->setAutoSize(true);
$activeSheet->getColumnDimension('AF')->setAutoSize(true);
$activeSheet->getColumnDimension('AG')->setAutoSize(true);
$activeSheet->getColumnDimension('AH')->setAutoSize(true);
$activeSheet->getColumnDimension('AI')->setAutoSize(true);
$activeSheet->getColumnDimension('AJ')->setAutoSize(true);
$activeSheet->getColumnDimension('AK')->setAutoSize(true);
$activeSheet->getColumnDimension('AL')->setAutoSize(true);
$activeSheet->getColumnDimension('AM')->setAutoSize(true);
$activeSheet->getColumnDimension('AN')->setAutoSize(true);
$activeSheet->getColumnDimension('AO')->setAutoSize(true);
$activeSheet->getColumnDimension('AP')->setAutoSize(true);
$activeSheet->getColumnDimension('AQ')->setAutoSize(true);
$activeSheet->getColumnDimension('AR')->setAutoSize(true);
$activeSheet->getStyle("A1:AR1")->getFont()->setBold(true);

$date = date("d-m-Y");
$filename = 'exportAllRejectDataReview'.$date.'.xlsx';

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='. $filename);
header('Cache-Control: max-age=0');
$Excel_writer->save('php://output');
exit();
?>
