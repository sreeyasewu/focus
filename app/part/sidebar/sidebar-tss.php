<aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href=<?php echo BASE_URL  ?>>
                    <img src="assets/images/icon/logo-aus.png" alt="Cool Admin" />
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">

                        <li
                         <?php 
                         if (isset($_GET['mod'])) {
                               //do nothing
                            }else{
                                echo "class='active'";
                            }
                        ?>
                        >
                            <a href="index.php">
                            <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                        </li>

                        

                    

                        <li
                        <?php if (isset($_GET['mod'])) {
                            $mod = $_GET['mod'];
                            if ($mod == 'products') {
                                echo "class='active'";
                            }
                        } ?>
                        >
                            <a class="js-arrow" href="#">
                                <i class="fas fa-server"></i>Freezer Management</a>
                                <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="index.php?mod=freezer&class=add">Add Freezer</a>
                                </li>
                                <li>
                                    <a href="index.php?mod=freezer&class=view">View Freezer</a>
                                </li>
                            </ul>
                        </li>

                        <li
                         <?php if (isset($_GET['mod'])) {
                            $mod = $_GET['mod'];
                            if ($mod == 'investors') {
                                echo "class='active'";
                            }
                        } ?>
                        >
                            <a class="js-arrow" href="#">
                                <i class="fas fa-server"></i>Freezer Database</a>
                                <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <!-- <li>
                                    <a href="index.php?mod=investors&class=add">Add Investor</a>
                                </li> -->
                                <li>
                                    <a href="index.php?mod=freezerdatabase&class=view">View Freezer</a>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a class="js-arrow" href="#">
                                <i class="fas fa-building"></i>Placement & Removal</a>
                                <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="index.php?mod=freezer&class=req_tarik">Request Tarik</a>
                                </li>
                                <li>
                                    <a href="index.php?mod=manageremoval">Penambahan/penempatan Freezer</a>
                                </li>
                            </ul>
                            <!-- <a href="index.php?mod=manageplacement"> -->
                            <!-- <i class="fas fa-building"></i>Cancel Placement & Removal</a> -->
                        </li>

                    </ul>
                </nav>
            </div>
        </aside>

