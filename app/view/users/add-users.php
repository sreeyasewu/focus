<?php 
    if (isset($_GET['status'])) {
        $status = $_GET['status'];

        if ($status == 'success') {
            echo '<div class="alert alert-success" role="alert">
                Pengguna baru berhasil di tambahkan
                <a href="index.php?mod=users&class=view" class="alert-link">view user</a>.
            </div>';
        }elseif ($status == 'failed') {
            echo '<div class="alert alert-danger" role="alert">
                Pengguna sudah terdaftar
            </div>';
        }else{
            echo '<div class="alert alert-warning" role="alert">
                Terjadi kesalahan koneksi internet, silkahkan ulangi kembali
            </div>';
        }
        
    }
 ?>
<div class="row">
    <div class="col-md-12">
        <h3 class="title-5 m-b-35">new users</h3>

        <div class="card">
        <div class="card-header">
            <strong>Add New User</strong>
        </div>
        <div class="card-body card-block">

            <form action="model/users/add.php" method="post" enctype="multipart/form-data" class="form-horizontal">
                
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">NIK</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="text" id="text-input" name="nik" placeholder="NIK" class="form-control" required>
                        <small class="form-text text-muted">NIK Sesuai dengan KTP</small>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Nama</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="text" id="text-input" name="nama" placeholder="Nama Lengkap" class="form-control" required>
                        <small class="form-text text-muted">Nama Sesuai dengan KTP</small>
                    </div>
                </div>
                
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="email-input" class=" form-control-label">Email</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="email" id="email-input" name="email" placeholder="email@email.com" class="form-control" required>
                        <small class="help-block form-text">Email yang aktif dan masih di gunakan</small>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="email-input" class=" form-control-label">Area</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="text" id="email-input" name="area" placeholder="Jakarta Pusat" class="form-control" required>
                        <small class="help-block form-text">Masukkan informasi area user</small>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Nomor HP</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="text" id="text-input" name="no_hp" placeholder="089780012931" class="form-control" required>
                        <small class="form-text text-muted">Masukkan Nomor HP yang dapat dihubungi</small>
                    </div>
                </div>
                
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="password-input" class=" form-control-label">Password</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="password" id="password-input" name="password" placeholder="Password" class="form-control" required="">
                        <small class="help-block form-text">Masukkan password untuk mengakses akun</small>
                    </div>
                </div>
                
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="select" class=" form-control-label">Roles</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <select name="role" id="select" class="form-control" required>
                            <option disabled selected>Pilih role pengguna</option>
                            <option value="ADMIN">Admin</option>
                            <option value="FREEZER MANAGEMENT">Freezer Management</option>
                            <option value="ROUTE TO MARKET">Route to Market</option>
                            <option value="VENDOR">Vendor</option>
                            <option value="COMMAND CENTER">Command Center</option>
                            <!-- <option value="SURVEYOR">Surveyor</option>
                            <option value="SUPERVISOR">Supervisor</option>
                            <option value="FINANCE">Finance</option>
                            <option value="OWNER">Owner</option>
                            <option value="COLLECTION">Collection</option> -->
                        </select>
                    </div>
                </div>
                
                </div>
                <div class="card-footer">
                    <button type="submit" class="au-btn au-btn--green">
                        <i class="fa fa-floppy-o"></i> Save
                    </button>
                    <button type="reset" class="au-btn au-btn--red">
                        <i class="fa fa-ban"></i> Reset
                    </button>
                </div>

            </form>
        </div>
    </div>
    </div>
</div>