<?php 
    if (isset($_GET['status'])) {
        $status = $_GET['status'];

        if ($status == 1) {
            echo '<div class="alert alert-success" role="alert">
                Berhasil memperbaruhi data
            </div>';
        }elseif ($status == 0) {
            echo '<div class="alert alert-danger" role="alert">
                Gagal memperbaruhi data
            </div>';
        }else{
            echo '<div class="alert alert-warning" role="alert">
                Terjadi kesalahan koneksi internet, silkahkan ulangi kembali
            </div>';
        }
        
    }

    include ('config/config.php');
    $id = $_GET['id'];

    /* Get data survey */
    $q = "SELECT * FROM focus_freezer_master WHERE id_freezer = '$id'";
    $d = mysqli_query($koneksi,$q);
    $r = mysqli_fetch_assoc($d);

 ?>




<div class="row">
    <div class="col-md-12">
        <h3 class="title-5 m-b-35">Edit data</h3>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
        <div class="card-header">
            <strong>Edit</strong>
        </div>
        <div class="card-body card-block">
    <form id="form1" name="form1" method="post" action="model/freezerdatabase/edit.php" enctype="multipart/form-data" class="form-horizontal">
        
        <div class="row form-group">
            <div class="col col-md-3">
                <label for="iddistributor" class=" form-control-label">Nama Outlet</label>
            </div>
            <div class="col-12 col-md-9">
                <input type="text" name="nama_outlet" id="OutletName" value="<?php echo $r['Outletname'] ?>" class="form-control"/>

                <input type="hidden" name="id" value="<?php echo $r['id_freezer'] ?>">
                
            </div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3">
                <label for="distnumber">BFI Code</label>
            </div>
            <div class="col-12 col-md-9">
                <input type="text" name="bfi_code" id="IDBelfood" value="<?php echo $r['IDBelfoodd'] ?>" class="form-control"/>
                <input type="hidden" name="bficode" value="<?php echo $r['IDBelfoodd']; ?>">
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="distname">FreezerCode</label>
            </div>
            <div class="col-12 col-md-9">
                <input type="text" name="freezercodedisabled" id="distname" value="<?php echo $r['FreezerCode'] ?>" class="form-control" disabled/>
                <input type="hidden" name="freezercode" value="<?php echo $r['FreezerCode']; ?>">
            </div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3">
                <label for="custid">Supplier</label>
            </div>
            <div class="col-12 col-md-9">
                <input type="text" name="supplier" id="custid" value="<?php echo $r['Supplier'] ?>" class="form-control"/>
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="custname">Brand</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="brand" id="custname" value="<?php echo $r['Brand'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="sales">Type</label>
            </div>
            <div class="col-md-9">
                <select name="tipe" class="select_bfi form-control" required>
                <option value="<?php echo $r['Type'] ?>" selected><?php echo $r['Type'] ?></option>
                <option value="">--------------</option>
                <?php 
                    include ('config/config.php');
                    //cari data semua BFI
                    $sql = "SELECT * FROM focus_tipe_freezer ORDER BY id_tipe";
                    $dql = mysqli_query($koneksi,$sql);
                    

                    while ($rql = mysqli_fetch_assoc($dql)) {
                ?>
                    <option value="<?php echo $rql['tipe'] ?>"><?php echo strtoupper($rql['tipe']." - ".$rql['keterangan'])?></option>

                <?php 
                    }


                 ?>
                

            </select>
                <!-- <input type="text" name="type" id="sales" value="<?php echo $r['Type'] ?>" class="form-control" /> -->
            </div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3">
                <label for="status">BarcodeFANumber</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="barcode" id="status" value="<?php echo $r['BarcodeFANumber'] ?>" class="form-control" />
            </div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3">
                <label for="status">Alamat</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="alamat" id="alamat" value="<?php echo $r['Alamat'] ?>" class="form-control" />
            </div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3">
                <label for="segment">Nomor Mesin</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="nomormesin" id="segment" value="<?php echo $r['ID_Number'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="channel">Batch PO</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="batchpo" id="channel" value="<?php echo $r['BatchPO'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="subchannel">Status</label>
            </div>
            <div class="col-md-9">
                <!--
                <input type="text" name="statusdisabled" id="subchannel" value="<?php echo $r['Keterangan'] ?>" class="form-control" disabled/>
                -->
                <!--<input type="text" name="status" class="form-control" value="<?php echo $r['Keterangan']; ?>">-->
                <select name="StatusFreezer" class="form-control" required>
                <option selected value="<?php echo $r['StatusFreezer'] ?>"><?php echo $r['StatusFreezer'] ?></option>
                <option value="">-----------------</option>
                <option value="IN FIELD">IN FIELD</option>
                <option value="WAREHOUSE">WAREHOUSE</option>
                <option value="DISPOSED">DISPOSED</option>
            </select>
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="segmentnumber">Freezer Owner</label>
            </div>
            <div class="col-md-9">
                
            <select name="owner" class="form-control" required>
                <option selected value="<?php echo $r['Freezer_Owner'] ?>"><?php echo $r['Freezer_Owner'] ?></option>
                <option value="">-----------------</option>
                <option value="Rent">Rent</option>
                <option value="Own">Own</option>
            </select>
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="subchannelnumber">Latitude</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="latitude" value="<?php echo $r['Latitude'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="account">Longitude</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="longitude" value="<?php echo $r['Longitude'] ?>" class="form-control" />
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="address">Tanggal Pasang</label>
            </div>
            <div class="col-md-9">
                <input type="date" name="tgl_pasang" id="address" class="form-control" value="<?php echo $r['TanggalPasang'] ?>"/>
            </div>
        </div>

        <div class="row form-group">
            <div class="col col-md-3">
                <label for="address">Tanggal Penarikan</label>
            </div>
            <div class="col-md-9">
                <input type="date" name="tgl_penarikan" class="form-control" value="<?php echo $r['TanggalPenarikan'] ?>"/>
            </div>
        </div>

        

        <div class="card-footer" style="text-align: right">
            <button type="submit" class="au-btn au-btn--green">
                <i class="fa fa-floppy-o"></i> Update
            </button>
            <button type="reset" class="au-btn au-btn--red">
                <i class="fa fa-ban"></i> Reset
            </button>
        </div>
    </form>
</div>
</div>
</div>


<!-- end row -->
