<?php 

    //hitung jumlah Customer
    $area = $_SESSION['area'];
    $vendor = $_SESSION['nama'];
    $acc = $vendor.'-'.$area;
    include ('config/config.php');
    
 ?>

<div class="row m-t-25">
    <div class="col-md-12">
        <h3 class="title-5 m-b-35">Switch/Removal Process</h3>
        <br> <a href="export-switch-removal.php" target="_blank" class="btn btn-dark btn-sm">Download All Switch/Removal Data</a>
        <hr>
        <!-- <div class="table-data__tool">
        
        </div> -->
        <div class="table-responsive table-data" style="height: 100%">
            <table id="tableok" class="mdl-data-table" style="width:100%">
                <thead>
                    <tr>
                        
                        <td>No</td>
                        <td>Request</td>
                        <td>Date Assign</td>
                        <td>Date Plan by Vendor</td>
                        <td>Date Process</td>
                        <td>BFI Code</td>
                        <td>Outlet Name</td>
                        <td>Distributor Name</td>
                        <td>Alamat</td>
                        <td>Kelurahan</td>
                        <td>Kecamatan</td>
                        <td>Kab/Kota</td>
                        <td>Provinsi</td>
                        <td>Placement Date</td>
                        <td>Removal/Switch Date</td>
                        <td>Removal/Switch Plan Date</td>
                        <td>Removal/Switch Status</td>
                        <td>Type (New)</td>
                        <td>Barcode (New)</td>
                        <td>Type (Old)</td>
                        <td>Barcode (Old)</td>
                        <td>Supplier Name</td>
                        <td>BarcodeFA</td>
                        <td>Removal/Switch Reason</td>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $no = 1;
                        
                        //ambil data requestnya dulu
                        // $vendor = $_SESSION['nama'];
                        $query = "SELECT * FROM freezer_request ORDER BY id_request DESC LIMIT 0,100";
                        $data = mysqli_query($koneksi,$query);

                        while ($r = mysqli_fetch_assoc($data)) {



                     ?>
                    <tr>
                        <?php 
                            $id_freezer = $r['id_freezer'];
                            $q = "SELECT * FROM focus_freezer_master WHERE id_freezer = '$id_freezer'";
                            $d = mysqli_query($koneksi,$q);
                            $result = mysqli_fetch_assoc($d);
                         ?>    
                        
                        <td><?php echo $no ?></td>
                        <td>
                            <?php echo strtoupper($r['action']) ?>
                        </td>
                        <td>
                            <?php echo $r['tgl_request'] ?>
                        </td>
                        <td>
                            <?php 
                                $dp = $r['tgl_plan'];
                                $ss = $r['status'];
                                if ($dp == '0000-00-00' and $ss == 'success') {
                                    echo "Vendor tidak memasukkan tanggal plan";
                                }elseif ($dp == '0000-00-00') {
                                    echo "Vendor belum memasukkan tanggal plan";
                                }else{
                                    echo $dp; 
                                }
                             ?>
                        </td>
                        <td>
                            <?php echo $r['tgl_action'] ?>
                        </td>
                        <td>
                            <?php echo $result['IDBelfoodd'] ?>
                        </td>
                        <td>
                            <?php echo strtoupper($result['Outletname']) ?>
                        </td>
                        <td>
                            <?php echo $result['DistributorName'] ?>
                        </td>
                        <td>
                            <?php echo $result['Alamat'] ?>
                        </td>
                        <td>
                            <?php echo $result['Kelurahan'] ?>
                        </td>
                        <td>
                            <?php echo $result['Kecamatan'] ?>
                        </td>

                        <td>
                            <?php echo $result['KabKota'] ?>
                        </td>
                        <td>
                            <?php echo $result['Propinsi'] ?>
                        </td>
                        
                        <td>
                            <?php echo $result['TanggalPasang'] ?>
                        </td>
                        <td>
                            <?php
				if ($r['status'] == 'success') {
					echo $r['tgl_action'];
				}
				else {
					echo "";
				}
			    ?>
                        </td>
                        <td>
                            <?php echo $r['tgl_plan'] ?>
                        </td>
                        <td>
                            <?php 
                                // $id_freezer = $result['id_freezer'];
                                // $cari = "SELECT action, status from freezer_request WHERE id_freezer = '$id_freezer'";
                                // $qc = mysqli_query($koneksi,$cari);
                                // $rc = mysqli_fetch_assoc($qc);

                                echo $status = $r['status'];
                                // $action = $r['action'];

                                // if ($status == 'success') {
                                //     if ($action == 'remove') {
                                //         echo "Removal Successful";
                                //     }elseif($action == 'switch'){
                                //         echo "Switch Successful";
                                //     }else{
                                //         //show nothing
                                //     }
                                // }elseif ($status == 'FAILED') {
                                //     if ($action == 'remove') {
                                //         echo "Removal Failed";
                                //     }elseif($action == 'switch'){
                                //         echo "Switch Failed";
                                //     }else{
                                //         //show nothing
                                //     }
                                // }else{
                                //     echo 'Waiting for status update';
                                    
                                // }
                             ?>
                        </td>
                        <td>
                            <?php echo $result['Type'] ?>
                        </td>
                        <td>
                            <?php echo $result['ID_Number'] ?>
                        </td>
                        <td>
                            <?php echo $result['Type_Switching'] ?>
                        </td>
                        <td>
                            <?php echo $result['ID_Switching'] ?>
                        </td>
                        <td>
                            <?php echo $r['supplier'] ?>
                        </td>
                        <td>
                            <?php echo $result['BarcodeFANumber'] ?>
                        </td>
                        <td>
                            <?php echo $r['keterangan'] ?>
                        </td>
                        <!-- <td>
                            <div class="table-data-feature">
                                
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <a href="index.php?mod=users&class=edit&id=<?php echo $result['id_user']; ?>" ><i class="zmdi zmdi-edit"></i></a>
                                </button>
                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <a href="index.php?mod=users&class=delete&id=<?php echo $result['id_user'] ?>"><i class="zmdi zmdi-delete"></i></a>
                                </button>
                            </div>
                        </td> -->
                    </tr>

                     <?php $no++; } ?>
                </tbody>
            </table>
        </div>

    </div>
                    
</div>


<br>
<br>
<h3 class="title-5 m-b-35">Additional Freezer Placement Reject</h3>
<br> <a href="export-freezer-reject.php" target="_blank" class="btn btn-dark btn-sm">Download All Freezer Placement Reject Data</a>
<hr>

<div class="row">
    <div class="col-md-12">
        <div class="table-responsive table-data" style="height: auto">
            <table id="tableok2" class="mdl-data-table" style="width:100%">
                <thead>
                    <tr>
                        <td>No</td>
                        <td>Reject Date</td>
                        <td>Keterangan</td>
                        <td>BFI Code</td>
                        <td>Nama Outlet</td>
                        <td>Nama Distributor</td>
                        <td>Tanggal Survey</td>
                        <td>Freezer Owner</td>
                        <td>Nama Vendor</td>
                        <td>IDNumber</td>
                        <td>Tipe Freezer</td>
                        <td>Nama PIC</td>
                        <td>No Telp PIC</td>
                        <td>Alamat</td>
                        <td>Provinsi</td>
                        <td>Kabupaten / Kota</td>
                        <td>Kecamatan</td>
                        <td>Kelurahan</td>
                        <td>Kode Pos</td>
                        <td>Status</td>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $no = 1;
                        
                        $query33 = "SELECT * FROM focus_additional_reject ORDER BY id_reject DESC LIMIT 0,100";
                        $data33 = mysqli_query($koneksi,$query33);

                        while ($result33 = mysqli_fetch_assoc($data33)) {
                     ?>
                    <tr>

                        <!-- cari datangnya dari mana -->
                        <?php 
                                if ($result['id_distributor'] == 0) {
                                    //maka itu artinya primary
                                    $id_freezer = $result33['id_freezer'];

                                    $cp = "SELECT * FROM MDS_CustomerPrimary WHERE Code = '$id_freezer'";

                                    $stmt = mysqli_query( $koneksi, $cp );
                                    $result2 = mysqli_fetch_assoc($stmt);
                                    
                                    $tipe2 = $result2['MarketId_Name'];
                                    $photo2 = '';
                                    $photo32 = '';
                                    if ($tipe2 == '' or $tipe2 == NULL) {

                                        //karna dia secondary, cari lagi datanya dari tabel secondary mds
                                        $cp2 = "SELECT * FROM MDS_CustomerSecondary WHERE Code = '$id_freezer'";
                                        $dcp2 = mysqli_query( $koneksi, $cp2 );
                                        $results2 = mysqli_fetch_assoc($dcp2);

                                        $custid2 = $results2['Code'];
                                        $custname2 = $results2['Name'];
                                        $distname2 = $results2['DistName'];
                                        $tgl_survey2 = "Tidak tersedia";
                                        $tipe2 = "02 {IN DIRECT}";
                                        $pic2 = $results2['PIC'];
                                        $phone2 = $results2['Phone'];
                                        $latitude2 = $results2['Latitude'];
                                        $longitude2 = $results2['Longitude'];
                                        $address2 = $results2['Alamat'];
                                        $city2 = $results2['KabKota'];
                                        $province2 = $results2['Propinsi'];
                                        $kecamatan2 = $results2['Kecamatan'];
                                        $kelurahan2 = $results2['Kelurahan'];
                                        $kodepos2 = $results2['KodePos'];
                                    }else{
                                        $custid2 = $result2['Code'];
                                        $custname2 = $result2['Name'];
                                        $distname2 = $result2['System_Distributor_Name'];
                                        $tipe2 = "01 {DIRECT}";
                                        $pic2 = $result2['PIC'];
                                        $phone2 = $result2['Phone'];
                                        $latitude2 = $result2['Latitude'];
                                        $longitude2 = $result2['Longitude'];
                                        $address2 = $result2['Street'];
                                        $city2 = $result2['City'];
                                        $province2 = $result2['SalesDistrictName'];
                                        $kecamatan2 = $result2['SPVillageName'];
                                        $kelurahan2 = '';
                                        $kodepos2 = $result2['ZipCode'];
                                    }
                                }else{

                                    $id_distributor = $result33['id_distributor'];
                                    $cs = "SELECT * FROM distributor WHERE iddistributor = '$id_distributor'";
                                    $dcs = mysqli_query($koneksi,$cs);
                                    $result34 = mysqli_fetch_assoc($dcs); 

                                    $custid2 = $result34['custid'];
                                    $custname2 = $result34['custname'];
                                    $distname2 = $result34['distname'];
                                    $tgl_survey2 = $result34['inputdatetime'];
                                    $tipe2 = 'SECONDARY';
                                    $address2 = $result34['address'];
                                    $province2 = $result34['province'];
                                    $city2 =$result34['city'];
                                    $kecamatan2 =$result34['kecamatan'];
                                    $kelurahan2 =$result34['kelurahan'];
                                    $kodepos2 = $result34['kodepos'];
                                    $photo32 = "http://appict.appsbelfoods.com/".$result34['photo3']."";
                                    $photo2 = "http://appict.appsbelfoods.com/".$result34['photo'].""; 
                                    $pic2 = $result34['contactname'];
                                    $phone2 = $result34['telephone'];
                                    $latitude2 = $result34['latitude'];
                                    $longitude2 = $result34['longitude'];

                                }
                             ?>

                        <td><?php echo $no ?></td>
                        <td>
                            <?php echo $result33['date']; ?>
                        </td>
                        <td>
                            <?php echo $result33['ket']; ?>
                        </td>
                        <td>
                            <?php echo $custid2 ?>
                        </td>
                        <td><?php echo $custname2 ?></td>
                        <td>
                            <?php echo $distname2 ?>
                        </td>
                        <td>
                            <?php echo $tgl_survey2 ?>
                        </td>
                        <td>
                            <?php echo $result33['freezer_owner'] ?>
                        </td>
                        <td>
                            <?php echo $result33['nama_vendor'] ?>
                        </td>
                        <td>
                            <?php echo $result33['IDNumber'] ?>
                        </td>
                        
                        <td>
                            <?php echo $result33['tipe']; ?>
                        </td>
                        <td>
                            <?php echo $pic2 ?>
                        </td>
                        <td>
                            <?php echo $phone2 ?>
                        </td>
                       
                        <td>
                            <?php echo $address2 ?>
                        </td>
                        <td>
                            <?php echo $province2 ?>
                        </td>
                        <td>
                            <?php echo $city2 ?>
                        </td>
                        <td>
                            <?php echo $kecamatan2 ?>
                        </td>
                        <td>
                            <?php echo $kelurahan2 ?>
                        </td>
                        <td>
                            <?php echo $kodepos2 ?>
                        </td>                    
                        <td>
                            <?php echo $result33['status'] ?>
                        </td>
                    </tr>

                     <?php $no++; } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
