<?php 
    if ($_SESSION['role'] == 'ADMIN') {
        include ('mobile/mobile-admin.php');
        //tampilkan keseluruhan
    }elseif ($_SESSION['role'] == 'FREEZER MANAGEMENT') {
        include ('mobile/mobile-freezerplacement.php');
        //hanya menampilkan data event saja dan proses approval
        // dashboard, view bod event, view corporate event
    }elseif ($_SESSION['role'] == 'ROUTE TO MARKET') {
        include ('mobile/mobile-rtm.php');
        //hanya menampilkan penginputan saja
        // dashboard, add bod event, add corporate event
    }elseif ($_SESSION['role'] == 'VENDOR') {
        include ('mobile/mobile-vendor.php');
        //hanya menampilkan penginputan saja
        // dashboard, add bod event, add corporate event
    }elseif ($_SESSION['role'] == 'COMMAND CENTER') {
        include ('mobile/mobile-commandcenter.php');
        //hanya menampilkan penginputan saja
        // dashboard, add bod event, add corporate event
    }else{
        include ('mobile/mobile-investor.php');
        //karyawan hanya dapat melihat agenda yang termasuk dirinya saja
    }
 ?>


