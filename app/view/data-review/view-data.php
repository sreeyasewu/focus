<!-- jQuery Library -->
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<style type="text/css">
    /* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content/Box */
.modal-content {
  background-color: #fefefe;
  margin: 15% auto; /* 15% from the top and centered */
  padding: 20px;
  border: 1px solid #888;
  width: 19%; /* Could be more or less, depending on screen size */
}

/* The Close Button */
.close {
  color: #aaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: black;
  text-decoration: none;
  cursor: pointer;
}
</style>


<?php 
    if (isset($_GET['status'])) {
        $status = $_GET['status'];

        if ($status == 1) {
            echo '<div class="alert alert-success" role="alert">
                Register NOO Succees
            </div>';
        }elseif ($status == 0) {
            echo '<div class="alert alert-danger" role="alert">
                Terjadi Kesalahan! silahkan ulangi
            </div>';
        }elseif ($status == 11) {
            echo '<div class="alert alert-success" role="alert">
                Data berhasil di perbaruhi
            </div>';
        }elseif ($status == 50) {
            echo '<div class="alert alert-success" role="alert">
                Status berhasil di ubah
            </div>';
        }else{
            echo '<div class="alert alert-warning" role="alert">
                Terjadi kesalahan koneksi internet, silkahkan ulangi kembali
            </div>';
        }
    }
 ?>
<div class="row">
<div class="col-md-12">
        <!-- DATA TABLE -->
        <h3 class="title-5 m-b-35">
            <br>
            <?php 
            $q_n = "SELECT count(*) as notif FROM focus_freezer_placement WHERE approved_status = 'REJECT BY FREEZER TEAM'";
            $d_n = mysqli_query($koneksi,$q_n);
            $r = mysqli_fetch_assoc($d_n);
            $notif = $r['notif'];

            $q_nClose = "SELECT count(*) as notif FROM focus_freezer_placement WHERE approved_status = 'CLOSED'";
            $d_nClose = mysqli_query($koneksi,$q_nClose);
            $rClose = mysqli_fetch_assoc($d_nClose);
            $notif_close = $rClose['notif'];

            //data review
            $q_n2 = "SELECT count(*) as notif FROM distributor WHERE statustoko = 'DEAL'";
            $d_n2 = mysqli_query($koneksi,$q_n2);
            $r2 = mysqli_fetch_assoc($d_n2);
            $ttl = $r2['notif'];

            //data placement
            $q_n3 = "SELECT count(*) as notif FROM focus_freezer_placement";
            $d_n3 = mysqli_query($koneksi,$q_n3);
            $r3 = mysqli_fetch_assoc($d_n3);
            $ttl2 = $r3['notif'];

            $notif2 = $ttl - $ttl2;

            $q_n4 = "select count(*) as notif from distributor d WHERE statustoko = 'DEAL' AND d.iddistributor NOT IN (SELECT p.`id_distributor` FROM focus_freezer_placement p)";
            $d_n4 = mysqli_query($koneksi,$q_n4);
            $r4 = mysqli_fetch_assoc($d_n4);
            $notif_new = $r4['notif'];

               
            echo '<a href="index.php?mod=datareview&class=view" class="btn btn-primary">NEW <span class="badge badge-danger">'.$notif_new.'</span></a>
                <a href="index.php?mod=datareview&class=reject_freezer" class="btn btn-dark">REJECT (FREEZER PLACEMENT) <span class="badge badge-danger">'.$notif.'</span></a>
                <a href="index.php?mod=datareview&class=close_freezer" class="btn btn-dark">CLOSED (FREEZER PLACEMENT) <span class="badge badge-danger">'.$notif_close.'</span></a>';
                       

               
             ?>

           
        </h3>
        <div class="table-data__tool">
           <a href="export-new-data-review.php" class="btn btn-dark btn-sm">Download All New Data Review</a>
        </div>
        <div>
            
            <div class="row form-group">
                <div class="col-sm-3 col-md-2">
                    <label for="text-input" class=" form-control-label"><b>Filter by date</b></label>
                </div>
                <div class="col-sm-3 col-md-2">
                    <input type="text" id="tanggal_mulai" name="tanggal_mulai" class="form-control" placeholder="Mulai" required>
                </div>
                <div class="col-sm-3 col-md-2">
                    <input type="text" id="tanggal_akhir" name="tanggal_akhir" class="form-control" placeholder="Akhir" required>
                    
                </div>
                <button name="custom" id="custom" class="btn btn-sm btn-primary">
                    Filter
                </button>
                
            </div>


           
            <hr>

            <!--
            <form action="" method="POST">
            <div class="row form-group">
                <div class="col-sm-3 col-md-2">
                    <label for="text-input" class=" form-control-label"><b>Full Search</b></label>
                </div>
                <div class="col-sm-3 col-md-2">
                    <input type="text" name="search" class="form-control" placeholder="By nama outlet atau kode bfi" required>
                </div>
                <div class="col-sm-3 col-md-2">
                <button type="submit" name="fullsearch" value="custom" class="btn btn-sm btn-primary form-control">
                    Filter
                </button>
                </div>
            </div>
            <hr>
            <br>
            </form>
            -->
        </div>


        <div class="table-responsive table-data" style="height: auto">

            <!-- Table -->
            <table id="surveyTable" class="mdl-data-table table table-borderless" style="width:100%">

              <thead>
                <tr>
                        <td><input type="checkbox" name="select_all" id="select_all" onClick="selectAll();"> Select All</td>
                        <td>Action</td>
                        <td>Nama Outlet</td>
                        <td>BFI Code</td>
                        <td>Nama Distributor</td>
                        <td>Area Distributor</td>
                        <td>Tgl Survey</td>
                        <td>Nama Surveyor</td>
                        <td>Status Kunjungan</td>
                        <td>Channel</td>
                        <td>Sub-Channel</td>
                        <td>Segment</td>
                        <td>Nama PIC</td>
                        <td>No KTP</td>
                        <td>No Telp PIC</td>
                        <td>Longitude</td>
                        <td>Latitude</td>
                        <td>Alamat</td>
                        <td>Provinsi</td>
                        <td>Kabupaten/Kota</td>
                        <td>Kecamatan</td>
                        <td>Keluarahan</td>
                        <td>Kode Pos</td>
                        <td>Jam Buka (WIB)</td>
                        <td>Jam Tutup (WIB)</td>
                        <td>Luas Bangunan</td>
                        <td>Paket Awal</td>
                        <td>Foto - Outlet Tampak Depan</td>
                        <td>Foto - Daerah Depan Sekitar Toko</td>
                        <td>Foto - Lokasi Freezer yang disepakati</td>
                        <td>Foto - Deal Form + KTP</td>
                        <td>Lokasi Sekitar</td>
                        <td>Patokan</td>
                        <td>Lokasi (1)</td>
                        <td>Lokasi (2)</td>
                        <td>Freezer Es Krim</td>
                        <td>Merk Freezer Es Krim</td>
                        <td>Freezer Makanan Beku</td>
                        <td>Merk Freezer Makanan Beku</td>
                        <td>Produk Utama</td>
                        <td>Jumlah Pengunjung</td>
                        <td>Pendapatan/Hari (IDR)</td>
                        <td>Tipe Pengunjung</td>
                        <td>Kapasitas Listrik</td>
                        <td>Kondisi Fisik Outlet</td>
                        <td>Resiko Freezer</td>
                        <td>Spacae 1 x 1.5 m</td>
                        <td>Tipe Freezer</td>
                        <td>Area Penempatan Freezer</td>
                        <td>Visibilitas Freezer di Jalur Utama</td>
                        <td>Layanan Tambahan</td>
                        <td>Target/Bulan (kg)</td>
                        <td>Issue/Follow Up Action</td>
                        <td>Status</td>
                        <td>SLA</td>
                                   
                </tr>
              </thead>
               <tfoot>
                    <td>
                        <form action="model/datareview/registernoobatch.php" method="POST">

                            <button type="button" class="btn btn-success" id="save_noo" name="save_noo" value="Register NOO">Verify Data</button>

                            <input type="hidden" name="list_noo" id="list_noo">

                            <button type="submit" name="submit_noo" class="btn btn-primary" id="submit_noo">Register NOO</button>
                        </form>
                    </td>        
                </tfoot>
            </table>

           

        </div>
        
                            

                            
       
        <!-- END DATA TABLE -->
    </div>
</div>


 <script type="text/javascript">


            $(document).ready(function()
            {
           

             var surveyTable =  $('#surveyTable').DataTable({
                  'processing': true,
                  'serverSide': true,
                  'serverMethod': 'post',
                  'ajax': {
                      'url':'API/review.php',
                      'dataType': 'json',
                      'dataSrc': "data",
                      'data': function(data){
                          // Read values
                          var tanggal_mulai = $('#tanggal_mulai').val();
                          var tanggal_akhir = $('#tanggal_akhir').val();

                          // Append to data
                          data.tanggal_mulai = tanggal_mulai;
                          data.tanggal_akhir = tanggal_akhir;
                       }

                  },
                  'columns': [
                     { data: 'checkbox' },
                     { 
                        data: function(data, type, row, meta){
                            if(type === 'display'){
                                //data = '<a href="' + data.custid + '">' + data.custname + '</a>';
                                data = '<a href="index.php?mod=datareview&class=edit&id='+ data.iddistributor+'" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Edit data"><i class="fas fa fa-edit"></i></a> <a onclick="myFunction('+data.iddistributor+')" id="data-action-'+data.iddistributor+'" class="btn btn-sm btn-primary popup" data-toggle="tooltip" data-placement="top" title="Ubah Status" data-id="'+data.iddistributor+'" acuan="'+ data.iddistributor +'"><i class="fas fa fa-hand-point-down"></i></a>';

                            }

                            return data;
                         }
                     }, 
                     { data: 'custname' },
                     { data: 'custid' },
                     { data: 'distname' },
                     { data: 'coveragearea' },
                     { data: 'inputdatetime' },
                     { data: 'sales' },
                     { data: 'status' },
                     { data: 'channel' },
                     { data: 'subchannel' },
                     { data: 'segment' },
                     { data: 'nameowner' },
                     { data: 'noktp' },
                     { data: 'telephone' },
                     { data: 'longitude' },
                     { data: 'latitude' },
                     { data: 'address' },
                     { data: 'province' },
                     { data: 'city' },
                     { data: 'kecamatan' },
                     { data: 'kelurahan' },
                     { data: 'kodepos' },
                     { data: 'timeopen' },
                     { data: 'timeclosed' },
                     { data: 'luasbangunan' },
                     { data: 'paketfreezer' },
                     { data: 'link_photo' },
                     { data: 'link_photo2' },
                     { data: 'link_photo3' },
                     { data: 'link_photo4' },
                     { data: 'sebutkanlingkungan' },
                     { data: 'patokan' },
                     { data: 'lokasi1' },
                     { data: 'lokasi2' },
                     { data: 'freezerdalamtoko' },
                     { data: 'merekfreezer' },
                     { data: 'frozenFood' },
                     { data: 'merekfrozenfood' },
                     { data: 'productutama' },
                     { data: 'estimasipengunjung' },
                     { data: 'pendapatan' },
                     { data: 'mayoritas' },
                     { data: 'elektricity' },
                     { data: 'kondisifisik' },
                     { data: 'resikofreezerhilang' },
                     { data: 'tersediatempat' },
                     { data: 'pilihanfreezeroutlet' },
                     { data: 'tempatfreezer' },
                     { data: 'penilaianlokasi' },
                     { data: 'sebutkanlingkungan' },
                     { data: 'targetbulan' },
                     { data: 'followupaction' },
                     { data: 'statustoko' },
                     { data: 'sla' }
                  ],
                  "columnDefs": [
                    { "orderable": false, "targets": 0 },
                    { "orderable": false, "targets": 1 }
                  ],
                  //"pageLength": 10,
                  oLanguage: {sProcessing: "<div id='loader_data'></div>"},
                  dom: 'Bfrtipl',
                  buttons: [
                        'copy',
                        {
                            extend: 'excelHtml5',
                            exportOptions: {
                                 modifier : {
                                     order : 'index', // 'current', 'applied','index', 'original'
                                     page : 'all', // 'all', 'current'
                                     search : 'none' // 'none', 'applied', 'removed'
                                 },
                                 columns: [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53]
                             },
                            messageTop: 'The information in this table is copyright to PT. Sierad Produce Tbk.'
                        },
                        {
                            extend: 'print',
                              text: '<i class="fa fa-print"></i> Print',
                              title: '<center>Laporan '+ $('h2').text() + '<hr>',
                              exportOptions: {
                                columns: ':not(.no-print)'
                              },
                              footer: true,
                              autoPrint: true
                        }
                    ]

               });
              

                $('#custom').click(function(){
                    var tanggal_mulai = document.getElementById("tanggal_mulai").value;
                    var tanggal_akhir = document.getElementById("tanggal_akhir").value;

                    if ((Date.parse(tanggal_akhir) <= Date.parse(tanggal_mulai))) {
                      alert("Tanggal Akhir Harus Lebih Besar dari Tanggal Mulai");
                      document.getElementById("tanggal_akhir").value = "";
                      return false;
                    }
                    surveyTable.draw();
                });
            
            });
</script>



<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>

     <form action="model/datareview/action.php" method="post">
                                <input type="hidden" name="id_distributor" value="">

                                <div class="row">
                                    <div class="col-md-12">
                                        <small>Ganti Status</small>
                                    <br>
                                        <select name="action" class="form-control" style="width:200px" required>
                                            <option value="REJECT">REJECT</option>
                                            <option value="POSTPONE">POSTPONE</option>
                                        </select>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-sm btn-success">
                                            Update
                                        </button>
                                   
                                        <a onclick="batal()" class="btn btn-sm btn-dark batal-action">Batal</a>
                                    </div>
                                 </div>
                                    
                                </div>
                            </form>

    
  </div>

</div>
<script type="text/javascript">

    function myFunction(id) {
     
     modal.style.display = "block";
     $('input[name="id_distributor"]').val(id);
 

    }

    function batal()
    {
        modal.style.display = "none";
    }


    // Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementsByClassName("popup");

//var button = document.getElementsByClassName('popup')[0].id;
// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on the button, open the modal
btn.onclick = function() {
  modal.style.display = "block";
  
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}

</script>
         
                   