<?php 
    if (isset($_GET['status'])) {
        $status = $_GET['status'];

        if ($status == 'success') {
            echo '<div class="alert alert-success" role="alert">
                Rejection code baru berhasil di tambahkan
                <a href="index.php?mod=rejectioncode&class=view" class="alert-link">view code</a>.
            </div>';
        }elseif ($status == 'failed') {
            echo '<div class="alert alert-danger" role="alert">
                Rejection code sudah ada di database
            </div>';
        }else{
            echo '<div class="alert alert-warning" role="alert">
                Terjadi kesalahan koneksi internet, silkahkan ulangi kembali
            </div>';
        }
        
    }
 ?>
<div class="row">
    <div class="col-md-12">
        <h3 class="title-5 m-b-35">new rejection code</h3>

        <div class="card">
        <div class="card-header">
            <strong>Add New Rejection Code</strong>
        </div>
        <div class="card-body card-block">

            <form action="model/rejectioncode/add.php" method="post" enctype="multipart/form-data" class="form-horizontal">
                
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Code</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="text" id="text-input" name="code" placeholder="" class="form-control" required>
                        <small class="form-text text-muted">Kode Rejection</small>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Keterangan</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="text" id="text-input" name="ket" placeholder="" class="form-control" required>
                        <small class="form-text text-muted">Detail Rejection</small>
                    </div>
                </div>
                
                
                
                </div>
                <div class="card-footer">
                    <button type="submit" class="au-btn au-btn--green">
                        <i class="fa fa-floppy-o"></i> Save
                    </button>
                    <button type="reset" class="au-btn au-btn--red">
                        <i class="fa fa-ban"></i> Reset
                    </button>
                </div>

            </form>
        </div>
    </div>
    </div>
</div>