<?php 

    //hitung jumlah Customer
    $area = $_SESSION['area'];
    $vendor = $_SESSION['nama'];
    $acc = $vendor.'-'.$area;
    include('config/config.php');

 ?>
 
 <html>
    <head>
        <meta charset="utf-8">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Export freezer reject</title>
        <link rel="stylesheet" href="assets/css/data_tables/data_tables.css">
        <link rel="stylesheet" href="assets/css/data_tables/material/datatable_material.css">
        <link rel="stylesheet" href="assets/css/data_tables/material/material.css">
        <link rel="stylesheet" href="assets/css/data_tables/buttons.dataTables.min.css">
    </head>
    <body>

<div class="row">
    <div class="col-md-12">
        <div class="table-responsive table-data" style="height: auto">
            <table id="tableok2" class="mdl-data-table" style="width:100%">
                <thead>
                    <tr>
                        <td>No</td>
                        <td>Reject Date</td>
                        <td>Keterangan</td>
                        <td>BFI Code</td>
                        <td>Nama Outlet</td>
                        <td>Nama Distributor</td>
                        <td>Tanggal Survey</td>
                        <td>Freezer Owner</td>
                        <td>Nama Vendor</td>
                        <td>IDNumber</td>
                        <td>Tipe Freezer</td>
                        <td>Nama PIC</td>
                        <td>No Telp PIC</td>
                        <td>Alamat</td>
                        <td>Provinsi</td>
                        <td>Kabupaten / Kota</td>
                        <td>Kecamatan</td>
                        <td>Kelurahan</td>
                        <td>Kode Pos</td>
                        <td>Status</td>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $no = 1;
                        
                        $query33 = "SELECT * FROM focus_additional_reject ORDER BY id_reject DESC";
                        $data33 = mysqli_query($koneksi,$query33);

                        while ($result33 = mysqli_fetch_assoc($data33)) {
                     ?>
                    <tr>

                        <!-- cari datangnya dari mana -->
                        <?php 
                                if ($result['id_distributor'] == 0) {
                                    //maka itu artinya primary
                                    $id_freezer = $result33['id_freezer'];

                                    $cp = "SELECT * FROM MDS_CustomerPrimary WHERE Code = '$id_freezer'";
                                    
                                    $dcp = mysqli_query( $koneksi, $cp );
                                    $result2 = mysqli_fetch_assoc($dcp);
                                    
                                    $tipe2 = $result2['MarketId_Name'];
                                    $photo2 = '';
                                    $photo32 = '';
                                    if ($tipe2 == '' or $tipe2 == NULL) {

                                        //karna dia secondary, cari lagi datanya dari tabel secondary mds
                                        $cp2 = "SELECT * FROM MDS_CustomerSecondary WHERE Code = '$id_freezer'";
                                        $dcp2 = mysqli_query( $koneksi, $cp2 );
                                        $results2 = mysqli_fetch_assoc($dcp2);

                                        $custid2 = $results2['Code'];
                                        $custname2 = $results2['Name'];
                                        $distname2 = $results2['DistName'];
                                        $tgl_survey2 = "Tidak tersedia";
                                        $tipe2 = "02 {IN DIRECT}";
                                        $pic2 = $results2['PIC'];
                                        $phone2 = $results2['Phone'];
                                        $latitude2 = $results2['Latitude'];
                                        $longitude2 = $results2['Longitude'];
                                        $address2 = $results2['Alamat'];
                                        $city2 = $results2['KabKota'];
                                        $province2 = $results2['Propinsi'];
                                        $kecamatan2 = $results2['Kecamatan'];
                                        $kelurahan2 = $results2['Kelurahan'];
                                        $kodepos2 = $results2['KodePos'];
                                    }else{
                                        $custid2 = $result2['Code'];
                                        $custname2 = $result2['Name'];
                                        $distname2 = $result2['System_Distributor_Name'];
                                        $tipe2 = "01 {DIRECT}";
                                        $pic2 = $result2['PIC'];
                                        $phone2 = $result2['Phone'];
                                        $latitude2 = $result2['Latitude'];
                                        $longitude2 = $result2['Longitude'];
                                        $address2 = $result2['Street'];
                                        $city2 = $result2['City'];
                                        $province2 = $result2['SalesDistrictName'];
                                        $kecamatan2 = $result2['SPVillageName'];
                                        $kelurahan2 = '';
                                        $kodepos2 = $result2['ZipCode'];
                                    }
                                }else{

                                    $id_distributor = $result33['id_distributor'];
                                    $cs = "SELECT * FROM distributor WHERE iddistributor = '$id_distributor'";
                                    $dcs = mysqli_query($koneksi,$cs);
                                    $result34 = mysqli_fetch_assoc($dcs); 

                                    $custid2 = $result34['custid'];
                                    $custname2 = $result34['custname'];
                                    $distname2 = $result34['distname'];
                                    $tgl_survey2 = $result34['inputdatetime'];
                                    $tipe2 = 'SECONDARY';
                                    $address2 = $result34['address'];
                                    $province2 = $result34['province'];
                                    $city2 =$result34['city'];
                                    $kecamatan2 =$result34['kecamatan'];
                                    $kelurahan2 =$result34['kelurahan'];
                                    $kodepos2 = $result34['kodepos'];
                                    $photo32 = "http://appict.appsbelfoods.com/".$result34['photo3']."";
                                    $photo2 = "http://appict.appsbelfoods.com/".$result34['photo'].""; 
                                    $pic2 = $result34['contactname'];
                                    $phone2 = $result34['telephone'];
                                    $latitude2 = $result34['latitude'];
                                    $longitude2 = $result34['longitude'];

                                }
                             ?>

                        <td><?php echo $no ?></td>
                        <td>
                            <?php echo $result33['date']; ?>
                        </td>
                        <td>
                            <?php echo $result33['ket']; ?>
                        </td>
                        <td>
                            <?php echo $custid2 ?>
                        </td>
                        <td><?php echo $custname2 ?></td>
                        <td>
                            <?php echo $distname2 ?>
                        </td>
                        <td>
                            <?php echo $tgl_survey2 ?>
                        </td>
                        <td>
                            <?php echo $result33['freezer_owner'] ?>
                        </td>
                        <td>
                            <?php echo $result33['nama_vendor'] ?>
                        </td>
                        <td>
                            <?php echo $result33['IDNumber'] ?>
                        </td>
                        
                        <td>
                            <?php echo $result33['tipe']; ?>
                        </td>
                        <td>
                            <?php echo $pic2 ?>
                        </td>
                        <td>
                            <?php echo $phone2 ?>
                        </td>
                       
                        <td>
                            <?php echo $address2 ?>
                        </td>
                        <td>
                            <?php echo $province2 ?>
                        </td>
                        <td>
                            <?php echo $city2 ?>
                        </td>
                        <td>
                            <?php echo $kecamatan2 ?>
                        </td>
                        <td>
                            <?php echo $kelurahan2 ?>
                        </td>
                        <td>
                            <?php echo $kodepos2 ?>
                        </td>                    
                        <td>
                            <?php echo $result33['status'] ?>
                        </td>
                    </tr>

                     <?php $no++; } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script src="assets/vendor/jquery-3.2.1.min.js"></script>
    <script src="assets/vendor/jquery-datatables.js"></script>
    <script type="text/javascript" src="assets/js/data_tables/data_tables.js"></script>

    <script src="assets/js/data_tables/material/datatable_material.js"></script>
    <script src="assets/js/data_tables/dataTables.buttons.min.js"></script>
    <script src="assets/js/data_tables/buttons.print.min.js"></script>
    <script src="assets/js/data_tables/buttons.html5.min.js"></script>
    <script src="assets/js/data_tables/vfs_fonts.js"></script>
    <script src="assets/js/data_tables/pdfmake.min.js"></script>
    <script src="assets/js/data_tables/jszip.min.js"></script>
            
            
            <script>

    //$(document).ready(function() {

        $('#tableok').DataTable( {
        columnDefs: [
            {
                targets: [ 0, 1, 2 ],
                className: 'mdl-data-table__cell--non-numeric'
            }
        ],

        "deferRender": true,

        

        "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
            // if ( aData[52] == "YES" )
            // {
            //     $('td', nRow).css('background-color', '#007bff');
            //     $('td', nRow).css('color', 'White');
            //     $('td', nRow).css('font-weight', 'bold');
            // }
            if ( aData[51] == "REJECT")
            {
                $('td', nRow).css('background-color', '#9e9e9e');
                $('td', nRow).css('color', 'White');
                $('td', nRow).css('font-weight', 'bold');
            }
            if ( aData[51] == "POSTPONE")
            {
                $('td', nRow).css('background-color', 'background-color: rgba(244, 67, 54, 0.66)');
                $('td', nRow).css('color', 'White');
                $('td', nRow).css('font-weight', 'bold');
            }
            if ( aData[51] == "DEAL")
            {
                $('td', nRow).css('background-color', 'rgba(76, 175, 80, 0.65)');
                $('td', nRow).css('color', 'White');
                $('td', nRow).css('font-weight', 'bold');
            }
            else if ( aData[7] == "Lunas" )
            {
                $('td', nRow).css('background-color', '#007bff');
                $('td', nRow).css('color', 'White');
                $('td', nRow).css('font-weight', 'bold');
            }else if ( aData[12] == "canceled") 
            {
                $('td', nRow).css('background-color', '#FF9800');
                $('td', nRow).css('color', 'White');
                $('td', nRow).css('font-weight', 'bold');
            }
            else if ( aData[53] == "OVERDUE") 
            {
                $('td', nRow).css('background-color', '#FFC107');
                $('td', nRow).css('color', 'White');
                $('td', nRow).css('font-weight', 'bold');
            }
        },

        dom: 'Bfrtip',

        buttons: [
            'copy',
            {
                extend: 'excel',
                messageTop: 'The information in this table is copyright to PT. Sierad Produce Tbk.'
            },
            // {
            //     extend: 'pdf',
            //     messageBottom: null
            // },
            {
                extend: 'print',
                  text: '<i class="fa fa-print"></i> Print',
                  title: '<center>Laporan '+ $('h2').text() + '<hr>',
                  exportOptions: {
                    columns: ':not(.no-print)'
                  },
                  footer: true,
                  autoPrint: true
            }
        ]

    } );

    $('#tableok2').DataTable( {
        columnDefs: [
            {
                targets: [ 0, 1, 2 ],
                className: 'mdl-data-table__cell--non-numeric'
            }
        ],

        "deferRender": true,

        "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
            if ( aData[9] == "cancel" )
            {
                $('td', nRow).css('background-color', '#ff4b5a');
                $('td', nRow).css('color', 'White');
                $('td', nRow).css('font-weight', 'bold');
            }
            else if ( aData[9] == "ongoing" )
            {
                $('td', nRow).css('background-color', '#007bff');
                $('td', nRow).css('color', 'White');
                $('td', nRow).css('font-weight', 'bold');
            }
        },
        
        initComplete: function() {
           $('.buttons-excel').click()
        },

        dom: 'Bfrtip',

        buttons: [
            'copy',
            {
                extend: 'excel',
                messageTop: 'The information in this table is copyright to PT. Sierad Produce Tbk.'
            },
            // {
            //     extend: 'pdf',
            //     messageBottom: null
            // },
            {
                extend: 'print',
                  text: '<i class="fa fa-print"></i> Print',
                  title: '<center>Laporan '+ $('h2').text() + '<hr>',
                  exportOptions: {
                    columns: ':not(.no-print)'
                  },
                  footer: true,
                  autoPrint: true
            }
        ]
        

    } );

    // $('#tableok2').DataTable( {
    //     columnDefs: [
    //         {
    //             targets: [ 0, 1, 2 ],
    //             className: 'mdl-data-table__cell--non-numeric'
    //         }
    //     ],

    //     "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
    //         if ( aData[9] == "cancel" )
    //         {
    //             $('td', nRow).css('background-color', '#ff4b5a');
    //             $('td', nRow).css('color', 'White');
    //             $('td', nRow).css('font-weight', 'bold');
    //         }
    //         else if ( aData[9] == "ongoing" )
    //         {
    //             $('td', nRow).css('background-color', '#007bff');
    //             $('td', nRow).css('color', 'White');
    //             $('td', nRow).css('font-weight', 'bold');
    //         }
    //     },

    //     dom: 'Bfrtip',

    //     buttons: [
    //         'copy',
    //         {
    //             extend: 'excel',
    //             messageTop: 'The information in this table is copyright to PT. Sierad Produce Tbk.'
    //         },
    //         // {
    //         //     extend: 'pdf',
    //         //     messageBottom: null
    //         // },
    //         {
    //             extend: 'print',
    //               text: '<i class="fa fa-print"></i> Print',
    //               title: '<center>Laporan '+ $('h2').text() + '<hr>',
    //               exportOptions: {
    //                 columns: ':not(.no-print)'
    //               },
    //               footer: true,
    //               autoPrint: true
    //         }
    //     ]

    // } );
    //});
    
   
    

</script>
</body>
</html>