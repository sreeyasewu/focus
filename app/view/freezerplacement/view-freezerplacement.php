<?php 
include('config/config.php');
//include('config/mds.php');

$role = $_SESSION['role'];
$placedby = $_SESSION['nama'];
$area = trim($_SESSION['area']);
$vendor = $_SESSION['nama'];
$acc = $vendor.'-'.$area;

$areaSession = trim($_SESSION['area']);
$vendorSession = $_SESSION['nama'];

    if (isset($_GET['status'])) {
        $status = $_GET['status'];

        if ($status == 1) {
            echo '<div class="alert alert-success" role="alert">
                Data updated
            </div>';
        }elseif ($status == 0) {
            echo '<div class="alert alert-danger" role="alert">
                Terjadi Kesalahan! silahkan coba kembali
            </div>';
        }elseif ($status == 9) {
            echo '<div class="alert alert-danger" role="alert">
                Mohon pilih vendor!
            </div>';
        }elseif ($status == 99) {
            echo '<div class="alert alert-danger" role="alert">
                Mohon pilih status kepemilikan!
            </div>';
        }elseif ($status == 990) {
            echo '<div class="alert alert-danger" role="alert">
                Mohon pilih area vendor!
            </div>';
        }else{
            echo '<div class="alert alert-warning" role="alert">
                Terjadi kesalahan koneksi internet, silkahkan ulangi kembali
            </div>';
        }
    }

    if (isset($_GET['editstatus'])) {
        $status = $_GET['editstatus'];

        if ($status == 1) {
            echo '<div class="alert alert-success" role="alert">
                Data user berhasil di perbaruhi
            </div>';
        }elseif ($status == 0) {
            echo '<div class="alert alert-danger" role="alert">
                Terjadi Kesalahan! tidak dapat mengubah data user
            </div>';
        }else{
            echo '<div class="alert alert-warning" role="alert">
                Terjadi kesalahan koneksi internet, silkahkan ulangi kembali
            </div>';
        }
    }
 ?>

 <?php if ($role == 'VENDOR'): ?>
     <div class="row">
<div class="col-md-12">
        
        
        <h4>
            View Freezer Placement

        </h4>
        <br>
        <h4>New Placement Request</h4>
        <hr>
		<form action="" method="POST">
		<?php
			if (isset($_POST['customReqDate'])) {
				$tanggalMulai1 = mysqli_escape_string($koneksi,$_POST['tanggal_mulai']);
				$tanggalSelesai1 = mysqli_escape_string($koneksi,$_POST['tanggal_akhir']);
				$tanggalMulai1Edit = date('Y-m-d', strtotime($tanggalMulai1));
				$tanggalSelesai1Edit = date('Y-m-d', strtotime($tanggalSelesai1));
				//$tanggalMulai1= date('Y-m-d', $tanggalMulai1Edit);
				//$tanggalSelesai1= date('Y-m-d', $tanggalSelesai1Edit);
				
				$dateclause = " and approved_date >='$tanggalMulai1Edit' and approved_date <= '$tanggalSelesai1Edit' ";
			} else {
				$tanggalMulai1 = "Mulai";
				$tanggalSelesai1 = "Selesai";
				$dateclause = "";
				
			}
			
		?>
		<div class="row form-group">
			<div class="col-sm-3 col-md-2">
				<label for="text-input" class=" form-control-label"><b>Assign Date</b></label>
			</div>
			<div class="col-sm-3 col-md-2">
				<input type="text" id="tanggal_mulai" name="tanggal_mulai" class="form-control" placeholder="Awal" required>
			</div>
			<div class="col-sm-3 col-md-2">
				<input type="text" id="tanggal_akhir" name="tanggal_akhir" class="form-control" placeholder="Akhir" required>
				
			</div>
			<button type="submit" name="customReqDate" value="custom" class="btn btn-sm btn-primary">
				Search
			</button>
		</div>

		</form>
		<hr>
        <div class="table-responsive table-data" style="height: auto">
            <table id="tableok2" class="mdl-data-table" style="width:100%">
                <thead>
                    <tr>
                        <td>Validate</td>
                        <td>Action</td>
                        <td>Assign Date</td>
                        <td>Plan Placement Date Update</td>
                        <td>Plan Placement Date</td>
                        <td>No</td>
                        <td>Sequence Number</td>
                        <td>Tipe Customer</td>
                        <td>BFI Code</td>
                        <td>Nama Outlet</td>
                        <td>Nama Distributor</td>
                        <td>Tanggal Survey</td>
                        <td>Freezer Owner</td>
                        <td>Nama Vendor</td>
                        <td>Tipe Freezer</td>
                        <td>Email to Vendor Date</td>
                        <td>Placement Date</td>
                        <td>Lead Time</td>
                        <td>Status</td>
                        <td>Tanggal Validasi</td>
                        <td>Freezer Code</td>
                        <td>BatchPO</td>
                        <td>Nomor Mesin</td>
                        <td>Nama Supir</td>
                        <td>Tanggal Pengiriman</td>
                        <td>Realisasi Tipe Freezer</td>
                        <td>Nama PIC</td>
                        <td>No Telp PIC</td>
                        <td>Foto - Deal Form + KTP</td>
                        <td>Foto - Outlet Tampak Depan</td>
                        <td>Kondisi Fisik Outlet</td>
                        <!-- <td>Patokan</td> -->
                        <td>Longitude</td>
                        <td>Latitude</td>
                        <td>Vendor Longitude</td>
                        <td>Vendor Latitude</td>
                        <td>Alamat</td>
                        <td>Provinsi</td>
                        <td>Kabupaten / Kota</td>
                        <td>Kecamatan</td>
                        <td>Kelurahan</td>
                        <td>Kode Pos</td>
                        <td>Jam Buka</td>
                        <td>Jam Tutup</td>
                        <td>Updated By</td>
                        <!-- <td>Action</td> -->
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $no = 1;
                        
                        // $query = "SELECT * FROM distributor, focus_freezer_placement WHERE `focus_freezer_placement`.`approved_status` = 'APPROVED' AND `distributor`.`iddistributor`=`focus_freezer_placement`.`id_distributor` AND `focus_freezer_placement`.`placement_date` = '0000-00-00' AND `focus_freezer_placement`.`nama_vendor` LIKE '%$acc%'";
						if ($area == "") {
							$query = "SELECT * FROM focus_freezer_placement WHERE approved_status = 'APPROVED' AND placement_date = '0000-00-00' ".$dateclause." AND nama_vendor like '$acc%' GROUP BY IDNumber LIMIT 0,500";
						} else {
							$query = "SELECT * FROM focus_freezer_placement WHERE approved_status = 'APPROVED' AND placement_date = '0000-00-00' ".$dateclause." AND nama_vendor = '$acc' GROUP BY IDNumber LIMIT 0,300";
						}
                        $data = mysqli_query($koneksi,$query);

                        while ($result = mysqli_fetch_assoc($data)) {
                     ?>
                    <tr>

                        <!-- cari datangnya dari mana -->
                            <?php 
                                if ($result['id_distributor'] == 0) {
                                    //maka itu artinya bukan dari survey NOO
				    $id_freezer = $result['id_freezer'];

				    if ($result['cust_type'] == "Indirect") {
                                        //karna dia secondary, cari lagi datanya dari tabel secondary mds
                                        $cp = "SELECT * FROM MDS_CustomerSecondary WHERE Code = '$id_freezer'";
                                        $dcp = mysqli_query($koneksi, $cp);
                                        $results2 = mysqli_fetch_assoc($dcp);

                                        $custid = $results2['Code'];
                                        $custname = $results2['Name'];
                                        $distname = $results2['DistName'];

                                        $tipe = "Indirect";
                                        $pic = $results2['PIC'];
                                        $phone = $results2['Phone'];
                                        $latitude = $results2['Latitude'];
                                        $longitude = $results2['Longitude'];
                                        $address = $results2['Alamat'];
                                        $city = $results2['KabKota'];
                                        $province = $results2['Propinsi'];
                                        $kecamatan = $results2['Kecamatan'];
                                        $kelurahan = $results2['Kelurahan'];
                                        $kodepos = $results2['KodePos'];

                                        // untuk save ke database jd cuma di bagian ini aja di lainnya tidak
                                        $sales = $results2['SalesManDistributor_Code'];
                                        $distid = $results2['DistID'];
                                        $flagdu = $results2['Flag_DU_Code'];
                                        $area = $results2['Area'];
                                        $segment = $results2['Segment_Code'];

                                        $idpjp = $results2['IDPJP'];
                                        $idsup = $results2['IDSup'];
                                        $outletstatus = "Active";
                                        $dayvisit = $results2['Day Visit_Name'];
                                        $call = $results2['FOC_Code'];
                                        $m1 = substr($results2['Minggu_1_Name'], 0 ,1 );
                                        $m2 = substr($results2['Minggu_2_Name'], 0 ,1 );
                                        $m3 = substr($results2['Minggu_3_Name'], 0 ,1 );
                                        $m4 = substr($results2['Minggu_4_Name'], 0 ,1 );

                                        $cst = "Indirect";
                                        $tipe_freezer = $result['realisasi_tipe_freezer'];


                                    } else { // primary customer
                                        $cp = "SELECT * FROM MDS_CustomerPrimary WHERE Code = '$id_freezer'";
                                        $dcp = mysqli_query($koneksi, $cp);
                               	        $result2 = mysqli_fetch_assoc($dcp);

                        	        $tgl_survey = "Tidak tersedia";
                	                $tipe = $result2['MarketId_Name'];
        	                        $photo = '';
	                                $photo3 = '';


                                        $custid = $result2['Code'];
                                        $custname = $result2['Name'];
                                        $distname = $result2['System_Distributor_Name'];
                                        $tipe = "Direct";
                                        $pic = $result2['PIC'];
                                        $phone = $result2['Phone'];
                                        $latitude = $result2['Latitude'];
                                        $longitude = $result2['Longitude'];
                                        $address = $result2['Street'];
                                        $city = $result2['City'];
                                        $province = $result2['SalesDistrictName'];
                                        $kecamatan = $result2['SPVillageName'];
                                        $kelurahan = '';
                                        $kodepos = $result2['ZipCode'];

                                        $sales = $result2['SalesPerson_Code'];
                                        $distid = $result2['System_Distributor_ID'];
                                        $flagdu = $result2['Flag DU_Code'];
                                        $area = $result2['Regional_Name'];
                                        $segment = $result2['SegmentId_Code'];
                                        
                                        $idpjp = '';
                                        $idsup = '';
                                        $outletstatus = "Active";
                                        $dayvisit = '';
                                        $call = '';
                                        $m1 = '';
                                        $m2 = '';
                                        $m3 = '';
                                        $m4 = '';

                                        $cst = "Direct";
                                        $tipe_freezer = $result['realisasi_tipe_freezer'];
                                    }
                                }else{

                                    $id_distributor = $result['id_distributor'];
                                    $cs = "SELECT * FROM distributor WHERE iddistributor = '$id_distributor'";
                                    $dcs = mysqli_query($koneksi,$cs);
                                    $result3 = mysqli_fetch_assoc($dcs); 

                                    $custid = $result3['custid'];
                                    $custname = $result3['custname'];
                                    $distname = $result3['distname'];
                                    $tgl_survey = $result3['inputdatetime'];
                                    $tipe = 'Indirect';
                                    $photo3 = "https://appict.appsbelfoods.com/".$result3['photo3']."";
                                    $photo = "https://appict.appsbelfoods.com/".$result3['photo'].""; 
                                    $pic = $result3['contactname'];
                                    $phone = $result3['telephone'];
                                    $latitude = $result3['latitude'];
                                    $longitude = $result3['longitude'];
                                    // $tipe = "SECONDARY";
                                    $address = $result3['address'];
                                    $city = $result3['city'];
                                    $province = $result3['province'];
                                    $kecamatan = $result3['kecamatan'];
                                    $kelurahan = $result3['kelurahan'];
                                    $kodepos = $result3['kodepos'];
                                    $tipe_freezer = $result3['pilihanfreezeroutlet'];
                                }
                             ?>
                        <!-- cari datangnya dari mana -->
                        <td>

                            <?php if ($result['keterangan'] == 'VALIDATED'): ?>

                                    <span class="badge">VALIDATED</span>
                                <?php else: ?>
                                    <form action="model/freezerplacement/validate.php" method="POST">
                                        <input type="hidden" value="<?php echo $result['id_placement'] ?>" name="id_placement">

                                        <button type="submit" name="validate" class="btn btn-success">Validate</button>
                                    </form>
                            <?php endif ?>
                        </td>
                        <td>

                            <?php if ($result['coming_from'] == 'ADDITIONAL'): ?>
                                <form action="model/freezerplacement/placefreezeradditional.php" method="post" class="form-tindaklanjut" id="form-approved-<?php echo $result['id_freezer']?>">
                                

                                <?php 
                                    // if ($result['id_distributor'] == 0) {
                                    //     $idsend = $result['id_freezer'];
                                    //     $cs = "primary";
                                    // }else{
                                    //     $idsend = $result['id_distributor'];
                                    //     $cs = "secondary";
                                    // }
                                 ?>

                                 <input type="hidden" id="id" name="id" value=" <?php echo $id_freezer ?>">

                                 <input type="hidden" name="tipe" value="<?php echo $tipe ?>">

                                <input type="hidden" id="emailtovendor" name="emailtovendor" value="<?php echo $result['approved_date'] ?>">

                                <input type="hidden" name="placedby" value="<?php echo $placedby ?>">

                                <input type="hidden" name="idnumber" value="<?php echo $result['IDNumber'] ?>">

                                <input type="hidden" name="vendor" value="<?php echo explode("-", $result['nama_vendor'])[0]; ?>">

                                <input type="hidden" name="owner" value="<?php echo $result['freezer_owner'] ?>">

                                <input type="hidden" name="id_placement" value="<?php echo $result['id_placement'] ?>">

                                <input type="hidden" name="principal" value="<?php echo $result['principal'] ?>">

                                <input type="hidden" name="sales" value="<?php echo $sales ?>">
                                
                                <input type="hidden" name="distid" value="<?php echo $distid ?>">
                                
                                <input type="hidden" name="distname" value="<?php echo $distname ?>">

                                <input type="hidden" name="flagdu" value="<?php echo $flagdu ?>">
                                
                                <input type="hidden" name="area" value="<?php echo $area ?>">
                                
                                <input type="hidden" name="segment" value="<?php echo $segment ?>">
                                
                                <input type="hidden" name="idpjp" value="<?php echo $idpjp ?>">
                                
                                <input type="hidden" name="idsup" value="<?php echo $idsup ?>">

                                <input type="hidden" name="outletstatus" value="<?php echo $outletstatus ?> ">

                                <input type="hidden" name="dayvisit" value="<?php echo $dayvisit ?> ">

                                <input type="hidden" name="call" value="<?php echo $call ?>">

                                <input type="hidden" name="m1" value="<?php echo $m1 ?>">
                                
                                <input type="hidden" name="m2" value="<?php echo $m2 ?>">
                                
                                <input type="hidden" name="m3" value="<?php echo $m3 ?>">
                                
                                <input type="hidden" name="m4" value="<?php echo $m4 ?>">
                                
                                <input type="hidden" name="custid" value="<?php echo $custid ?>">
                                
                                <input type="hidden" name="custname" value="<?php echo $custname ?>">
                                
                                <input type="hidden" name="tipe" value="<?php echo $tipe ?>">
                                
                                <input type="hidden" name="pic" value="<?php echo $pic ?>">
                                
                                <input type="hidden" name="phone" value="<?php echo $phone ?>">
                                
                                <input type="hidden" name="latitude" value="<?php echo $latitude ?>">
                                
                                <input type="hidden" name="longitude" value="<?php echo $longitude ?>">
                                
                                <input type="hidden" name="address" value="<?php echo $address ?>">
                                
                                <input type="hidden" name="city" value="<?php echo $city ?>">
                                
                                <input type="hidden" name="province" value="<?php echo $province ?>">
                                
                                <input type="hidden" name="kecamatan" value="<?php echo $kecamatan ?>">
                                
                                <input type="hidden" name="kelurahan" value="<?php echo $kelurahan ?>">
                                
                                <input type="hidden" name="kodepos" value="<?php echo $kodepos ?>">




                                <!-- get secondary or primarynya -->

                                <?php 
                                    // $pr = "SELECT * FROM focus_freezer_master WHERE";
                                 ?>

                                <!-- <div class="row">
                                    <div class="col-md-12">
                                        <small>IDNumber</small>
                                    <br>
                                        <input type="text" id="idnumber" name="idnumber" class="form-control" required>
                                    </div>
                                </div> -->
                                
                                <div class="row">
                                    <div class="col-md-12">
                                        <small>Batch PO</small>
                                    <br>
                                    <select id="batchpo" name="batchpo" class="select_bfi form-control" required>
                                            <option value="">Select Batch PO</option>
                                            <?php
                                                include ('config/config.php');
                                                //cari data semua BFI
                                                $sql = "SELECT * FROM focus_batch_po ORDER BY batchpo";
                                                $dql = mysqli_query($koneksi,$sql);


                                                while ($rql = mysqli_fetch_assoc($dql)) {
                                            ?>
                                                <option value="<?php echo $rql['batchpo'] ?>"><?php echo strtoupper($rql['batchpo']." - ".$rql['keterangan'])?></option>

                                            <?php
                                                }


                                             ?>


                                        </select>

                                        <!-- <input type="text" id="batchpo" name="batchpo" class="form-control" required> -->
                                    </div>
                                </div>

                                <!-- <div class="row">
                                    <div class="col-md-12">
                                        <small>Nomor Mesin</small>
                                    <br>
                                        <input type="text" id="placement_notes" name="placement_notes" class="form-control" required>
                                    </div>
                                </div> -->

                                <div class="row">
                                    <div class="col-md-12">
                                        <small>Nama Supir</small>
                                    <br>
                                        <input type="text" id="namasupir" name="namasupir" class="form-control" required>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <small>Tanggal Pengiriman</small>
                                    <br>
                                        <input type="date" id="tanggal_pengiriman" name="tanggal_pengiriman" class="form-control" required>
                                    </div>
                                </div>

                                <?php if($result['freezer_owner'] == 'Jbp') { ?>
                                  <div class="row">
                                    <div class="col-md-12">
                                        <small>Realisasi Tipe Freezer</small>
                                        <br>
                                        
                                        <select name="realisasi_tipe_freezer" id="realisasi_tipe_freezerAsset" class="select_database form-control realisasi_tipe_freezerAsset" required>


                                        </select>
                                    </div>
                                </div>
                                <?php }else{ ?>
                                    
                                <div class="row">
                                    <div class="col-md-12">
                                        <small>Realisasi Tipe Freezer</small>
                                    <br>
                                        <select name="realisasi_tipe_freezer" class="select_bfi form-control" required>
                                            <option value="">Select Tipe Freezer</option>
                                            <?php 
                                                include ('config/config.php');
                                                //cari data semua BFI
                                                $sql = "SELECT * FROM focus_tipe_freezer ORDER BY id_tipe";
                                                $dql = mysqli_query($koneksi,$sql);
                                                

                                                while ($rql = mysqli_fetch_assoc($dql)) {
                                            ?>
                                                <option value="<?php echo $rql['tipe'] ?>"><?php echo strtoupper($rql['tipe']." - ".$rql['keterangan'])?></option>

                                            <?php 
                                                }


                                             ?>
                                            

                                        </select>    
                                                  
                                        <!--<input type="text" id="realisasi_tipe_freezer" name="realisasi_tipe_freezer" class="form-control" required>-->
                                    </div>
                                </div>
                            <?php } ?>
                                <?php if($result['freezer_owner'] == 'Jbp') { ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <small>Aset Freezer</small>
                                    <br>
                                        <select name="assetId" id="assetId" class="select_bfi form-control" onChange="javascript:get_brand()"  required>
                                            <option value="">Select Asset Freezer</option>
                                            <?php 
                                                include ('config/config.php');
                                                //cari data semua BFI
                                                $sql = "SELECT * FROM focus_asset_freezer WHERE vendor = '$vendorSession' AND area = '$areaSession' AND assetStatusId = '1' AND assetId !=''";

                                                $dql = mysqli_query($koneksi,$sql) or die(mysqli_error($koneksi));
                                                

                                                while ($rql = mysqli_fetch_assoc($dql)) {
                                            ?>
                                                <option value="<?php echo $rql['assetId'] ?>"><?php echo strtoupper($rql['assetId'])?></option>

                                            <?php 
                                                }


                                             ?>
                                            

                                        </select>               
                                        <!-- <input type="text" id="realisasi_tipe_freezer" name="realisasi_tipe_freezer" class="form-control" required> -->
                                    </div>
                                </div>
                                <?php } ?>
                               
                               <?php if($result['freezer_owner'] == 'Jbp') { ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <small>Brand</small>
                                    <br>
                                        
                                        <select name="brand" id="brandAsset" class="select_database form-control brandAsset" required>
                                        

                                    </select>
                                    </div>
                                </div>
                                <?php }else{ ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <small>Brand</small>
                                    <br>
                                    <select id="brand" name="brand" class="select_bfi form-control" required>
                                            <option disabled selected>Select Brand</option>
                                            <?php
                                                include ('config/config.php');
                                                //cari data semua BFI
                                                $sql = "SELECT * FROM focus_brand ORDER BY name";
                                                $dql = mysqli_query($koneksi,$sql);


                                                while ($rql = mysqli_fetch_assoc($dql)) {
                                            ?>
                                                <option value="<?php echo $rql['name'] ?>"><?php echo strtoupper($rql['name']." - ".$rql['ket'])?></option>

                                            <?php
                                                }


                                             ?>


                                        </select>

                                        

                                  
                                    </div>
                                </div>
                                <?php } ?>
                            

                                <?php 
                                    if ($result['freezer_owner'] == 'Rent' || $result['freezer_owner'] == 'RENT' || $result['freezer_owner'] == 'Own') {
                                        echo '<div class="row">
                                                    <div class="col-md-12">
                                                        <small>Nomor Mesin</small>
                                                    <br>
                                                        <input type="text" id="nomormesin" name="nomormesin" class="form-control" required>
                                                    </div>
                                                </div>';
                                    }else{
                                        ?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <small>Nomor Mesin</small>
                                                <br>
                                                <select name="nomormesin" id="nomormesinAsset" class="select_database form-control nomormesinAsset" required>


                                                </select>
                                            </div>
                                        </div>
                                                <!--
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <small>Nomor Mesin</small>
                                                    <br>
                                                        <input type="text" id="nomormesin" name="nomormesin" class="form-control" value="<?php echo $result['placement_notes'] ?>">
                                                    </div>
                                                </div>
                                                -->
					<?php
                                    }
                                 ?>

                                

                                <div class="row">
                                    <div class="col-md-12">
                                        <small>Vendor Longitude</small>
                                    <br>
                                        <input type="text" id="v_longitude" name="v_longitude" class="form-control" required>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <small>Vendor Latitude</small>
                                    <br>
                                        <input type="text" id="v_latitude" name="v_latitude" class="form-control" required>
                                    </div>
                                </div>

                                <!-- <div class="row">
                                    <div class="col-md-12">
                                        <small>IDNumber</small>
                                    <br>
                                        <input type="text" id="idnumber" name="idnumber" class="form-control" required>

                                        

                                    </select>
                                    </div>
                                </div> -->

                                
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-sm btn-success">
                                            Update
                                        </button>
                                   
                                        <a href="javascript:void();" class="btn btn-sm btn-dark batal-tindaklanjut">Batal</a>
                                    </div>
                                 </div>
                                    
                                </div>
                            </form>
                            

                            

                            <a href="javascript:void();" id="approved_placement_<?php echo $result['id_freezer']?>" class="btn btn-block btn-info approved_placement" data-toggle="tooltip" data-placement="top" title="Place Freezer Aditional" acuan="<?php echo $result['id_freezer']?>">
                                <i class="fas fa fa-hand-point-down"></i>
                            </a>
                            <br>

                            <form action="model/freezerplacement/reject-freezeradditional.php" method="post" class="form-tindaklanjut" id="form-tindaklanjut-<?php echo $result['id_placement'] ?>">
                                    <input type="hidden" id="id" name="id_placement" value="<?php echo $result['id_placement'] ?>">

                                    <input type="hidden" name="id_distributor" value="<?php echo $result['id_distributor'] ?>">
                                    <input type="hidden" name="id_freezer" value="<?php echo $result['id_freezer'] ?>">
                                    <input type="hidden" name="id_number" value="<?php echo $result['IDNumber'] ?>">
                                    <input type="hidden" name="owner" value="<?php echo $result['freezer_owner'] ?>">
                                    <input type="hidden" name="vendor" value="<?php echo $result['nama_vendor'] ?>">
                                    <input type="hidden" name="tipefreezeradd" value="<?php echo $result['realisasi_tipe_freezer'] ?>">


                                    <div class="row">
                                        <div class="col-md-12">
                                            <!-- <small>Masukkan Keterangan</small>
                                        <br> -->
                                           <!--  <input type="text" id="ket-tindaklanjut" name="keterangan" class="form-control" required> -->
                                            <select name="keterangan" id="namavendor" class="select_database form-control" required>

                                                <option value="">Pilih Rejection Code</option>

                                                <?php 
                                                    $query_user = "SELECT * FROM focus_rejection_code";
                                                    $data_user = mysqli_query($koneksi,$query_user);
                                                    while ($result_rejection = mysqli_fetch_assoc($data_user)) {
                                                        
                                                 ?>
                                                    <option value="<?php echo $result_rejection['ket'] ?>"><?php echo $result_rejection['rejection_code'] ?> - <?php echo $result_rejection['ket'] ?></option>

                                                <?php } ?>

                                            </select>
                                        </div>
                                    </div>
                                    <br>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-sm btn-success">
                                                Update
                                            </button>
                                       
                                            <a href="javascript:void();" class="btn btn-sm btn-dark batal-tindaklanjut">Batal</a>
                                        </div>
                                     </div>
                                        
                                    </div>
                                </form>
                                
                                
                                     <a href="javascript:void();" class="btn btn-block btn-dark tindaklanjut" id="rejectbtn" data-toggle="tooltip" data-placement="top" title="Reject Placement Additonal" acuan="<?php echo $result['id_placement'] ?>"><i class="fas fa fa-warning"> </i></a> 
                                 
                                 <!-- approved -->
                                 <br>

                                <?php else: ?>

                                    <!-- Jika iya rent dan own bedakan inputnya -->

                                    <?php if ($result['freezer_owner'] == 'Rent' || $result['freezer_owner'] == 'RENT'): ?>
                                        
                                        
                                            <form action="model/freezerplacement/reject-freezervendor.php" method="post" class="form-tindaklanjut" id="form-tindaklanjut-<?php echo $result['id_placement'] ?>">
                                                <input type="hidden" id="id" name="id_placement" value="<?php echo $result['id_placement'] ?>">

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <!-- <small>Masukkan Keterangan</small>
                                                    <br> -->
                                                       <!--  <input type="text" id="ket-tindaklanjut" name="keterangan" class="form-control" required> -->
                                                        <select name="keterangan" id="namavendor" class="select_database form-control" required>

                                                            <option value="">Pilih Rejection Code</option>

                                                            <?php 
                                                                $query_user = "SELECT * FROM focus_rejection_code";
                                                                $data_user = mysqli_query($koneksi,$query_user);
                                                                while ($result_rejection = mysqli_fetch_assoc($data_user)) {
                                                                    
                                                             ?>
                                                                <option value="<?php echo $result_rejection['ket'] ?>"><?php echo $result_rejection['rejection_code'] ?> - <?php echo $result_rejection['ket'] ?></option>

                                                            <?php } ?>

                                                        </select>
                                                    </div>
                                                </div>
                                                <br>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <button type="submit" class="btn btn-sm btn-success">
                                                            Update
                                                        </button>
                                                   
                                                        <a href="javascript:void();" class="btn btn-sm btn-dark batal-tindaklanjut">Batal</a>
                                                    </div>
                                                 </div>
                                                    
                                                </div>
                                            </form>
                                            

                                            
                                                 <a href="javascript:void();" class="btn btn-block btn-danger tindaklanjut" id="rejectbtn" data-toggle="tooltip" data-placement="top" title="Reject Placement" acuan="<?php echo $result['id_placement'] ?>"><i class="fas fa fa-warning"> </i></a> 
                                             
                                             <!-- approved -->
                                             <br>


                                             
                                            <form action="model/freezerplacement/placefreezer.php" method="post" class="form-tindaklanjut" id="form-approved-<?php echo $result['id_placement']?>">
                                                <input type="hidden" id="id" name="id_distributor" value=" <?php echo $result['id_distributor'] ?>">

                                                <input type="hidden" id="emailtovendor" name="emailtovendor" value="<?php echo $result['approved_date'] ?>">

                                                <input type="hidden" name="placedby" value="<?php echo $placedby ?>">

                                                <input type="hidden" name="owner" value="<?php echo $result['freezer_owner']; ?>">

                                                <input type="hidden" name="idnumber" value="<?php echo $result['IDNumber'] ?>">

                                                <input type="hidden" name="vendor" value="<?php echo explode("-", $result['nama_vendor'])[0]; ?>">

                                                <input type="hidden" name="id_placement" value="<?php echo $result['id_placement'] ?>">

                                		<input type="hidden" name="principal" value="<?php echo $result['principal'] ?>">

                                                <!-- <div class="row">
                                                    <div class="col-md-12">
                                                        <small>IDNumber</small>
                                                    <br>
                                                        <input type="text" id="idnumber" name="idnumber" class="form-control" required>
                                                    </div>
                                                </div> -->
                                                <?php if($result['freezer_owner'] == 'Jbp') { ?>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <small>Nomor Mesin</small>
                                                    <br>
                                                        <input type="text" id="nomormesin" name="nomormesin" class="form-control nomormesinAsset" required readonly>
                                                    </div>
                                                </div>
                                                <?php }else{ ?>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <small>Nomor Mesin</small>
                                                    <br>
                                                        <input type="text" id="nomormesin" name="nomormesin" class="form-control" required>
                                                    </div>
                                                </div>
                                                <?php } ?>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <small>Batch PO</small>
                                                    <br>
                                    <select id="batchpo" name="batchpo" class="select_bfi form-control" required>
                                            <option value="">Select Batch PO</option>
                                            <?php
                                                include ('config/config.php');
                                                //cari data semua BFI
                                                $sql = "SELECT * FROM focus_batch_po ORDER BY batchpo";
                                                $dql = mysqli_query($koneksi,$sql);


                                                while ($rql = mysqli_fetch_assoc($dql)) {
                                            ?>
                                                <option value="<?php echo $rql['batchpo'] ?>"><?php echo strtoupper($rql['batchpo']." - ".$rql['keterangan'])?></option>

                                            <?php
                                                }


                                             ?>


                                        </select>

                                                        <!-- <input type="text" id="batchpo" name="batchpo" class="form-control" required> -->
                                                    </div>
                                                </div>

                                                <!-- <div class="row">
                                                    <div class="col-md-12">
                                                        <small>Nomor Mesin</small>
                                                    <br>
                                                        <input type="text" id="placement_notes" name="placement_notes" class="form-control" required>
                                                    </div>
                                                </div> -->

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <small>Nama Supir</small>
                                                    <br>
                                                        <input type="text" id="namasupir" name="namasupir" class="form-control" required>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <small>Tanggal Pengiriman</small>
                                                    <br>
                                                        <input type="date" id="tanggal_pengiriman" name="tanggal_pengiriman" class="form-control" required>
                                                    </div>
                                                </div>
                                                <?php if($result['freezer_owner'] == 'Jbp') { ?>
                                                  <div class="row">
                                                    <div class="col-md-12">
                                                        <small>Realisasi Tipe Freezer</small>
                                                        <br>

                                                        <select name="realisasi_tipe_freezer" id="realisasi_tipe_freezerAsset" class="select_database form-control realisasi_tipe_freezerAsset" required>


                                                        </select>
                                                    </div>
                                                </div>
                                                <?php }else{ ?>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <small>Realisasi Tipe Freezer</small>
                                                    <br>
                                                        <select name="realisasi_tipe_freezer" class="select_bfi form-control" required>
                                                            <option value="">Select Tipe Freezer</option>
                                                            <?php 
                                                                include ('config/config.php');
                                                                //cari data semua BFI
                                                                $sql = "SELECT * FROM focus_tipe_freezer ORDER BY id_tipe";
                                                                $dql = mysqli_query($koneksi,$sql);
                                                                

                                                                while ($rql = mysqli_fetch_assoc($dql)) {
                                                            ?>
                                                                <option value="<?php echo $rql['tipe'] ?>"><?php echo strtoupper($rql['tipe']." - ".$rql['keterangan'])?></option>

                                                            <?php 
                                                                }


                                                             ?>
                                                            

                                                        </select>
                                                        <!-- <input type="text" id="realisasi_tipe_freezer" name="realisasi_tipe_freezer" class="form-control" required> -->
                                                    </div>
                                                </div>
                                             <?php } ?>

                                                <?php if($result['freezer_owner'] == 'Jbp') { ?>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <small>Aset Freezer</small>
                                                            <br>
                                                           <select name="assetId" id="assetId" class="select_bfi form-control" onChange="javascript:get_brand()" required>
                                                                <option value="">Select Asset Freezer</option>
                                                                <?php 
                                                                include ('config/config.php');
                                                //cari data semua BFI
                                                                $sql = "SELECT * FROM focus_asset_freezer WHERE vendor = '$vendorSession' AND area = '$areaSession' AND assetStatusId = '1' AND assetId !='' ";
                                                                $dql = mysqli_query($koneksi,$sql);


                                                                while ($rql = mysqli_fetch_assoc($dql)) {
                                                                    ?>
                                                                    <option value="<?php echo $rql['assetId'] ?>"><?php echo strtoupper($rql['assetId'])?></option>

                                                                    <?php 
                                                                }


                                                                ?>


                                                            </select>               
                                                            <!-- <input type="text" id="realisasi_tipe_freezer" name="realisasi_tipe_freezer" class="form-control" required> -->
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <?php if($result['freezer_owner'] == 'Jbp') { ?>
                                                   <div class="row">
                                                    <div class="col-md-12">
                                                        <small>Brand</small>
                                                        <br>

                                                        <select name="brand" id="brandAsset" class="select_database form-control brandAsset" required>


                                                        </select>
                                                    </div>
                                                </div>
                                            <?php }else{ ?>
                                                
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <small>Brand</small>
                                                        <br>
                                                        <select id="brand" name="brand" class="select_bfi form-control" required>
                                                            <option disabled selected>Select Brand</option>
                                                            <?php
                                                            include ('config/config.php');
                                                //cari data semua BFI
                                                            $sql = "SELECT * FROM focus_brand ORDER BY name";
                                                            $dql = mysqli_query($koneksi,$sql);


                                                            while ($rql = mysqli_fetch_assoc($dql)) {
                                                                ?>
                                                                <option value="<?php echo $rql['name'] ?>"><?php echo strtoupper($rql['name']." - ".$rql['ket'])?></option>

                                                                <?php
                                                            }


                                                            ?>


                                                        </select>
                                                    </div>
                                                </div>
                                                <?php } ?>

                                               

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <small>Vendor Longitude</small>
                                                    <br>
                                                        <input type="text" id="v_longitude" name="v_longitude" class="form-control" required>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <small>Vendor Latitude</small>
                                                    <br>
                                                        <input type="text" id="v_latitude" name="v_latitude" class="form-control" required>
                                                    </div>
                                                </div>

                                                
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <button type="submit" class="btn btn-sm btn-success">
                                                            Update
                                                        </button>
                                                   
                                                        <a href="javascript:void();" class="btn btn-sm btn-dark batal-tindaklanjut">Batal</a>
                                                    </div>
                                                 </div>
                                                    
                                                </div>
                                            </form>
                                            

                                            

                                            <a href="javascript:void();" id="approved_placement_<?php echo $result['id_placement']?>" class="btn btn-block btn-primary approved_placement" data-toggle="tooltip" data-placement="top" title="Place Freezer" acuan="<?php echo $result['id_placement']?>">
                                                <i class="fas fa fa-hand-point-down"></i>
                                            </a>
                                        <?php else: ?>
                                            <form action="model/freezerplacement/reject-freezervendor.php" method="post" class="form-tindaklanjut" id="form-tindaklanjut-<?php echo $result['id_placement'] ?>">
                                                <input type="hidden" id="id" name="id_placement" value="<?php echo $result['id_placement'] ?>">

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <!-- <small>Masukkan Keterangan</small>
                                                    <br> -->
                                                        <!-- <input type="text" id="ket-tindaklanjut" name="keterangan" class="form-control" required> -->
                                                        <select name="keterangan" id="namavendor" class="select_database form-control" required>
                                                            <option value="">Pilih Rejection Code</option>

                                                            <?php 
                                                                $query_user = "SELECT * FROM focus_rejection_code";
                                                                $data_user = mysqli_query($koneksi,$query_user);
                                                                while ($result_rejection = mysqli_fetch_assoc($data_user)) {
                                                                    
                                                             ?>
                                                                <option value="<?php echo $result_rejection['ket'] ?>"><?php echo $result_rejection['rejection_code'] ?> - <?php echo $result_rejection['ket'] ?></option>

                                                            <?php } ?>

                                                        </select>
                                                    </div>
                                                </div>
                                                <br>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <button type="submit" class="btn btn-sm btn-success">
                                                            Update
                                                        </button>
                                                   
                                                        <a href="javascript:void();" class="btn btn-sm btn-dark batal-tindaklanjut">Batal</a>
                                                    </div>
                                                 </div>
                                                    
                                                </div>
                                            </form>
                                            

                                            
                                                 <a href="javascript:void();" class="btn btn-block btn-danger tindaklanjut" id="rejectbtn" data-toggle="tooltip" data-placement="top" title="Reject Placement" acuan="<?php echo $result['id_placement'] ?>"><i class="fas fa fa-warning"> </i></a> 
                                             
                                             <!-- approved -->
                                             <br>


                                             
                                            <form action="model/freezerplacement/placefreezer.php" method="post" class="form-tindaklanjut" id="form-approved-<?php echo $result['id_placement']?>">
                                                <input type="hidden" id="id" name="id_distributor" value=" <?php echo $result['id_distributor'] ?>">

                                                <input type="hidden" id="emailtovendor" name="emailtovendor" value="<?php echo $result['approved_date'] ?>">

                                                <input type="hidden" name="placedby" value="<?php echo $placedby ?>">
                                                <input type="hidden" name="owner" value="<?php echo $result['freezer_owner']; ?>">
                                                <input type="hidden" name="idnumber" value="<?php echo $result['IDNumber'] ?>">
                                                <input type="hidden" name="vendor" value="<?php echo explode("-", $result['nama_vendor'])[0]; ?>">
                                                <input type="hidden" name="id_placement" value="<?php echo $result['id_placement'] ?>">

                                                
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <small>Batch PO</small>
                                                    <br>
                                    <select id="batchpo" name="batchpo" class="select_bfi form-control" required>
                                            <option value="">Select Batch PO</option>
                                            <?php
                                                include ('config/config.php');
                                                //cari data semua BFI
                                                $sql = "SELECT * FROM focus_batch_po ORDER BY batchpo";
                                                $dql = mysqli_query($koneksi,$sql);


                                                while ($rql = mysqli_fetch_assoc($dql)) {
                                            ?>
                                                <option value="<?php echo $rql['batchpo'] ?>"><?php echo strtoupper($rql['batchpo']." - ".$rql['keterangan'])?></option>

                                            <?php
                                                }


                                             ?>


                                        </select>

                                                        <!-- <input type="text" id="batchpo" name="batchpo" class="form-control" required> -->
                                                    </div>
                                                </div>

                                                <!-- <div class="row">
                                                    <div class="col-md-12">
                                                        <small>Nomor Mesin</small>
                                                    <br>
                                                        <input type="text" id="placement_notes" name="placement_notes" class="form-control" required>
                                                    </div>
                                                </div> -->
                                                <?php if($result['freezer_owner'] == 'Jbp') { ?>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <small>Nomor Mesin</small>
                                                    <br>
                                                         <select name="nomormesin" id="nomormesinAsset" class="select_database form-control nomormesinAsset" required>


                                                        </select>
                                                    </div>
                                                </div>
                                                <?php }else{ ?>
                                                    <div class="row">
                                                    <div class="col-md-12">
                                                        <small>Nomor Mesin</small>
                                                    <br>
                                                        <input type="text" id="nomormesin" name="nomormesin" class="form-control" value="<?php echo $result['placement_notes'] ?>">
                                                    </div>
                                                </div>
                                                <?php } ?>
                                                

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <small>Nama Supir</small>
                                                    <br>
                                                        <input type="text" id="namasupir" name="namasupir" class="form-control" required>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <small>Tanggal Pengiriman</small>
                                                    <br>
                                                        <input type="date" id="tanggal_pengiriman" name="tanggal_pengiriman" class="form-control" required>
                                                    </div>
                                                </div>

                                                <?php if($result['freezer_owner'] == 'Jbp') { ?>
                                                  <div class="row">
                                                    <div class="col-md-12">
                                                        <small>Realisasi Tipe Freezer</small>
                                                        <br>

                                                        <select name="realisasi_tipe_freezer" id="realisasi_tipe_freezerAsset" class="select_database form-control realisasi_tipe_freezerAsset" required>


                                                        </select>
                                                    </div>
                                                </div>
                                            <?php }else{ ?>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <small>Realisasi Tipe Freezer</small>
                                                    <br>
                                                    <select name="realisasi_tipe_freezer" class="select_bfi form-control" required>
                                                            <option value="">Select Tipe Freezer</option>
                                                            <?php 
                                                                include ('config/config.php');
                                                                //cari data semua BFI
                                                                $sql = "SELECT * FROM focus_tipe_freezer ORDER BY id_tipe";
                                                                $dql = mysqli_query($koneksi,$sql);
                                                                

                                                                while ($rql = mysqli_fetch_assoc($dql)) {
                                                            ?>
                                                                <option value="<?php echo $rql['tipe'] ?>"><?php echo strtoupper($rql['tipe']." - ".$rql['keterangan'])?></option>

                                                            <?php 
                                                                }


                                                             ?>
                                                            

                                                        </select>
                                                        <!-- <input type="text" id="realisasi_tipe_freezer" name="realisasi_tipe_freezer" class="form-control" required> -->
                                                    </div>
                                                </div>
                                                <?php } ?>
                                                <?php if($result['freezer_owner'] == 'Jbp') { ?>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <small>Aset Freezer</small>
                                                            <br>
                                                            <select name="assetId" id="assetId" class="select_bfi form-control" onChange="javascript:get_brand()" required>
                                                                <option value="">Select Asset Freezer</option>
                                                                <?php 
                                                                include ('config/config.php');
                                                //cari data semua BFI
                                                                $sql = "SELECT * FROM focus_asset_freezer WHERE vendor = '$vendorSession' AND area = '$areaSession' AND assetStatusId = '1' AND assetId !='' ";
                                                                $dql = mysqli_query($koneksi,$sql);


                                                                while ($rql = mysqli_fetch_assoc($dql)) {
                                                                    ?>
                                                                    <option value="<?php echo $rql['assetId'] ?>"><?php echo strtoupper($rql['assetId'])?></option>

                                                                    <?php 
                                                                }


                                                                ?>


                                                            </select>               
                                                            <!-- <input type="text" id="realisasi_tipe_freezer" name="realisasi_tipe_freezer" class="form-control" required> -->
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                
                                                
                                                <?php if($result['freezer_owner'] == 'Jbp') { ?>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <small>Brand</small>
                                                        <br>

                                                        <select name="brand" id="brandAsset" class="select_database form-control brandAsset" required>


                                                        </select>
                                                    </div>
                                                </div>
                                                <?php }else{ ?>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <small>Brand</small>
                                                    <br>
                                                        <input type="text" name="brand" class="form-control" required>

                                                    </div>
                                                </div>
                                                <?php } ?>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <small>Vendor Longitude</small>
                                                    <br>
                                                        <input type="text" id="v_longitude" name="v_longitude" class="form-control" required>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <small>Vendor Latitude</small>
                                                    <br>
                                                        <input type="text" id="v_latitude" name="v_latitude" class="form-control" required>
                                                    </div>
                                                </div>

                                                
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <button type="submit" class="btn btn-sm btn-success">
                                                            Update
                                                        </button>
                                                   
                                                        <a href="javascript:void();" class="btn btn-sm btn-dark batal-tindaklanjut">Batal</a>
                                                    </div>
                                                 </div>
                                                    
                                                </div>
                                            </form>
                                            

                                            

                                            <a href="javascript:void();" id="approved_placement_<?php echo $result['id_placement']?>" class="btn btn-block btn-primary approved_placement" data-toggle="tooltip" data-placement="top" title="Place Freezer" acuan="<?php echo $result['id_placement']?>">
                                                <i class="fas fa fa-hand-point-down"></i>
                                            </a>

                                    <?php endif ?>
                                       
                            <?php endif ?>

                            <!-- <div class="table-data-feature"> -->
                             
                             
                        </td>
                        <td>
                            <?php echo $result['approved_date']; ?>
                        </td>
                        <td>

                            <?php /* if ($result['plan_placement_date'] == '' or $result['plan_placement_date'] == '0000-00-00'): */ ?>
                                <form action="model/freezerplacement/ppd.php" method="post">
                                <input type="hidden" id="id" name="id_placement" value=" <?php echo $result['id_placement'] ?>">

                                <input type="hidden" name="bficode" value="<?php echo $result['custid'] ?>">

                                <div class="row">
                                    <div class="col-md-12">
                                        
                                        <input type="date" name="ppd" value="<?php echo $result['plan_placement_date'] ?>" class="form-control" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-sm btn-success btn-block">
                                            Update
                                        </button>
                                   
                                        <!-- <a href="#" class="btn btn-sm btn-dark batal-tindaklanjut">Batal</a> -->
                                    </div>
                                 </div>
                                    
                                </div>
                            </form>

                            <?php /* else: */ ?>
                                <?php /* echo $result['plan_placement_date'] */ ?>
                            <?php /* endif */ ?>
                            

                            <!-- <a href="#" id="approved_placement_<?php echo $result['iddistributor']?>" class="btn btn-sm btn-info approved_placement" data-toggle="tooltip" data-placement="top" title="Plan Placement Date" acuan="<?php echo $result['id_distributor']?>">
                                <i class="fas fa fa-calendar"></i>
                            </a> -->
                        </td>
                        <td><?php

                         if ($result['plan_placement_date'] == '' or $result['plan_placement_date'] == '0000-00-00')
                         {
                            echo "";
                         }
                         else
                         {
                            echo $result['plan_placement_date'];
                         }

                         ?>
                             
                         </td>
                        <td><?php echo $no ?></td>
                        <td><?php echo $result['IDNumber'] ?></td>
                        <td>
                            <?php 
                                if ($tipe == 'Direct') {
                                    echo "PRIMARY";
                                }else{
                                    echo "SECONDARY";
                                }
                             ?>
                        </td>
                        <td>
                            <?php 
                                // echo $result['custid'] 
                                echo $custid;
                            ?>
                        </td>
                        <td>
                            <?php 
                                // echo $result['custname'] 
                                echo $custname;
                            ?>
                        </td>
                        <td>
                            <?php 
                                // echo $result['distname']
                                echo $distname; 
                            ?>
                        </td>
                        <td>
                            <?php 
                                // echo $result['inputdatetime'] 
                                echo $tgl_survey;
                                ?>
                        </td>
                        <td>
                            <?php echo $result['freezer_owner'] ?>
                        </td>
                        <td>
                            <?php echo $result['nama_vendor'] ?>
                        </td>
                        <td>
                            <?php 
                                // if ($result['coming_from'] == 'ADDITIONAL') {
                                //     echo $result['realisasi_tipe_freezer'];
                                // }else{
                                //     echo $result3['pilihanfreezeroutlet'];
                                // }
                                echo $tipe_freezer;
                             ?>
                        </td>
                        <td>
                            <?php echo $result['approved_date'] ?>
                        </td>
                        <td>
                            <?php 

                                if ($result['placement_date'] == '0000-00-00') {
                                    echo 'Tanggal belum tersedia';
                                }else{
                                    echo $result['placement_date']; 
                                } 
                                ?>
                        </td>
                        <td>
                            <?php
                                echo $result['lead_time'].' Hari';
                            ?>
                        </td>
                        <td>
                            <?php 
                                $status = $result['approved_status'];
                                $keterangan = $result['keterangan'];
                                $tanggal_penempatan = $result['placement_date'];

                                if ($status == 'APPROVED' AND $tanggal_penempatan !== '0000-00-00') {
                                    echo '<span class="role marketing">SUCCESS</span>';
                                }elseif ($status == 'APPROVED' AND $keterangan == 'VALIDATED'){
                                    echo '<span class="role marketing">VALIDATED BY VENDOR</span>';
                                }elseif ($status == 'APPROVED' AND $keterangan == '') {
                                     echo '<span class="role admin">WAITING FOR VENDOR VALIDATION</span>';
                                }else{
                                    echo '<span class="role admin">PROCESSING BY VENDOR</span>';
                                }
                             ?>
                        </td>
                        <td>
                            <?php echo $result['approved_date'] ?>
                        </td>
                        <td>
                            <?php echo $result['freezercode'] ?>
                        </td>
                        <td>
                            <?php echo $result['batch_po'] ?>
                        </td>
                        <td>
                            <?php echo $result['placement_notes'] ?>
                        </td>
                        <td>
                            <?php echo $result['nama_supir'] ?>
                        </td>
                        <td>
                            <?php 

                               if ($result['tanggal_pengiriman'] == '0000-00-00') {
                                    echo "Tanggal belum tersedia";
                               }else{
                                    echo $result['tanggal_pengiriman']; 
                               }
                            ?>
                        </td>
                        <td>
                            <?php 
                                echo $result['realisasi_tipe_freezer'] 
                           
                            ?>
                        </td>
                        <td>
                            <?php echo $pic ?>
                        </td>
                        <td>
                            <?php echo $phone ?>
                        </td>
                        <td>

                            <?php 
                            
                                if ($photo3 == '') {
                                    # code...
                                }else{
                                    echo '<a href="'.$photo.'">View</a>';
                                     // echo '<a href="'.$photo.'"><img src="'.$photo.'" width="50px" height="50px">'.$photo.'</a>';
                                }
                           

                            ?>
                            
                        </td>
                        <td>
                            <?php 
                                if ($photo == '') {
                                    # code...
                                }else{
                                    echo '<a href="'.$photo.'">View</a>';
                                     // echo '<a href="'.$photo.'"><img src="'.$photo.'" width="50px" height="50px">'.$photo.'</a>';
                                }

                            ?>
                        </td>
                        <td>
                            <?php echo $result3['kondisifisik'] ?>
                        </td>
                        <td>
                            <?php echo $longitude ?>
                        </td>
                        <td>
                            <?php echo $latitude ?>
                        </td>
                        <td>
                            <?php echo $result['vendor_longitude'] ?>
                        </td>
                        <td>
                            <?php echo $result['vendor_latitude'] ?>
                        </td>
                        <td>
                            <?php echo $address ?>
                        </td>
                        <td>
                            <?php echo $province ?>
                        </td>
                        <td>
                            <?php echo $city ?>
                        </td>
                        <td>
                            <?php echo $kecamatan ?>
                        </td>
                        <td>
                            <?php echo $kelurahan ?>
                        </td>
                        <td>
                            <?php echo $kodepos ?>
                        </td>
                        <td>
                            <?php echo $result3['timeopen'] ?>
                        </td>
                        <td>
                            <?php echo $result3['timeclosed'] ?>
                        </td>
                        <td>
                            <?php echo $result['placed_by'] ?>
                        </td>
                        
                    </tr>

                     <?php $no++; } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

        <!-- END DATA TABLE -->

        <br>

<br>
<h4>Placement</h4>
<hr>
<form action="" method="POST">
<?php
	if (isset($_POST['customPlaceDate'])) {
		$tanggalMulai = mysqli_escape_string($koneksi,$_POST['tanggal_mulai']);
		$tanggalSelesai = mysqli_escape_string($koneksi,$_POST['tanggal_akhir']);
		$tanggalMulaiEdit = date('Y-m-d', strtotime($tanggalMulai));
		$tanggalSelesaiEdit = date('Y-m-d', strtotime($tanggalSelesai));
		//$tanggalMulai= date('Y-m-d', $tanggalMulaiEdit);
		//$tanggalSelesai= date('Y-m-d', $tanggalSelesaiEdit);
		
		$dateclause = " and placement_date >='$tanggalMulaiEdit' and placement_date <= '$tanggalSelesaiEdit' ";
	} else {
		$dateclause = "";
	}
	
?>
<div class="row form-group">
	<div class="col-sm-3 col-md-2">
		<label for="text-input" class=" form-control-label"><b>Placed Sys Date</b></label>
	</div>
	<div class="col-sm-3 col-md-2">
		<input type="text" id="tanggal_mulai" name="tanggal_mulai" class="form-control start_date" placeholder="Awal" required>
	</div>
	<div class="col-sm-3 col-md-2">
		<input type="text" id="tanggal_akhir" name="tanggal_akhir" class="form-control end_date" placeholder="Akhir" required>
		
	</div>
	<button type="submit" name="customPlaceDate" value="custom" class="btn btn-sm btn-primary">
		Search
	</button>
</div>

</form>
<hr>
<div class="row">
    <div class="col-md-12">
        <div class="table-responsive table-data" style="height: auto">
            <table id="tableok" class="mdl-data-table" style="width:100%">
                <thead>
                    <tr>
                        <td>No</td>
                        <td>Assign Date</td>
                        <td>BFI Code</td>
                        <td>Nama Outlet</td>
                        <td>Nama Distributor</td>
                        <td>Tanggal Survey</td>
                        <td>Freezer Owner</td>
                        <td>Nama Vendor</td>
                        <td>Tipe Freezer</td>
                        <td>Email to Vendor Date</td>
                        <td>Placement Date</td>
                        <td>Lead Time</td>
                        <td>Status</td>
                        <td>Tanggal Validasi</td>
                        <td>Plan Placement Date</td>
                        <td>Freezer Code</td>
                        <td>Sequence Number</td>
                        <td>BatchPO</td>
                        <td>Nomor Mesin</td>
                        <td>Nama Supir</td>
                        <td>Tanggal Pengiriman</td>
                        <td>Realisasi Tipe Freezer</td>
                        <td>Nama PIC</td>
                        <td>No Telp PIC</td>
                        <td>Foto - Deal Form + KTP</td>
                        <td>Foto - Outlet Tampak Depan</td>
                        <td>Kondisi Fisik Outlet</td>
                        <!-- <td>Patokan</td> -->
                        <td>Longitude</td>
                        <td>Latitude</td>
                        <td>Vendor Longitude</td>
                        <td>Vendor Latitude</td>
                        <td>Alamat</td>
                        <td>Provinsi</td>
                        <td>Kabupaten / Kota</td>
                        <td>Kecamatan</td>
                        <td>Kelurahan</td>
                        <td>Kode Pos</td>
                        <td>Jam Buka</td>
                        <td>Jam Tutup</td>
                        <td>Updated By</td>
                        <!-- <td>Action</td> -->
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $no = 1;
                        
						if ($area == "") {
							$query = "SELECT * FROM focus_freezer_placement WHERE approved_status = 'APPROVED' and placement_date != '0000-00-00' ".$dateclause." AND nama_vendor LIKE '$acc%' order by IDNumber desc limit 0, 500";
						} else {
							$query = "SELECT * FROM focus_freezer_placement WHERE approved_status = 'APPROVED' and placement_date != '0000-00-00' ".$dateclause." AND nama_vendor like '$acc%' order by IDNumber desc limit 0, 300";
						}
                        $data = mysqli_query($koneksi,$query);

                        while ($result = mysqli_fetch_assoc($data)) {
                     ?>
                    <tr>

                        <!-- cari datangnya dari mana -->
                        <?php 
                                if ($result['id_distributor'] == 0) {
                                    //maka itu artinya bukan dari survey NOO
                                    $id_freezer = $result['id_freezer'];

                                    if ($result['cust_type'] == "Indirect") {
					//karna dia secondary, cari lagi datanya dari tabel secondary mds
                                        $cp2 = "SELECT * FROM MDS_CustomerSecondary WHERE Code = '$id_freezer'";
                                        $dcp2 = mysqli_query($koneksi, $cp2);
                                        $results2 = mysqli_fetch_assoc($dcp2);

                                        $custid = $results2['Code'];
                                        $custname = $results2['Name'];
                                        $distname = $results2['DistName'];

                                        $tipe = "Indirect";
                                        $pic = $results2['PIC'];
                                        $phone = $results2['Phone'];
                                        $latitude = $results2['Latitude'];
                                        $longitude = $results2['Longitude'];
                                        $address = $results2['Alamat'];
                                        $city = $results2['KabKota'];
                                        $province = $results2['Propinsi'];
                                        $kecamatan = $results2['Kecamatan'];
                                        $kelurahan = $results2['Kelurahan'];
                                        $kodepos = $results2['KodePos'];
                                        $tipe_freezer = $result['realisasi_tipe_freezer'];
                                    } else { // primary customer
                                        $cp = "SELECT * FROM MDS_CustomerPrimary WHERE Code = '$id_freezer'";
                                        $dcp = mysqli_query($koneksi, $cp);
                                        $result2 = mysqli_fetch_assoc($dcp);

                                        $tgl_survey = "Tidak tersedia";
                                        $tipe = $result2['MarketId_Name'];
                                        $photo = '';
                                        $photo3 = '';

                                        $custid = $result2['Code'];
                                        $custname = $result2['Name'];
                                        $distname = $result2['System_Distributor_Name'];
                                        $tipe = "Direct";
                                        $pic = $result2['PIC'];
                                        $phone = $result2['Phone'];
                                        $latitude = $result2['Latitude'];
                                        $longitude = $result2['Longitude'];
                                        $address = $result2['Street'];
                                        $city = $result2['City'];
                                        $province = $result2['SalesDistrictName'];
                                        $kecamatan = $result2['SPVillageName'];
                                        $kelurahan = '';
                                        $kodepos = $result2['ZipCode'];
                                        $tipe_freezer = $result['realisasi_tipe_freezer'];
                                    }
                                }else{

                                    $id_distributor = $result['id_distributor'];
                                    $cs = "SELECT * FROM distributor WHERE iddistributor = '$id_distributor'";
                                    $dcs = mysqli_query($koneksi,$cs);
                                    $result3 = mysqli_fetch_assoc($dcs); 

                                    $custid = $result3['custid'];
                                    $custname = $result3['custname'];
                                    $distname = $result3['distname'];
                                    $tgl_survey = $result3['inputdatetime'];
                                    $tipe = 'Indirect';
                                    $photo3 = "https://appict.appsbelfoods.com/".$result3['photo3']."";
                                    $photo = "https://appict.appsbelfoods.com/".$result3['photo'].""; 
                                    $pic = $result3['contactname'];
                                    $phone = $result3['telephone'];
                                    $latitude = $result3['latitude'];
                                    $longitude = $result3['longitude'];
                                    // $tipe = "SECONDARY";
                                    $address = $result3['address'];
                                    $city = $result3['city'];
                                    $province = $result3['province'];
                                    $kecamatan = $result3['kecamatan'];
                                    $kelurahan = $result3['kelurahan'];
                                    $kodepos = $result3['kodepos'];
                                    $tipe_freezer = $result3['pilihanfreezeroutlet'];
                                }
                             ?>

                
                        <td><?php echo $no ?></td>
                        <td><?php echo $result['approved_date'] ?></td>
                        <td>
                            <?php echo $custid ?>
                        </td>
                        <td><?php echo $custname ?></td>
                        <td>
                            <?php echo $distname ?>
                        </td>
                        <td>
                            <?php echo $tgl_survey ?>
                        </td>
                        <td>
                            <?php echo $result['freezer_owner'] ?>
                        </td>
                        <td>
                            <?php echo $result['nama_vendor'] ?>
                        </td>
                        <td>
                            <?php 
                                // if ($result['coming_from'] == 'ADDITIONAL') {
                                //     echo $result['realisasi_tipe_freezer'];
                                // }else{
                                //     echo $result['pilihanfreezeroutlet'];
                                // }

                                echo $tipe_freezer;
                             ?>
                        </td>
                        <td>
                            <?php echo $result['approved_date'] ?>
                        </td>
                        <td>
                            <?php 

                                if ($result['placement_date'] == '0000-00-00') {
                                    echo 'Tanggal belum tersedia';
                                }else{
                                    echo $result['placement_date'];
                                } 
                                ?>
                        </td>
                        <td>
                            <?php
                                echo $result['lead_time'].' Hari';
                            ?>
                        </td>
                        <td>
                            <?php 
                                $status = $result['approved_status'];
                                $tanggal_penempatan = $result['placement_date'];

                                if ($status == 'APPROVED' AND $tanggal_penempatan !== '0000-00-00') {
                                    echo '<span class="role marketing">SUCCESS</span>';
                                }elseif ($status == 'APPROVED' AND $keterangan == 'VALIDATED'){
                                    echo '<span class="role marketing">VALIDATED BY VENDOR</span>';
                                }elseif ($status == 'APPROVED' AND $keterangan == '') {
                                     echo '<span class="role admin">WAITING FOR VENDOR VALIDATION</span>';
                                }else{
                                    echo '<span class="role admin">PROCESSING BY VENDOR</span>';
                                }
                             ?>
                        </td>
                        <td>
                            <?php echo $result['approved_date'] ?>
                        </td>
                        <td>
                            <?php echo $result['plan_placement_date'] ?>
                        </td>
                        <td>
                            <?php echo $result['freezercode'] ?>
                        </td>
                        <td>
                            <?php echo $result['IDNumber'] ?>
                        </td>
                        <td>
                            <?php echo $result['batch_po'] ?>
                        </td>
                        <td>
                            <?php echo $result['placement_notes'] ?>
                        </td>
                        <td>
                            <?php echo $result['nama_supir'] ?>
                        </td>
                        <td>
                            <?php 

                               if ($result['tanggal_pengiriman'] == '0000-00-00') {
                                    echo "Tanggal belum tersedia";
                               }else{
                                    echo $result['tanggal_pengiriman']; 
                               }
                            ?>
                        </td>
                        <td>
                            <?php echo $result['realisasi_tipe_freezer'] ?>
                        </td>
                        <td>
                            <?php echo $pic ?>
                        </td>
                        <td>
                            <?php echo $phone ?>
                        </td>
                        <td>

                            <?php 
                            
                                if ($photo3 == '') {
                                    # code...
                                }else{
                                    echo '<a href="'.$photo.'">View</a>';
                                     // echo '<a href="'.$photo.'"><img src="'.$photo.'" width="50px" height="50px">'.$photo.'</a>';
                                }
                           

                            ?>
                            
                        </td>
                        <td>
                            <?php 
                                if ($photo == '') {
                                    # code...
                                }else{
                                    echo '<a href="'.$photo.'">View</a>';
                                     // echo '<a href="'.$photo.'"><img src="'.$photo.'" width="50px" height="50px">'.$photo.'</a>';
                                }

                            ?>
                        </td>
                        <td>
                            <?php echo $result3['kondisifisik'] ?>
                        </td>
                        <td>
                            <?php echo $longitude ?>
                        </td>
                        <td>
                            <?php echo $latitude ?>
                        </td>
                        <td>
                            <?php echo $result['vendor_longitude'] ?>
                        </td>
                        <td>
                            <?php echo $result['vendor_latitude'] ?>
                        </td>
                        <td>
                            <?php echo $address ?>
                        </td>
                        <td>
                            <?php echo $province ?>
                        </td>
                        <td>
                            <?php echo $city ?>
                        </td>
                        <td>
                            <?php echo $kecamatan ?>
                        </td>
                        <td>
                            <?php echo $kelurahan ?>
                        </td>
                        <td>
                            <?php echo $kodepos ?>
                        </td>
                        <td>
                            <?php echo $result3['timeopen'] ?>
                        </td>
                        <td>
                            <?php echo $result3['timeclosed'] ?>
                        </td>
                        <td>
                            <?php echo $result['placed_by'] ?>
                        </td>
                    </tr>

                     <?php $no++; } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


</div>

     <?php else: ?>
        <div class="row">
<div class="col-md-12">
        
        
        <h4>
            View Freezer Placement
            <br>
            <br>
            <?php 
                        include ('config/config.php');
                        $q_n = "SELECT count(*) as notif FROM focus_freezer_placement WHERE approved_status = 'REJECT BY VENDOR'";
                        $d_n = mysqli_query($koneksi,$q_n);
                        $r = mysqli_fetch_assoc($d_n);
                        $notif = $r['notif'];

                        $q_n2 = "SELECT count(*) as notif FROM focus_freezer_placement WHERE approved_status = 'REGISTERED' AND id_distributor !=0 AND id_freezer = ''";

                        $d_n2 = mysqli_query($koneksi,$q_n2);
                        $r2 = mysqli_fetch_assoc($d_n2);
                        $notif2 = $r2['notif'];
                
                    echo ' <a href="index.php?mod=freezerplacement&class=view" class="btn btn-primary">NEW <span class="badge badge-danger">'.$notif2.'</span></a>
                        <a href="index.php?mod=datareview&class=reject_vendor" class="btn btn-dark">REJECT (VENDOR) <span class="badge badge-danger">'.$notif.'</span></a>';
                
             ?>
        </h4>
        <br>
        <h4>New Placement Request</h4>
        <hr>
        <div class="table-responsive table-data" style="height: auto">
            <table id="tableok" class="mdl-data-table" style="width:100%">
                <thead>
                    <tr>
                        <td>Action</td>
                        <td>No</td>
                        <td>Sequence Number</td>
                        <td>Assign Date</td>
                        <td>Nama Outlet</td>
                        <td>BFI Code</td>
                        <td>Nama Distributor</td>
                        <td>Area Distributor</td>
                        <td>Tgl Survey</td>
                        <td>Nama Surveyor</td>
                        <td>Status Kunjungan</td>
                        <td>Channel</td>
                        <td>Sub-Channel</td>
                        <td>Segment</td>
                        <td>Nama PIC</td>
                        <td>No KTP</td>
                        <td>No Telp PIC</td>
                        <td>Longitude</td>
                        <td>Latitude</td>
                        <td>Alamat</td>
                        <td>Provinsi</td>
                        <td>Kabupaten/Kota</td>
                        <td>Kecamatan</td>
                        <td>Keluarahan</td>
                        <td>Kode Pos</td>
                        <td>Jam Buka (WIB)</td>
                        <td>Jam Tutup (WIB)</td>
                        <td>Luas Bangunan</td>
                        <td>Paket Awal</td>
                        <td>Foto - Outlet Tampak Depan</td>
                        <td>Foto - Daerah Depan Sekitar Toko</td>
                        <td>Foto - Deal Form + KTP</td>
                        <td>Lokasi Sekitar</td>
                        <td>Patokan</td>
                        <td>Lokasi (1)</td>
                        <td>Lokasi (2)</td>
                        <td>Freezer Es Krim</td>
                        <td>Merk Freezer Es Krim</td>
                        <td>Freezer Makanan Beku</td>
                        <td>Merk Freezer Makanan Beku</td>
                        <td>Produk Utama</td>
                        <td>Jumlah Pengunjung</td>
                        <td>Pendapatan/Hari (IDR)</td>
                        <td>Tipe Pengunjung</td>
                        <td>Kapasitas Listrik</td>
                        <td>Kondisi Fisik Outlet</td>
                        <td>Resiko Freezer</td>
                        <td>Spacae 1 x 1.5 m</td>
                        <td>Tipe Freezer</td>
                        <td>Area Penempatan Freezer</td>
                        <td>Visibilitas Freezer di Jalur Utama</td>
                        <td>Layanan Tambahan</td>
                        <td>Target/Bulan (kg)</td>
                        <td>Note Issue</td>
                        <td>Alasan Isuse</td>
                        <td>Issue/Follow Up Action</td>
                        <td>Alasan Tidak Sesuai</td>
                        <td>Follow Update</td>
                        <td>Status</td>
                        <td>Request Date</td>
                        <td>Keterangan</td>
                        
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $no = 1;
                        // include ('config/config.php');
                        $query = "SELECT * FROM distributor, focus_freezer_placement WHERE `focus_freezer_placement`.`id_distributor` = `distributor`.`iddistributor` and `focus_freezer_placement`.`approved_status` = 'REGISTERED' GROUP BY id_distributor,rtm_request_date  LIMIT 0,100";
                        $data = mysqli_query($koneksi,$query);

                        while ($result = mysqli_fetch_assoc($data)) {
                     ?>
                    <tr>
                        <td>
                            <!-- <div class="table-data-feature"> -->
                                
                            <form action="model/freezerplacement/reject-freezerteam.php" method="post" class="form-tindaklanjut" id="form-tindaklanjut-<?php echo $result['iddistributor'] ?>">
                                <input type="hidden" id="id" name="id_placement" value="<?php echo $result['id_placement'] ?>">

                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- <small>Masukkan Keterangan</small>
                                    <br>
                                        <input type="text" id="ket-tindaklanjut" name="keterangan" class="form-control" required> -->
                                        <select name="keterangan" id="namavendor" class="select_database form-control" required>
                                        <option value="">Pilih Rejection Code</option>

                                        <?php 
                                            $query_user = "SELECT * FROM focus_rejection_code";
                                            $data_user = mysqli_query($koneksi,$query_user);
                                            while ($result_rejection = mysqli_fetch_assoc($data_user)) {
                                                
                                         ?>
                                            <option value="<?php echo $result_rejection['ket'] ?>"><?php echo $result_rejection['rejection_code'] ?> - <?php echo $result_rejection['ket'] ?></option>

                                        <?php } ?>

                                    </select>
                                    </div>

                                    
                                </div>
                                <br>

                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-sm btn-success">
                                            Update
                                        </button>
                                   
                                        <a href="javascript:void();" class="btn btn-sm btn-dark batal-tindaklanjut">Batal</a>
                                    </div>
                                 </div>
                                    
                                </div>
                            </form>
                            

                            
                                 <a href="javascript:void();" class="btn btn-block btn-danger tindaklanjut" id="rejectbtn" data-toggle="tooltip" data-placement="top" title="Reject Placement" acuan="<?php echo $result['iddistributor'] ?>"><i class="fas fa fa-warning"> </i></a> 
                             
                             <!-- approved -->
                             <br>
                             
                            <form action="model/freezerplacement/approve-freezerteam.php" method="post" class="form-tindaklanjut" id="form-approved-<?php echo $result['iddistributor']?>">
                                <input type="hidden" id="id" name="id_placement" value=" <?php echo $result['id_placement'] ?>">

                                <div class="row">
                                    <div class="col-md-12">
                                        <small>Principal</small>
                                    <br>
                                    <select id="principal" name="principal" class="select_bfi form-control" required>
                                            <option disabled selected>Select Principal</option>
                                            <?php
                                                include ('config/config.php');
                                                //cari data semua BFI
                                                $sql = "SELECT * FROM focus_principal ORDER BY id_principal";
                                                $dql = mysqli_query($koneksi,$sql);


                                                while ($rql = mysqli_fetch_assoc($dql)) {
                                            ?>
                                                <option value="<?php echo $rql['principal'] ?>" <?php if($rql['id_principal']==1) echo 'selected'; ?>><?php echo $rql['principal']." - ".$rql['keterangan'] ?></option>

                                            <?php
                                                }

                                             ?>
                                        </select>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-12">
                                        <small>Freezer Owner</small>
                                    <br>
                                        <!-- <input type="text" class="freezerowner" name="owner" class="form-control" required> -->

                                        <select name="owner" class="freezerowner<?php echo $result['iddistributor']  ?> " class="form-control" onChange="rentownplace(<?php echo $result['iddistributor'] ?>);" required>
                                            <option selected>Status kepemilikan</option>
                                            <option value="Rent">Rent</option>
                                            <option value="Own">Own</option>
                                            <option value="Jbp">Jbp</option>
                                        </select>
                                        
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <small>Nama Vendor</small>
                                    <br>
                                        <!-- <input type="text" id="namavendor" name="namavendor" class="form-control" required> -->

                                        <select name="namavendor" id="namavendorselect<?php echo $result['iddistributor'] ?>" class="select_database form-control namavendorselect" onChange="selectvendor(<?php echo $result['iddistributor'] ?>);" required>
                                        <option >Pilih Nama Vendor</option>

                                        <?php 
                                            $query_user = "SELECT * FROM user WHERE role = 'VENDOR' GROUP BY nama";
                                            $data_user = mysqli_query($koneksi,$query_user);
                                            while ($result_user = mysqli_fetch_assoc($data_user)) {
                                                
                                         ?>
                                            <option value="<?php echo $result_user['nama'] ?>"><?php echo $result_user['nama'] ?></option>

                                        <?php } ?>

                                        </select>
                                    </div>
                                </div>

                             
                                <!-- <div id="areavendor"></div>  -->

                                <div class="col-12 col-md-9">
                                    <input type="text" name="namavendor_pilih" id="namavendor_pilih<?php echo $result['iddistributor'] ?>" class="form-control namavendor_pilih" hidden>
                                </div>

                                 <div class="row">
                                    <div class="col-md-12">
                                        <small>Area Vendor</small>
                                    <br>
                                        

                                        <select name="areavendor" id="areavendor<?php echo $result['iddistributor'] ?>" class="select_database form-control" required>
                                        <option>Pilih Area</option>

                                        <?php 
                                            $query_user = "SELECT area FROM user WHERE role = 'VENDOR' GROUP BY area";
                                            $data_user = mysqli_query($koneksi,$query_user);
                                            while ($result_user = mysqli_fetch_assoc($data_user)) {
                                                
                                         ?>
                                            <option value="<?php echo $result_user['area'] ?>"><?php echo $result_user['area'] ?></option>

                                        <?php } ?>

                                    </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <small>Placement Notes</small>
                                    <br>
                                        <input type="text" name="notes" class="form-control">

                                        

                                    </select>
                                    </div>
                                </div>

                                <div class="row nomor_own nomor_mesin<?php echo $result['iddistributor'] ?>" id="nomor_mesin">
                                    <div class="col-md-12">
                                        <small>Nomor Mesin</small>
                                    <br>
                                        <input type="text" name="nomormesin" class="form-control">

                                        

                                    </select>
                                    </div>
                                </div>

                                <!-- <div class="row id_number">
                                    <div class="col-md-12">
                                        <small>IDNumber</small>
                                    <br>

                                        <input type="text" id="idnumber" name="idnumber" class="form-control">

                                        

                                    </select>
                                    </div>
                                </div> -->


                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-sm btn-success">
                                            Update
                                        </button>
                                   
                                        <a href="javascript:void();" class="btn btn-sm btn-dark batal-tindaklanjut">Batal</a>
                                    </div>
                                 </div>
                                    
                                <!-- </div> -->
                            </form>
                            

                            

                            <a href="javascript:void();" id="approved_placement_<?php echo $result['iddistributor']?>" class="btn btn-block btn-primary approved_placement" data-toggle="tooltip" data-placement="top" title="Approve Placement" acuan="<?php echo $result['id_distributor']?>">
                                <i class="fas fa fa-check"></i>
                            </a>
                             
                           
                            
                               <!--  <a href="index.php?mod=freezerplacement&class=placement&id=<?php echo $result['iddistributor'] ?>" id="approved_placement_<?php echo $result['iddistributor']?>" class="btn btn-sm btn-primary approved_placement" data-toggle="tooltip" data-placement="top" title="Approve Placement">
                                <i class="fas fa fa-check"></i>
                            </a> -->
                            <!-- </div> -->
                        </td>
                        <td><?php echo $no ?></td>
                        <td><?php echo $result['IDNumber'] ?></td>
                        <td><?php echo $result['rtm_request_date'] ?></td>
                        <td>
                            <?php echo $result['custname'] ?>
                        </td>
                        <td>
                            <?php echo $result['custid'] ?>
                        </td>
                        <td>
                            <?php echo $result['distname'] ?>
                        </td>
                        <td>
                            <?php echo $result['coveragearea'] ?>
                        </td>
                       
                        <td>
                            <?php 
                                echo $result['inputdatetime'];
                            ?>
                        </td>

                        <td>
                            
                            <?php echo $result['sales']; ?>
                        </td>
                        <td>
                            <?php 
                                $status = $result['status'];

                                if ($status == 1) {
                                     echo 'Revisit';
                                 }else{
                                    echo 'New Outlet';
                                 } ?>
                        </td>
                        <td>
                            <?php echo $result['channel'] ?>
                        </td>

                        <td>
                            <?php echo $result['subchannel'] ?>
                        </td>

                        <td>
                            <?php echo $result['segment'] ?>
                        </td>

                        <td>
                            <?php echo $result['nameowner'] ?>
                        </td>

                        <td>
                            <?php echo $result['noktp'] ?>
                        </td>
                        <td>
                            <?php echo $result['telephone'] ?>
                        </td>
                        <td>
                            <?php echo $result['longitude'] ?>
                        </td>
                        <td>
                            <?php echo $result['latitude'] ?>
                        </td>

                        <td>
                            <?php echo $result['address'] ?>
                        </td>

                        <td>
                            <?php echo $result['province'] ?>
                        </td>

                        <td>
                            <?php echo $result['city'] ?>
                        </td>

                        <td>
                            <?php echo $result['kecamatan'] ?>
                        </td>

                        <td>
                            <?php echo $result['kelurahan'] ?>
                        </td>

                        <td>
                            <?php echo $result['kodepos'] ?>
                        </td>

                        <td>
                            <?php echo $result['timeopen'] ?>
                        </td>

                        <td>
                            <?php echo $result['timeclosed'] ?>
                        </td>

                        <td>
                            <?php echo $result['luasbangunan'] ?>
                        </td>

                        <td>
                            <?php echo $result['paketfreezer'] ?>
                        </td>

                        <td>
                            <?php 
                            $photo = 'https://appict.appsbelfoods.com/'.$result['photo'];
                            
                            // echo '<a href="'.$photo.'"><img src="'.$photo.'"></a>';
                            echo '<a href="'.$photo.'">View</a>';

                            ?>
                        </td>
                        <td>
                            <?php 
                            $photo = 'https://appict.appsbelfoods.com/'.$result['photo2'];
                            
                            // echo '<a href="'.$photo.'"><img src="'.$photo.'"></a>';
                            echo '<a href="'.$photo.'">View</a>';

                            ?>
                        </td>
                        <td>
                            <?php 
                            $photo = 'https://appict.appsbelfoods.com/'.$result['photo3'];
                            
                            // echo '<a href="'.$photo.'"><img src="'.$photo.'"></a>';
                            echo '<a href="'.$photo.'">View</a>';

                            ?>
                        </td>

                        <td>
                            <?php echo $result['sebutkanlingkungan'] ?>
                        </td>

                        <td>
                            <?php echo $result['patokan'] ?>
                        </td>
                        <td>
                            <?php echo $result['lokasi1'] ?>
                        </td>
                        <td>
                            <?php echo $result['lokasi2'] ?>
                        </td>
                        <td>
                            <?php echo $result['freezerdalamtoko'] ?>
                        </td>
                        <td>
                            <?php echo $result['merekfreezer'] ?>
                        </td>
                        <td>
                            <?php echo $result['frozenFood'] ?>
                        </td>

                        <td>
                            <?php echo $result['merekfrozenfood'] ?>
                        </td>

                        <td>
                            <?php echo $result['productutama'] ?>
                        </td>
                        <td>
                            <?php echo $result['estimasipengunjung'] ?>
                        </td>
                        <td>
                            <?php echo $result['pendapatan'] ?>
                        </td>

                        <td>
                            <?php echo $result['mayoritas'] ?>
                        </td>
                        <td>
                            <?php echo $result['elektricity'] ?>
                        </td>
                        <td>
                            <?php echo $result['kondisifisik'] ?>
                        </td>

                        <td>
                            <?php echo $result['resikofreezerhilang'] ?>
                        </td>

                        <td>
                            <?php echo $result['tersediatempat'] ?>
                        </td>

                        <td>
                            <?php 
                                if ($result['coming_from'] == 'ADDITIONAL') {
                                    echo $result['realisasi_tipe_freezer'];
                                }else{
                                    echo $result['pilihanfreezeroutlet'];
                                }
                             ?>
                        </td>

                        <td>
                            <?php echo $result['tempatfreezer'] ?>
                        </td>

                        <td>
                            <?php echo $result['penilaianlokasi'] ?>
                        </td>

                        <td>
                            <?php echo $result['sebutkanlingkungan'] ?>
                        </td>

                        <td>
                            <?php echo $result['targetbulanan'] ?>
                        </td>

                        <td>
                            <?php echo $result['notesissue'] ?>
                        </td>

                        <td>
                            <?php echo $result['alasanissue'] ?>
                        </td>

                        <td>
                            <?php echo $result['followupaction'] ?>
                        </td>

                        <td>
                            <?php echo $result['alasantidaksesuai'] ?>
                        </td>

                        <td>
                            <?php echo $result['followupdate'] ?>
                        </td>

                        <td>
                            <?php echo $result['statustoko'] ?>
                        </td>
                        <td>
                            <?php echo $result['rtm_request_date'] ?>
                        </td>
                        <td>
                            <?php echo $result['keterangan'] ?>
                        </td>
                        <?php $no++; } ?>
                    </tr>

                     
                </tbody>
            </table>
        </div>
    </div>
</div>

        <!-- END DATA TABLE -->

        <br>

<br>
<h4>Placement</h4><br> <a href="export-placement.php" class="btn btn-dark btn-sm">Download All Placement Data</a>
<hr>
<div class="row">
    <div class="col-md-12">
        <div class="table-responsive table-data" style="height: auto">
            <table id="tableok2" class="mdl-data-table" style="width:100%">
                <thead>
                    <tr>
                        <td>No</td>
                        <td>Sequence Number</td>
                        <td>Assign Date</td>
                        <td>BFI Code</td>
                        <td>Nama Outlet</td>
                        <td>Nama Distributor</td>
                        <td>Tanggal Survey</td>
                        <td>Freezer Owner</td>
                        <td>Nama Vendor</td>
                        <td>Tipe Freezer</td>
                        <td>Email to Vendor Date</td>
                        <td>Placement Date</td>
                        <td>Lead Time</td>
                        <td>Status</td>
                        <td>Tanggal Validasi</td>
                        <td>Plan Placement Date</td>
                        <td>Freezer Code</td>
                        <td>BatchPO</td>
                        <td>Nomor Mesin</td>
                        <td>Nama Supir</td>
                        <td>Tanggal Pengiriman</td>
                        <td>Realisasi Tipe Freezer</td>
                        <td>Nama PIC</td>
                        <td>No Telp PIC</td>
                        <td>Foto - Deal Form + KTP</td>
                        <td>Foto - Outlet Tampak Depan</td>
                        <td>Kondisi Fisik Outlet</td>
                        <td>Longitude</td>
                        <td>Latitude</td>
                        <td>Vendor Longitude</td>
                        <td>Vendor Latitude</td>
                        <td>Alamat</td>
                        <td>Provinsi</td>
                        <td>Kabupaten / Kota</td>
                        <td>Kecamatan</td>
                        <td>Kelurahan</td>
                        <td>Kode Pos</td>
                        <td>Jam Buka</td>
                        <td>Jam Tutup</td>
                        <td>Updated By</td>
                        <!-- <td>Action</td> -->
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $no = 1;
                        
                        $query = "SELECT * FROM focus_freezer_placement WHERE approved_status = 'APPROVED' GROUP BY IDNumber ORDER BY id_placement DESC LIMIT 0,10";
                        $data = mysqli_query($koneksi,$query);

                        while ($result = mysqli_fetch_assoc($data)) {
                     ?>
                    <tr>

                        <!-- cari datangnya dari mana -->
                            <?php 
                                if ($result['id_distributor'] == 0) {
                                    //maka itu artinya bukan dari survey NOO
                                    $id_freezer = $result['id_freezer'];

                                    if ($result['cust_type'] == "Indirect") {

                                        //karna dia secondary, cari lagi datanya dari tabel secondary mds
                                        $cp2 = "SELECT * FROM MDS_CustomerSecondary WHERE Code = '$id_freezer'";
                                        $dcp2 = mysqli_query($koneksi, $cp2);
                                        $results2 = mysqli_fetch_assoc($dcp2);

                                        $custid = $results2['Code'];
                                        $custname = $results2['Name'];
                                        $distname = $results2['DistName'];

                                        $tipe = "Indirect";
                                        $pic = $results2['PIC'];
                                        $phone = $results2['Phone'];
                                        $latitude = $results2['Latitude'];
                                        $longitude = $results2['Longitude'];
                                        $address = $results2['Alamat'];
                                        $city = $results2['KabKota'];
                                        $province = $results2['Propinsi'];
                                        $kecamatan = $results2['Kecamatan'];
                                        $kelurahan = $results2['Kelurahan'];
                                        $kodepos = $results2['KodePos'];
                                        $tipe_freezer = $result['realisasi_tipe_freezer'];
                                    } else { // primary customer
                                        $cp = "SELECT * FROM MDS_CustomerPrimary WHERE Code = '$id_freezer'";
                                        $dcp = mysqli_query($koneksi, $cp);
                                        $result2 = mysqli_fetch_assoc($dcp);

                                        $tgl_survey = "Tidak tersedia";
                                        $tipe = $result2['MarketId_Name'];
                                        $photo = '';
                                        $photo3 = '';

                                        $custid = $result2['Code'];
                                        $custname = $result2['Name'];
                                        $distname = $result2['System_Distributor_Name'];
                                        $tipe = "Direct";
                                        $pic = $result2['PIC'];
                                        $phone = $result2['Phone'];
                                        $latitude = $result2['Latitude'];
                                        $longitude = $result2['Longitude'];
                                        $address = $result2['Street'];
                                        $city = $result2['City'];
                                        $province = $result2['SalesDistrictName'];
                                        $kecamatan = $result2['SPVillageName'];
                                        $kelurahan = '';
                                        $kodepos = $result2['ZipCode'];
                                        $tipe_freezer = $result['realisasi_tipe_freezer'];
                                    }
                                }else{

                                    $id_distributor = $result['id_distributor'];
                                    $cs = "SELECT * FROM distributor WHERE iddistributor = '$id_distributor'";
                                    $dcs = mysqli_query($koneksi,$cs);
                                    $result3 = mysqli_fetch_assoc($dcs); 

                                    $custid = $result3['custid'];
                                    $custname = $result3['custname'];
                                    $distname = $result3['distname'];
                                    $tgl_survey = $result3['inputdatetime'];
                                    $tipe = 'SECONDARY';
                                    $photo3 = "https://appict.appsbelfoods.com/".$result3['photo3']."";
                                    $photo = "https://appict.appsbelfoods.com/".$result3['photo'].""; 
                                    $pic = $result3['contactname'];
                                    $phone = $result3['telephone'];
                                    $latitude = $result3['latitude'];
                                    $longitude = $result3['longitude'];
                                    $tipe = "SECONDARY";
                                    $address = $result3['address'];
                                    $city = $result3['city'];
                                    $province = $result3['province'];
                                    $kecamatan = $result3['kecamatan'];
                                    $kelurahan = $result3['kelurahan'];
                                    $kodepos = $result3['kodepos'];
                                    $tipe_freezer = $result3['pilihanfreezeroutlet'];
                                }
                             ?>
                        <td><?php echo $no ?></td>
                        <td><?php echo $result['IDNumber']; ?></td>
                        <td><?php echo $result['approved_date']; ?></td>
                        <td>
                            <?php echo $custid ?>
                        </td>
                        <td><?php echo $custname ?></td>
                        <td>
                            <?php echo $distname ?>
                        </td>
                        <td>
                            <?php echo $tgl_survey ?>
                        </td>
                        <td>
                            <?php echo $result['freezer_owner'] ?>
                        </td>
                        <td>
                            <?php echo $result['nama_vendor'] ?>
                        </td>
                        <td>
                            <?php 
                                // if ($result['coming_from'] == 'ADDITIONAL') {
                                //     echo $result['realisasi_tipe_freezer'];
                                // }else{
                                //     echo $result['pilihanfreezeroutlet'];
                                // }
                                echo $tipe_freezer;
                             ?>
                        </td>
                        <td>
                            <?php echo $result['approved_date'] ?>
                        </td>
                        <td>
                            <?php 

                                if ($result['placement_date'] == '0000-00-00') {
                                    echo 'Tanggal belum tersedia';
                                }else{
                                    echo $result['placement_date'];
                                } 
                                ?>
                        </td>
                        <td>
                            <?php
                                echo $result['lead_time'].' Hari';
                            ?>
                        </td>
                        <td>
                            <?php 
                                $status = $result['approved_status'];
                                $keterangan = $result['keterangan'];
                                $tanggal_penempatan = $result['placement_date'];

                                if ($status == 'APPROVED' AND $tanggal_penempatan !== '0000-00-00') {
                                    echo '<span class="role marketing">SUCCESS</span>';
                                }elseif ($status == 'APPROVED' AND $keterangan == 'VALIDATED'){
                                    echo '<span class="role marketing">VALIDATED BY VENDOR</span>';
                                }elseif ($status == 'APPROVED' AND $keterangan == '') {
                                     echo '<span class="role admin">WAITING FOR VENDOR VALIDATION</span>';
                                }else{
                                    echo '<span class="role admin">PROCESSING BY VENDOR</span>';
                                }
                             ?>
                        </td>
                        <td>
                            <?php echo $result['approved_date'] ?>
                        </td>
                        <td>
                            <?php echo $result['plan_placement_date'] ?>
                        </td>
                        <td>
                            <?php echo $result['freezercode'] ?>
                        </td>
                        <td>
                            <?php echo $result['batch_po'] ?>
                        </td>
                        <td>
                            <?php echo $result['placement_notes'] ?>
                        </td>
                        <td>
                            <?php echo $result['nama_supir'] ?>
                        </td>
                        <td>
                            <?php 

                               if ($result['tanggal_pengiriman'] == '0000-00-00') {
                                    echo "Tanggal belum tersedia";
                               }else{
                                    echo $result['tanggal_pengiriman']; 
                               }
                            ?>
                        </td>
                        <td>
                            <?php echo $result['realisasi_tipe_freezer'] ?>
                        </td>
                        <td>
                            <?php echo $pic ?>
                        </td>
                        <td>
                            <?php echo $phone ?>
                        </td>
                        <td>

                            <?php 
                            
                                if ($photo3 == '') {
                                    # code...
                                }else{
                                     echo '<a href="'.$photo.'">View</a>';
                                     // echo '<a href="'.$photo.'"><img src="'.$photo.'" width="50px" height="50px">'.$photo.'</a>';
                                }
                           

                            ?>
                            
                        </td>
                        <td>
                            <?php 
                                if ($photo == '') {
                                    # code...
                                }else{
                                    echo '<a href="'.$photo.'">View</a>';
                                     // echo '<a href="'.$photo.'"><img src="'.$photo.'" width="50px" height="50px">'.$photo.'</a>';
                                }

                            ?>
                        </td>
                        <td>
                            <?php echo $result3['kondisifisik'] ?>
                        </td>
                        <td>
                            <?php echo $longitude ?>
                        </td>
                        <td>
                            <?php echo $latitude ?>
                        </td>
                        <td>
                            <?php echo $result['vendor_longitude'] ?>
                        </td>
                        <td>
                            <?php echo $result['vendor_latitude'] ?>
                        </td>
                        <td>
                            <?php echo $address ?>
                        </td>
                        <td>
                            <?php echo $province ?>
                        </td>
                        <td>
                            <?php echo $city ?>
                        </td>
                        <td>
                            <?php echo $kecamatan ?>
                        </td>
                        <td>
                            <?php echo $kelurahan ?>
                        </td>
                        <td>
                            <?php echo $kodepos ?>
                        </td>
                        <td>
                            <?php echo $result3['timeopen'] ?>
                        </td>
                        <td>
                            <?php echo $result3['timeclosed'] ?>
                        </td>
                        <td>
                            <?php echo $result['placed_by'] ?>
                        </td>
                    </tr>

                     <?php $no++; } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


</div>
 <?php endif ?>





