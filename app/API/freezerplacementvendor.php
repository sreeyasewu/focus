<?php
## Database configuration
include '../config/config.php';

$role = $_SESSION['role'];
$placedby = $_SESSION['nama'];
$area = trim($_SESSION['area']);
$vendor = 'BIG';
$acc = $vendor.'-'.$area;

## Read value
/*
$row = 1;
$rowperpage = 10; // Rows display per page
$columnSortOrder = 'desc'; // asc or desc
$searchValue = mysqli_real_escape_string($koneksi,''); // Search value
*/

$draw = $_POST['draw'];
$row = $_POST['start'];
$rowperpage = $_POST['length']; // Rows display per page
$columnIndex = $_POST['order'][0]['column']; // Column index
$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
$searchValue = mysqli_real_escape_string($koneksi,$_POST['search']['value']); // Search value


## Search 
$searchQuery = " ";
if($searchValue != ''){
   $searchQuery = " and (custname like '%".$searchValue."%' or 
        custid like '%".$searchValue."%' or 
        distname like'%".$searchValue."%' ) ";
}

## Total number of records without filtering
if ($area == "") {
  $sel = mysqli_query($koneksi,"select count(*) as allcount from focus_freezer_placement WHERE approved_status = 'APPROVED' AND nama_vendor like '$acc%'");
}else{
  $sel = mysqli_query($koneksi,"select count(*) as allcount from focus_freezer_placement WHERE approved_status = 'APPROVED' AND nama_vendor ='$acc'");
}
$records = mysqli_fetch_assoc($sel);
$totalRecords = $records['allcount'];

## Total number of record with filtering
if ($area == "") {

$sel = mysqli_query($koneksi,"SELECT count(*) as allcount FROM focus_freezer_placement WHERE approved_status = 'APPROVED' ".$searchQuery." AND nama_vendor like '$acc%'");

}else{
  $sel = mysqli_query($koneksi,"SELECT count(*) as allcount FROM focus_freezer_placement WHERE approved_status = 'APPROVED' ".$searchQuery." AND nama_vendor like '$acc%'");
}

$records = mysqli_fetch_assoc($sel);
$totalRecordwithFilter = $records['allcount'];

## Fetch records
//$empQuery = "select * from distributor WHERE 1 ".$searchQuery." order by iddistributor ".$columnSortOrder." limit ".$row.",".$rowperpage;

if ($area == "") {
              $empQuery = "SELECT * FROM focus_freezer_placement WHERE approved_status = 'APPROVED' ".$searchQuery." AND nama_vendor like '$acc%' GROUP BY IDNumber order by IDNumber ".$columnSortOrder." limit ".$row.",".$rowperpage;
            } else {
              $empQuery = "SELECT * FROM focus_freezer_placement WHERE approved_status = 'APPROVED' ".$searchQuery." AND nama_vendor = ".$acc." GROUP BY IDNumber order by IDNumber ".$columnSortOrder." limit ".$row.",".$rowperpage;
            }


$empRecords = mysqli_query($koneksi, $empQuery);
$data = array();

while ($result = mysqli_fetch_assoc($empRecords)) {

 if ($result['id_distributor'] == 0) {
                                    //maka itu artinya bukan dari survey NOO
  $id_freezer = $result['id_freezer'];

  if ($result['cust_type'] == "Indirect") {
          //karna dia secondary, cari lagi datanya dari tabel secondary mds
    $cp2 = "SELECT * FROM MDS_CustomerSecondary WHERE Code = '$id_freezer'";
    $dcp2 = mysqli_query($koneksi, $cp2);
    $results2 = mysqli_fetch_assoc($dcp2);

    $custid = $results2['Code'];
    $custname = $results2['Name'];
    $distname = $results2['DistName'];

    $tipe = "Indirect";
    $pic = $results2['PIC'];
    $phone = $results2['Phone'];
    $latitude = $results2['Latitude'];
    $longitude = $results2['Longitude'];
    $address = $results2['Alamat'];
    $city = $results2['KabKota'];
    $province = $results2['Propinsi'];
    $kecamatan = $results2['Kecamatan'];
    $kelurahan = $results2['Kelurahan'];
    $kodepos = $results2['KodePos'];
    $tipe_freezer = $result['realisasi_tipe_freezer'];

                                    } else { // primary customer
                                      $cp = "SELECT * FROM MDS_CustomerPrimary WHERE Code = '$id_freezer'";
                                      $dcp = mysqli_query($koneksi, $cp);
                                      $result2 = mysqli_fetch_assoc($dcp);

                                      $tgl_survey = "Tidak tersedia";
                                      $tipe = $result2['MarketId_Name'];
                                      $photo = '';
                                      $photo3 = '';

                                      $custid = $result2['Code'];
                                      $custname = $result2['Name'];
                                      $distname = $result2['System_Distributor_Name'];
                                      $tipe = "Direct";
                                      $pic = $result2['PIC'];
                                      $phone = $result2['Phone'];
                                      $latitude = $result2['Latitude'];
                                      $longitude = $result2['Longitude'];
                                      $address = $result2['Street'];
                                      $city = $result2['City'];
                                      $province = $result2['SalesDistrictName'];
                                      $kecamatan = $result2['SPVillageName'];
                                      $kelurahan = '';
                                      $kodepos = $result2['ZipCode'];
                                      $tipe_freezer = $result['realisasi_tipe_freezer'];
                                    }
  }else{

                                    $id_distributor = $result['id_distributor'];
                                    $cs = "SELECT * FROM distributor WHERE iddistributor = '$id_distributor'";
                                    $dcs = mysqli_query($koneksi,$cs);
                                    $result3 = mysqli_fetch_assoc($dcs); 

                                    $custid = $result3['custid'];
                                    $custname = $result3['custname'];
                                    $distname = $result3['distname'];
                                    $tgl_survey = $result3['inputdatetime'];
                                    $tipe = 'Indirect';
                                    $photo3 = "https://appict.appsbelfoods.com/".$result3['photo3']."";
                                    $photo = "https://appict.appsbelfoods.com/".$result3['photo'].""; 
                                    $pic = $result3['contactname'];
                                    $phone = $result3['telephone'];
                                    $latitude = $result3['latitude'];
                                    $longitude = $result3['longitude'];
                                    // $tipe = "SECONDARY";
                                    $address = $result3['address'];
                                    $city = $result3['city'];
                                    $province = $result3['province'];
                                    $kecamatan = $result3['kecamatan'];
                                    $kelurahan = $result3['kelurahan'];
                                    $kodepos = $result3['kodepos'];
                                    $tipe_freezer = $result3['pilihanfreezeroutlet'];
                                  }
   if ($result['placement_date'] == '0000-00-00') {
      $placement_date = 'Tanggal belum tersedia';
   }else{
      $placement_date = $result['placement_date'];
   } 

    $status = $result['approved_status'];
    $tanggal_penempatan = $result['placement_date'];
    $keterangan = $result['keterangan'];

    if ($status == 'APPROVED' AND $tanggal_penempatan !== '0000-00-00') {
        $span_status = '<span class="role marketing">SUCCESS</span>';
      }elseif ($status == 'APPROVED' AND $keterangan == 'VALIDATED'){
        $span_status = '<span class="role marketing">VALIDATED BY VENDOR</span>';
      }elseif ($status == 'APPROVED' AND $keterangan == '') {
       $span_status = '<span class="role admin">WAITING FOR VENDOR VALIDATION</span>';
     }else{
       $span_status = '<span class="role admin">PROCESSING BY VENDOR</span>';
    }


    if ($result['tanggal_pengiriman'] == '0000-00-00') {
      $tanggal_pengiriman = "Tanggal belum tersedia";
    }else{
      $tanggal_pengiriman = $result['tanggal_pengiriman']; 
    }

    if ($tipe == 'Direct') {
      $tipe_customer = "PRIMARY";
    }else{
      $tipe_customer = "SECONDARY";
    }


    if ($result['keterangan'] == 'VALIDATED') {

      $span_validate = '<span class="badge">VALIDATED</span>';
    }else{

      $span_validate = '<form action="model/freezerplacement/validate.php" method="POST">
      <input type="hidden" value="'.$result['id_placement'].'" name="id_placement">

      <button type="submit" name="validate" class="btn btn-success">Validate</button>
      </form>';
    }

    if ($result['coming_from'] == 'ADDITIONAL') {
     $span_action = '<a href="javascript:void();" id="approved_placement_'.$result['id_freezer'].'" class="btn btn-block btn-info approved_placement" data-toggle="tooltip" data-placement="top" title="Place Freezer Aditional" acuan="'.$result['id_freezer'].'"><i class="fas fa fa-hand-point-down"></i></a>

     <a href="javascript:void();" class="btn btn-block btn-dark tindaklanjut" id="rejectbtn" data-toggle="tooltip" data-placement="top" title="Reject Placement Additonal" acuan="'.$result['id_placement'].'"><i class="fas fa fa-warning"> </i></a> 
     ';
   }else{

    if ($result['freezer_owner'] == 'Rent' || $result['freezer_owner'] == 'RENT')
    {

      $span_action = '<a href="javascript:void();" class="btn btn-block btn-danger tindaklanjut" id="rejectbtn" data-toggle="tooltip" data-placement="top" title="Reject Placement" acuan="'.$result['id_placement'].'"><i class="fas fa fa-warning"> </i></a> 

      <a href="javascript:void();" id="approved_placement_'.$result['id_placement'].'" class="btn btn-block btn-primary approved_placement" data-toggle="tooltip" data-placement="top" title="Place Freezer" acuan="'.$result['id_placement'].'">
      <i class="fas fa fa-hand-point-down"></i>
      </a>
      ';
    }else{

      $span_action = '<a href="javascript:void();" class="btn btn-block btn-danger tindaklanjut" id="rejectbtn" data-toggle="tooltip" data-placement="top" title="Reject Placement" acuan="'.$result['id_placement'].'"><i class="fas fa fa-warning"> </i></a>

      <a href="javascript:void();" id="approved_placement_'.$result['id_placement'].'" class="btn btn-block btn-primary approved_placement" data-toggle="tooltip" data-placement="top" title="Place Freezer" acuan="'.$result['id_placement'].'">
      <i class="fas fa fa-hand-point-down"></i>
      </a>
      ';
    }

  }


   $data[] = array( 
      "validate" => $span_validate,
      "action" => $span_action,
      "approved_date"=>$result['approved_date'],
      "plan_placement_date" => '',
      "IDNumber"=>$result['IDNumber'],
      "tipe_customer"=>$tipe_customer,
      "custid"=>$custid,
      "custname"=>$custname,
      "distname"=>$distname,
      "tgl_survey"=>$tgl_survey,
      "freezer_owner"=>$result['freezer_owner'],
      "nama_vendor"=>$result['nama_vendor'],
      "tipe_freezer"=>$tipe_freezer,
      "approved_date"=>$result['approved_date'],
      "placement_date"=>$placement_date,
      "lead_time"=>$result['lead_time'].' Hari',
      "span_status"=>$span_status,
      "approved_date"=>$result['approved_date'],
      "freezercode"=>$result['freezercode'], 
      "batch_po"=>$result['batch_po'],
      "placement_notes"=>$result['placement_notes'],
      "nama_supir"=>$result['nama_supir'],
      "tanggal_pengiriman"=>$tanggal_pengiriman,
      "realisasi_tipe_freezer"=>$result['realisasi_tipe_freezer'],
      "pic"=>$pic,
      "phone"=>$phone   
    );
}

## Response
$response = array(
  "draw" => intval($draw),
  "iTotalRecords" => $totalRecords,
  "iTotalDisplayRecords" => $totalRecordwithFilter,
  "aaData" => $data
);

echo json_encode($response);