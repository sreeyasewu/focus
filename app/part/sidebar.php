<?php 
    if ($_SESSION['role'] == 'ADMIN') {
        include ('sidebar/sidebar-admin.php');
        //tampilkan keseluruhan
    }elseif ($_SESSION['role'] == 'FREEZER MANAGEMENT') {
        include ('sidebar/sidebar-freezerplacement.php');
        //hanya menampilkan data event saja dan proses approval
        // dashboard, view bod event, view corporate event
    }elseif ($_SESSION['role'] == 'ROUTE TO MARKET') {
        include ('sidebar/sidebar-rtm.php');
        //hanya menampilkan penginputan saja
        // dashboard, add bod event, add corporate event
    }elseif ($_SESSION['role'] == 'VENDOR') {
        include ('sidebar/sidebar-vendor.php');
        //hanya menampilkan penginputan saja
        // dashboard, add bod event, add corporate event
    }elseif ($_SESSION['role'] == 'COMMAND CENTER') {
        include ('sidebar/sidebar-commandcenter.php');
        //hanya menampilkan penginputan saja
        // dashboard, add bod event, add corporate event
    }elseif ($_SESSION['role'] == 'TSS') {
        include ('sidebar/sidebar-tss.php');
        //hanya menampilkan penginputan saja
        // dashboard, add bod event, add corporate event
    }else{
        include ('sidebar/sidebar-investor.php');
        //karyawan hanya dapat melihat agenda yang termasuk dirinya saja
    }
 ?>


