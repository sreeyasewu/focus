
<!-- jQuery Library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


        <div>
           
            <div class="row form-group">
                <div class="col-sm-3 col-md-2">
                    <label for="text-input" class=" form-control-label"><b>Filter by date</b></label>
                </div>
                <div class="col-sm-3 col-md-2">
                    <input type="text" id="tanggal_mulai" name="tanggal_mulai" class="form-control" placeholder="Mulai" required>
                </div>
                <div class="col-sm-3 col-md-2">
                    <input type="text" id="tanggal_akhir" name="tanggal_akhir" class="form-control" placeholder="Akhir" required>
                    
                </div>
                <button type="submit" name="custom" id="custom" value="custom" class="btn btn-sm btn-primary">
                    Filter
                </button>
            </div>


           
            <hr>
        </div>

<div class="table-responsive table-data" style="height: auto">
<!-- Table -->
<table id="surveyTable" class="mdl-data-table table table-borderless" style="width:100%">

  <thead>
    <tr>
   
                      
                        <td>Nama Outlet</td>
                        <td>BFI Code</td>
                        <td>Nama Distributor</td>
                        <td>Area Distributor</td>
                        <td>Tgl Survey</td>
                        <td>Nama Surveyor</td>
                        <td>Status Kunjungan</td>
                        <td>Channel</td>
                        <td>Sub-Channel</td>
                        <td>Segment</td>
                        <td>Nama PIC</td>
                        <td>No KTP</td>
                        <td>No Telp PIC</td>
                        <td>Longitude</td>
                        <td>Latitude</td>
                        <td>Alamat</td>
                        <td>Provinsi</td>
                        <td>Kabupaten/Kota</td>
                        <td>Kecamatan</td>
                        <td>Keluarahan</td>
                        <td>Kode Pos</td>
                        <td>Jam Buka (WIB)</td>
                        <td>Jam Tutup (WIB)</td>
                        <td>Luas Bangunan</td>
                        <td>Paket Awal</td>
                        <td>Foto - Outlet Tampak Depan</td>
                        <td>Foto - Daerah Depan Sekitar Toko</td>
                        <td>Foto - Lokasi Freezer yang disepakati</td>
                        <td>Foto - Deal Form + KTP</td>
                        <td>Lokasi Sekitar</td>
                        <td>Patokan</td>
                        <td>Lokasi (1)</td>
                        <td>Lokasi (2)</td>
                        <td>Freezer Es Krim</td>
                        <td>Merk Freezer Es Krim</td>
                        <td>Freezer Makanan Beku</td>
                        <td>Merk Freezer Makanan Beku</td>
                        <td>Produk Utama</td>
                        <td>Jumlah Pengunjung</td>
                        <td>Pendapatan/Hari (IDR)</td>
                        <td>Tipe Pengunjung</td>
                        <td>Kapasitas Listrik</td>
                        <td>Kondisi Fisik Outlet</td>
                        <td>Resiko Freezer</td>
                        <td>Spacae 1 x 1.5 m</td>
                        <td>Tipe Freezer</td>
                        <td>Area Penempatan Freezer</td>
                        <td>Visibilitas Freezer di Jalur Utama</td>
                        <td>Layanan Tambahan</td>
                        <td>Target/Bulan (kg)</td>
                        <td>Issue/Follow Up Action</td>
                        <td>Alasan Issue</td>
                        <td>Status</td>
                        <td>NOO</td>
                       
    </tr>
  </thead>

</table>
</div>
<script type="text/javascript">
$(document).ready(function(){
  
  var surveyTable = $('#surveyTable').DataTable({
      'processing': true,
      'serverSide': true,
      'serverMethod': 'post',
      'ajax': {
          'url':'API/survey.php',
          'dataType': 'json',
                      'dataSrc': "data",
                      'data': function(data){
                          // Read values
                          var tanggal_mulai = $('#tanggal_mulai').val();
                          var tanggal_akhir = $('#tanggal_akhir').val();
                         // console.log(tanggal_akhir);
                          // Append to data
                          data.tanggal_mulai = tanggal_mulai;
                          data.tanggal_akhir = tanggal_akhir;
                       }
      },
      "lengthMenu": [
        [ 10, 25, 50, 100, -1 ],
        [ '10 rows', '25 rows', '50 rows', '100 rows', 'Show all' ]
      ],
      "dom": '<"datatable-header"flB><"datatable-scroll-wrap"rt><"datatable-footer"ip>',
      "buttons": [
          'csv', 'excel'
      ],
      'columns': [
         { data: 'custname' },
         { data: 'custid' },
         { data: 'distname' },
         { data: 'coveragearea' },
         { data: 'inputdatetime' },
         { data: 'sales' },
         { data: 'status' },
         { data: 'channel' },
         { data: 'subchannel' },
         { data: 'segment' },
         { data: 'nameowner' },
         { data: 'noktp' },
         { data: 'telephone' },
         { data: 'longitude' },
         { data: 'latitude' },
         { data: 'address' },
         { data: 'province' },
         { data: 'city' },
         { data: 'kecamatan' },
         { data: 'kelurahan' },
         { data: 'kodepos' },
         { data: 'timeopen' },
         { data: 'timeclosed' },
         { data: 'luasbangunan' },
         { data: 'paketfreezer' },
         { data: 'photo' },
         { data: 'photo2' },
         { data: 'photo3' },
         { data: 'photo4' },
         { data: 'sebutkanlingkungan' },
         { data: 'patokan' },
         { data: 'lokasi1' },
         { data: 'lokasi2' },
         { data: 'freezerdalamtoko' },
         { data: 'merekfreezer' },
         { data: 'frozenFood' },
         { data: 'merekfrozenfood' },
         { data: 'productutama' },
         { data: 'estimasipengunjung' },
         { data: 'pendapatan' },
         { data: 'mayoritas' },
         { data: 'elektricity' },
         { data: 'kondisifisik' },
         { data: 'resikofreezerhilang' },
         { data: 'tersediatempat' },
         { data: 'pilihanfreezeroutlet' },
         { data: 'tempatfreezer' },
         { data: 'penilaianlokasi' },
         { data: 'sebutkanlingkungan' },
         { data: 'targetbulanan' },
         { data: 'followupaction' },
         { data: 'alasanissue' },
         { data: 'statustoko' },
         { data: 'is_noo' },

      ]
   });

   $('#custom').click(function(){
    var tanggal_mulai = document.getElementById("tanggal_mulai").value;
    var tanggal_akhir = document.getElementById("tanggal_akhir").value;
    
    if ((Date.parse(tanggal_akhir) <= Date.parse(tanggal_mulai))) {
      alert("Tanggal Akhir Harus Lebih Besar dari Tanggal Mulai");
      document.getElementById("tanggal_akhir").value = "";
      return false;
    }
    surveyTable.draw();
  });

});
</script>

