<?php 

ob_start();
error_reporting(E_ALL);
ini_set('display_errors', 1);

include ('../../config/config.php');
include_once "../../email/lib/swift_required.php";

date_default_timezone_set("Asia/Jakarta");


$kode = $_POST['customer'];
$rtm_request_date =  date('Y-m-d');
$comingfrom = 'ADDITIONAL';
// $tipe = $_POST['tipe'];
$vendor = $_POST['namavendor'];
// $idnumber = $_POST['idnumber'];
$area = $_POST['areavendor'];
$owner = $_POST['owner'];

//$addisi = $_POST['addisi'];

$freezerid = $kode;


$vendorarea = $vendor."-".$area;

$nomormesin = $_POST['nomormesin'];

$principal = $_POST['principal'];

$q = "SELECT MAX(IDNumber) as idnumber from focus_freezer_placement ORDER by id_placement DESC LIMIT 0,1";
$d = mysqli_query($koneksi,$q);
$r = mysqli_fetch_assoc($d);

$idnumber = $r['idnumber'];

if ($idnumber >= 30001) {
	$idnumber = $idnumber + 1;
}else{
	$idnumber = 30001;
}

// echo $idnumber;


if ($nomormesin == '') {
	$nomormesin = '';
}

if ($owner == 'OWN' || $owner == 'Own') {
	$lastdigit = substr($nomormesin, -4);
	$year = date('y');
	$freezercode = 'FZ.'.$year.'.'.$lastdigit.'.'.$idnumber;
}

//$approved = 'APPROVED';

//masukkan ke tabel freezer_placement
// $ss = "SELECT * FROM distributor WHERE custid = '$kode'";
// $ds = mysqli_query($koneksi,$ss);
// $rs = mysqli_fetch_assoc($ds);

// $id_distributor = $rs['iddistributor'];

// if ($id_distributor == '') {
	$id_distributor = '0';
// ?
	
echo $kode."<br/>";
echo $id_distributor."<br/>";
echo $freezerid."<br/>";

//find Id distributor 

$lokasi = $area;

//echo $id_distributor;
//hanya masukkan id_distributor saja 
$approved_date = date('Y-m-d');
$jamMulai = date('h:i');


$placement_end = date('Y-m-d', strtotime("+3 days"));
$jamSelesai = '10:00';

$status = 'APPROVED';
$emailtovendor = "Email to vendor sended at ".date('Y-m-d');

$tipe_freezer = $_POST['realisasi_tipe_freezer'];

$cust_type = "";

if ($_POST['tipecustomer'] == 'SECONDARY') {
	$cust_type = "Indirect";
} else {
	$cust_type = "Direct";
}


if ($vendor == '' or $vendor == 'Pilih Nama Vendor') {
	header('location:../../index.php?mod=freezer&class=view&status=9');
}elseif ($owner == '' or $owner == 'Status kepemilikan') {
	header('location:../../index.php?mod=freezer&class=view&status=99');
}else{

	if ($owner == 'RENT' || $owner == 'Rent') {

		$statement = "INSERT INTO focus_freezer_placement VALUES ('','$id_distributor','$freezerid','$idnumber','$owner','','$vendorarea','','','','','','','$tipe_freezer','','','$status','$approved_date','','$rtm_request_date','$emailtovendor','$comingfrom','','','$principal','$cust_type','$approved_date')";
	}else{
		$statement = "INSERT INTO focus_freezer_placement VALUES ('','$id_distributor','$freezerid','$idnumber','$owner','$freezercode','$vendorarea','','$nomormesin','','','','','$tipe_freezer','','','$status','$approved_date','','$rtm_request_date','$emailtovendor','$comingfrom','','','$principal','$cust_type','$approved_date')";
	}
	// $statement = "UPDATE focus_freezer_placement SET freezer_owner = '$owner', nama_vendor= '$vendor', approved_status = '$status', approved_date = '$approved_date', keterangan = '$emailtovendor', freezercode = '$freezercode' WHERE id_distributor = '$id'";
	if ($update = mysqli_query($koneksi,$statement) or die(mysqli_error($koneksi))) {
	//get vendor email 

	$vendor2 = "SELECT * FROM user WHERE nama = '$vendor'";
	$vendor2 = mysqli_query($koneksi,$vendor2);
	$vendor2 = mysqli_fetch_assoc($vendor2); 

	$email_vendor = $vendor2['email'];

	//$credentials = explode("\r\n", file_get_contents("../../email/gmail.txt"));
	$credentials = ['focus.belfoods.com','465','ssl','admin@focus.belfoods.com','*K7wka~woy!{'];
		$from_address = $credentials[3];
		$organizer = $from_address;
		$from = array($from_address);
		$subject = 'New Frezer Placement Request';
		$topic = "Freezer Placement";
		$summary = 'New Freezer Placement Request';
 		$description = 'Permohonan peletakan freezer';

 		$location = $lokasi;
		// $to = array(
		//     'mr.vero@usa.com',
		//     'irvan.fahlepi@gmail.com'
		// );

 		
		$to = array(
			$email_vendor,
			
			//'daniel.sukarjo@sieradproduce.com'
		);

		$transport = Swift_SmtpTransport::newInstance($credentials[0], intval($credentials[1]), $credentials[2]);
		$transport->setUsername($from_address);
		$transport->setPassword($credentials[4]);
		$swift = Swift_Mailer::newInstance($transport);

		$nl = "\r\n";
		$attendee = "";
		$to_string = "";
		foreach ($to as $e => $n) {
		    $e = is_integer($e) ? $n : $e;
		    $attendee .= "ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;";
		    $attendee .= "PARTSTAT=NEEDS-ACTION;RSVP=TRUE;CN=$n;X-NUM-GUESTS=0:mailto:$e$nl";
		    $to_string .= $e . ",";
		}
		$to_string = substr($to_string, 0, strlen($to_string) - 1);
		$src_tz = new DateTimeZone('Asia/Jakarta');
		$destination_tz = new DateTimeZone('GMT');
		date_default_timezone_set($src_tz->getName());
		//$today = date("Y-m-d H:i");

		$mulai = $approved_date.' '.$jamMulai;
		$selesai = $placement_end.' '.$jamSelesai;

		$meeting_time = new DateTime($mulai, $src_tz);
		$meeting_time->setTimezone($destination_tz);

		$meeting_end = new DateTime($selesai, $src_tz);
		$meeting_end->setTimezone($destination_tz);

		//$dt_stamp =  $meeting_time->format("Ymd\THis\Z");
		//$dt_start = $meeting_time->add(new DateInterval("PT20M"))->format("Ymd\THis\Z");
		//$dt_end = $meeting_time->add(new DateInterval("PT30M"))->format("Ymd\THis\Z");

		$dt_stamp =  $meeting_time->format("Ymd\THis\Z");
		$dt_start = $meeting_time->format("Ymd\THis\Z");
		$dt_end = $meeting_end->format("Ymd\THis\Z");

		$repeat_rule = "";
		//$repeat_rule = $nl . "RRULE:FREQ=WEEKLY;COUNT=2;BYDAY=WE,FR";
		//$repeat_rule = $nl . "RRULE:FREQ=WEEKLY;UNTIL=20170228T040000Z;BYDAY=MO,TU,WE,TH,FR";
		//$repeat_rule = $nl . "RRULE:FREQ=MONTHLY;COUNT=4;BYMONTHDAY=4";

		$eventStatus = "CONFIRMED"; /* TENTATIVE,CONFIRMED,CANCELLED */

		$html = "<html>\n";
		$html .= "<body>\n";
		$html .= 'Dear '.$vendor.'.<br>';
		$html .= '<br>Anda menerima request untuk penambahan freezer dengan detail sebagai berikut :';
		$html .= 'Tanggal Request: '.$approved_date. '<br>';
		$html .= 'Dengan Tipe:'.$tipe_freezer.'<br>';
		$html .= 'Mohon untuk segera diproses. Untuk info lebih detail silahlan login ke platform focus di link berikut ini <a href="http://focus.appsbelfoods.com" taget="_blank">Login Focus</a><br>Terima Kasih';
		$html .= "</body>\n";
		$html .= "</html>\n";

		$calendar_invite = file_get_contents("../../email/skeleton-advanced.txt");

		$calendar_invite = str_replace("__FROM__", $from_address, $calendar_invite);
		$calendar_invite = str_replace("__TO__", $to_string, $calendar_invite);
		$calendar_invite = str_replace("__SUBJECT__", $subject, $calendar_invite);
		$calendar_invite = str_replace("__TOPIC__", $topic, $calendar_invite);
		$calendar_invite = str_replace("__ORGANIZER__", $organizer, $calendar_invite);
		$calendar_invite = str_replace("__ATTENDEE__", $attendee, $calendar_invite);
		$calendar_invite = str_replace("__DESCRIPTION__", $description, $calendar_invite);
		$calendar_invite = str_replace("__LOCATION__", $location, $calendar_invite);
		$calendar_invite = str_replace("__SUMMARY__", $summary, $calendar_invite);
		$calendar_invite = str_replace("__EVENT_STATUS__", $eventStatus, $calendar_invite);
		$calendar_invite = str_replace("__HTML__", $html, $calendar_invite);
		$calendar_invite = str_replace("__DTSTART__", $dt_start, $calendar_invite);
		$calendar_invite = str_replace("__DTEND__", $dt_end, $calendar_invite);
		$calendar_invite = str_replace("__DTSTAMP__", $dt_stamp, $calendar_invite);
		$calendar_invite = str_replace("__REPEAT_RULE__", $repeat_rule, $calendar_invite);
		$calendar_invite = str_replace("__BOUNDARY__", md5(time()) . rand(0, 99999999), $calendar_invite);

		$messageObject = new Swift_MyMessage();
		$messageObject->setFrom($from);
		$messageObject->setTo($to);
		$messageObject->setRawContent($calendar_invite);

		if ($recipients = $swift->send($messageObject, $failures)) {
		    echo 'Vendor Notification successfully sent!';
		    header('location:../../index.php?mod=freezer&class=add&status=1');
		}else {
		    echo "There was an error:\n";
		    header('location:../../index.php?mod=freezer&class=add&status=0');
		    print_r($failures);
		}
    
	}else{
		
	    header('location:../../index.php?mod=freezer&class=add&status=0');
	}
}




 ?>
